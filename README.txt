Welcome to Instinct-3D Engine, written by Todd Elliott.

This is a personal project of mine that I started working on 5 or so years ago, but have not worked on for a long time - I have resumed work on it within the last few months as I have once again have some motivation! I have spent the majority of this time cleaning up older code and refactoring, before I move to continue with the features. It has also been through several iterations (starting from scratch several times) in order to make large scale improvements.
I was originally self taught with C++ before having studied it at university, and then ultimately gaining more experience on the job. The game engine uses DirectX 11, which I have not formally studied, but learnt from reading books and following online tutorials. I found that once I was able to understand the concepts, I was able to implement them myself. I've always enjoyed the challenge and the endless learning and improving of my own game engine, rather than using somebody else's to make my own game.

The functionality for the engine itself is written into a DLL, and I then have a small program that takes a few different arguments to construct a small testing studio.
Everything I have done has been written from scratch except that I use the Assimp framework for loading models which I then place into my own format, and MS DirectX texture loading source.

Note that not every feature is necessary fully working at the moment, but everything listed has worked correctly in past iterations, and may need to be re-implented in the current iteration.
The main features of my engine are:

--Thread Pool
	Whilst multithreading may not be seen as a necessity at this stage, I like to be prepared for the future development. After a lot of research, I was able to piece together an effective thread pool that has the benefit of automatically taking on any task that is assigned to it, without the overhead of ever having to create or destroy threads.

--Automatic Resource Management
	Every entity, model, and texture that is created or loaded is automatically managed by my engine. The loading of models and their textures is handled on its own seperate thread. All models and geometry are rendered and updated automatically once created. All resources are graciously released on shutdown.

--Deferred Rendering
	The rendering system operates in a 2-pass sequence; Phase 1 involves writing the depth buffers for lighting, and rendering several pieces of information to the GBuffer, include diffuse, normals, specular power, and shadows calculated from depth maps. (In a previous version of the engine, the GBuffer contained binormals and tangents for normal/bump mapping - I am yet to decide whether to take this route again or to instead use tessellation paired with displacement mapping). Phase 2 involves combining each render texture of the GBuffer together to create the final screen texture.
	This is efficient as it ensures that expensive lighting calculations will only ever be carried out on the same fixed number of pixels each frame.

--Cascaded Shadow Mapping
	My first attempt with a single shadow map was successful but not appropriate when scaled over a large area. My current implementation of cascaded shadow mapping works as expected. It now requires soft shadow edges, as well as exported functionality for setting the cascade distances and possibility the number of cascades. (The number is, at this time, hardcoded to 3 cascades)

--Frustum Culling
	Everything outside of the view frustum will be culled and not staged for rendering. This includes all entities based on their model's bounding box (AABB), as well as each terrain cell (and ocean/water cell); terrain is broken up into several cells designed specifically for culling. 

--Geometry Instancing
	Each frame, the engine compiles a list of every model that is utilized more than once, and puts their individual orientation in an instance buffer; sending only a single copy of the geometry to the vertex shader. This way I can render hundreds of the same model at very little cost. As expected, this has temporarily broken shadow casting of entity models - this is something that I'm currently working to fix.

--Heightmap Loading
	I'm able to load in a bitmap heightmap in order to create a realistic landscape. This is very helpful and will continue to be up until the time that I can create terrain with a world editor.
	
--Procedural Rendering
	Terrain texture layers can be given a specific height and slope value to be rendered on. For example, a cliff face texture could be configured to be rendered on the steepest slopes, while grass on flat ground. Snow could be configured to be rendered at the top of a mountain. Although I'm still trying to interpolate the texture edges on the height configured bounds.
	This information is fed to the shader and determined in real time. However, it is likely that this feature will only be temporary, as once I have a world editor, it is much more likely that a texture mapping mechanism will replace this.
	
--Post Processing Effects
	PPFX shaders can be quite easily added in. At the moment I only have a sunlight god-rays shader available, of which I was able to piece together the HLSL with the help of a tutorial. It looks very nice, but is a bit buggy around the edge of the viewport, so it still needs some work.
	
--Normal Mapping
	This feature was in a previous iteration, and will likely be one of the next features I bring back to this iteration; Normal Mapping is the process of using a "Normal Texture" that matches the general shape and size of its corresponding diffuse texture, but uses RGB values to represent normals, binormals, and tangents. These values are used to determine the suface normals of the polygon they are assigned to, and then based on calculations with the scene lighting, each pixel will be lit a certain amount dependent on light direction, in order to produce an illusion of bumpiness and details on a completely flat surface. Essentially a cheap way of cheating detail onto the scene without requiring high poly models.

Other features that I have started on but have only made little to no progress on are:

--World Editor
	Using QT5 I'm able to create an application of which I can insert my render window into a QT frame, and then write a handful of tools for creating terrain and shape the landscape, inserting entities, etc.
	Only very minimal progress has been made with this. Unfortunately this is not currently in my repository.

--Foliage and Foliage Layers
	I have only done a minimum skeleton for this. I halted work on this when I realised I would need geometry instancing.
	
--Graphical User Interface
	I started writing a GUI based on one that I have written for a 2D game in the past. However I have not made much progress here, as I may consider using a 3rd party GUI. And hence the code for this has been removed from the repository but backed up incase I decided to carry on with my own.

--Networking
	I wrote a basic server and client network interface using winsock, just for a bit of fun. But its really not needed at this stage. More likely to be one of the last things to be done. I pulled this functionality out of the repository due to its incompleteness.