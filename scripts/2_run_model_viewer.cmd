xcopy /y "..\build_msvc_debug\engine\engine.dll" "..\build_msvc_debug\teststudio"
xcopy /y "..\3rdparty\assimp\bin\Debug\assimp-vc140-mt.dll" "..\build_msvc_debug\teststudio"

cd ..\build_msvc_debug\teststudio\
teststudio.exe --test=modelviewer --lighting=day --resolution=1280x720 --windowed

pause