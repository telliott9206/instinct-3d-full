/*
 * AssimpHelper.h
 *
 * Created 16/04/2022
 * Written by Todd Elliott
 *
 * Used for loading in any kind of geometry within Assimp's capability.
 * As the geometry is read, it is loaded into the Instinct3D format.
 */

#ifndef _ASSIMPHELPER_H
#define _ASSIMPHELPER_H

#include <Engine/Model/Model.h>
#include <3rdparty/Assimp/Importer.hpp>
#include <3rdparty/Assimp/scene.h>
#include <3rdparty/Assimp/postprocess.h>
#include <3rdparty/Assimp/material.h>
#include <3rdparty/Assimp/color4.h>

namespace Instinct
{
	class Model;

	class AssimpHelper
	{
	public:
		AssimpHelper() = delete;
		AssimpHelper( const AssimpHelper& ) = delete;
		AssimpHelper& operator = ( const AssimpHelper& ) = delete;
		~AssimpHelper() = delete;

		static void LoadModelToInstinct( const std::string& file, Model* modelOut );
		static XMFLOAT4 AssimpColourToXMFLOAT4( const aiColor4D& colour );
	};
}

#endif
