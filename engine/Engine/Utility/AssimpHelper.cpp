/*
* AssimpHelper.h
*
* Created 16/04/2022
* Written by Todd Elliott
*/

#include "AssimpHelper.h"
#include <Engine.h>
#include <Engine/Model/Vertex.h>
#include <Engine/Model/Mesh.h>
#include <Engine/Texture/Texture.h>

namespace Instinct
{
	void AssimpHelper::LoadModelToInstinct( const std::string& file, Model* modelOut )
	{
		//TODO: Break this up into multiple functions
		Assimp::Importer importer;

		const aiScene* scene = importer.ReadFile( file, aiProcess_Triangulate | aiProcess_ConvertToLeftHanded );
		if( !scene )
		{
			Engine::Print( "Failed to load model %s", file.c_str() );
			return;
		}

		const auto meshCount = scene->mNumMeshes;
		const aiVector3D zero( 0.f, 0.f, 0.f );

		for( uint32_t i = 0u ; i < meshCount ; ++ i )
		{
			auto* aimesh = scene->mMeshes[ i ];

			const auto vertexCount	= aimesh->mNumVertices;
			const auto faces		= aimesh->mNumFaces;
			std::vector<Vertex> vertices( vertexCount );
			std::vector<uint32_t> indices( faces * 3 );

			//Read in vertices
			for( uint32_t j = 0u ; j < vertexCount ; ++ j )
			{
				const auto& pos			= aimesh->mVertices[ j ];
				const auto& normal		= aimesh->HasNormals() ? aimesh->mNormals[ j ] : zero;
				const auto& texcoord	= /*pMesh->HasTextureCoords(0) ? */aimesh->mTextureCoords[ 0 ][ j ];// : &Zero3D;

				vertices[ j ].mPosition = XMFLOAT3( pos.x, pos.y, pos.z );
				vertices[ j ].mNormal = XMFLOAT3( normal.x, normal.y, normal.z );
				vertices[ j ].mTexcoord = XMFLOAT2( texcoord.x, texcoord.y );
			}

			//Read in indices
			uint32_t count = 0u;
			for( uint32_t j = 0u ; j < faces ; ++ j ) 
			{
				const auto& face = aimesh->mFaces[ j ];
				assert( face.mNumIndices == 3 );

				for( auto k = 0 ; k < 3 ; ++ k )
				{
					indices[ count ] = face.mIndices[ k ];
					++ count;
				}
			}

			auto mesh = Mesh<Vertex>::Create( vertices, indices );
			mesh->SetMaterialIndex( aimesh->mMaterialIndex - 1 );

			modelOut->CalculateBoundingBox();
			modelOut->AddModelEntry( mesh );
		}

		const auto numMaterials = scene->mNumMaterials;
		for( uint32_t i = 0u ; i < numMaterials ; ++ i )
		{
			const auto* material = scene->mMaterials[ i ];

			//Load in the texture
			aiString Path;

			if( material->GetTextureCount( aiTextureType_DIFFUSE ) > 0 ) 
			{
				if( material->GetTexture( aiTextureType_DIFFUSE, 0, &Path, nullptr, nullptr, nullptr, nullptr, nullptr ) == aiReturn_SUCCESS )
				{
					const std::string directory = file.substr( 0, file.find_last_of( '/' ) + 1 );
					modelOut->AddDiffuseTexture( Texture::Load( directory + Path.data, false ) );
				}
			}
			else if( material->GetTextureCount( aiTextureType_NORMALS ) > 0 ) 
			{
				if( material->GetTexture( aiTextureType_NORMALS, 0, &Path, nullptr, nullptr, nullptr, nullptr, nullptr ) == aiReturn_SUCCESS )
				{
					const std::string directory = file.substr( 0, file.find_last_of( '/' ) + 1 );
					modelOut->AddNormalTexture( Texture::Load( directory + Path.data, false ) );
				}
			}
			else if( material->GetTextureCount( aiTextureType_SPECULAR ) > 0 ) 
			{
				if( material->GetTexture( aiTextureType_SPECULAR, 0, &Path, nullptr, nullptr, nullptr, nullptr, nullptr ) == aiReturn_SUCCESS )
				{
					const std::string directory = file.substr( 0, file.find_last_of( '/' ) + 1 );
					modelOut->AddSpecularTexture( Texture::Load( directory + Path.data, false ) );
				}
			}

			//Load in the material properties
			aiColor4D diffuse, ambient, specular;
			float specularPower;

			aiGetMaterialColor( material, AI_MATKEY_COLOR_TRANSPARENT, &diffuse );
			aiGetMaterialColor( material, AI_MATKEY_COLOR_AMBIENT, &ambient );
			aiGetMaterialColor( material, AI_MATKEY_COLOR_SPECULAR, &specular );
			aiGetMaterialFloat( material, AI_MATKEY_SHININESS, &specularPower );
			
			modelOut->AddMaterial( Material( 
				AssimpColourToXMFLOAT4( diffuse ), 
				AssimpColourToXMFLOAT4( ambient ), 
				AssimpColourToXMFLOAT4( specular ), 
				specularPower ) );
		}
	}

	DirectX::XMFLOAT4 AssimpHelper::AssimpColourToXMFLOAT4( const aiColor4D& colour )
	{
		return XMFLOAT4( colour.r, colour.g, colour.b, colour.a );
	}
}
