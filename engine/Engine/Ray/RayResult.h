/*
 * RayResult.h
 *
 * Created 28/07/2022
 * Written by Todd Elliott
 *
 * Class that stores the information of a ray cast result
 */

#ifndef _RAYRESULT_H
#define _RAYRESULT_H

#include <Instinct.h>

namespace Instinct
{
	class Entity;

	struct RayResult
	{
		friend class RayCast;

	//private:
		RayResult() = default;

	public:
		bool						mHitSomething;
		std::shared_ptr<Entity>		mHitEntity;

		XMFLOAT3					mRayStartPosition;
		XMFLOAT3					mHitPosition;
		XMFLOAT3					mRayDirection;

		float						mRayDistance;
	};
}

#endif
