/*
 * RayCast.cpp
 *
 * Created 28/07/2022
 * Written by Todd Elliott
 */

#include "RayCast.h"
#include <Engine/Camera.h>
#include <Engine/World.h>
#include <Engine/Terrain/Terrain.h>
#include <Engine/Ray/RayResult.h>

namespace Instinct
{
	std::shared_ptr<RayResult> RayCast::Create( const XMFLOAT3& origin, const XMFLOAT3& direction, RayType type )
	{
		auto result = std::make_shared<RayResult>();
		result->mRayStartPosition = origin;
		result->mRayDirection = direction;

		const auto fxmorigin = XMLoadFloat3( &origin );
		const auto fxmdir = XMLoadFloat3( &direction );

		if( type == RayType::kTerrainOnly )
		{
			const auto terrain = Engine::Instance().GetCurrentWorld()->GetTerrain();
			const auto& cells = terrain->GetGeometryCells();
			const auto cellCount = cells.size();

			auto found = false;
			float dist;

			for( const auto& cell : cells )
			{
				if( found )
				{
					break;
				}

				const auto models = cell->GetEntryList();
				const auto vertices = models[ 0 ]->GetVertices();
				const auto indices = models[ 0 ]->GetIndices();
				const uint32_t vertexCount = vertices.size();

				for( auto i = 0u ; i < vertexCount / 3 ; i ++ ) 
				{
					//indices
					int i0 = indices[ i * 3 + 0 ];
					int i1 = indices[ i * 3 + 1 ];
					int i2 = indices[ i * 3 + 2 ];

					//vertices for single tri
					XMVECTOR v0 = XMVectorSet( vertices[ i0 ].mPosition.x, vertices[ i0 ].mPosition.y, vertices[ i0 ].mPosition.z, 0.0f );
					XMVECTOR v1 = XMVectorSet( vertices[ i1 ].mPosition.x, vertices[ i1 ].mPosition.y, vertices[ i1 ].mPosition.z, 0.0f );
					XMVECTOR v2 = XMVectorSet( vertices[ i2 ].mPosition.x, vertices[ i2 ].mPosition.y, vertices[ i2 ].mPosition.z, 0.0f );

					if( TriangleTests::Intersects( fxmorigin, fxmdir, v0, v1, v2, dist ) || TriangleTests::Intersects( fxmorigin, fxmdir, v0, v2, v1, dist ) ) 
					{
						//FOUND
						found = true;
						break;
					}
				}
			}

			XMStoreFloat3( &result->mHitPosition, XMVectorAdd( fxmorigin, XMVectorScale( fxmdir, dist ) ) );
	
			if( result->mHitPosition.x > 512.f || result->mHitPosition.z > 512 )
			{
				int a = 0;
			}

			result->mHitSomething = found;
			result->mHitEntity = nullptr;
			result->mRayDistance = dist;
		}

		return result;
	}
}
