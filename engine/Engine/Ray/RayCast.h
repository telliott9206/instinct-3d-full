/*
 * RayCast.h
 *
 * Created 28/07/2022
 * Written by Todd Elliott
 *
 * Class that represents a casting of a ray
 */

#ifndef _RAYCAST_H
#define _RAYCAST_H

#include <Instinct.h>

namespace Instinct
{
	struct RayResult;

	class RayCast
	{
	public:
		enum RayType
		{
			kTerrainOnly = 0,
			kEntityOnly,
			kAll
		};

		static std::shared_ptr<RayResult> Create( const XMFLOAT3& origin, const XMFLOAT3& direction, RayType type );
	};
}

#endif
