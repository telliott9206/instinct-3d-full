/*
 * CascadedShadowMap.h
 *
 * Created 30/05/2019
 * Written by Todd Elliott
 */

#ifndef _CASCADEDSHADOWMAP_H
#define _CASCADEDSHADOWMAP_H

#include <Instinct.h>
#include <Engine/Texture/Texture2DArray.h>

#define SHADOW_CASCADES_COUNT	3

namespace Instinct
{
	class CascadedShadowMap: public Texture2DArray
	{
	public:
		CascadedShadowMap() = default;
		~CascadedShadowMap();

		static CascadedShadowMap* Create();
	};
};

#endif