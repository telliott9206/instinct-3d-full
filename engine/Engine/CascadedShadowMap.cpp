/*
 * CascadedShadowMap.cpp
 *
 * Created 30/05/2019
 * Written by Todd Elliott
 */

#include "CascadedShadowMap.h"
#include <Engine/Render/Context.h>
#include <System.h>

namespace Instinct
{
	CascadedShadowMap::~CascadedShadowMap()
	{
	}

	CascadedShadowMap* CascadedShadowMap::Create()
	{
		Context* pContext = System::Instance().GetContext();
		ID3D11Device* pDevice = pContext->GetDevice();

		float flNear = pContext->GetScreenNear();
		float flFar = pContext->GetScreenFar();

		const UINT iQualityValues[ 4 ][ 3 ] = 
		{
			{ 256, 128, 64		},
			{ 512, 256, 128		},
			{ 1024, 512, 256	},
			{ 2048, 1024, 512	}
		};

		int iIndex { pContext->GetShadowQuality() };

		UINT iQuality[ 3 ];
		iQuality[ 0 ] = iQualityValues[ iIndex ][ 0 ];
		iQuality[ 1 ] = iQualityValues[ iIndex ][ 1 ];
		iQuality[ 2 ] = iQualityValues[ iIndex ][ 2 ];

		//create the new object
		CascadedShadowMap* pTexture = new CascadedShadowMap();

		//store the number of buffers
		pTexture->mBufferCount = SHADOW_CASCADES_COUNT;

		//fill out the 2d texture description
		D3D11_TEXTURE2D_DESC textureDesc {};

		textureDesc.MipLevels			= 1;
		textureDesc.ArraySize			= 1;
		textureDesc.Format				= DXGI_FORMAT_R32G32B32A32_FLOAT;
		textureDesc.SampleDesc.Count	= 1;
		textureDesc.Usage				= D3D11_USAGE_DEFAULT;
		textureDesc.BindFlags			= D3D11_BIND_RENDER_TARGET | D3D11_BIND_SHADER_RESOURCE;
		textureDesc.CPUAccessFlags		= 0;
		textureDesc.MiscFlags			= 0;

		for( int i = 0 ; i < SHADOW_CASCADES_COUNT ; i ++ )
		{
			textureDesc.Width			= iQuality[ i ];
			textureDesc.Height			= iQuality[ i ];

			if( FAILED( pDevice->CreateTexture2D( &textureDesc, NULL, &pTexture->mRenderTargetTexture[ i ] ) ) )
			{
				DeleteObject( pTexture );
				System::Error( d3derr, "Failed to create texture 2D for Cascaded Shadow Map" );

				return nullptr;
			}
		}

		//fill out the render target view description
		D3D11_RENDER_TARGET_VIEW_DESC renderTargetViewDesc {};
	
		renderTargetViewDesc.Format = textureDesc.Format;
		renderTargetViewDesc.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE2D;
		renderTargetViewDesc.Texture2D.MipSlice = 0;

		// Create the render target view.
		for( int i = 0 ; i < SHADOW_CASCADES_COUNT ; i ++ )
		{
			if( FAILED( pDevice->CreateRenderTargetView( pTexture->mRenderTargetTexture[ i ], &renderTargetViewDesc, &pTexture->mRenderTargetView[ i ] ) ) )
			{
				DeleteObject( pTexture );
				System::Error( d3derr, "Failed to create render target view for Cascaded Shadow Map" );

				return nullptr;
			}
		}

		//fill out the shader resource view
		D3D11_SHADER_RESOURCE_VIEW_DESC shaderResourceViewDesc {};
	
		shaderResourceViewDesc.Format						= textureDesc.Format;
		shaderResourceViewDesc.ViewDimension				= D3D11_SRV_DIMENSION_TEXTURE2D;
		shaderResourceViewDesc.Texture2D.MostDetailedMip	= 0;
		shaderResourceViewDesc.Texture2D.MipLevels			= 1;

		for( int i = 0 ; i < SHADOW_CASCADES_COUNT ; i ++ )
		{
			if( FAILED( pDevice->CreateShaderResourceView( pTexture->mRenderTargetTexture[ i ], &shaderResourceViewDesc, &pTexture->mShaderResourceView[ i ] ) ) )
			{
				DeleteObject( pTexture );
				System::Error( d3derr, "Failed to create shader resource view for Cascaded Shadow Map" );

				return nullptr;
			}
		}

		//fill out the description for the depth buffer
		D3D11_TEXTURE2D_DESC depthBufferDesc {};

		depthBufferDesc.Width				= iQuality[ 0 ];
		depthBufferDesc.Height				= iQuality[ 0 ];
		depthBufferDesc.MipLevels			= 1;
		depthBufferDesc.ArraySize			= 1;
		depthBufferDesc.Format				= DXGI_FORMAT_D24_UNORM_S8_UINT; //DXGI_FORMAT_D24_UNORM_S8_UINT;
		depthBufferDesc.SampleDesc.Count	= 1;
		depthBufferDesc.SampleDesc.Quality	= 0;
		depthBufferDesc.Usage				= D3D11_USAGE_DEFAULT;
		depthBufferDesc.BindFlags			= D3D11_BIND_DEPTH_STENCIL;
		depthBufferDesc.CPUAccessFlags		= 0;
		depthBufferDesc.MiscFlags			= 0;

		if( FAILED( pDevice->CreateTexture2D( &depthBufferDesc, NULL, &pTexture->mDepthStencilBuffer ) ) )
		{
			DeleteObject( pTexture );
			System::Error( d3derr, "Failed to create depth stencil buffer for Cascaded Shadow Map" );

			return nullptr;
		}

		//fill out the description for the depth stencil view
		D3D11_DEPTH_STENCIL_VIEW_DESC depthStencilViewDesc {};

		depthStencilViewDesc.Format				= DXGI_FORMAT_D24_UNORM_S8_UINT;
		depthStencilViewDesc.ViewDimension		= D3D11_DSV_DIMENSION_TEXTURE2D;
		depthStencilViewDesc.Texture2D.MipSlice = 0;

		if( FAILED( pDevice->CreateDepthStencilView( pTexture->mDepthStencilBuffer, &depthStencilViewDesc, &pTexture->mDepthStencilView ) ) )
		{
			DeleteObject( pTexture );
			System::Error( d3derr, "Failed to create depth stencil view for Cascaded Shadow Map" );

			return nullptr;
		}

		//set up the viewport
		pTexture->mViewport.Width		= static_cast<float>( iQuality[ 0 ] );
		pTexture->mViewport.Height		= static_cast<float>( iQuality[ 0 ] );
		pTexture->mViewport.MinDepth	= 0.0f;
		pTexture->mViewport.MaxDepth	= 1.0f;
		pTexture->mViewport.TopLeftX	= 0.0f;
		pTexture->mViewport.TopLeftY	= 0.0f;

		return pTexture;
	}
}
