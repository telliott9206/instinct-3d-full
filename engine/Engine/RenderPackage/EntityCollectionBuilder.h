/*
 * EntityCollectionBuilder.h
 *
 * Created 08/05/2022
 * Written by Todd Elliott
 *
 * Builds a collection of entities to be rendered based on the given data.
 */

#ifndef _ENTITYCOLLECTIONBUILDER_H
#define _ENTITYCOLLECTIONBUILDER_H

#define ENTITY_WORKERS	1

#include "RenderCollectionBuilderInterface.h"
#include <Engine/RenderPackage/RenderPackage.h>

namespace Instinct
{
	class EntityCollectionBuilder : public RenderCollectionBuilderInterface
	{
	public:
		void BuildCollection( const DirectX::BoundingFrustum& frustum, const std::vector<std::shared_ptr<Entity>>& allEntities, RenderPackage& renderPackage );
	};
}

#endif
