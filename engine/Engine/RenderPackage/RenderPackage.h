/*
 * RenderPackage.h
 *
 * Created 15/04/2019
 * Written by Todd Elliott
 *
 * A collection of information that is used in the depth phase and deferred phase of
 * rendering containing entity and geomoetry cell data. This data is populated using
 * multithreaded operations.
 */

#ifndef _RENDERPACKAGE_H
#define _RENDERPACKAGE_H

#include <Instinct.h>
#include <Engine/Terrain/GeometryCell.h>

namespace Instinct
{
	class Entity;

	struct RenderPackage
	{
		void Clear()
		{
			mTerrainGeometryCellList.clear();
			mOceanGeometryCellList.clear();
			mEntityList.clear();
		}

		std::vector<std::shared_ptr<GeometryCell>>	mTerrainGeometryCellList;
		std::vector<std::shared_ptr<GeometryCell>>	mOceanGeometryCellList;
		std::vector<std::shared_ptr<Entity>>		mEntityList;
	};
};

#endif
