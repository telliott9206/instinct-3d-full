/*
 * GeometryCellCollectionBuilder.h
 *
 * Created 08/05/2022
 * Written by Todd Elliott
 *
 * Builds a collection of geometry cells to be rendered based on the given data.
 */

#ifndef _GEOMETRYCELLCOLLECTIONBUILDER_H
#define _GEOMETRYCELLCOLLECTIONBUILDER_H

#define GEOMETRY_CELL_WORKERS	2
#define OCEAN_CELL_WORKERS		2

#include "RenderCollectionBuilderInterface.h"
#include <Engine/RenderPackage/RenderPackage.h>

namespace Instinct
{
	class Camera;
	class Terrain;

	class GeometryCellCollectionBuilder : public RenderCollectionBuilderInterface
	{
	public:
		void BuildCollection( const std::shared_ptr<Camera>& camera, const std::shared_ptr<Terrain>& terrain, const std::vector<DirectX::BoundingFrustum*>& frustums, 
			std::vector<std::shared_ptr<GeometryCell>>& terrainGeometryCellList );
	};
}

#endif
