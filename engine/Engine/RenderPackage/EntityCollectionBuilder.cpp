/*
 * EntityCollectionBuilder.cpp
 *
 * Created 08/05/2022
 * Written by Todd Elliott
 */

#include "EntityCollectionBuilder.h"
#include <Engine/Utility/MathHelper.h>
#include <Engine/Thread/ThreadPool.h>
#include <Engine/Entity/Entity.h>

namespace Instinct
{
	void EntityCollectionBuilder::BuildCollection( const DirectX::BoundingFrustum& frustum, const std::vector<std::shared_ptr<Entity>>& allEntities, RenderPackage& renderPackage )
	{
		const auto allEntitiesCount = allEntities.size();
		if( allEntitiesCount == 0 )
		{
			//Don't continue if there's no work to do!
			return;
		}

		//Prepare the workload
		auto work = allEntitiesCount / ENTITY_WORKERS;
		if( work == 0u && allEntitiesCount > 0u )
		{
			//If work was 0, then there are more threads then there is work to be done
			work = 1;
		}

		//Fix the workload in the case of not a perfectly even division of work.
		//The first thread will take up the extra work
		std::vector<uint32_t> workLoad( ENTITY_WORKERS, work );
		if( allEntitiesCount % ENTITY_WORKERS != 0 )
		{
			workLoad[ 0 ] += allEntitiesCount - work * ENTITY_WORKERS;
		}

		std::vector<tAsyncTask> tasks;

		//create the tasks and push them into the thread pool
		for( auto i = 0 ; i < ENTITY_WORKERS ; ++ i )
		{
			tasks.push_back( ThreadPool::Instance().Run( [ this, &frustum, &workLoad, i, &renderPackage, &allEntities ] 
				{
					const auto startPos = i > 0 ? workLoad[ i - 1 ] + 1 : 0;
					const auto endPos = startPos + workLoad[ i ] - 1;

					for( auto j = startPos ; j < endPos ; ++ j )
					{
						auto entity = allEntities[ j ];
						if( !entity)
						{
							continue;
						}

						const auto model = entity->GetModel();
						const auto& entries = model->GetEntryList();

						if( entries.empty() )
						{
							continue;
						}

						const auto& aabb = model->GetAABB();
						const auto& postiion = aabb.Center;

						auto newAabb = aabb;
						newAabb.Center = MathHelper::AddVector3D( newAabb.Center, entity->GetPosition() );

						if( frustum.Contains( newAabb ) )
						{
							std::lock_guard<std::mutex> lock( mPackMutex );
							renderPackage.mEntityList.push_back( entity );
						}
					}
				} ) );
		}

		//wait for tasks to finish before going ahead
		for( auto i = 0 ; i < ENTITY_WORKERS ; ++ i )
		{
			JOIN( tasks.at( i ) );
		}
	}
}
