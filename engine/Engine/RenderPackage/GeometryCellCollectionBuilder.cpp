/*
* GeometryCellCollectionBuilder.cpp
*
* Created 08/05/2022
* Written by Todd Elliott
*
* Builds a collection of geometry cells to be rendered based on the given data.
*/

#include "GeometryCellCollectionBuilder.h"
#include <Engine/Utility/MathHelper.h>
#include <Engine/Thread/ThreadPool.h>
#include <Engine/Terrain/Terrain.h>
#include <Engine/Camera.h>

namespace Instinct
{
	void GeometryCellCollectionBuilder::BuildCollection( const std::shared_ptr<Camera>& camera, const std::shared_ptr<Terrain>& terrain, const std::vector<DirectX::BoundingFrustum*>& frustums, 
		std::vector<std::shared_ptr<GeometryCell>>& terrainGeometryCellList )
	{
		if( !terrain )
		{
			//if terrain is nullptr then there is no geometry cells
			return;	
		}

		const auto& cameraPos = camera->GetPosition();
		const auto screenFar = Engine::Instance().GetContext()->GetScreenFar();

		//calculate the number of cells per thread
		const auto& cells = terrain->GetGeometryCells();
		const auto workLoad = terrain->GetGeometryCellsCount() / GEOMETRY_CELL_WORKERS;

		//prepare the tasks
		std::vector<tAsyncTask> tasks;

		//create the tasks and push them into the thread pool
		for( auto i = 0 ; i < GEOMETRY_CELL_WORKERS ; ++ i )
		{
			tasks.push_back( ThreadPool::Instance().Run( [ this, &frustums, &terrainGeometryCellList, &cells, workLoad, i, &cameraPos, &screenFar ] 
				{
					for( auto j = i * workLoad ; j < workLoad * ( i + 1 ) ; ++ j )
					{
						auto& cell = cells[ j ];

						if( MathHelper::PointDistance3D( cameraPos, cell->GetPosition() ) > screenFar )
						{
							continue;
						}

						auto aabb = cell->GetAABB();
						aabb.Center = MathHelper::AddVector3D( aabb.Center, cell->GetPosition() );

						for( const auto& frustum : frustums )
						{
							if( frustum->Contains( aabb ) )
							{
								std::lock_guard<std::mutex> lock( mPackMutex );
								terrainGeometryCellList.push_back( cell );

								break;
							}
						}
					}
				} ) );
		}

		//wait for tasks to finish before going ahead
		for( auto i = 0 ; i < GEOMETRY_CELL_WORKERS ; ++ i )
		{
			JOIN( tasks.at( i ) );
		}
	}
}
