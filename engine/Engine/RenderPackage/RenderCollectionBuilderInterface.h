/*
 * RenderCollectionBuilderInterface.h
 *
 * Created 08/05/2022
 * Written by Todd Elliott
 *
 * Interface for the render package collections to be built.
 */

#ifndef _RENDERCOLLECTIONBUILDERINTERFACE_H
#define _RENDERCOLLECTIONBUILDERINTERFACE_H

#include <Instinct.h>

namespace Instinct
{
	class RenderCollectionBuilderInterface
	{
	public:
		virtual ~RenderCollectionBuilderInterface() = 0;

	protected:
		std::mutex mPackMutex;
	};
}

#endif
