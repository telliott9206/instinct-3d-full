/*
 * Material.h
 *
 * Created 03/05/2022
 * Written by Todd Elliott
 */

#ifndef _MATERIAL_H
#define _MATERIAL_H

#include <Instinct.h>

namespace Instinct
{
	struct Material
	{
		Material( const XMFLOAT4& diffuse, const XMFLOAT4& ambient, const XMFLOAT4& specular, float specularPower ) :
			mDiffuse( diffuse ),
			mAmbient( ambient ),
			mSpecular( specular ),
			mSpecularPower( specularPower )
		{
		}

		XMFLOAT4 mDiffuse;
		XMFLOAT4 mAmbient;
		XMFLOAT4 mSpecular;
		float mSpecularPower;

		XMFLOAT3 _padding;
	};
}

#endif
