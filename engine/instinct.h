/*
 * Instinct.h
 *
 * Created 30/11/2018
 * Written by Todd Elliott
 *
 * Contains the lib definitions for DirectX 11 and several useful
 * macro definitions.
 */

#ifndef _INSTINCT_H
#define _INSTINCT_H

#ifndef WIN32_LEAN_AND_MEAN
	#define WIN32_LEAN_AND_MEAN
#endif

#include <Windows.h>
#include <mutex>
#include <d3d11.h>

#pragma comment( lib, "d3d11.lib" )
#pragma comment( lib, "dxgi.lib" )
#pragma comment( lib, "D3DCompiler.lib" )

#define DIRECTINPUT_VERSION 0x0800
#include <dinput.h>
#pragma comment( lib, "dinput8.lib" )
#pragma comment( lib, "dxguid.lib" )

//#include <DirectXMath.h>
#include <DirectXCollision.h>
using namespace DirectX;

//D3D messages
#define d3derr		"Direct 3D Error"
#define dinputerr	"Direct Input Error"
#define generr		"Error"

//Common includes
#include <string>
#include <iostream>
#include <vector>
#include <map>
#include <algorithm>

//COM object releasing
#define ReleaseCOM( p )		{ if( p != nullptr ) { p->Release(); p = nullptr; } }

//Macros for function import/export
#if COMPILING_ENGINE
	#define DLLEXPORT __declspec( dllexport )
	#pragma comment ( lib, "assimp-vc140-mt.lib" )
#else
	#define DLLEXPORT __declspec( dllimport )
#endif

//Macros for aligning class by 16 byte for XMMATRIX
#define ALIGN16 __declspec( align( 16 ) ) 
#define OPERATORS_ALIGN16	void* operator new( size_t i )			\
							{										\
								return _aligned_malloc ( i, 16 );	\
							}										\
							void operator delete( void* p )			\
							{										\
								_aligned_free( p );					\
							}										\

//Macros for bit manipulation
#define FBitSet( x, y )		( x |= ( 1 << y ) )
#define FBitGet( x, y )		( x & ( 1 << y ) )
#define FBitStrip( x, y )	( x &= ~( 1 << y ) )

#define RADIAN	0.0174532925f

//ONLY CHANGE BELOW HERE - CONFIGURABLE OPTIONS
#define	SHADOW_CASCADES_COUNT	3
#define FACE_VERTEX_COUNT		6

//DO NOT CHANGE BELOW HERE!

#endif
