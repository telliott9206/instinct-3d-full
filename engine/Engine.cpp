/*
 * Engine.cpp
 *
 * Created 30/11/2018
 * Written by Todd Elliott
 */

#include "Engine.h"
#include <Engine/Input/Keyboard.h>
#include <Engine/Input/Mouse.h>
#include <Engine/Shaders/Shader.h>
#include <Engine/GameTime.h>
#include <Engine/Manager/ShaderManager.h>
#include <Engine/Shaders/MergeShader.h>
#include <Engine/World.h>

namespace Instinct
{
	Engine::Engine() :
		mShutdown( false )
	{
	}

	void Engine::LoadEngine( const std::function<void()>& onStarted, const std::function<bool()>& onFrame, const std::function<void()>& onEnd )
	{
		this->mOnStartedCallback	= onStarted;
		this->mOnFrameCallback		= onFrame;
		this->mOnEndCallback		= onEnd;

		Run();
	}

	void Engine::Run( std::shared_ptr<Context> context, std::shared_ptr<World> world )
	{
		mContext = context;
		mWorld = world;

		Run();
	}

	void Engine::Run()
	{
	#ifdef _DEBUG
		std::cout << "Starting Instinct-3D Game Engine ... " << std::endl;
		PrintHardwareInfo();
	#endif

		if( FAILED( DirectInput8Create( GetModuleHandle( NULL ), DIRECTINPUT_VERSION, IID_IDirectInput8, (void**)&mDirectInput, NULL) ) )
		{
			Engine::Error( dinputerr, "Failed to initialise Direct Input" );
			return;
		}

		if( !Keyboard::Instance().Init( mDirectInput ) )
		{
			Engine::Error( dinputerr, "Failed to initialize keyboard input device." );
			return;
		}

		if( !Mouse::Instance().Init( mDirectInput ) )
		{
			Engine::Error( dinputerr, "Failed to initialize mouse input device." );
			return;
		}

		auto& clock			= Time::Instance();
		clock.Reset();

		auto& shaderManager	= ShaderManager::Instance();
		auto* renderWindow	= mContext->GetRenderWindow();

		if( mOnStartedCallback != nullptr )
		{
			mOnStartedCallback();
		}

		MSG msg { 0 };
		auto& keyboard	= Keyboard::Instance();
		auto& mouse = Mouse::Instance();

		while( !mShutdown )
		{
			if( PeekMessage( &msg, NULL, 0, 0, PM_REMOVE ) )
			{
				TranslateMessage( &msg );
				DispatchMessage( &msg );

				if( msg.message == WM_QUIT )
				{
					break;
				}
			}

			clock.Tick();
			keyboard.UpdateState();
			mouse.UpdateState();

			if( mOnFrameCallback != nullptr )
			{
				if( !mOnFrameCallback() )
				{
					break;
				}
			}

			auto& deviceContext = mContext->GetDeviceContext();
			if( mWorld != nullptr )
			{
				mWorld->Update( deviceContext, clock.GetDeltaTime() );
				RenderFrame();
			}

			keyboard.UpdateOldState();
			mouse.UpdateOldState();
		}

		if( mOnEndCallback != nullptr )
		{
			mOnEndCallback();
		}

		exit( 1 );
	}

	void Engine::RenderFrame()
	{
		if( mContext != nullptr )
		{
			auto& deviceContext = mContext->GetDeviceContext();
		
			if( mWorld != nullptr )
			{
				auto camera = mWorld->GetActiveCamera();
				if( camera )
				{
					BindDeferredRenderPass( deviceContext );
					BindBackBufferRenderPass( deviceContext );
				}
			}
		}
	}

	void Engine::BindDeferredRenderPass( ID3D11DeviceContext& deviceContext )
	{
		//Render depth
		mWorld->Render( deviceContext, tRenderPhase::kDepth );

		//Render the GBuffer
		mContext->SetGraphicBufferRenderTarget( deviceContext );
		mWorld->Render( deviceContext, tRenderPhase::kDiffuse );
	}

	void Engine::BindBackBufferRenderPass( ID3D11DeviceContext& deviceContext )
	{
		auto effectsTexture = ShaderManager::Instance().GetPPEffectsTexture();

		const auto renderPPFX = !ShaderManager::Instance().GetPPEffects().empty();
		if( renderPPFX )
		{
			effectsTexture->ClearRenderTarget( deviceContext );
			effectsTexture->SetRenderTarget( deviceContext );

			mContext->BindRenderWindowOrthoTexture( deviceContext );
			ShaderManager::Instance().GetMergeShader().Render( deviceContext );
		}

		//Prepare to render to the back buffer
		mContext->ResetViewAndBackBuffer();
		mContext->DisableDepthBuffer();

		mContext->SceneBegin( 0.f, 0.2f, 0.6f, 1.f );

		if( renderPPFX )
		{
			ShaderManager::Instance().RenderPPEffects( deviceContext, *effectsTexture.get() );
		}
		else
		{
			mContext->BindRenderWindowOrthoTexture( deviceContext );
			ShaderManager::Instance().GetMergeShader().Render( deviceContext );
		}

		//Present!
		mContext->EnableDepthBuffer();
		mContext->SceneEnd();
	}

	//Note: original logic in this function was not written by me
	void Engine::PrintHardwareInfo()
	{
		int CPUInfo[ 4 ] = { -1 };
		unsigned nExIds, i = 0;
		char CPUBrandString[ 0x40 ];

		//get the information associated with each extended ID
		__cpuid( CPUInfo, 0x80000000 );
		nExIds = CPUInfo[ 0 ];

		for( i = 0x80000000 ; i <= nExIds ; ++ i )
		{
			__cpuid( CPUInfo, i );
			//interpret CPU brand string
			if( i == 0x80000002 )
			{
				memcpy( CPUBrandString, CPUInfo, sizeof( CPUInfo ) );
			}
			else if( i == 0x80000003 )
			{
				memcpy( CPUBrandString + 16, CPUInfo, sizeof( CPUInfo ) );
			}
			else if( i == 0x80000004 )
			{
				memcpy( CPUBrandString + 32, CPUInfo, sizeof( CPUInfo ) );
			}
		}

		//string includes manufacturer, model and clockspeed
		SYSTEM_INFO sysInfo;
		GetSystemInfo( &sysInfo );
		Engine::Print( "CPU: %s, Number of threads: %u", CPUBrandString, sysInfo.dwNumberOfProcessors );

		if( mContext != nullptr )
		{
			Engine::Print( "GPU: %s, %dmb video memory", mContext->GetDeviceDescription().c_str(), mContext->GetDeviceMemory() );
		}
	}

	std::string Engine::GetResourceDirectory()
	{
		//if this is debug, then return the resource directory as per the git repo

		char buf[ 512 ];
		GetModuleFileName( NULL, buf, sizeof buf - 1 );
		std::string path( buf );
#ifdef _DEBUG
		return path.substr( 0, path.find_last_of( "\\/" ) ) + "/../../resources/";
#else
		return path.substr( 0, path.find_last_of( "\\/" ) );
#endif
	}

	void Engine::Error( const std::string& caption, const char format[], ... )
	{
		char text[ 256 ];

		va_list args;
		va_start( args, format );
		vsnprintf_s( text, sizeof( text ) - 1, format, args );	//Format the text into the buffer
		va_end( args );

	#ifdef _DEBUG
		std::cout << text << std::endl;
	#endif

		MessageBoxA( NULL, text, caption.c_str(), MB_OK );
	}

	void Engine::Print( const char format[], ... )
	{
		char text[ 256 ];

		va_list args;
		va_start( args, format );
		vsnprintf_s( text, sizeof( text ) - 1, format, args );
		va_end( args );

		std::lock_guard<std::mutex> lock( mConsoleOutMutex );
		std::cout << text << std::endl;
	}
}
