/*
 * Entity.cpp
 *
 * Created 29/11/2018
 * Written by Todd Elliott
 */

#include "Entity.h"
#include <Engine/Camera.h>
#include <Engine/World.h>
#include <Engine/Model/Model.h>
#include <Engine/Terrain/Terrain.h>
#include <Engine.h>

namespace Instinct
{
	Entity::~Entity()
	{
	}

	std::shared_ptr<Entity> Entity::Create()
	{
		auto world = Engine::Instance().GetCurrentWorld();

		if( world == nullptr )
		{
			Engine::Error( generr, "Cannot create entity: No World present" );
			return {};
		}

		auto entity = std::make_shared<Entity>();
		return entity;
	}

	void Entity::Update( const std::shared_ptr<Camera>& camera, float delta )
	{
	}

	void Entity::SetVerticalAlignmentToTerrain( float offset )
	{
		auto world = Engine::Instance().GetCurrentWorld();

		if( world == nullptr )
		{
			Engine::Print( "Error occurred in SetVerticalAlignmentToTerrain() - World does not exist" );
			return;
		}

		const auto terrain = world->GetTerrain();
		if( &terrain == nullptr )
		{
			Engine::Print( "Error occurred in SetVerticalAlignmentToTerrain() - Terrain does not exist" );
			return;
		}

		const auto height = terrain->GetTerrainHeightAtPosition( { mPosition.x, mPosition.z } );
		SetPosition( mPosition.x, height + offset, mPosition.z );
	}
}
