/*
 * Basic.h
 *
 * Created 14/11/2021
 * Written by Todd Elliott
 *
 * The base class object for anything that can be placed in the world. Less complex than Entity.
 * It is primarily used for units of foliage; simply an orientation and a model.
 */

#ifndef _BASIC_H
#define _BASIC_H

#include <Instinct.h>
#include <Engine/Model/Model.h>

namespace Instinct
{
	class Basic
	{
	public:
		Basic();
		virtual ~Basic();

		DLLEXPORT const XMFLOAT3& GetPosition() const						{ return mPosition; }
		DLLEXPORT virtual void SetPosition( const XMFLOAT3& pos )			{ mPosition = pos; }
		DLLEXPORT virtual void SetPosition( float x, float y, float z )		{ mPosition.x = x; mPosition.y = y; mPosition.z = z; }

		DLLEXPORT const XMFLOAT3& GetRotation() const						{ return mRotation; }
		DLLEXPORT virtual void SetRotation( const XMFLOAT3& rotation )		{ mRotation = rotation; }
		DLLEXPORT virtual void SetRotation( float x, float y, float z )		{ mRotation.x = x; mRotation.y = y; mRotation.z = z; }

		DLLEXPORT const XMFLOAT3& GetScale() const							{ return mScale; }
		DLLEXPORT void SetScale( const XMFLOAT3& scale )					{ mScale = scale; }
		DLLEXPORT void SetScale( float x, float y, float z )				{ mScale = XMFLOAT3( x, y, z ); }

		DLLEXPORT std::shared_ptr<Model> GetModel()	const					{ return mModel; }	//Can be nullptr!
		DLLEXPORT void SetModel( std::shared_ptr<Model> model );

		virtual XMMATRIX CalculateWorldMatrix() const;

	protected:
		XMFLOAT3				mPosition;
		XMFLOAT3				mRotation;
		XMFLOAT3				mScale;

		std::shared_ptr<Model>	mModel;
	};
}

#endif
