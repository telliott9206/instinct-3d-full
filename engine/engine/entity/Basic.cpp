/*
 * Basic.cpp
 *
 * Created 17/11/2021
 * Written by Todd Elliott
 */

#include "Basic.h"

namespace Instinct
{
	Basic::Basic() :
		mPosition( { 0.f, 0.f, 0.f } ),
		mRotation( { 0.f, 0.f, 0.f } ),
		mScale( { 1.f, 1.f, 1.f } ),
		mModel( nullptr )
	{
	}

	Basic::~Basic()
	{
	}

	void Basic::SetModel( std::shared_ptr<Model> model )
	{
		if( mModel )
		{
			mModel->DecrementInstanceCount();
		}

		mModel = model;
		mModel->IncrementInstanceCount();
	}

	XMMATRIX Basic::CalculateWorldMatrix() const
	{
		XMMATRIX position, rotation, scale;

		position = XMMatrixTranslation( mPosition.x, mPosition.y, mPosition.z );
		rotation = XMMatrixRotationRollPitchYaw( mRotation.x, mRotation.y, mRotation.z );
		scale = XMMatrixScaling( mScale.x, mScale.y, mScale.z );

		return ( XMMatrixIdentity() * scale * rotation * position );
	}
}
