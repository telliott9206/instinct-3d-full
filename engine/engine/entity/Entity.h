/*
 * Entity.h
 *
 * Created 29/11/2018
 * Written by Todd Elliott
 *
 * The root note for anything placed in the world, for anything more complex than a foliage unit. 
 */

#ifndef _ENTITY_H
#define _ENTITY_H

#include "Basic.h"

namespace Instinct
{
	class Model;
	class Camera;

	class Entity : public Basic
	{
	public:
		Entity() = default;
		virtual ~Entity();

		/* @brief	factory function for instantiating an entity object
		 *
		 * @return	shared_ptr to newly instantiated entity object
		 */
		DLLEXPORT static std::shared_ptr<Entity> Create();

		virtual void Update( const std::shared_ptr<Camera>& camera, float delta );
		DLLEXPORT void SetVerticalAlignmentToTerrain( float offset = 0.f );

		DLLEXPORT float GetAlpha() const							{ return mAlpha; }
		DLLEXPORT void SetAlpha( float alpha )						{ mAlpha = alpha; }

		DLLEXPORT bool IsLightAffected() const						{ return mIsLightAffected; }
		DLLEXPORT void SetLightAffected( bool lightAffected )		{ mIsLightAffected = lightAffected; }

		DLLEXPORT float GetReflectivePower() const					{ return mReflectivePower; }
		DLLEXPORT void SetRefelectivePower( float power )			{ mReflectivePower = power; }

		DLLEXPORT float GetSpecularPower() const					{ return mSpecularPower; }
		DLLEXPORT void SetSpecularPower( float power )				{ mSpecularPower = power; }

		DLLEXPORT const bool IsSprite() const						{ return mIsSprite; }

	protected:
		float	mAlpha				{ 1.f };
		float	mReflectivePower	{ 0.f };
		float	mSpecularPower		{ 0.f };

		bool	mIsLightAffected	{ true };
		bool	mIsSprite			{ false };
		bool	mIsSunSource		{ false };
	};
};

#endif
