/*
 * WaterShader.cpp
 *
 * Created 19/12/2018
 * Written by Todd Elliott
 */

#include "WaterShader.h"
#include <Engine/Camera.h>
#include <Engine/World.h>
#include <Engine/Model/Model.h>
#include <Engine/Utility/Util.h>
#include <Engine/Utility/DirectXHelper.h>
#include <Engine/Texture/Texture.h>
#include <Engine/Texture/Texture2DArray.h>
#include <Engine/Entity/Entity.h>
#include <Engine/Ocean.h>
#include <Engine/Terrain/GeometryCell.h>
#include <Engine/Terrain/Terrain.h>
#include <Engine.h>

namespace Instinct
{
	WaterShader::~WaterShader()
	{
	}

	std::unique_ptr<WaterShader> WaterShader::Load()
	{
		const auto input = Util::MultibyteToWideChar( std::string( Engine::GetResourceDirectory() + "Shaders/Water.fx" ).c_str() );
		const auto flags = Shader::GetShaderConstructionFlags();
		ID3D10Blob* compilationMsgs = nullptr, *vs, *ps;

		//VERTEX SHADER
		D3DCompileFromFile( input, 0, D3D_COMPILE_STANDARD_FILE_INCLUDE, "VS", VERTEX_SHADER_VERSION, flags, 0, &vs, &compilationMsgs );
		if( compilationMsgs != 0 )
		{
			Engine::Error( d3derr, (char*)compilationMsgs->GetBufferPointer() );
			ReleaseCOM( compilationMsgs );

		#ifndef _DEBUG
			return nullptr;
		#endif
		}

		//PIXEL SHADER
		D3DCompileFromFile( input, 0, D3D_COMPILE_STANDARD_FILE_INCLUDE, "PS", PIXEL_SHADER_VERSION, flags, 0, &ps, &compilationMsgs );
		if( compilationMsgs != 0 )
		{
			Engine::Error( d3derr, (char*)compilationMsgs->GetBufferPointer() );
			ReleaseCOM( compilationMsgs );

		#ifndef _DEBUG
			return nullptr;
		#endif
		}

		auto shader = std::make_unique<WaterShader>();
		auto context = Engine::Instance().GetContext();
		auto& device = context->GetDevice();
		auto& deviceContext = context->GetDeviceContext();

		//create the shaders
		device.CreateVertexShader( vs->GetBufferPointer(), vs->GetBufferSize(), nullptr, &shader->mVertexShader );
		device.CreatePixelShader( ps->GetBufferPointer(), ps->GetBufferSize(), nullptr, &shader->mPixelShader );

		//create the input layout
		D3D11_INPUT_ELEMENT_DESC layout[] =
		{
			{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
			{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
			{ "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 }
		};

		const auto hr = shader->CreateInputLayout( "Water.fx", *vs, *layout, 3 );
		ReleaseCOM( vs );
		ReleaseCOM( ps );

		if( FAILED( hr ) ||
			!DirectXHelper::CreateConstantBuffer<cbPerObject>( shader->mPerObjectBuffer, D3D11_USAGE_DEFAULT, D3D11_USAGE_DEFAULT ) ||
			!DirectXHelper::CreateConstantBuffer<cbPerFrameEx>( shader->mPerFrameBuffer, D3D11_USAGE_DEFAULT, D3D11_USAGE_DEFAULT ) ||
			!DirectXHelper::CreateConstantBuffer<cbPerLightingChange>( shader->mPerLightingBuffer, D3D11_USAGE_DEFAULT, D3D11_USAGE_DEFAULT )  ||
			!shader->CreateTextureSamplers()
			)
		{
			return nullptr;
		}

	#ifdef _DEBUG
		Engine::Print( "Water shader loaded" );
	#endif

		return std::move( shader );
	}

	void WaterShader::RenderGeometry( ID3D11DeviceContext& deviceContext, const std::shared_ptr<Ocean>& ocean, const std::shared_ptr<Camera>& camera, const std::vector<std::shared_ptr<GeometryCell>>& geometryCells )
	{
		cbPerObject buf;
		buf.mWorld = XMMatrixTranspose( XMMatrixTranslation( 0.f, 0.f, 0.f ) );

		BindPerObjectBuffer( deviceContext, buf );
		BindPerFrameBuffer( deviceContext, camera );
		BindTextures( deviceContext, ocean );

		deviceContext.IASetInputLayout( mInputLayout );

		deviceContext.VSSetShader( mVertexShader, 0, 0 );
		deviceContext.HSSetShader( nullptr, 0, 0 );		//Note: Hull and domain not actually needed unless reverting to tesselation shading
		deviceContext.DSSetShader( nullptr, 0, 0 );
		deviceContext.PSSetShader( mPixelShader, 0, 0 );

		deviceContext.PSSetSamplers( 0, 1, &mSampleStateClamp );
		deviceContext.PSSetSamplers( 1, 1, &mSampleStateWrap );

		for( auto& cell : geometryCells )
		{
			cell->RenderTerrainCell( deviceContext );
		}
	}

	void WaterShader::BindTextures( ID3D11DeviceContext& deviceContext, const std::shared_ptr<Ocean>& ocean ) const
	{
		//Only a single diffuse and normal texture for water
		auto diffuseTexture = ocean->GetDiffuseTextureList()[ 0 ];
		if( diffuseTexture != nullptr )
		{
			auto** diffuse = diffuseTexture->GetResource();
			deviceContext.PSSetShaderResources( 0, 1, diffuse );
		}

		auto normalTexture = ocean->GetNormalTextureList()[ 0 ];
		if( normalTexture != nullptr )
		{
			auto** normal = normalTexture->GetResource();
			deviceContext.PSSetShaderResources( 1, 1, normal );
		}
	}
}
