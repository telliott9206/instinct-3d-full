/*
 * SkydomeShader.h
 *
 * Created 08/03/2019
 * Written by Todd Elliott
 *
 * A shader used specifically for shading skydomes due to the skydome nature of
 * rendering gradient colours.
 */

#ifndef _SKYDOMESHADER_H
#define _SKYDOMESHADER_H

#include "Shader.h"

namespace Instinct
{
	class Skydome;

	class SkydomeShader: public Shader
	{
	public:
		struct cbSkyColour
		{
			XMFLOAT4 centreColour;
			XMFLOAT4 apexColour;
		};

		SkydomeShader() = default;
		~SkydomeShader();

		static std::unique_ptr<SkydomeShader> Load();
		
		void Render( ID3D11DeviceContext& deviceContext, const std::shared_ptr<Skydome>& pEntity );

	private:
		void BindSkyColourBuffer( ID3D11DeviceContext& deviceContext );

	private:
		ID3D11Buffer* mSkyColourBuffer;
	};
};

#endif
