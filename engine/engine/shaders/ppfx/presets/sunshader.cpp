/*
 * Sunshader.cpp
 *
 * Created 09/05/2019
 * Written by Todd Elliott
 */

#include "SunShader.h"
#include <Engine/Utility/Util.h>
#include <Engine/Render/Context.h>
#include <Engine/Entity/Entity.h>
#include <Engine/Camera.h>
#include <Engine/World.h>
#include <Engine/Model/Billboard.h>
#include <Engine/Model/Model.h>
#include <Engine/Texture/Texture.h>
#include <Engine/Utility/DirectXHelper.h>
#include <Engine/Utility/MathHelper.h>

#include <fstream>

namespace Instinct
{
	SunShader::~SunShader()
	{
	}

	std::unique_ptr<SunShader> SunShader::Load( const std::string& file, const std::string& vfunc, const std::string& pfunc, const D3D11_INPUT_ELEMENT_DESC& desc, uint32_t elements )
	{
		std::ifstream fin( file );
		if( fin.fail() )
		{
			Engine::Error( "Load Failed", "The file %s does not exist!", file.c_str() );
			return nullptr;
		}
		fin.close();

		const auto flags = Shader::GetShaderConstructionFlags();
		const auto* input = Util::MultibyteToWideChar( file.c_str() );
		ID3D10Blob* compilationMsgs = nullptr, *vs, *ps;

		//VERTEX SHADER
		D3DCompileFromFile( input, 0, D3D_COMPILE_STANDARD_FILE_INCLUDE, vfunc.c_str(), VERTEX_SHADER_VERSION, flags, 0, &vs, &compilationMsgs );
		if( compilationMsgs != 0 )
		{
			Engine::Error( d3derr, static_cast<char*>( compilationMsgs->GetBufferPointer() ) );
			ReleaseCOM( compilationMsgs );

			return nullptr;
		}
	
		//PIXEL SHADER
		D3DCompileFromFile( input, 0, D3D_COMPILE_STANDARD_FILE_INCLUDE, pfunc.c_str(), PIXEL_SHADER_VERSION, flags, 0, &ps, &compilationMsgs );
		if( compilationMsgs != 0 )
		{
			Engine::Error( d3derr, static_cast<char*>( compilationMsgs->GetBufferPointer() ) );
			ReleaseCOM( compilationMsgs );

			return nullptr;
		}

		auto shader = std::make_unique<SunShader>();
		auto& device = Engine::Instance().GetContext()->GetDevice();

		//create the shaders
		device.CreateVertexShader( vs->GetBufferPointer(), vs->GetBufferSize(), nullptr, &shader->mVertexShader );
		device.CreatePixelShader( ps->GetBufferPointer(), ps->GetBufferSize(), nullptr, &shader->mPixelShader );
	
		//create the input layout
		const auto hr = shader->CreateInputLayout( file, *vs, desc, elements );
		ReleaseCOM( vs );
		ReleaseCOM( ps );

		if( FAILED( hr ) ||
			!DirectXHelper::CreateConstantBuffer<cbPerObject>( shader->mPerObjectBuffer, D3D11_USAGE_DEFAULT, D3D11_USAGE_DEFAULT ) ||
			!DirectXHelper::CreateConstantBuffer<cbPerFrameEx>( shader->mPerFrameBuffer, D3D11_USAGE_DEFAULT, D3D11_USAGE_DEFAULT ) ||
			!DirectXHelper::CreateConstantBuffer<cbPerLightingChange>( shader->mPerLightingBuffer, D3D11_USAGE_DEFAULT, D3D11_USAGE_DEFAULT ) ||
			!DirectXHelper::CreateConstantBuffer<cbSunBuffer>( shader->mSunBuffer, D3D11_USAGE_DYNAMIC, D3D11_CPU_ACCESS_WRITE ) ||
			!shader->CreateTextureSamplers() )
		{
			return nullptr;
		}

	#ifdef _DEBUG
		Engine::Print( "Shader %s loaded", input );
	#endif

		return std::move( shader );
	}

	void SunShader::Render( ID3D11DeviceContext& deviceContext, const std::shared_ptr<Entity>& entity )
	{
		auto camera = Engine::Instance().GetCurrentWorld()->GetActiveCamera();
		if( !camera )
		{
	#ifdef _DEBUG
			Engine::Print( "Warning: No camera present to render" );
	#endif
			return;
		}

		auto model = entity->GetModel();
		BindPerObjectBuffer( deviceContext, entity );

		deviceContext.IASetInputLayout( mInputLayout );

		deviceContext.VSSetShader( mVertexShader, 0, 0 );
		deviceContext.HSSetShader( nullptr, 0, 0 );
		deviceContext.DSSetShader( nullptr, 0, 0 );
		deviceContext.PSSetShader( mPixelShader, 0, 0 );

		deviceContext.PSSetSamplers( 0, 1, &mSampleState );
		model->Render( deviceContext );
	}

	void SunShader::BindSunBuffer( ID3D11DeviceContext& deviceContext )
	{
		cbSunBuffer buf;

		auto world = Engine::Instance().GetCurrentWorld();
		auto camera = world->GetActiveCamera();
		const auto& position = world->GetActiveSun()->GetPosition();

		const auto& view = camera->GetViewMatrix();
		const auto& proj = camera->GetProjectionMatrix();

		buf.mSunPosition = MathHelper::WorldToScreen( position, view, proj );
		DirectXHelper::CopyToBuffer<cbSunBuffer>( deviceContext, mSunBuffer, &buf );
		deviceContext.PSSetConstantBuffers( 0, 1, &mSunBuffer );
	}
}
