/*
 * SunShader.h
 *
 * Created 09/05/2019
 * Written by Todd Elliott
 *
 * A preset sun shader that provides glowing for the sun texture as well as god-rays.
 */

#include <Engine/Shaders/Shader.h>

#ifndef _SUNSHADER_H
#define _SUNSHADER_H

namespace Instinct
{
	class Context;
	class Camera;
	class Entity;

	struct cbSunBuffer
	{
		XMFLOAT2 mSunPosition;
		XMFLOAT2 _padding;
	};

	class SunShader: public Shader
	{
	public:
		SunShader() = default;
		~SunShader();

		static std::unique_ptr<SunShader> Load( const std::string& file, const std::string& vFunc, const std::string& pfunc, const D3D11_INPUT_ELEMENT_DESC& desc, uint32_t elements );
		void Render( ID3D11DeviceContext& deviceContext, const std::shared_ptr<Entity>& entity ) override;

		void BindSunBuffer( ID3D11DeviceContext& deviceContext );

	private:
		ID3D11Buffer* mSunBuffer;
	};
};

#endif
