/*
 * PPShader.h
 *
 * Created 24/03/2019
 * Written by Todd Elliott
 *
 * A post processing shader. It can be one of many as part of a post processing effect.
 */

#ifndef _PPSHADER_H
#define _PPSHADER_H

#include <Instinct.h>
#include <Engine/Shaders/Mergeshader.h>

namespace Instinct
{
	class Texture2D;

	class PPShader : public MergeShader
	{
	public:
		PPShader() = default;
		~PPShader() = default;

		DLLEXPORT static std::shared_ptr<PPShader> Load( const std::string& file, int width, int height );

		Texture2D* Render( ID3D11DeviceContext& deviceContext, Texture2D& original, Texture2D& previous, bool renderToBackBuffer );

		ID3D11InputLayout* GetInputLayout() const	{ return mInputLayout; }
		Texture2D* GetTexture2D() const				{ return mRenderTexture.get(); }

	private:
		std::unique_ptr<Texture2D> mRenderTexture;
	};
};

#endif
