/*
 * PPEffect.cpp
 *
 * Created 24/03/2019
 * Written by Todd Elliott
 */

#include <Engine/Shaders/PPFX/PPEffect.h>
#include <Engine/Shaders/PPFX/PPShader.h>
#include <Engine/Shaders/MergeShader.h>
#include <Engine/Camera.h>
#include <Engine/World.h>
#include <Engine/Texture/Texture2DArray.h>
#include <Engine/Manager/ShaderManager.h>
#include <Engine.h>

namespace Instinct
{
	PPEffect::PPEffect() :
		mShaderCount( 0 ),
		mIsEnabled( false )
	{
	}

	PPEffect::~PPEffect()
	{
	}

	std::shared_ptr<PPEffect> PPEffect::Create( std::vector<std::shared_ptr<PPShader>>& shaders )
	{
		if( shaders.empty() )
		{
			Engine::Error( generr, "Attempted to create instance of PPEffect with 0 pass shaders." );
			return nullptr;
		}

		auto effect = std::make_shared<PPEffect>();

		effect->mShaderCount = shaders.size();
		effect->mShaders = shaders;

		ShaderManager::Instance().AddPPEffect( effect );

		return effect;
	}

	void PPEffect::Render( ID3D11DeviceContext& deviceContext, Texture2D& renderTexture )
	{
		deviceContext.IASetInputLayout( mShaders[ 0 ]->GetInputLayout() );
		deviceContext.IASetPrimitiveTopology( D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST );

		deviceContext.HSSetShader( nullptr, 0, 0 );
		deviceContext.DSSetShader( nullptr, 0, 0 );

		Texture2D* texture = nullptr;

		auto i = 0;
		for( auto sh : mShaders )
		{
			texture = sh->Render( deviceContext, renderTexture, i == 0 ? renderTexture : *texture, false );
			i ++;
		}
	}
}
