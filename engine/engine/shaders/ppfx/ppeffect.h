/*
 * PPEffect.h
 * Created 24/03/2019
 * Written by Todd Elliott
 *
 * A post processing effect class that can store multiple shaders as part of the effect.
 */

#ifndef _PPEFFECT_H
#define _PPEFFECT_H

#include <Instinct.h>
#include <Engine/Texture/Texture2D.h>

namespace Instinct
{
	class PPShader;

	class PPEffect
	{
	public:
		PPEffect();
		~PPEffect();

		DLLEXPORT static std::shared_ptr<PPEffect> Create( std::vector<std::shared_ptr<PPShader>>& shaders );

		void Render( ID3D11DeviceContext& deviceContext, Texture2D& renderTexture );

		DLLEXPORT void Enable()					{ mIsEnabled = true; }
		DLLEXPORT void Disable()				{ mIsEnabled = false; }
		DLLEXPORT const bool IsEnabled() const	{ return mIsEnabled; }

	private:
		std::vector<std::shared_ptr<PPShader>>	mShaders;
		int										mShaderCount;

		bool									mIsEnabled;
	};
};

#endif
