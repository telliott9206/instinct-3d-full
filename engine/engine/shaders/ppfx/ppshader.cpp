/*
 * PPShader.cpp
 *
 * Created 24/03/2019
 * Written by Todd Elliott
 */

#include "PPShader.h"
#include <Engine/Utility/Util.h>
#include <Engine/Render/Context.h>
#include <Engine/Texture/Texture2D.h>
#include <Engine/Texture/Texture2DArray.h>
#include <Engine.h>

namespace Instinct
{
	std::shared_ptr<PPShader> PPShader::Load( const std::string& file, int width, int height )
	{
		std::ifstream fin( file );
		if( fin.fail() )
		{
			Engine::Error( "Load Failed", "The file %s does not exist!", file.c_str() );
			return nullptr;
		}
		fin.close();

		const auto flags = Shader::GetShaderConstructionFlags();
		const auto* str = Util::MultibyteToWideChar( file.c_str() );
		ID3D10Blob* compilationMsgs = nullptr, *vs, *ps;

		//VERTEX SHADER
		D3DCompileFromFile( str, 0, D3D_COMPILE_STANDARD_FILE_INCLUDE, "VS", VERTEX_SHADER_VERSION, flags, 0, &vs, &compilationMsgs );
		if( compilationMsgs != 0 )
		{
			Engine::Error( d3derr, static_cast<char*>( compilationMsgs->GetBufferPointer() ) );
			ReleaseCOM( compilationMsgs );

		#ifndef _DEBUG
			return nullptr;
		#endif
		}

		//PIXEL SHADER
		D3DCompileFromFile( str, 0, D3D_COMPILE_STANDARD_FILE_INCLUDE, "PS", PIXEL_SHADER_VERSION, flags, 0, &ps, &compilationMsgs );
		if( compilationMsgs != 0 )
		{
			Engine::Error( d3derr, static_cast<char*>( compilationMsgs->GetBufferPointer() ) );
			ReleaseCOM( compilationMsgs );

		#ifndef _DEBUG
			return nullptr;
		#endif
		}

		auto shader = std::make_unique<PPShader>();
		auto& device = Engine::Instance().GetContext()->GetDevice();

		//create the shaders
		device.CreateVertexShader( vs->GetBufferPointer(), vs->GetBufferSize(), nullptr, &shader->mVertexShader );
		device.CreatePixelShader( ps->GetBufferPointer(), ps->GetBufferSize(), nullptr, &shader->mPixelShader );

		//create the input layout
		D3D11_INPUT_ELEMENT_DESC layout[] =		//define the input layout description for the input assembler
		{
			{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
			{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 }
		};

		const auto hr = shader->CreateInputLayout( file, *vs, *layout, 2 );
		ReleaseCOM( vs );
		ReleaseCOM( ps );

		shader->mRenderTexture = Texture2D::Create( width, height );

		if( !shader->CreateTextureSamplers() )
		{
			return nullptr;
		}

	#ifdef _DEBUG
		Engine::Print( "PPFX Shader %s loaded", file.c_str() );
	#endif

		return shader;
	}

	Texture2D* PPShader::Render( ID3D11DeviceContext& deviceContext, Texture2D& original, Texture2D& previous, bool renderToBackBuffer )
	{
		//TODO: This whole function is ugly and unclear. Each element of the GBuffer should be well named
		deviceContext.IASetPrimitiveTopology( D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST );

		if( !renderToBackBuffer )
		{
			mRenderTexture->ClearRenderTarget( deviceContext );
			mRenderTexture->SetRenderTarget( deviceContext );
		}

		auto* buffer = Engine::Instance().GetContext()->GetGraphicBuffer();

		auto* r1 = buffer->GetShaderResourceView( 0 );
		auto* r2 = buffer->GetShaderResourceView( 1 );
		auto* r3 = buffer->GetShaderResourceView( 2 );
		auto* r4 = buffer->GetShaderResourceView( 3 );
	
		deviceContext.PSSetShaderResources( 2, 1, &r1 );
		deviceContext.PSSetShaderResources( 3, 1, &r2 );
		deviceContext.PSSetShaderResources( 4, 1, &r3 );
		deviceContext.PSSetShaderResources( 5, 1, &r4 );
	
		auto* r = original.GetShaderResourceView();
		deviceContext.PSSetShaderResources( 0, 1, &r );

		//if( previous != nullptr )
		{
			auto* s = previous.GetShaderResourceView();
			deviceContext.PSSetShaderResources( 1, 1, &s );
		}

		deviceContext.VSSetShader( mVertexShader, 0, 0 );
		//deviceContext.HSSetShader( nullptr, 0, 0 );
		//deviceContext.DSSetShader( nullptr, 0, 0 );
		deviceContext.PSSetShader( mPixelShader, 0, 0 );

		deviceContext.DrawIndexed( 6, 0, 0 );

		return mRenderTexture.get();
	}
}
