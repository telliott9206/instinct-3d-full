/*
 * Shader.cpp
 *
 * Created 08/12/2018
 * Written by Todd Elliott
 */

#include "Shader.h"
#include <Engine/Utility/Util.h>
#include <Engine/Render/Context.h>
#include <Engine/Entity/Entity.h>
#include <Engine/Camera.h>
#include <Engine/World.h>
#include <Engine/Model/Model.h>
#include <Engine/Texture/Texture.h>
#include <Engine/Model/Billboard.h>
#include <Engine/Utility/DirectXHelper.h>
#include <Engine/Terrain/Terrain.h>
#include <Engine/Ocean.h>
#include <Engine/Light/DirectionalLight.h>
#include <Engine/Model/ModelInstanceData.h>

namespace Instinct
{
	Shader::~Shader()
	{
		ReleaseCOM( mVertexShader );
		ReleaseCOM( mPixelShader );
		ReleaseCOM( mInputLayout );
		ReleaseCOM( mPerObjectBuffer );
		ReleaseCOM( mPerFrameBuffer );
		ReleaseCOM( mPerLightingBuffer );
	}

	std::unique_ptr<Shader> Shader::Load( const std::string& file, const std::string& vfunc, const std::string& pfunc, const D3D11_INPUT_ELEMENT_DESC& desc, uint32_t elements )
	{
		std::ifstream fin( file );
		if( fin.fail() )
		{
			Engine::Error( "Load Failed", "The file %s does not exist!", file.c_str() );
			return nullptr;
		}

		fin.close();
		ID3D10Blob* compilationMsgs = 0, *vs, *ps;
		const auto input = Util::MultibyteToWideChar( file.c_str() );
		const auto flags = GetShaderConstructionFlags();

		//VERTEX SHADER
		D3DCompileFromFile( input, 0, D3D_COMPILE_STANDARD_FILE_INCLUDE, vfunc.c_str(), VERTEX_SHADER_VERSION, flags, 0, &vs, &compilationMsgs );
		if( compilationMsgs != 0 )
		{
			Engine::Error( d3derr, static_cast<char*>( compilationMsgs->GetBufferPointer() ) );
			ReleaseCOM( compilationMsgs );

		#ifndef _DEBUG
			return nullptr;
		#endif
		}
	
		//PIXEL SHADER
		D3DCompileFromFile( input, 0, D3D_COMPILE_STANDARD_FILE_INCLUDE, pfunc.c_str(), PIXEL_SHADER_VERSION, flags, 0, &ps, &compilationMsgs );
		if( compilationMsgs != 0 )
		{
			Engine::Error( d3derr, static_cast<char*>( compilationMsgs->GetBufferPointer() ) );
			ReleaseCOM( compilationMsgs );

		#ifndef _DEBUG
			return nullptr;
		#endif
		}

		auto& device = Engine::Instance().GetContext()->GetDevice();
		auto shader = std::make_unique<Shader>();
		device.CreateVertexShader( vs->GetBufferPointer(), vs->GetBufferSize(), nullptr, &shader->mVertexShader );
		device.CreatePixelShader( ps->GetBufferPointer(), ps->GetBufferSize(), nullptr, &shader->mPixelShader );
	
		const auto hr = shader->CreateInputLayout( file, *vs, desc, elements );
		ReleaseCOM( vs );
		ReleaseCOM( ps );

		if( !hr ||
			!DirectXHelper::CreateConstantBuffer<cbPerObject>( shader->mPerObjectBuffer, D3D11_USAGE_DEFAULT, D3D11_USAGE_DEFAULT, "Per Object Buffer" ) ||
			!DirectXHelper::CreateConstantBuffer<cbPerFrameEx>( shader->mPerFrameBuffer, D3D11_USAGE_DEFAULT, D3D11_USAGE_DEFAULT, "Per Frame Buffer" ) ||
			!DirectXHelper::CreateConstantBuffer<cbPerLightingChange>( shader->mPerLightingBuffer, D3D11_USAGE_DEFAULT, D3D11_USAGE_DEFAULT, "Lighting Buffer" ) ||
			!shader->CreateTextureSamplers()
			)
		{
			return nullptr;
		}
	
	#ifdef _DEBUG
		Engine::Print( "Shader %s loaded", file.c_str() );
	#endif

		return std::move( shader );
	}

	void Shader::Render( ID3D11DeviceContext& deviceContext, const std::shared_ptr<Entity>& entity )
	{
		//Not used by base class

		auto model	= entity->GetModel();
		auto camera = Engine::Instance().GetCurrentWorld()->GetActiveCamera();

		if( camera == nullptr )
		{
#ifdef _DEBUG
			Engine::Print( "Warning: No camera present to render" );
#endif
			return;
		}

		BindPerObjectBuffer( deviceContext, entity );
		BindPerFrameBuffer( deviceContext, camera );
		BindPerLightingBuffer( deviceContext );

		deviceContext.IASetInputLayout( mInputLayout );
		BindShaders( deviceContext );
		BindTextureSamplers( deviceContext );

		model->Render( deviceContext );
	}

	void Shader::RenderTerrain( ID3D11DeviceContext& deviceContext, const std::shared_ptr<Terrain>& terrain )
	{
		//Not likely to be used unless perhaps low graphics settings - required for vtable
	}

	void Shader::RenderOcean( ID3D11DeviceContext& deviceContext, const std::shared_ptr<Ocean>& terrain )
	{
		//Not likely to be used unless perhaps low graphics settings - required for vtable
	}

	void Shader::Bind( ID3D11DeviceContext& deviceContext )
	{
		//Prepare the shader for rendering
		BindShaders( deviceContext );
		BindTextureSamplers( deviceContext );
		SetInputLayout( deviceContext );
	}

	void Shader::BindInstanceBuffer( ID3D11DeviceContext& deviceContext, const std::shared_ptr<Mesh<Vertex>>& modelEntry, ID3D11Buffer* instanceBuffer )
	{
		uint32_t stride[ 2 ] =
		{
			sizeof( Vertex ),
			sizeof( ModelInstanceData )
		};

		uint32_t offset[ 2 ] = 
		{
			0,
			0
		};

		ID3D11Buffer* buffers[ 2 ] =
		{
			modelEntry->GetVertexBuffer(),
			instanceBuffer
		};

		deviceContext.IASetVertexBuffers( 0, 2, buffers, stride, offset );
		deviceContext.IASetIndexBuffer( modelEntry->GetIndexBuffer(), DXGI_FORMAT_R32_UINT, 0 );
	}

	void Shader::RenderInstanced( ID3D11DeviceContext& deviceContext, uint32_t instanceCount, uint32_t vertexCount, uint32_t indexCount, uint32_t modelEntryIndex, 
		const std::shared_ptr<Entity>& baseEntity, int cascadeIndex )
	{
		auto camera = Engine::Instance().GetCurrentWorld()->GetActiveCamera();
		BindPerFrameBuffer( deviceContext, camera );

		const auto model = baseEntity->GetModel();
		const auto& diffuseTextures = model->GetDiffuseTextureList();
		const auto& models = model->GetEntryList();

		if( models.empty() )
		{
			return;
		}

		auto entry = models[ modelEntryIndex ];
		if( static_cast<size_t>( entry->GetMaterialIndex() ) > diffuseTextures.size() - 1 || diffuseTextures.empty() )
		{
			return;
		}
	
		auto texture = diffuseTextures.at( entry->GetMaterialIndex() );
		if( texture != nullptr )
		{
			auto** resource = texture->GetResource();
			deviceContext.PSSetShaderResources( SHADER_PS_Offset_Textures, 1, resource );
		}

		deviceContext.DrawIndexedInstanced( indexCount, instanceCount, 0, 0, 0 );
	}

	void Shader::BindShaders( ID3D11DeviceContext& deviceContext )
	{
		deviceContext.VSSetShader( mVertexShader, 0, 0 );
		deviceContext.HSSetShader( nullptr, 0, 0 );
		deviceContext.DSSetShader( nullptr, 0, 0 );
		deviceContext.PSSetShader( mPixelShader, 0, 0 );
	}

	void Shader::BindTextureSamplers( ID3D11DeviceContext& deviceContext )
	{
		ID3D11SamplerState* samplers[ 3 ] = 
		{
			mSampleStateClamp,
			mSampleStateWrap,
			mSampleState
		};

		deviceContext.PSSetSamplers( 0, 3, samplers );
	}

	void Shader::SetInputLayout( ID3D11DeviceContext& deviceContext )
	{
		deviceContext.IASetInputLayout( mInputLayout );
	}

	bool Shader::CreateTextureSamplers()
	{
		if( !CreateTextureSampler( mSampleState, D3D11_FILTER_ANISOTROPIC, D3D11_TEXTURE_ADDRESS_WRAP, D3D11_COMPARISON_ALWAYS, 2.f, 8 ) ||
			!CreateTextureSampler( mSampleStateWrap, D3D11_FILTER_ANISOTROPIC, D3D11_TEXTURE_ADDRESS_WRAP, D3D11_COMPARISON_ALWAYS, 0.f, 8 ) ||
			!CreateTextureSampler( mSampleStateClamp, D3D11_FILTER_MIN_MAG_MIP_LINEAR, D3D11_TEXTURE_ADDRESS_CLAMP, D3D11_COMPARISON_ALWAYS, 0.f, 8 )  )
		{
			return false;
		}

		return true;
	}

	bool Shader::CreateInputLayout( const std::string& file, ID3D10Blob& temp, const D3D11_INPUT_ELEMENT_DESC& desc, uint32_t elements )
	{
		auto& device = Engine::Instance().GetContext()->GetDevice();
		if( ( FAILED( device.CreateInputLayout( &desc, elements, temp.GetBufferPointer(), temp.GetBufferSize(), &mInputLayout ) ) ) )
		{
			Engine::Error( d3derr, "Failed to create input layout for shader %s", file.c_str() );
			return false;
		}

		return true;
	}

	void Shader::BindPerObjectBuffer( ID3D11DeviceContext& deviceContext, const std::shared_ptr<Entity>& entity, bool instanced )
	{
		cbPerObject objectBuf;
		objectBuf.mWorld = XMMatrixTranspose( entity->CalculateWorldMatrix() );

		deviceContext.UpdateSubresource( mPerObjectBuffer, 0, 0, &objectBuf, 0, 0 );
		deviceContext.VSSetConstantBuffers( 0, 1, &mPerObjectBuffer );
	}

	void Shader::BindPerObjectBuffer( ID3D11DeviceContext& deviceContext, const cbPerObject& buffer )
	{
		deviceContext.UpdateSubresource( mPerObjectBuffer, 0, 0, &buffer, 0, 0 );
		deviceContext.VSSetConstantBuffers( 0, 1, &mPerObjectBuffer );
	}

	void Shader::BindPerFrameBuffer( ID3D11DeviceContext& deviceContext, const std::shared_ptr<Camera>& camera )
	{
		auto light = Engine::Instance().GetCurrentWorld()->GetLight();
		cbPerFrameEx frameBuf;

		for( auto i = 0 ; i < SHADOW_CASCADES_COUNT ; ++ i )
		{
			const auto& view = light->GetViewMatrix( i );
			const auto& proj = light->GetProjectionMatrix( i );
			const auto& position = light->GetCascadePositions( i );

			frameBuf.mLightView[ i ] = XMMatrixTranspose( view );
			frameBuf.mLightProj[ i ] = XMMatrixTranspose( proj );
			frameBuf.mLightPosition[ i ] = position;
		}

		frameBuf.mView = XMMatrixTranspose( camera->GetViewMatrix() );
		frameBuf.mProj = XMMatrixTranspose( camera->GetProjectionMatrix() );
		frameBuf.mEyePosition = camera->GetPosition();

		deviceContext.UpdateSubresource( mPerFrameBuffer, 0, 0, &frameBuf, 0, 0 );
		deviceContext.VSSetConstantBuffers( SHADER_VS_Offset_FrameBuffer, 1, &mPerFrameBuffer );
	}

	void Shader::BindPerLightingBuffer( ID3D11DeviceContext& deviceContext )
	{
		auto light = Engine::Instance().GetCurrentWorld()->GetLight();
		if( !light  )
		{
			return;
		}

		cbPerLightingChange buf;
		buf.mDirection = light->GetRotation();
		buf.mDiffuseColour = light->GetDiffuseColour();
		buf.mAmbientColour = light->GetAmbientColour();

		deviceContext.UpdateSubresource( mPerLightingBuffer, 0, 0, &buf, 0, 0 );
		deviceContext.PSSetConstantBuffers( 0, 1, &mPerLightingBuffer );
	}

	DWORD Shader::GetShaderConstructionFlags()
	{
		DWORD flags = 0;
		flags |= D3D10_SHADER_ENABLE_STRICTNESS;

	#ifdef _DEBUG
		flags |= D3D10_SHADER_DEBUG;
		//flags |= D3D10_SHADER_SKIP_OPTIMIZATION;
	#endif

		return flags;
	}

	bool Shader::CreateTextureSampler( ID3D11SamplerState*& sampleStateOutput, D3D11_FILTER filter, D3D11_TEXTURE_ADDRESS_MODE mode, D3D11_COMPARISON_FUNC compFunc, float mipLoadBias, int maxAnisop )
	{
		auto& device = Engine::Instance().GetContext()->GetDevice();
		D3D11_SAMPLER_DESC desc {};

		desc.Filter	= filter;
		desc.AddressU = mode;
		desc.AddressV = mode;
		desc.AddressW = mode;
		desc.MipLODBias = mipLoadBias;
		desc.MaxAnisotropy = maxAnisop;
		desc.ComparisonFunc = compFunc;
		desc.BorderColor[ 0 ] = 0;
		desc.BorderColor[ 1 ] = 0;
		desc.BorderColor[ 2 ] = 0;
		desc.BorderColor[ 3 ] = 0;
		desc.MinLOD = 0;
		desc.MaxLOD = D3D11_FLOAT32_MAX;

		if( FAILED( device.CreateSamplerState( &desc, &sampleStateOutput ) ) )
		{
			Engine::Error( d3derr, "Failed to create sampler state" );
			return false;
		}

		return true;
	}
}
