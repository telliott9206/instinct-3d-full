/*
 * SimpleShader.h
 *
 * Created 28/10/2021
 * Written by Todd Elliott
 *
 * A shader that renders only simple vertices (those with only a position and colour)
 */

#ifndef _SIMPLESHADER_H

#include "Shader.h"

namespace Instinct
{
	class GeometryCell;

	class SimpleShader: public Shader
	{
	public:
		struct cbPerObject
		{
			XMMATRIX matWorld;
		};

		SimpleShader() = default;
		~SimpleShader();

		static std::unique_ptr<SimpleShader> Load();

		void Render( ID3D11DeviceContext& deviceContext, const std::shared_ptr<Entity>& entity ) override;
		void Render( ID3D11DeviceContext& deviceContext, const std::shared_ptr<GeometryCell>& cell );
		void BindPerObjectBuffer( ID3D11DeviceContext& deviceContext, const std::shared_ptr<Entity>& entity, bool instanced = false ) override;

	private:
	};
}

#endif
