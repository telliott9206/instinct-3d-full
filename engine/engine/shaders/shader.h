/*
 * Shader.h
 *
 * Created 08/12/2018
 * Written by Todd Elliott
 *
 * THe base class of all shaders that performs basic shading without normal mapped detail.
 */

#ifndef _SHADER_H
#define _SHADER_H

#include <Instinct.h>
#include <Engine.h>
#include <Engine/Model/Vertex.h>
#include <Engine/Model/Mesh.h>
#include <d3dcompiler.h>

#define SHADER_VS_Offset_FrameBuffer				1
#define SHADER_PS_Offset_Textures					0

#define TERRAINSHADER_PS_Offset_TerrainLayers		0
#define TERRAINSHADER_PS_Offset_ShadowCascades		1

#define TERRAINSHADER_PS_Offset_ShadowMaps			1
#define TERRAINSHADER_PS_Offset_TextureLayers		4

#define VERTEX_SHADER_VERSION						"vs_5_0"
#define PIXEL_SHADER_VERSION						"ps_5_0"

namespace Instinct
{
	class Context;
	class Model;
	class Camera;
	class Terrain;
	class Entity;
	class Ocean;
	struct ModelInstanceData;

	struct cbPerObject
	{
		XMMATRIX mWorld;
	};

	struct cbPerFrame
	{
		//TODO: check if this is even needed anymore?
		XMMATRIX mView;
		XMMATRIX mProj;
		//
		XMFLOAT3 mEyePosition;
		float _padding;
	};

	struct cbPerLightingChange
	{
		//TODO: remove this struct, move to perframe
		XMFLOAT3 mDirection;
		float _padding;
		//
		XMFLOAT4 mDiffuseColour;
		XMFLOAT4 mAmbientColour;
	};

	struct cbPerFrameEx
	{
		XMMATRIX mView;
		XMMATRIX mProj;
		//
		XMFLOAT3 mEyePosition;
		float _padding;
		// 
		XMMATRIX mLightView[ SHADOW_CASCADES_COUNT ];
		XMMATRIX mLightProj[ SHADOW_CASCADES_COUNT ];
		//
		XMFLOAT3 mLightPosition[ SHADOW_CASCADES_COUNT ];
		XMFLOAT3 _padding2;
	};

	class Shader
	{
	public:
		Shader() = default;
		virtual ~Shader();

		static std::unique_ptr<Shader> Load( const std::string& file, const std::string& vfunc, const std::string& pfunc, const D3D11_INPUT_ELEMENT_DESC& desc, uint32_t elements );
		virtual void Render( ID3D11DeviceContext& deviceContext, const std::shared_ptr<Entity>& entity );
		void RenderTerrain( ID3D11DeviceContext& deviceContext, const std::shared_ptr<Terrain>& terrain );
		void RenderOcean( ID3D11DeviceContext& deviceContext, const std::shared_ptr<Ocean>& terrain );

		virtual void Bind( ID3D11DeviceContext& deviceContext );

		void BindInstanceBuffer( ID3D11DeviceContext& deviceContext, const std::shared_ptr<Mesh<Vertex>>& modelEntry, ID3D11Buffer* instanceBuffer );
		virtual void RenderInstanced( ID3D11DeviceContext& deviceContext, uint32_t instanceCount, uint32_t vertexCount, uint32_t indexCount, uint32_t modelEntryIndex, 
			const std::shared_ptr<Entity>& baseEntity, int cascadeIndex );

	protected:
		virtual void BindShaders( ID3D11DeviceContext& deviceContext );
		virtual void BindTextureSamplers( ID3D11DeviceContext& deviceContext );
		virtual void SetInputLayout( ID3D11DeviceContext& deviceContext );

		virtual bool CreateTextureSamplers();
		virtual bool CreateInputLayout( const std::string& file, ID3D10Blob& temp, const D3D11_INPUT_ELEMENT_DESC& desc, uint32_t elements );
		
		virtual void BindPerObjectBuffer( ID3D11DeviceContext& deviceContext, const std::shared_ptr<Entity>& entity, bool instanced = false );
		virtual void BindPerObjectBuffer( ID3D11DeviceContext& deviceContext, const cbPerObject& buffer );
		virtual void BindPerFrameBuffer( ID3D11DeviceContext& deviceContext, const std::shared_ptr<Camera>& camera );
		virtual void BindPerLightingBuffer( ID3D11DeviceContext& deviceContext );

		static DWORD GetShaderConstructionFlags();
		static bool CreateTextureSampler( ID3D11SamplerState*& sampleStateOutput, D3D11_FILTER filter, D3D11_TEXTURE_ADDRESS_MODE mode, D3D11_COMPARISON_FUNC compFunc, float mipLoadBias, int maxAnisop );

	protected:
		ID3D11VertexShader*		mVertexShader;
		ID3D11PixelShader*		mPixelShader;
		ID3D11InputLayout*		mInputLayout;

		ID3D11Buffer*			mPerObjectBuffer;
		ID3D11Buffer*			mDebugBuffer;
		ID3D11Buffer*			mPerFrameBuffer;
		ID3D11Buffer*			mPerLightingBuffer;

		ID3D11SamplerState*		mSampleState;
		ID3D11SamplerState*		mSampleStateClamp;
		ID3D11SamplerState*		mSampleStateWrap;
	};
};

#endif
