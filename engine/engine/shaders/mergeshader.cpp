/*
 * Mergeshader.h
 *
 * Created 05/03/2019
 * Written by Todd Elliott
 */

#include "MergeShader.h"

#include <Engine.h>
#include <Engine/Texture/Texture2DArray.h>
#include <Engine/Utility/DirectXHelper.h>
#include <Engine/World.h>
#include <Engine/Camera.h>
#include <Engine/Light/DirectionalLight.h>
#include <Engine/Utility/Util.h>

namespace Instinct
{
	MergeShader::~MergeShader()
	{
		ReleaseCOM( mCameraBuffer );
	}

	std::unique_ptr<MergeShader> MergeShader::Load()
	{
		const auto input = Util::MultibyteToWideChar( std::string( Engine::GetResourceDirectory() + "Shaders/Merge.fx" ).c_str() );
		const auto flags = Shader::GetShaderConstructionFlags();
		ID3D10Blob* compilationMsgs = 0, *vs, *ps;

		//VERTEX SHADER
		D3DCompileFromFile( input, 0, D3D_COMPILE_STANDARD_FILE_INCLUDE, "VS", VERTEX_SHADER_VERSION, flags, 0, &vs, &compilationMsgs );
		if( compilationMsgs != 0 )
		{
			Engine::Error( d3derr, static_cast<char*>( compilationMsgs->GetBufferPointer() ) );
			ReleaseCOM( compilationMsgs );

		#ifndef _DEBUG
			return nullptr;
		#endif
		}

		//PIXEL SHADER
		D3DCompileFromFile( input, 0, D3D_COMPILE_STANDARD_FILE_INCLUDE, "PS", PIXEL_SHADER_VERSION, flags, 0, &ps, &compilationMsgs );
		if( compilationMsgs != 0 )
		{
			Engine::Error( d3derr, static_cast<char*>( compilationMsgs->GetBufferPointer() ) );
			ReleaseCOM( compilationMsgs );

		#ifndef _DEBUG
			return nullptr;
		#endif
		}

		auto shader = std::make_unique<MergeShader>();
		auto& device = Engine::Instance().GetContext()->GetDevice();

		//create the shaders
		device.CreateVertexShader( vs->GetBufferPointer(), vs->GetBufferSize(), nullptr, &shader->mVertexShader );
		device.CreatePixelShader( ps->GetBufferPointer(), ps->GetBufferSize(), nullptr, &shader->mPixelShader );
	
		//create the input layout
		D3D11_INPUT_ELEMENT_DESC layout[] =
		{
			{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
			{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 }
		};

		HRESULT hr = shader->CreateInputLayout( "Merge.fx", *vs, *layout, 2 );
		ReleaseCOM( vs );
		ReleaseCOM( ps );

		if( FAILED( hr ) ||
			!shader->CreateTextureSamplers() ||
			!DirectXHelper::CreateConstantBuffer<cbPerObject>( shader->mPerObjectBuffer, D3D11_USAGE_DEFAULT, D3D11_USAGE_DEFAULT ) ||
			!DirectXHelper::CreateConstantBuffer<cbPerFrame>( shader->mPerFrameBuffer, D3D11_USAGE_DEFAULT, D3D11_USAGE_DEFAULT ) ||
			!DirectXHelper::CreateConstantBuffer<cbPerLightingChange>( shader->mPerLightingBuffer, D3D11_USAGE_DEFAULT, D3D11_USAGE_DEFAULT ) ||
			!DirectXHelper::CreateConstantBuffer<cbCamera>( shader->mCameraBuffer, D3D11_USAGE_DEFAULT, D3D11_USAGE_DEFAULT ) )
		{
			return nullptr;
		}

	#ifdef _DEBUG
		Engine::Print( "Merge shader loaded" );
	#endif

		return std::move( shader );
	}

	void MergeShader::Render( ID3D11DeviceContext& deviceContext )
	{
		BindObjectBuffer( deviceContext );
		BindFrameBuffer( deviceContext );
		BindLightBuffer( deviceContext );
		BindGraphicBuffer( deviceContext );

		deviceContext.IASetInputLayout( mInputLayout );
		deviceContext.VSSetShader( mVertexShader, 0, 0 );
		deviceContext.HSSetShader( nullptr, 0, 0 );
		deviceContext.DSSetShader( nullptr, 0, 0 );
		deviceContext.PSSetShader( mPixelShader, 0, 0 );
		deviceContext.PSSetSamplers( 0, 1, &mSampleState );
		deviceContext.DrawIndexed( 6, 0, 0 );
	}

	bool MergeShader::CreateTextureSamplers()
	{
		return Shader::CreateTextureSampler( mSampleState, D3D11_FILTER_MIN_MAG_MIP_POINT, D3D11_TEXTURE_ADDRESS_CLAMP, D3D11_COMPARISON_ALWAYS, 0.f, 8 );
	}

	void MergeShader::BindObjectBuffer( ID3D11DeviceContext& deviceContext )
	{
		cbPerObject buf;
		buf.mWorld = XMMatrixTranspose( XMMatrixIdentity() );
		deviceContext.UpdateSubresource( mPerObjectBuffer, 0, 0, &buf, 0, 0 );
		deviceContext.VSSetConstantBuffers( 0, 1, &mPerObjectBuffer );
	}

	void MergeShader::BindFrameBuffer( ID3D11DeviceContext& deviceContext )
	{
		auto& system = Engine::Instance();
		auto camera = system.GetCurrentWorld()->GetActiveCamera();
		if( !camera )
		{
	#ifdef _DEBUG
			Engine::Print( "Warning: No camera present to render" );
	#endif

			return;
		}

		cbPerFrame buf2;
		const auto& ortho = system.GetContext()->GetRenderWindow()->GetOrthogonalMatrix();
		buf2.mView = XMMatrixTranspose( camera->GetBaseViewMatrix() );
		buf2.mProj = XMMatrixTranspose( ortho );
		deviceContext.UpdateSubresource( mPerFrameBuffer, 0, 0, &buf2, 0, 0 );
		deviceContext.VSSetConstantBuffers( 1, 1, &mPerFrameBuffer );
	}

	void MergeShader::BindLightBuffer( ID3D11DeviceContext& deviceContext )
	{
		auto& system = Engine::Instance();
		auto world = system.GetCurrentWorld();
		auto light = world->GetLight();

		cbPerLightingChange buf;
		buf.mDirection = light->GetRotation();
		buf.mAmbientColour = light->GetAmbientColour();
		buf.mDiffuseColour = light->GetDiffuseColour();
		deviceContext.UpdateSubresource( mPerLightingBuffer, 0, 0, &buf, 0, 0 );
		deviceContext.PSSetConstantBuffers( 0, 1, &mPerLightingBuffer );
	}

	void MergeShader::BindGraphicBuffer( ID3D11DeviceContext& deviceContext )
	{
		auto& system = Engine::Instance();
		auto context = system.GetContext();
		auto* buf = context->GetGraphicBuffer();
		ID3D11ShaderResourceView* r1 = buf->GetShaderResourceView( Context::tGBuffer::kDiffuse );
		ID3D11ShaderResourceView* r2 = buf->GetShaderResourceView( Context::tGBuffer::kNormal );
		ID3D11ShaderResourceView* r3 = buf->GetShaderResourceView( Context::tGBuffer::kTangent );
		ID3D11ShaderResourceView* r4 = buf->GetShaderResourceView( 3 );	//TODO: Fix this up ...names are all wrong .. needs to be redone

		deviceContext.PSSetShaderResources( 0, 1, &r1 );
		deviceContext.PSSetShaderResources( 1, 1, &r2 );
		deviceContext.PSSetShaderResources( 2, 1, &r3 );
		deviceContext.PSSetShaderResources( 3, 1, &r4 );
	}
}
