/*
 * skydomeshader.cpp
 *
 * Created 18/03/2019
 * Written by Todd Elliott
 */

#include "skydomeshader.h"
#include <Engine/Utility/DirectXHelper.h>
#include <Engine/Utility/Util.h>
#include <Engine/Entity/Entity.h>
#include <Engine/Model/Model.h>
#include <Engine/Skydome.h>
#include <Engine/World.h>
#include <Engine/Camera.h>

namespace Instinct
{
	SkydomeShader::~SkydomeShader()
	{
	}

	std::unique_ptr<SkydomeShader> SkydomeShader::Load()
	{
		const auto input = Util::MultibyteToWideChar( std::string( Engine::GetResourceDirectory() + "Shaders/Skydome.fx" ).c_str() );
		const auto flags = Shader::GetShaderConstructionFlags();
		ID3D10Blob* compilationMsgs = nullptr, *vs, *ps;

		//VERTEX SHADER
		D3DCompileFromFile( input, 0, D3D_COMPILE_STANDARD_FILE_INCLUDE, "VS", VERTEX_SHADER_VERSION, flags, 0, &vs, &compilationMsgs );
		if( compilationMsgs != 0 )
		{
			Engine::Error( d3derr, (char*)compilationMsgs->GetBufferPointer() );
			ReleaseCOM( compilationMsgs );

		#ifndef _DEBUG
			return nullptr;
		#endif
		}
	
		//PIXEL SHADER
		D3DCompileFromFile( input, 0, D3D_COMPILE_STANDARD_FILE_INCLUDE, "PS", PIXEL_SHADER_VERSION, flags, 0, &ps, &compilationMsgs );
		if( compilationMsgs != 0 )
		{
			Engine::Error( d3derr, (char*)compilationMsgs->GetBufferPointer() );
			ReleaseCOM( compilationMsgs );

		#ifndef _DEBUG
			return nullptr;
		#endif
		}

		auto shader = std::make_unique<SkydomeShader>();
		auto& device = Engine::Instance().GetContext()->GetDevice();

		device.CreateVertexShader( vs->GetBufferPointer(), vs->GetBufferSize(), nullptr, &shader->mVertexShader );
		device.CreatePixelShader( ps->GetBufferPointer(), ps->GetBufferSize(), nullptr, &shader->mPixelShader );
	
		D3D11_INPUT_ELEMENT_DESC layout[] =
		{
			{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 }
		};

		const auto hr = shader->CreateInputLayout( "Skydome.fx", *vs, *layout, 1 );
		ReleaseCOM( vs );
		ReleaseCOM( ps );

		if( FAILED( hr ) ||
			!DirectXHelper::CreateConstantBuffer<cbPerObject>( shader->mPerObjectBuffer, D3D11_USAGE_DEFAULT, D3D11_USAGE_DEFAULT ) ||
			!DirectXHelper::CreateConstantBuffer<cbPerFrame>( shader->mPerFrameBuffer, D3D11_USAGE_DEFAULT, D3D11_USAGE_DEFAULT ) ||
			!DirectXHelper::CreateConstantBuffer<cbSkyColour>( shader->mSkyColourBuffer, D3D11_USAGE_DEFAULT, D3D11_USAGE_DEFAULT ) )
		{
			return nullptr;
		}
	
	#ifdef _DEBUG
		Engine::Print( "Skydome shader loaded" );
	#endif

		return std::move( shader );
	}

	void SkydomeShader::Render( ID3D11DeviceContext& deviceContext, const std::shared_ptr<Skydome>& entity )
	{
		auto model = entity->GetModel();
		BindPerObjectBuffer( deviceContext, entity );

		auto camera = Engine::Instance().GetCurrentWorld()->GetActiveCamera();
		BindPerFrameBuffer( deviceContext, camera );
		BindSkyColourBuffer( deviceContext );

		deviceContext.IASetInputLayout( mInputLayout );

		deviceContext.VSSetShader( mVertexShader, NULL, 0 );
		deviceContext.HSSetShader( nullptr, 0, 0 );
		deviceContext.DSSetShader( nullptr, 0, 0 );
		deviceContext.PSSetShader( mPixelShader, NULL, 0 );
	
		model->Render( deviceContext );
	}

	void SkydomeShader::BindSkyColourBuffer( ID3D11DeviceContext& deviceContext )
	{
		auto skydome = Engine::Instance().GetCurrentWorld()->GetSkydome();

		cbSkyColour buf;
		buf.apexColour = skydome->GetApexColour();
		buf.centreColour = skydome->GetCentreColour();

		deviceContext.UpdateSubresource( mSkyColourBuffer, 0, 0, &buf, 0, 0 );
		deviceContext.PSSetConstantBuffers( 0, 1, &mSkyColourBuffer );
	}
}
