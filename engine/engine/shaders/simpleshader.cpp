/*
 * SimpleShader.cpp
 *
 * Created 28/10/2021
 * Written by Todd Elliott
 */

#include "SimpleShader.h"
#include <Engine/Utility/Util.h>
#include <Engine/Entity/Entity.h>
#include <Engine/Terrain/GeometryCell.h>
#include <Engine/Model/Model.h>
#include <Engine/Model/VisualBoundingBox.h>
#include <Engine/Camera.h>
#include <Engine/World.h>
#include <Engine/Utility/DirectXHelper.h>
#include <Engine.h>

namespace Instinct
{
	SimpleShader::~SimpleShader()
	{
	}

	std::unique_ptr<SimpleShader> SimpleShader::Load()
	{
		const auto input = Util::MultibyteToWideChar( std::string( Engine::GetResourceDirectory() + "Shaders/Simple.fx" ).c_str() );
		const auto flags = Shader::GetShaderConstructionFlags();
		ID3D10Blob* compilationMsgs = nullptr, *vs, *ps;

		//VERTEX SHADER
		D3DCompileFromFile( input, 0, D3D_COMPILE_STANDARD_FILE_INCLUDE, "VS", VERTEX_SHADER_VERSION, flags, 0, &vs, &compilationMsgs );
		if( compilationMsgs != 0 )
		{
			Engine::Error( d3derr, (char*)compilationMsgs->GetBufferPointer() );
			ReleaseCOM( compilationMsgs );

		#ifndef _DEBUG
			return nullptr;
		#endif
		}

		//PIXEL SHADER
		D3DCompileFromFile( input, 0, D3D_COMPILE_STANDARD_FILE_INCLUDE, "PS", PIXEL_SHADER_VERSION, flags, 0, &ps, &compilationMsgs );
		if( compilationMsgs != 0 )
		{
			Engine::Error( d3derr, (char*)compilationMsgs->GetBufferPointer() );
			ReleaseCOM( compilationMsgs );

		#ifndef _DEBUG
			return nullptr;
		#endif
		}

		auto shader = std::make_unique<SimpleShader>();
		auto context = Engine::Instance().GetContext();
		auto& device = context->GetDevice();

		//create the shaders
		device.CreateVertexShader( vs->GetBufferPointer(), vs->GetBufferSize(), nullptr, &shader->mVertexShader );
		device.CreatePixelShader( ps->GetBufferPointer(), ps->GetBufferSize(), nullptr, &shader->mPixelShader );

		//create the input layout
		D3D11_INPUT_ELEMENT_DESC layout[] =
		{
			{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
			{ "TEXCOORD", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 }
		};

		const bool hr = shader->CreateInputLayout( "simple.vs", *vs, *layout, 2 );
		ReleaseCOM( vs );
		ReleaseCOM( ps );

		if( !hr || 
			!DirectXHelper::CreateConstantBuffer<cbPerObject>( shader->mPerObjectBuffer, D3D11_USAGE_DEFAULT, D3D11_USAGE_DEFAULT ) || 
			!DirectXHelper::CreateConstantBuffer<cbPerFrameEx>( shader->mPerFrameBuffer, D3D11_USAGE_DEFAULT, D3D11_USAGE_DEFAULT ) )
		{
			return nullptr;
		}

	#ifdef _DEBUG
		Engine::Print( "Simple shader loaded" );
	#endif

		return std::move( shader );
	}

	void SimpleShader::Render( ID3D11DeviceContext& deviceContext, const std::shared_ptr<Entity>& entity )
	{
		auto camera = Engine::Instance().GetCurrentWorld()->GetActiveCamera();
		BindPerObjectBuffer( deviceContext, entity );
		BindPerFrameBuffer( deviceContext, camera );

		const auto model = entity->GetModel();
		auto* boundingBox = model->GetVisualBoundingBox();
		boundingBox->Render( deviceContext );
	}

	void SimpleShader::Render( ID3D11DeviceContext& deviceContext, const std::shared_ptr<GeometryCell>& cell )
	{
		auto camera = Engine::Instance().GetCurrentWorld()->GetActiveCamera();

		cbPerObject buf;
		//buf.matWorld = XMMatrixTranspose( cell->GetPosition() );
		const auto& position = cell->GetPosition();
		buf.matWorld = XMMatrixTranspose( XMMatrixIdentity() * XMMatrixTranslation( position.x, position.y, position.z ) );

		deviceContext.UpdateSubresource( mPerObjectBuffer, 0, 0, &buf, 0, 0 );
		deviceContext.VSSetConstantBuffers( 0, 1, &mPerObjectBuffer );

		//BindPerObjectBuffer( pDeviceContext, pEntity );
		BindPerFrameBuffer( deviceContext, camera );

		auto* boundingBox = cell->GetVisualBoundingBox();
		boundingBox->Render( deviceContext );
	}

	void SimpleShader::BindPerObjectBuffer( ID3D11DeviceContext& deviceContext, const std::shared_ptr<Entity>& entity, bool instanced )
	{
		cbPerObject objectBuf;
		objectBuf.matWorld = XMMatrixTranspose( entity->CalculateWorldMatrix() );

		deviceContext.UpdateSubresource( mPerObjectBuffer, 0, 0, &objectBuf, 0, 0 );
		deviceContext.VSSetConstantBuffers( 0, 1, &mPerObjectBuffer );
	}
}
