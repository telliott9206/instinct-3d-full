/*
 * DetailShader.cpp
 *
 * Created 19/12/2018
 * Written by Todd Elliott
 */

#include <Engine/Shaders/DetailShader.h>
#include <Engine/Camera.h>
#include <Engine/World.h>
#include <Engine/Model/Model.h>
#include <Engine/Utility/Util.h>
#include <Engine/Utility/DirectXHelper.h>
#include <Engine/Texture/Texture.h>
#include <Engine/Entity/Entity.h>
#include <Engine/Ocean.h>
#include <Engine/Terrain/GeometryCell.h>
#include <Engine/Light/DirectionalLight.h>
#include <Engine/Terrain/Terrain.h>
#include <Engine/Terrain/TerrainTextureLayer.h>
#include <Engine.h>

namespace Instinct
{
	DetailShader::~DetailShader()
	{
		ReleaseCOM( mTerrainLayerBuffer );
	}

	std::unique_ptr<DetailShader> DetailShader::Load()
	{
		const auto input = Util::MultibyteToWideChar( std::string( Engine::GetResourceDirectory() + "Shaders/Terrain.fx" ).c_str() );
		const auto flags = Shader::GetShaderConstructionFlags();
		ID3D10Blob* compilationMsgs = nullptr, *vs, *ps;

		//VERTEX SHADER
		D3DCompileFromFile( input, 0, D3D_COMPILE_STANDARD_FILE_INCLUDE, "VS", VERTEX_SHADER_VERSION, flags, 0, &vs, &compilationMsgs );
		if( compilationMsgs != 0 )
		{
			Engine::Error( d3derr, (char*)compilationMsgs->GetBufferPointer() );
			ReleaseCOM( compilationMsgs );

		#ifndef _DEBUG
			return nullptr;
		#endif
		}

		//PIXEL SHADER
		D3DCompileFromFile( input, 0, D3D_COMPILE_STANDARD_FILE_INCLUDE, "PS", PIXEL_SHADER_VERSION, flags, 0, &ps, &compilationMsgs );
		if( compilationMsgs != 0 )
		{
			Engine::Error( d3derr, (char*)compilationMsgs->GetBufferPointer() );
			ReleaseCOM( compilationMsgs );

		#ifndef _DEBUG
			return nullptr;
		#endif
		}

		auto shader = std::make_unique<DetailShader>();
		auto context = Engine::Instance().GetContext();
		auto& device = context->GetDevice();
		auto& deviceContext = context->GetDeviceContext();

		//create the shaders
		device.CreateVertexShader( vs->GetBufferPointer(), vs->GetBufferSize(), nullptr, &shader->mVertexShader );
		device.CreatePixelShader( ps->GetBufferPointer(), ps->GetBufferSize(), nullptr, &shader->mPixelShader );
	
		//create the input layout
		D3D11_INPUT_ELEMENT_DESC layout[] =
		{
			{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
			{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
			{ "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 }
		};

		const bool hr = shader->CreateInputLayout( "terrain.vs", *vs, *layout, 3 );
		ReleaseCOM( vs );
		ReleaseCOM( ps );

		if( !hr ||
			!DirectXHelper::CreateConstantBuffer<cbPerObject>( shader->mPerObjectBuffer, D3D11_USAGE_DEFAULT, D3D11_USAGE_DEFAULT, "Per Object Buffer" ) ||
			!DirectXHelper::CreateConstantBuffer<cbPerFrameEx>( shader->mPerFrameBuffer, D3D11_USAGE_DEFAULT, D3D11_USAGE_DEFAULT, "Per Frame Buffer" ) ||
			!DirectXHelper::CreateConstantBuffer<cbPerLightingChange>( shader->mPerLightingBuffer, D3D11_USAGE_DEFAULT, D3D11_USAGE_DEFAULT, "Lighting Buffer" )  ||
			!DirectXHelper::CreateConstantBuffer<cbTerrainLayers>( shader->mTerrainLayerBuffer, D3D11_USAGE_DYNAMIC, D3D11_CPU_ACCESS_WRITE, "Terrain Layer Buffer" )  ||
			!shader->CreateTextureSamplers()
		)
		{
			return nullptr;
		}
	
	#ifdef _DEBUG
		Engine::Print( "Displacement shader loaded" );
	#endif

		return std::move( shader );
	}

	void DetailShader::RenderGeometry( ID3D11DeviceContext& deviceContext, const std::shared_ptr<Terrain>& terrain, const std::shared_ptr<Camera>& camera, 
		std::vector<std::shared_ptr<GeometryCell>>& geometryCells )
	{
		if( !mTerrainLayerBuffer )
		{
			Engine::Print( "Warning: Terrain not rendered because Terrain Layer Buffer not created yet!" );
			return;
		}

		auto directionalLight = Engine::Instance().GetCurrentWorld()->GetLight();
		if( !directionalLight )
		{
			Engine::Print( "Warning: Terrain not rendered because no Directional Light has been created yet!" );
			return;
		}

		cbPerObject buf;
		//buf.mWorld = XMMatrixTranspose(  );
		buf.mWorld = XMMatrixTranspose( XMMatrixIdentity() * XMMatrixTranslation( 0.f, 0.f, 0.f ) );

		BindPerObjectBuffer( deviceContext, buf );
		BindPerFrameBuffer( deviceContext, camera );
		BindPerLightingBuffer( deviceContext );
		BindTerrainLayerBuffer( deviceContext, terrain );

		directionalLight->BindShadowCascadesBuffer( deviceContext );

		const auto& cellList = geometryCells;
		for( const auto& cell : cellList )
		{
			cell->RenderTerrainCell( deviceContext );
		}
	}

	void DetailShader::UpdateTerrainLayerBuffer( ID3D11DeviceContext& deviceContext, const std::vector<TerrainTextureLayer>& layers )
	{
		cbTerrainLayers buf;
		buf.mTerrainLayerCount = layers.size();

		for( auto i = 0 ; i < buf.mTerrainLayerCount ; ++ i )
		{
			buf.mTerrainTextureLayers[ i ] = layers[ i ].mOptions;
		}

		DirectXHelper::CopyToBuffer<cbTerrainLayers>( deviceContext, mTerrainLayerBuffer, &buf );
	}

	void DetailShader::BindTerrainLayerBuffer( ID3D11DeviceContext& deviceContext, const std::shared_ptr<Terrain>& terrain )
	{
		deviceContext.PSSetConstantBuffers( TERRAINSHADER_PS_Offset_TerrainLayers, 1, &mTerrainLayerBuffer );

		if( terrain->GetTextureLayerCount() == 0 )
		{
			return;
		}

		ID3D11ShaderResourceView** resource = terrain->GetTextureArray()->GetShaderResourceView();
		deviceContext.PSSetShaderResources( TERRAINSHADER_PS_Offset_TextureLayers, 1, resource );
	}
}
