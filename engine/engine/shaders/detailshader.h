/*
 * detailshader.h
 *
 * Created 11/09/2020
 * Written by Todd Elliott
 *
 * A shader used for rendering normal mapped detailed to geometry (TODO: normal mapping still to be done).
 */

#ifndef _DETAILSHADER_H
#define _DETAILSHADER_H

#include "Shader.h"
#include <Engine/Terrain/TerrainTextureLayer.h>

namespace Instinct
{
	class Model;
	class Terrain;
	class Entity;
	class Ocean;
	class Camera;
	class GeometryCell;

	struct TerrainLayer;
	struct RenderPackage;

	class DetailShader : public Shader
	{
	public:
		struct cbPerFrameEx
		{
			XMMATRIX mView;										//16	
			XMMATRIX mProj;										//16

			XMFLOAT3 mEyePosition;								//12
			float _padding;										//4		= 16

			XMMATRIX mLightView[ SHADOW_CASCADES_COUNT ];		//48
			XMMATRIX mLightProj[ SHADOW_CASCADES_COUNT ];		//48

			XMFLOAT3 vLightPosition[ SHADOW_CASCADES_COUNT ];	//36
			XMFLOAT3 _padding2;									//12	= 48
		};

		struct cbTerrainLayers
		{	
			int mTerrainLayerCount;
			XMFLOAT3 _padding;

			cbTerrainTextureLayerOptions mTerrainTextureLayers[ 3 ];
		};

	public:
		DetailShader() = default;
		virtual ~DetailShader();

		static std::unique_ptr<DetailShader> Load();

		virtual void RenderGeometry( ID3D11DeviceContext& deviceContext, const std::shared_ptr<Terrain>& terrain, const std::shared_ptr<Camera>& camera, 
			std::vector<std::shared_ptr<GeometryCell>>& geometryCells );
		void UpdateTerrainLayerBuffer( ID3D11DeviceContext& deviceContext, const std::vector<TerrainTextureLayer>& layers );

	private:
		void BindTerrainLayerBuffer( ID3D11DeviceContext& deviceContext, const std::shared_ptr<Terrain>& terrain );

	private:
		ID3D11Buffer* mTerrainLayerBuffer;
	};
};

#endif
