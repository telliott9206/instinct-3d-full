/*
 * MergeShader.h
 *
 * Created 05/03/2019
 * Written by Todd Elliott
 *
 * A shader merges each render texture of the GBuffer together to create the final output
 * that is rendered to the back buffer.
 */

#ifndef _MERGE_SHADER_H
#define _MERGE_SHADER_H

#include "Shader.h"

namespace Instinct
{
	class MergeShader: public Shader
	{
	public:
		struct DirectionalLightBuffer
		{
			XMFLOAT3	mLightDirection;
			float		_padding;
			XMFLOAT4	mDiffuseColour;
			XMFLOAT4	mAmbientColour;
		};

		struct cbCamera
		{
			XMFLOAT3	mEyeDirection;
			float		_padding;
		};

	public:
		MergeShader() = default;
		~MergeShader();

		static std::unique_ptr<MergeShader> Load();

		void Render( ID3D11DeviceContext& deviceContext );

	protected:
		bool CreateTextureSamplers() override;

	private:
		void BindObjectBuffer( ID3D11DeviceContext& deviceContext );
		void BindFrameBuffer( ID3D11DeviceContext& deviceContext );
		void BindLightBuffer( ID3D11DeviceContext& deviceContext );
		void BindGraphicBuffer( ID3D11DeviceContext& deviceContext );

	private:
		ID3D11Buffer* mCameraBuffer;
	};
};

#endif
