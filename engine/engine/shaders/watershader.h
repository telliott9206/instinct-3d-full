/*
 * WaterShader.h
 *
 * Created 21/03/2019
 * Written by Todd Elliott
 *
 * A shader that is not much different from the Detail Shader.
 * This may be returned to its original state in a previous iteration it used tessellation.
 */

#ifndef _WATERSHADER_H
#define _WATERSHADER_H

#include "DetailShader.h"

namespace Instinct
{
	class Model;
	class Terrain;
	class Entity;
	class Ocean;
	class Camera;
	struct RenderPackage;

	class WaterShader : public DetailShader
	{
	public:
		WaterShader() = default;
		virtual ~WaterShader();

		static std::unique_ptr<WaterShader> Load();

		void RenderGeometry( ID3D11DeviceContext& deviceContext, const std::shared_ptr<Ocean>& ocean, const std::shared_ptr<Camera>& camera, 
			const std::vector<std::shared_ptr<GeometryCell>>& geometryCells );

	private:
		void BindTextures( ID3D11DeviceContext& deviceContext, const std::shared_ptr<Ocean>& ocean ) const;
	};
};

#endif
