/*
 * DepthShader.h
 *
 * Created 03/04/2019
 * Written by Todd Elliott
 */

#include <Engine/Shaders/DepthShader.h>
#include <Engine/Terrain/GeometryCell.h>
#include <Engine/Camera.h>
#include <Engine/World.h>
#include <Engine/Model/Model.h>
#include <Engine/Utility/Util.h>
#include <Engine/Utility/DirectXHelper.h>
#include <Engine/Texture/Texture.h>
#include <Engine/Terrain/Terrain.h>
#include <Engine/Light/DirectionalLight.h>
#include <Engine/Entity/Entity.h>
#include <Engine.h>

namespace Instinct
{
	DepthShader::~DepthShader()
	{
		ReleaseCOM( mDepthBuffer );
	}

	std::unique_ptr<DepthShader> DepthShader::Load()
	{
		const auto input = Util::MultibyteToWideChar( std::string( Engine::GetResourceDirectory() + "Shaders/Depth.fx" ).c_str() );
		const auto flags = Shader::GetShaderConstructionFlags();
		ID3D10Blob* compilationMsgs = nullptr, *vs, *ps;

		//VERTEX SHADER
		D3DCompileFromFile( input, 0, D3D_COMPILE_STANDARD_FILE_INCLUDE, "VS", VERTEX_SHADER_VERSION, flags, 0, &vs, &compilationMsgs );
		if( compilationMsgs != 0 )
		{
			Engine::Error( d3derr, static_cast<char*>( compilationMsgs->GetBufferPointer() ) );
			ReleaseCOM( compilationMsgs );

			return nullptr;
		}

		//PIXEL SHADER
		D3DCompileFromFile( input, 0, D3D_COMPILE_STANDARD_FILE_INCLUDE, "PS", PIXEL_SHADER_VERSION, flags, 0, &ps, &compilationMsgs );
		if( compilationMsgs != 0 )
		{
			Engine::Error( d3derr, static_cast<char*>( compilationMsgs->GetBufferPointer() ) );
			ReleaseCOM( compilationMsgs );

			return nullptr;
		}

		auto shader = std::make_unique<DepthShader>();
		auto& pDevice = Engine::Instance().GetContext()->GetDevice();

		//create the shaders
		pDevice.CreateVertexShader( vs->GetBufferPointer(), vs->GetBufferSize(), nullptr, &shader->mVertexShader );
		pDevice.CreatePixelShader( ps->GetBufferPointer(), ps->GetBufferSize(), nullptr, &shader->mPixelShader );
	
		//create the input layout
		D3D11_INPUT_ELEMENT_DESC layout[] =
		{
			{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
			{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
			{ "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },

			{ "TEXCOORD", 1, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, 0, D3D11_INPUT_PER_INSTANCE_DATA, 1 },
			{ "TEXCOORD", 2, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_INSTANCE_DATA, 1 },
			{ "TEXCOORD", 3, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_INSTANCE_DATA, 1 },
			{ "TEXCOORD", 4, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_INSTANCE_DATA, 1 },

			{ "TEXCOORD", 5, DXGI_FORMAT_R32_FLOAT, 1, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_INSTANCE_DATA, 1 },
			{ "TEXCOORD", 6, DXGI_FORMAT_R32_FLOAT, 1, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_INSTANCE_DATA, 1 },
			{ "TEXCOORD", 7, DXGI_FORMAT_R32_FLOAT, 1, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_INSTANCE_DATA, 1 },
			{ "TEXCOORD", 8, DXGI_FORMAT_R32_UINT, 1, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_INSTANCE_DATA, 1 },
			{ "TEXCOORD", 9, DXGI_FORMAT_R32_UINT, 1, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_INSTANCE_DATA, 1 }
		};

		const bool hr = shader->CreateInputLayout( "Depth.fx", *vs, *layout, sizeof( layout ) / sizeof( layout[ 0 ] ) );
		ReleaseCOM( vs );
		ReleaseCOM( ps );

		if( !hr )
		{
			return nullptr;
		}

		if( !DirectXHelper::CreateConstantBuffer<cbPerObject>( shader->mPerObjectBuffer, D3D11_USAGE_DEFAULT, D3D11_USAGE_DEFAULT ) ||
			!DirectXHelper::CreateConstantBuffer<cbDepthBuffer>( shader->mDepthBuffer, D3D11_USAGE_DYNAMIC, D3D11_CPU_ACCESS_WRITE ) ||
			!shader->CreateTextureSamplers() )
		{
			return nullptr;
		}
	
	#ifdef _DEBUG
		Engine::Print( "Depth shader loaded" );
	#endif

		return std::move( shader );
	}

	void DepthShader::RenderTerrain( ID3D11DeviceContext& deviceContext, const std::shared_ptr<Terrain>& terrain, const std::shared_ptr<Camera>& camera, 
		const std::vector<std::shared_ptr<GeometryCell>>& cellList, int cascadeIndex )
	{
		cbPerObject buf;
		buf.mWorld = XMMatrixTranspose( XMMatrixTranslation( 0.f, 0.f, 0.f ) );

		BindPerObjectBuffer( deviceContext, buf );
		BindDepthBuffer( deviceContext, cascadeIndex, true );

		for( auto& cell : cellList )
		{
			cell->Render( deviceContext );
		}
	}

	void DepthShader::RenderInstanced( ID3D11DeviceContext& deviceContext, uint32_t instanceCount, uint32_t vertexCount, uint32_t indexCount, uint32_t modelEntryIndex, 
		const std::shared_ptr<Entity>& baseEntity, int cascadeIndex )
	{
		auto camera = Engine::Instance().GetCurrentWorld()->GetActiveCamera();
		if( !camera )
		{
	#ifdef _DEBUG
			Engine::Print( "Warning: No camera present to render" );
	#endif
			return;
		}

		Bind( deviceContext );

		//cbPerObject buf;
		//buf.mWorld = XMMatrixTranspose( XMMatrixTranslation( 0.f, 0.f, 0.f ) );
		//BindPerObjectBuffer( deviceContext, &buf );

		BindDepthBuffer( deviceContext, cascadeIndex );

		const auto model = baseEntity->GetModel();
		const auto models = model->GetEntryList();
		if( models.empty() )
		{
			return;
		}

		deviceContext.DrawIndexedInstanced( indexCount, instanceCount, 0, 0, 0 );
	}

	bool DepthShader::CreateTextureSamplers()
	{
		return Shader::CreateTextureSampler( mSampleState, D3D11_FILTER_MIN_MAG_MIP_POINT, D3D11_TEXTURE_ADDRESS_WRAP, D3D11_COMPARISON_ALWAYS, 0.f, 8 );
	}

	void DepthShader::BindDepthBuffer( ID3D11DeviceContext& deviceContext, int cascadeIndex, bool isTerrain )
	{
		auto light = Engine::Instance().GetCurrentWorld()->GetLight();
		cbDepthBuffer buf;

		const auto& view = light->GetViewMatrix( cascadeIndex );
		const auto& proj = light->GetProjectionMatrix( cascadeIndex );

		buf.matView = XMMatrixTranspose( view );
		buf.matProj = XMMatrixTranspose( proj );
		buf.mIsTerrain = isTerrain;

		DirectXHelper::CopyToBuffer<cbDepthBuffer>( deviceContext, mDepthBuffer, &buf );
		deviceContext.VSSetConstantBuffers( 1, 1, &mDepthBuffer );
	}

	void DepthShader::Render( ID3D11DeviceContext& deviceContext, const std::shared_ptr<Entity>& entity, int cascadeIndex )
	{
		const auto model = entity->GetModel();

		BindPerObjectBuffer( deviceContext, entity );
		BindDepthBuffer( deviceContext, cascadeIndex );

		deviceContext.IASetInputLayout( mInputLayout );
		deviceContext.VSSetShader( mVertexShader, 0, 0 );
		deviceContext.PSSetShader( mPixelShader, 0, 0 );
		deviceContext.IASetPrimitiveTopology( D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST );

		model->Render( deviceContext );
	}
}
