/*
 * depthshader.h
 *
 * Created 03/04/2019
 * Written by Todd Elliott
 *
 * A shader used for rendering the depth buffer.
 */

#ifndef _DEPTHSHADER_H
#define _DEPTHSHADER_H

#include <Instinct.h>
#include "Shader.h"

namespace Instinct
{
	class Camera;
	class GeometryCell;
	struct RenderPackage;

	class DepthShader: public Shader
	{
	public:
		struct cbDepthBuffer
		{
			XMMATRIX matView;
			XMMATRIX matProj;
			//
			bool mIsTerrain;
			XMFLOAT3 _padding;
		};

	public:
		DepthShader() = default;
		virtual ~DepthShader();

		static std::unique_ptr<DepthShader> Load();

		void RenderTerrain( ID3D11DeviceContext& deviceContext, const std::shared_ptr<Terrain>& terrain, const std::shared_ptr<Camera>& camera, 
			const std::vector<std::shared_ptr<GeometryCell>>& cellList, int iascadeIndex );
		void RenderInstanced( ID3D11DeviceContext& deviceContext, uint32_t instanceCount, uint32_t vertexCount, uint32_t indexCount, uint32_t modelEntryIndex, 
			const std::shared_ptr<Entity>& baseEntity, int cascadeIndex ) override;

		virtual void Render( ID3D11DeviceContext& deviceContext, const std::shared_ptr<Entity>& entity, int cascadeIndex );

	private:
		bool CreateTextureSamplers() override;
		void BindDepthBuffer( ID3D11DeviceContext& deviceContext, int cascadeIndex, bool isTerrain = false );

	private:
		ID3D11Buffer*		mDepthBuffer;

		ID3D11SamplerState* mSamplerClamp;
		ID3D11SamplerState* mSamplerWrap;
	};
};

#endif
