/*
 * GameTime.h
 *
 * Created 04/03/2019
 * Written by Todd Elliott
 *
 * Used for keeping track of game time.
 */

#ifndef _TIME_H
#define _TIME_H

#include <Instinct.h>

namespace Instinct
{
	class Time
	{
	public:
		DLLEXPORT static Time& Instance()
		{
			static Time inst;
			return inst;
		}

	private:
		Time();
		~Time() = default;

	public:
		float GetGameTime() const;	//not implemented
		float GetDeltaTime() const; //in seconds

		void Reset();
		void Start();	//not implemented
		void Stop();	//not implemented
		void Tick();

		void CalculateFrameStats();

	private:
		double	mSecondsPerCount;
		double	mDeltaTime;

		__int64 mBaseTime;
		__int64 mPausedTime;
		__int64 mStopTime;
		__int64 mPrevTime;
		__int64 mCurrentTime;

		bool	mStopped;

		float	mFramesPerSecond;
	};
};

#endif
