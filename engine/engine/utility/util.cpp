/*
 * Util.cpp
 *
 * Created 07/12/2018
 * Written by Todd Elliott
 */

#include "Util.h"
#include <iostream>
#include <sstream>

namespace Instinct
{
    const wchar_t* Util::MultibyteToWideChar( const char* input )
    {
	    const size_t cSize = strlen( input ) + 1;
        wchar_t* wc = new wchar_t[ cSize ];
        mbstowcs( wc, input, cSize );

        return wc;
    }

    LPCWSTR Util::StdStringToWindowsLPString( const std::string& input )
    {
        const auto str = std::wstring( input.begin(), input.end() );
        return str.c_str();
    }

    void Util::GetFileExtension( const std::string& file, std::string& output )
    {
        output = file.substr( file.find_last_of( "." ) + 1 );
    }

    std::vector<std::string> Util::StringSplit( std::string input, char delim )
    {
        std::stringstream ss( input );
        std::string item;
        std::vector<std::string> splittedStrings;

        while( getline( ss, item, delim ) )
        {
           splittedStrings.push_back( item );
        }

	    return splittedStrings;
    }

    std::string Util::StringTrim( std::string& input )
    {
	    /*
	    input.erase( input.begin(), std::find_if( input.begin(), input.end(),
                std::not1( std::ptr_fun<int, int>( std::isspace ) ) ) );
	    */

	    return input;
    }
}
