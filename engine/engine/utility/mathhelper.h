/*
 * MathHelper.h
 *
 * Created 10/05/2019
 * Written by Todd Elliott
 *
 * Provides a selection of functions for dealing with vectors and other 
 * 3 dimensional calculations.
 */

#ifndef _MATH_HELPER_H
#define _MATH_HELPER_H

#include <Instinct.h>

namespace Instinct
{
	class Camera;

	class MathHelper
	{
	public:
		MathHelper() = delete;
		MathHelper( const MathHelper& ) = delete;
		MathHelper& operator = ( const MathHelper& ) = delete;
		~MathHelper() = delete;

		DLLEXPORT static XMFLOAT3 WorldToProjection( const XMFLOAT3& input, const XMMATRIX& view, const XMMATRIX& proj );
		DLLEXPORT static XMFLOAT2 WorldToScreen( const XMFLOAT3& input, const XMMATRIX& view, const XMMATRIX& proj );

		DLLEXPORT static float PointDistance2D( const XMFLOAT2& point, const XMFLOAT2& target );
		DLLEXPORT static float PointDistance3D( const XMFLOAT3& point, const XMFLOAT3& target );
		
		DLLEXPORT static float VectorLength3D( const XMFLOAT3& input );

		DLLEXPORT static void VectorNormalize3D( XMFLOAT3& input );

		DLLEXPORT static XMFLOAT2 AngleToVector( float angle );
		DLLEXPORT static float DotProduct( const XMFLOAT2& p, const XMFLOAT2& q );
		DLLEXPORT static XMFLOAT3 CrossProduct( const XMFLOAT3& vec1, const XMFLOAT3& vec2, const XMFLOAT3& vec3 );

		DLLEXPORT static XMFLOAT3 AddVector3D( const XMFLOAT3& vec1, const XMFLOAT3& vec2 );
		DLLEXPORT static XMFLOAT3 DivideVector3D( const XMFLOAT3& vec1, float diviser );
	};
};

#endif
