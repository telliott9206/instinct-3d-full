/*
 * DirectXHelper.cpp
 *
 * Created 20/12/2018
 * Written by Todd Elliott
 */

#include "DirectXHelper.h"
#include <Engine/Utility/Util.h>
#include <Engine/Model/Vertex.h>
#include <Engine/Model/Model.h>
#include <Engine/Entity/Entity.h>

namespace Instinct
{
	XMMATRIX DirectXHelper::GenerateViewMatrixLookAt( const XMFLOAT3& viewPosition, const XMFLOAT3& lookAt )
	{
		const auto up = XMFLOAT3( 0.f, 1.f, 0.f );
		const auto p = XMVECTOR( XMLoadFloat3( &viewPosition ) );
		const auto q = XMVECTOR( XMLoadFloat3( &lookAt ) );
		const auto r = XMVECTOR( XMLoadFloat3( &up ) );

		return XMMatrixLookAtLH( p, q, r );
	}

	XMMATRIX DirectXHelper::GenerateViewMatrixLookDir( const XMFLOAT3& viewPosition, const XMFLOAT3& lookDir )
	{
		const auto pos	= XMVectorSet( viewPosition.x, viewPosition.y, viewPosition.z, 0.f );
		const auto up	= XMVectorSet( 0.f, 1.f, 0.f, 0.f );

		const auto r	= lookDir.y * RADIAN;
		const auto dir	= XMVectorSet( sinf( r ) + viewPosition.x, viewPosition.y + ( lookDir.x * RADIAN ), cosf( r ) + viewPosition.z, 0.f );

		return XMMatrixLookAtLH( pos, dir, up );
	}

	bool DirectXHelper::CreateDepthStencilView( ID3D11Device& device, int width, int height, DXGI_FORMAT format, int sampleCount, int sampleQuality, D3D11_DSV_DIMENSION viewDimension, 
		ID3D11Texture2D*& outputTex, ID3D11DepthStencilView*& outputView )
	{
		D3D11_TEXTURE2D_DESC desc {};

		desc.Width				= width;
		desc.Height				= height;
		desc.ArraySize			= 1;
		desc.MipLevels			= 1; //1 only for depth
		desc.Format				= format;
		desc.BindFlags			= D3D11_BIND_DEPTH_STENCIL;
		desc.Usage				= D3D11_USAGE_DEFAULT;
		desc.CPUAccessFlags		= 0;
		desc.MiscFlags			= 0;
		desc.SampleDesc.Count	= sampleCount;
		desc.SampleDesc.Quality = sampleQuality;

		if( FAILED( device.CreateTexture2D( &desc, NULL, &outputTex ) ) )
		{
			return false;
		}

		D3D11_DEPTH_STENCIL_VIEW_DESC stencilDesc {};

		stencilDesc.Format				= format;
		stencilDesc.ViewDimension		= viewDimension;
		stencilDesc.Texture2D.MipSlice	= 0;

		return !FAILED( device.CreateDepthStencilView( outputTex, &stencilDesc, &outputView ) );
	}

	bool DirectXHelper::CreateRasterizerState( ID3D11Device& device, D3D11_CULL_MODE cullMode, D3D11_FILL_MODE fillMode, ID3D11RasterizerState*& output )
	{
		D3D11_RASTERIZER_DESC desc {};

		desc.AntialiasedLineEnable	= true;
		desc.CullMode				= cullMode;
		desc.DepthBias				= 0;
		desc.DepthBiasClamp			= 0.f;
		desc.DepthClipEnable		= true;
		desc.FillMode				= fillMode;
		desc.FrontCounterClockwise	= false;
		desc.MultisampleEnable		= true;
		desc.ScissorEnable			= false;
		desc.SlopeScaledDepthBias	= 0.f;

		return !FAILED( device.CreateRasterizerState( &desc, &output ) );
	}

	bool DirectXHelper::CreateDepthStencilState( ID3D11Device& device, bool depthEnabled, ID3D11DepthStencilState*& output )
	{
		D3D11_DEPTH_STENCIL_DESC desc {};

		desc.DepthEnable					= depthEnabled;
		desc.DepthWriteMask					= D3D11_DEPTH_WRITE_MASK_ALL;
		desc.DepthFunc						= D3D11_COMPARISON_LESS;
		desc.StencilEnable					= true;
		desc.StencilReadMask				= 0xFF;
		desc.StencilWriteMask				= 0xFF;
		desc.FrontFace.StencilFailOp		= D3D11_STENCIL_OP_KEEP;
		desc.FrontFace.StencilDepthFailOp	= D3D11_STENCIL_OP_INCR;
		desc.FrontFace.StencilPassOp		= D3D11_STENCIL_OP_KEEP;
		desc.FrontFace.StencilFunc			= D3D11_COMPARISON_ALWAYS;
		desc.BackFace.StencilFailOp			= D3D11_STENCIL_OP_KEEP;
		desc.BackFace.StencilDepthFailOp	= D3D11_STENCIL_OP_DECR;
		desc.BackFace.StencilPassOp			= D3D11_STENCIL_OP_KEEP;
		desc.BackFace.StencilFunc			= D3D11_COMPARISON_ALWAYS;

		return !FAILED( device.CreateDepthStencilState( &desc, &output ) );
	}

	bool DirectXHelper::CreateBlendState( ID3D11Device& device, bool alphaToCoverage, bool blendEnabled, D3D11_BLEND destBlendAlpha, int writeMask, ID3D11BlendState*& output  )
	{
		D3D11_BLEND_DESC desc {};

		desc.AlphaToCoverageEnable				= alphaToCoverage;
		desc.RenderTarget[ 0 ].BlendEnable		= blendEnabled;
		desc.RenderTarget[ 0 ].SrcBlend			= D3D11_BLEND_SRC_ALPHA;
		desc.RenderTarget[ 0 ].DestBlend		= D3D11_BLEND_INV_SRC_ALPHA;
		desc.RenderTarget[ 0 ].BlendOp			= D3D11_BLEND_OP_ADD;
		desc.RenderTarget[ 0 ].SrcBlendAlpha	= D3D11_BLEND_ONE;
		desc.RenderTarget[ 0 ].DestBlendAlpha	= destBlendAlpha;
		desc.RenderTarget[ 0 ].BlendOpAlpha		= D3D11_BLEND_OP_ADD;
		desc.RenderTarget[ 0 ].RenderTargetWriteMask = writeMask;

		return !FAILED( device.CreateBlendState( &desc, &output ) );
	}

	void DirectXHelper::CreateViewport( int width, int height, D3D11_VIEWPORT& output )
	{
		ZeroMemory( &output, sizeof( D3D11_VIEWPORT ) );

		output.TopLeftX	= 0.f;	//TODO: Eventually allow this to be set
		output.TopLeftY	= 0.f;
		output.Width	= static_cast<float>( width );
		output.Height	= static_cast<float>( height );
		output.MinDepth	= 0.f;
		output.MaxDepth	= 1.f;
	}

	bool DirectXHelper::CreateRenderTargetTexture( ID3D11Device& device, int width, int height, DXGI_FORMAT format, ID3D11Texture2D*& output )
	{
		D3D11_TEXTURE2D_DESC desc {};

		desc.Width				= width;
		desc.Height				= height;
		desc.MipLevels			= 0;
		desc.ArraySize			= 1;
		desc.Format				= format;
		desc.Usage				= D3D11_USAGE_DEFAULT;
		desc.BindFlags			= D3D11_BIND_RENDER_TARGET | D3D11_BIND_SHADER_RESOURCE;
		desc.CPUAccessFlags		= 0;
		desc.MiscFlags			= D3D11_RESOURCE_MISC_GENERATE_MIPS;
		desc.SampleDesc.Count	= 1;
		desc.SampleDesc.Quality	= 0;

		return !FAILED( device.CreateTexture2D( &desc, NULL, &output ) );
	}

	bool DirectXHelper::CreateRenderTargetView( ID3D11Device& device, ID3D11Texture2D* renderTargetTexture, DXGI_FORMAT format, D3D11_RTV_DIMENSION viewDimension, ID3D11RenderTargetView*& output )
	{
		D3D11_RENDER_TARGET_VIEW_DESC desc {};

		desc.Format = format;
		desc.ViewDimension = viewDimension;
		desc.Texture2D.MipSlice = 0;

		// Create the render target view.
		return !FAILED( device.CreateRenderTargetView( renderTargetTexture, &desc, &output ) );
	}

	bool DirectXHelper::CreateShaderResourceView( ID3D11Device& device, ID3D11Texture2D* renderTargetTexture, DXGI_FORMAT format, D3D_SRV_DIMENSION viewDimension, ID3D11ShaderResourceView*& output )
	{
		D3D11_SHADER_RESOURCE_VIEW_DESC desc {};

		desc.Format						= format;
		desc.ViewDimension				= viewDimension;
		desc.Texture2D.MostDetailedMip	= 0;
		desc.Texture2D.MipLevels		= 1;

		return !FAILED( device.CreateShaderResourceView( renderTargetTexture, &desc, &output ) );
	}
}
