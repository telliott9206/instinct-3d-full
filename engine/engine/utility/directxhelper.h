/*
 * DirectXHelper.h
 *
 * Created 20/12/2018
 * Written by Todd Elliott
 *
 * Provides several functions for creating basic DirectX interface objects and other
 * misc functionality.
 */

#ifndef _DIRECTXHELPER_H
#define _DIRECTXHELPER_H

#include <Instinct.h>

namespace Instinct
{
	struct Vertex;
	class Entity;
	class Engine;

	class DirectXHelper
	{
	public:
		DirectXHelper() = delete;
		DirectXHelper( const DirectXHelper& ) = delete;
		DirectXHelper& operator = ( const DirectXHelper& ) = delete;
		~DirectXHelper() = delete;

		template<class T>
		static void CopyToBuffer( ID3D11DeviceContext& deviceContext, ID3D11Buffer* buffer, T* data, uint32_t dataCount = 1 );

		template<class T>
		static bool CreateConstantBuffer( ID3D11Buffer*& buffer, D3D11_USAGE usage, int cpuAccessFlags, const std::string& shaderName = "" );

		static XMMATRIX GenerateViewMatrixLookAt( const XMFLOAT3& viewPosition, const XMFLOAT3& lookAt );
		static XMMATRIX GenerateViewMatrixLookDir( const XMFLOAT3& viewPosition, const XMFLOAT3& lookDir );

		static bool CreateDepthStencilView( ID3D11Device& device, int width, int height, DXGI_FORMAT format, int sampleCount, int sampleQuality, D3D11_DSV_DIMENSION viewDimension, ID3D11Texture2D*& outputTex, ID3D11DepthStencilView*& outputView );
		static bool CreateRasterizerState( ID3D11Device& device, D3D11_CULL_MODE cullMode, D3D11_FILL_MODE fillMode, ID3D11RasterizerState*& output );
		static bool CreateDepthStencilState( ID3D11Device& device, bool depthEnabled, ID3D11DepthStencilState*& output );
		static bool CreateBlendState( ID3D11Device& device, bool alphaToCoverage, bool blendEnabled, D3D11_BLEND destBlendAlpha, int writeMask, ID3D11BlendState*& output );
		static void CreateViewport( int width, int height, D3D11_VIEWPORT& output );
		static bool CreateRenderTargetTexture( ID3D11Device& device, int width, int height, DXGI_FORMAT format, ID3D11Texture2D*& output );
		static bool CreateRenderTargetView( ID3D11Device& device, ID3D11Texture2D* renderTargetTexture, DXGI_FORMAT format, D3D11_RTV_DIMENSION viewDimension, ID3D11RenderTargetView*& output );
		static bool CreateShaderResourceView( ID3D11Device& device, ID3D11Texture2D* renderTargetTexture, DXGI_FORMAT format, D3D_SRV_DIMENSION viewDimension, ID3D11ShaderResourceView*& output );
	};

	template<class T>
	void DirectXHelper::CopyToBuffer( ID3D11DeviceContext& deviceContext, ID3D11Buffer* buffer, T* data, uint32_t dataCount )
	{
		D3D11_MAPPED_SUBRESOURCE mappedResource;
		T* ptr;

		//map the buffer to get a pointer to it
		deviceContext.Map( buffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource );
		ptr = (T*)mappedResource.pData;

		//update the buffer with the new data
		memcpy( ptr, data, sizeof( T ) * dataCount );

		//unmap the buffer
		deviceContext.Unmap( buffer, 0 );
	}

	template<class T>
	bool DirectXHelper::CreateConstantBuffer( ID3D11Buffer*& buffer, D3D11_USAGE usage, int cpuAccessFlags, const std::string& info )
	{
		D3D11_BUFFER_DESC desc;    
		ZeroMemory( &desc, sizeof( D3D11_BUFFER_DESC ) );

		desc.Usage = usage;
		desc.ByteWidth = sizeof( T );
		desc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
		desc.CPUAccessFlags = cpuAccessFlags;
		desc.MiscFlags = 0;

		auto& pDevice = Engine::Instance().GetContext()->GetDevice();
		if( FAILED( pDevice.CreateBuffer( &desc, NULL, &buffer ) ) )
		{
			Engine::Error( "D3D ERROR", "Failed to create constant buffer; %s", info );
			return false;
		}

		return true;
	}
};

#endif
