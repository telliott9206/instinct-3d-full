/*
 * Util.h
 *
 * Created 07/12/2018
 * Written by Todd Elliott
 *
 * Provides a selection of misc functions for the engine.
 */

#ifndef _UTIL_H
#define _UTIL_H

#include <Instinct.h>

namespace Instinct
{
	class Util
	{
	public:
		Util() = delete;
		Util( const Util& ) = delete;
		Util& operator = ( const Util& ) = delete;
		~Util() = delete;

		static const wchar_t* MultibyteToWideChar( const char* input );
		static LPCWSTR StdStringToWindowsLPString( const std::string& input );
		static void GetFileExtension( const std::string& file, std::string& output );

		static std::vector<std::string> StringSplit( std::string input, char delim );
		static std::string StringTrim( std::string& input );

		template<class T>
		static bool VectorContains( const std::vector<T>& searchVec, const T& searchTerm );
	};

	template<class T>
	bool Util::VectorContains( const std::vector<T>& searchVec, const T& searchTerm )
	{
		const auto iter = std::find( searchVec.cbegin(), searchVec.cend(), searchTerm );
		return iter != searchVec.cend();
	}
};

#endif
