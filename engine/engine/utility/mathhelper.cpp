/*
 * MathHelper.cpp
 *
 * Created 10/05/2019
 * Written by Todd Elliott
 */

#include "MathHelper.h"
#include <Engine/Camera.h>
#include <Engine/World.h>
#include <Engine/Terrain/Terrain.h>
#include <DirectXCollision.inl>

namespace Instinct
{
	XMFLOAT3 MathHelper::WorldToProjection( const XMFLOAT3& position, const XMMATRIX& view, const XMMATRIX& proj )
	{
		XMMATRIX vp = view * proj;
		XMVECTOR p = XMVector3Transform( XMLoadFloat3( &position ), vp );

		XMFLOAT3 ret;
		XMStoreFloat3( &ret, p );

		return ret;
	}

	XMFLOAT2 MathHelper::WorldToScreen( const XMFLOAT3& position, const XMMATRIX& view, const XMMATRIX& proj )
	{
		auto projectiveCoords = WorldToProjection( position, view, proj );
	
		projectiveCoords.x /= projectiveCoords.z;
		projectiveCoords.y /= projectiveCoords.z;

		auto x = ( projectiveCoords.x + 1.f ) / 2.f;
		auto y = ( 1.f - ( ( projectiveCoords.y + 1.f ) / 2.f ) );

		return XMFLOAT2( x, y );
	}

	float MathHelper::PointDistance2D( const XMFLOAT2& point, const XMFLOAT2& target )
	{
		return static_cast<float>( sqrt( pow( point.x - target.x, 2 ) + pow( point.y - target.y, 2 ) ) );
	}

	float MathHelper::PointDistance3D( const XMFLOAT3& point, const XMFLOAT3& target )
	{
		return static_cast<float>( sqrt( pow( point.x - target.x, 2 ) + pow( point.y - target.y, 2 ) + pow( point.z - target.z, 2 ) ) );
	}

	float MathHelper::VectorLength3D( const XMFLOAT3& input )
	{
		return static_cast<float>( sqrt( pow( input.x, 2 ) + pow( input.y, 2 ) + pow( input.y, 2 ) ) );
	}

	void MathHelper::VectorNormalize3D( XMFLOAT3& output )
	{
		const auto length = VectorLength3D( output );
		output = MathHelper::DivideVector3D( output, length );
	}

	XMFLOAT2 MathHelper::AngleToVector( float angle )
	{
		const auto radians = XMConvertToRadians( angle );
		return { cosf( radians ), sinf( radians ) };
	}

	float MathHelper::DotProduct( const XMFLOAT2& p, const XMFLOAT2& q )
	{
		auto product = 0.f;

		product += p.x * q.x;
		product += p.y * q.y;

		return product;
	}

	XMFLOAT3 MathHelper::CrossProduct( const XMFLOAT3& vec1, const XMFLOAT3& vec2, const XMFLOAT3& vec3 )
	{
		XMFLOAT3 cross1, cross2;

		cross1.x = vec1.x - vec3.x;
		cross1.y = vec1.y - vec3.y;
		cross1.z = vec1.z - vec3.z;
		cross2.x = vec3.x - vec2.x;
		cross2.y = vec3.y - vec2.y;
		cross2.z = vec3.z - vec2.z;

		XMFLOAT3 ret;
		ret.x = ( cross1.y * cross2.z ) - ( cross1.z * cross2.y );
		ret.y = ( cross1.z * cross2.x ) - ( cross1.x * cross2.z );
		ret.z = ( cross1.x * cross2.y ) - ( cross1.y * cross2.x );

		return ret;
	}

	XMFLOAT3 MathHelper::AddVector3D( const XMFLOAT3& vec1, const XMFLOAT3& vec2 )
	{
		XMFLOAT3 ret;

		ret.x = vec1.x + vec2.x;
		ret.y = vec1.y + vec2.y;
		ret.z = vec1.z + vec2.z;

		return ret;
	}

	XMFLOAT3 MathHelper::DivideVector3D( const XMFLOAT3& vec1, float diviser )
	{
		XMFLOAT3 ret;

		ret.x = vec1.x / diviser;
		ret.y = vec1.y / diviser;
		ret.z = vec1.z / diviser;

		return ret;
	}
}
