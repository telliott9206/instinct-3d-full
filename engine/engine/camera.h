/*
 * Camera.h
 *
 * Created 17/12/2018
 * Written by Todd Elliott
 *
 * Encapsulates all functionality required to generated the required matrices
 * for viewing the world from a certain loation.
 * Ideally this class would be a base class to multiple Camera type derived classes
 * but this would not be ideal for use with exported functions and changing the 
 * camera type.
 */

#ifndef _CAMERA_H
#define _CAMERA_H

#include <Instinct.h>
#include <Engine/Entity/Entity.h>

namespace Instinct
{
	class World;

	ALIGN16 class Camera : public Entity
	{
	public:
		OPERATORS_ALIGN16

		typedef enum
		{
			kStatic,
			kFreeLook,
			kFirstPerson,
			kThirdPerson,
			kEditor
		} 
		tCameraType;

		explicit Camera( tCameraType cameraType = tCameraType::kStatic );
		virtual ~Camera();

		/* @brief	factory function for instantiating a camera object
		 *
		 * @return	shared_ptr to newly instantiated camera object
		 */
		DLLEXPORT static std::shared_ptr<Camera> Create();

		void Update( float delta );
		void GenerateProjectionMatrix();
		void GenerateProjectionMatrix( XMMATRIX& proj, float distance );

		void LookAtPosition( const XMFLOAT3& targetPos );

		DLLEXPORT int GetType() const				{ return mType; }
		DLLEXPORT void SetType( int type )			{ mType = type; }

		const XMMATRIX& GetViewMatrix()	const		{ return mView; }
		const XMMATRIX& GetBaseViewMatrix()	const	{ return mBaseView; }
		const XMMATRIX& GetProjectionMatrix() const	{ return mProjection; }

	private:
		bool Init();

		void AcceptKeyboardInput( float delta );
		void AcceptMouseInput();

		void HandleFreeLookUpdate( float delta );
		void HandleEditorUpdate( float delta );
		

	private:
		int			mType;
		float		mMoveSpeed;

		XMFLOAT3	mTarget;
		XMFLOAT3	mUp;

		XMMATRIX	mView;
		XMMATRIX	mBaseView;
		XMMATRIX	mProjection;
	};
};

#endif
