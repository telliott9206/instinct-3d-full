/*
 * Mouse.h
 *
 * Created 06/12/2018
 * Written by Todd Elliott
 *
 * A singleton accessible class that controls the input of the mouse.
 */

#ifndef _MOUSE_H
#define _MOUSE_H

#include <Instinct.h>

namespace Instinct
{
	enum MouseButtons
	{
		kMouseLeft = 0,
		kMouseRight,
		kMouseMiddle
	};

	class Mouse
	{
	public:
		static Mouse& Instance()
		{
			static Mouse instance;
			return instance;
		}

	private:
		Mouse() = default;
		~Mouse();
		Mouse( const Mouse& ) = delete;
		Mouse( const Mouse&& ) = delete;
		Mouse& operator = ( const Mouse& ) = delete;
		Mouse& operator = ( const Mouse&& ) = delete;

	public:
		bool Init( IDirectInput8* input );
		void UpdateState();
		void UpdateOldState();

		DLLEXPORT static bool ButtonDown( int button );
		DLLEXPORT static bool ButtonPressed( int button );
		DLLEXPORT static bool ButtonReleased( int button );

		static const XMINT2 GetCursorPosition();

		const int GetChangeX() const { return mMouseState.lX; }
		const int GetChangeY() const { return mMouseState.lY; }

	private:
		IDirectInputDevice8*	mDevice;	//direct input device
		DIMOUSESTATE			mMouseState;
		DIMOUSESTATE			mMouseStateOld;
		
		int						mWindowWidth;
		int						mWindowHeight;
		int						mMouseX;
		int						mMouseY;
		int						mMouseChange;
		int						mMouseChangeY;
	};
};

#endif
