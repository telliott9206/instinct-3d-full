/*
 * Keyboard.h
 *
 * Created 02/12/2018
 * Written by Todd Elliott
 *
 * A singleton accessible class that controls the input for the keyboard.
 */

#ifndef _KEYBOARD_H
#define _KEYBOARD_H

#include <Instinct.h>

namespace Instinct
{
	class Keyboard
	{
	public:
		static Keyboard& Instance()
		{
			static Keyboard instance;
			return instance;
		}

	private:
		Keyboard() = default;
		~Keyboard();
		Keyboard( const Keyboard& ) = delete;
		Keyboard( const Keyboard&& ) = delete;
		Keyboard& operator = ( const Keyboard& ) = delete;
		Keyboard& operator = ( const Keyboard&& ) = delete;

	public:
		bool Init( IDirectInput8* pInput );
		void UpdateState();
		void UpdateOldState();

		DLLEXPORT static bool KeyDown( int key );
		DLLEXPORT static bool KeyPressed( int key );
		DLLEXPORT static bool KeyReleased( int key );

	private:
		IDirectInputDevice8*	mKeyboardDevice;	//direct input device
		unsigned char			mKeyState[ 256 ];			//storage for each keys state
		unsigned char			mKeyStateOld[ 256 ];
	};
};

#endif
