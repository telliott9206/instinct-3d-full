/*
 * Mouse.cpp
 *
 * Created 06/12/2018
 * Written by Todd Elliott
 */

#include "Mouse.h"
#include <Engine.h>

namespace Instinct
{
	Mouse::~Mouse()
	{
		mDevice->Unacquire();
	}

	bool Mouse::Init( IDirectInput8* input )
	{
		//TODO: Cleanup this function. A lot of commented out code that needs addressing

		auto context = Engine::Instance().GetContext();
		if( !context )
		{
			Engine::Error( dinputerr, "An instance of Context class must be instantiated in Game::start()" );
			return false;
		}

		auto* window = context->GetRenderWindow();
		if( !window )
		{
			Engine::Error( dinputerr, "An instance of Render Window class must be instantiated in Game::start()" );
			return false;
		}

		//init window dimensions
		auto* renderWindow = Engine::Instance().GetContext()->GetRenderWindow();
		mWindowWidth = renderWindow->GetWidth();
		mWindowHeight = renderWindow->GetHeight();

		//create the mouse device
		if( FAILED( input->CreateDevice( GUID_SysMouse, &mDevice, NULL ) ) )
		{
			//System::Error( dinputerr, "Failed to create mouse device" );
			//return false;
		}

		if( FAILED( mDevice->SetDataFormat( &c_dfDIMouse ) ) )
		{
			//System::Error( dinputerr, "Failed to set mouse device data format" );
			//return false;
		}

		const auto& hwnd = renderWindow->GetWindowHandle();
		if( FAILED( mDevice->SetCooperativeLevel( hwnd, DISCL_FOREGROUND | DISCL_NONEXCLUSIVE ) ) )
		{
			/*
			System::Error( dinputerr, "Failed to set mouse device cooperative level" );
			return false;
			*/
		}

		if( FAILED( mDevice->Acquire() ) )
		{
			//System::Error( dinputerr, "Failed to accquire mouse device" );
			//return false;
		}

		return true;
	}

	void Mouse::UpdateState()
	{
		//update the mouse position
		mMouseX += static_cast<int>( mMouseState.lX ) / 2;
		mMouseY += static_cast<int>( mMouseState.lY ) / 2;

		//and make sure the mouse pos stays within bounds
		if( mMouseX < 0 )
		{
			mMouseX = 0;
		}

		if( mMouseY < 0 )
		{
			mMouseY = 0;
		}

		if( mMouseX > mWindowWidth )
		{
			mMouseX = mWindowWidth;
		}

		if( mMouseY > mWindowHeight )
		{
			mMouseY = mWindowHeight;
		}

		//update the mouse state
		HRESULT hr = mDevice->GetDeviceState( sizeof( DIMOUSESTATE ), (LPVOID)&mMouseState );
		if( FAILED( hr ) )
		{
			if( ( hr == DIERR_INPUTLOST ) || ( hr == DIERR_NOTACQUIRED ) )
			{
				mDevice->Acquire();
				//Init( System::Instance().GetDirectInput() );
			}
			else
			{
				Engine::Error( dinputerr, "Mouse device lost" );
			};
		}
	}

	void Mouse::UpdateOldState()
	{
		mMouseStateOld = mMouseState;
	}

	bool Mouse::ButtonDown( int button )
	{
		const auto& mouse = Mouse::Instance();
		return mouse.mMouseState.rgbButtons[ button ] & 0x80 ? true : false;
	}

	bool Mouse::ButtonPressed( int button )
	{
		const auto& mouse = Mouse::Instance();
		return !( mouse.mMouseStateOld.rgbButtons[ button ] & 0x80 ) && mouse.mMouseState.rgbButtons[ button ] & 0x80;
	}

	bool Mouse::ButtonReleased( int button )
	{
		const auto& mouse = Mouse::Instance();
		return mouse.mMouseStateOld.rgbButtons[ button ] & 0x80 && !( mouse.mMouseState.rgbButtons[ button ] & 0x80 );
	}

	const XMINT2 Mouse::GetCursorPosition()
	{
		POINT p;
		XMINT2 ret { 0, 0 };

		if( GetCursorPos( &p ) )
		{
			ret.x = p.x;
			ret.y = p.y;
		}

		return ret;
	}
}
