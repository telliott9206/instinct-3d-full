/*
 * Keyboard.cpp
 *
 * Created 02/12/2018
 * Written by Todd Elliott
 */

#include "Keyboard.h"
#include <Engine.h>
#include <Engine/Render/Context.h>

namespace Instinct
{
	Keyboard::~Keyboard()
	{
		mKeyboardDevice->Unacquire();
	}

	bool Keyboard::Init( IDirectInput8* input )
	{
		auto context = Engine::Instance().GetContext();
		if( context == nullptr )
		{
			Engine::Error( dinputerr, "An instance of Context class must be instantiated in Game::start()" );
			return false;
		}

		auto window = context->GetRenderWindow();
		if( window == nullptr )
		{
			Engine::Error( dinputerr, "An instance of Render Window class must be instantiated in Game::start()" );
			return false;
		}

		if( FAILED( input->CreateDevice( GUID_SysKeyboard, &mKeyboardDevice, NULL ) ) )
		{
			Engine::Error( dinputerr, "Failed to create keyboard device" );
			return false;
		}

		if( FAILED( mKeyboardDevice->SetDataFormat( &c_dfDIKeyboard ) ) )
		{
			Engine::Error( dinputerr, "Failed to set keyboard device data format" );
			return false;
		}

		/*
		if( FAILED( keyboardDevice->SetCooperativeLevel( Singleton::instanceSystem()->, DISCL_FOREGROUND | DISCL_EXCLUSIVE ) ) )
		{
			Util::error( dinputerr, "Failed to set keyboard device cooperative level" );
			return false;
		}
		*/

		if( FAILED( mKeyboardDevice->Acquire() ) )
		{
			Engine::Error( dinputerr, "Failed to set acquire keyboard device" );
			return false;
		}

		return true;
	}

	void Keyboard::UpdateState()
	{
		//read the keyboard device
		HRESULT hr = mKeyboardDevice->GetDeviceState( sizeof( mKeyState ), (LPVOID)&mKeyState );

		if( FAILED( hr ) ) 
		{
			// If the keyboard lost focus or was not acquired then try to get control back.
			if( ( hr == DIERR_INPUTLOST) || ( hr == DIERR_NOTACQUIRED ) )
			{
				mKeyboardDevice->Acquire();
			}
		}
	}

	void Keyboard::UpdateOldState()
	{
		memcpy( mKeyStateOld, mKeyState, sizeof( mKeyState ) );
	}

	bool Keyboard::KeyDown( int key )
	{
		auto& keyboard = Keyboard::Instance();
		return keyboard.mKeyState[ key ] & 0x80 ? true : false;
	}

	bool Keyboard::KeyPressed( int key )
	{
		const auto& keyboard = Keyboard::Instance();
		return !( keyboard.mKeyStateOld[ key ] & 0x80 ) && keyboard.mKeyState[ key ] & 0x80;
	}

	bool Keyboard::KeyReleased( int key )
	{
		const auto& keyboard = Keyboard::Instance();
		return keyboard.mKeyStateOld[ key ] & 0x80 && !( keyboard.mKeyState[ key ] & 0x80 );
	}
}
