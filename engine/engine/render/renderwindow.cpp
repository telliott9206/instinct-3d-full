/*
 * Renderwindow.cpp
 *
 * Created 30/11/2018
 * Written by Todd Elliott
 */

#include "RenderWindow.h"
#include <Engine/Render/DeferredRenderTexture.h>
#include <Engine/Render/Context.h>
#include <Engine/Input/Mouse.h>
#include <Engine.h>
#include <Engine/Camera.h>
#include <Engine/World.h>

namespace Instinct
{
	RenderWindow::~RenderWindow()
	{
	}

	std::shared_ptr<RenderWindow> RenderWindow::Create( int width, int height, int flags, const std::string& title )
	{
		auto window = std::shared_ptr<RenderWindow>( new RenderWindow() ); //Manual shared_ptr for aligned 16 class

		window->mWidth = width;
		window->mHeight = height;
		window->mFlags = flags;
		window->mTitle = title;

		if( flags & ( 1 << tWindowFlags::kWindowed ) )
		{
			window->mIsWindowed = true;
		}

		window->mIsWindowed = true;

		//create the actual window
		window->CreateRenderWindow();
		window->CreateOrthogonalMatrix();

		return window;
	}

	std::shared_ptr<RenderWindow> RenderWindow::Embed( HWND hwnd, int width, int height )
	{
		auto window = std::shared_ptr<RenderWindow>( new RenderWindow() ); //Manual shared_ptr for aligned 16 class

		window->mWidth = width;
		window->mHeight = height;
		window->mFlags |= tWindowFlags::kWindowed;
		window->mTitle = "";
		window->mIsWindowed = true;

		window->mWindow = hwnd;

		window->CreateOrthogonalMatrix();

		return window;
	}

	void RenderWindow::CreateRenderWindow()
	{
		WNDCLASSEX wc;
		ZeroMemory( &wc, sizeof( WNDCLASSEX ) );

		wc.cbSize			= sizeof( WNDCLASSEX );				//size
		wc.style			= CS_HREDRAW | CS_VREDRAW;			//draw type
		wc.lpfnWndProc		= WindowProcess;					//process handler
		wc.hInstance		= GetModuleHandle( NULL );			//instance
		wc.hCursor			= LoadCursor( NULL, IDC_ARROW );	//cursor type
		wc.hbrBackground	= (HBRUSH)COLOR_WINDOW;				//background colour - changed later with graphics renderer
		wc.lpszClassName	= mTitle.c_str();					//registered name

		RegisterClassEx( &wc );	//register the class with the system

		//center the window
		RECT desktop;
		const HWND hDesktop = GetDesktopWindow();
		GetWindowRect( hDesktop, &desktop );
		const auto horizontal = desktop.right;
		const auto vertical = desktop.bottom;
		const auto xpos = ( horizontal / 2 ) - ( mWidth / 2 );
		const auto ypos = ( vertical / 2 ) - ( mHeight / 2 );

		mWindow = CreateWindowEx( NULL,
			mTitle.c_str(),					//window class name
			mTitle.c_str(),					//window title
			WS_OVERLAPPEDWINDOW,			//window style
			xpos,							//window x pos
			ypos,							//window y pos
			mWidth,							//Window width
			mHeight,						//window Height
			NULL,							//parent window
			NULL,							//menus ...
			GetModuleHandle( NULL ),		//application handle
			NULL
		);  

		ShowWindow( mWindow, 10 );			//Show the window
	}

	void RenderWindow::CreateOrthogonalTexture( ID3D11Device& device )
	{
		mOrthoTexture = DeferredRenderTexture::Create( device, mWidth, mHeight );
	}

	void RenderWindow::BindOrthoTexture( ID3D11DeviceContext& deviceContext )
	{
		mOrthoTexture->BindBuffers( deviceContext );
	}

	void RenderWindow::ComputeMouseRayInfo( XMFLOAT3& position, XMFLOAT3& direction )
	{
		const auto context = Engine::Instance().GetContext();
		const auto camera = Engine::Instance().GetCurrentWorld()->GetActiveCamera();
		const auto& view = camera->GetViewMatrix();
		const auto& proj = camera->GetProjectionMatrix();

		/*
		const auto x = (2.0f * mousePosition.x) / mWidth - 1.0f;
		const auto y = 1.0f - (2.0f * mousePosition.y) / mHeight;
		*/

		RECT rect;
		GetWindowRect( GetWindowHandle(), &rect );
		const auto left = static_cast<int32_t>( rect.left );
		const auto top = static_cast<int32_t>( rect.top );

		auto mousePosition = Mouse::GetCursorPosition();
		mousePosition.x += left;
		mousePosition.y += top;

		const auto x = mousePosition.x;
		const auto y = mousePosition.y;

		//const auto x = (2.0f * mousePosition.x) / mWidth - 1.0f;
		//const auto y = 1.0f - (2.0f * mousePosition.y) / mHeight;

		/*
		const auto mouseNear = DirectX::XMVectorSet( static_cast<float>( mousePosition.x ), static_cast<float>( mousePosition.y ), 0.f, 1.f );
		const auto mouseFar = DirectX::XMVectorSet((float)mousePosition.x, (float)mousePosition.y, 1.0f, 1.0f);
		const auto unprojectedNear = DirectX::XMVector3Unproject( mouseNear, 0, 0, static_cast<float>( mWidth ), static_cast<float>( mHeight ), context->GetScreenNear(), context->GetScreenFar(), proj, view, DirectX::XMMatrixIdentity() );

		XMFLOAT3 ret;
		XMStoreFloat3( &ret, unprojectedNear );

		const auto& cameraPos = camera->GetPosition();
		*/

		DirectX::XMVECTOR mouseNear = DirectX::XMVectorSet((float)x, (float)y, 0.f, 1.f);
		DirectX::XMVECTOR mouseFar = DirectX::XMVectorSet((float)x, (float)y, 1.f, 1.f);
		DirectX::XMVECTOR unprojectedNear = DirectX::XMVector3Unproject(mouseNear, (float)left, (float)top, (float)mWidth, (float)mHeight, 0.1f, 1000.f, proj, view, DirectX::XMMatrixIdentity());
		//DirectX::XMVECTOR unprojectedNear = DirectX::XMVector3Unproject(mouseNear, 0, 0, (float)mWidth, (float)mHeight, context->GetScreenNear(), context->GetScreenFar(), proj, view, DirectX::XMMatrixIdentity());
		DirectX::XMVECTOR unprojectedFar = DirectX::XMVector3Unproject(mouseFar, (float)left, (float)top, (float)mWidth, (float)mHeight, 0.1f, 1000.f, proj, view, DirectX::XMMatrixIdentity());
		//DirectX::XMVECTOR unprojectedFar = DirectX::XMVector3Unproject(mouseFar, 0, 0, (float)mWidth, (float)mHeight, context->GetScreenNear(), context->GetScreenFar(), proj, view, DirectX::XMMatrixIdentity());
		DirectX::XMVECTOR result = DirectX::XMVector3Normalize(DirectX::XMVectorSubtract(unprojectedFar, unprojectedNear));

		XMStoreFloat3( &position, unprojectedNear );
		XMStoreFloat3( &direction, result );
	}

	bool RenderWindow::IsMouseInside() const
	{
		RECT rect;
		GetWindowRect( GetWindowHandle(), &rect );

		const auto x = rect.left;
		const auto y = rect.top;
		const auto p = Mouse::Instance().GetCursorPosition();

		return p.x >= x && p.x <= ( x + mWidth ) && p.y >= y && p.y <= ( y + mHeight );
	}

	void RenderWindow::CreateOrthogonalMatrix()
	{
		mOrthoMat = XMMatrixOrthographicLH( static_cast<float>( mWidth ), static_cast<float>( mHeight ), 1.f, 1000.f );
		mOrthoMat = XMMatrixTranspose( mOrthoMat );
	}

	LRESULT CALLBACK WindowProcess( HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam )
	{
		switch( message )
		{
			case WM_DESTROY:	
			{
				PostQuitMessage( 0 );

				return 0;
			}
		}

		return DefWindowProc( hwnd, message, wParam, lParam );
	}
}
