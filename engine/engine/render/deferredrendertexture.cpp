/*
 * DefferedRenderTexture.cpp
 *
 * Created 05/03/2019
 * Written by Todd Elliott
 */

#include "DeferredRenderTexture.h"
#include <Engine/Render/Context.h>
#include <Engine.h>

namespace Instinct
{
	DeferredRenderTexture::DeferredRenderTexture( const std::vector<Vertex2D>& vertices, const std::vector<uint32_t>& indices ) :
		Mesh<Vertex2D>( vertices, indices )
	{
	}

	DeferredRenderTexture::~DeferredRenderTexture()
	{
	}

	std::unique_ptr<DeferredRenderTexture> DeferredRenderTexture::Create( ID3D11Device& device, int width, int height )
	{
		float left, right, top, bottom;

		left	= static_cast<float>( ( width / 2.f ) * -1 );
		right	= left + static_cast<float>( width );
		top		= static_cast<float>( height / 2.f );
		bottom	= top - static_cast<float>( height );

		std::vector<Vertex2D> vertices( DEFFERED_RENDER_TEXTURE_VERTEX_COUNT );
		std::vector<uint32_t> indices( DEFFERED_RENDER_TEXTURE_VERTEX_COUNT );

		//first triangle
		vertices[ 0 ].mPosition = XMFLOAT3( left, top, 0.f );
		vertices[ 0 ].mTexcoord = XMFLOAT2( 0.f, 0.f );
		vertices[ 1 ].mPosition = XMFLOAT3( right, bottom, 0.f );
		vertices[ 1 ].mTexcoord = XMFLOAT2( 1.f, 1.f );
		vertices[ 2 ].mPosition = XMFLOAT3( left, bottom, 0.0f );
		vertices[ 2 ].mTexcoord = XMFLOAT2( 0.f, 1.f );

		//second triangle
		vertices[ 3 ].mPosition = XMFLOAT3( left, top, 0.f );
		vertices[ 3 ].mTexcoord = XMFLOAT2( 0.f, 0.f);
		vertices[ 4 ].mPosition = XMFLOAT3( right, top, 0.f );
		vertices[ 4 ].mTexcoord = XMFLOAT2( 1.f, 0.f );
		vertices[ 5 ].mPosition = XMFLOAT3( right, bottom, 0.f );
		vertices[ 5 ].mTexcoord = XMFLOAT2( 1.f, 1.f );

		//load up the index array
		for( uint32_t i = 0u ; i < DEFFERED_RENDER_TEXTURE_VERTEX_COUNT ; ++ i )
		{
			indices[ i ] = i;
		}

		auto renderTexture = std::make_unique<DeferredRenderTexture>( vertices, indices );
		if( !renderTexture->CreateVertexBuffer( D3D11_USAGE_DEFAULT, 0 ) || !renderTexture->CreateIndexBuffer() )
		{
			return nullptr;
		}

	#ifdef _DEBUG
		Engine::Print( "Created deferred render texture" );
	#endif

		return std::move( renderTexture );
	}

	void DeferredRenderTexture::BindBuffers( ID3D11DeviceContext& deviceContext )
	{
		uint32_t stride, offset;

		stride = sizeof( Vertex2D );
		offset = 0u;

		deviceContext.IASetVertexBuffers( 0, 1, &mVertexBuffer, &stride, &offset );
		deviceContext.IASetIndexBuffer( mIndexBuffer, DXGI_FORMAT_R32_UINT, 0 );
		deviceContext.IASetPrimitiveTopology( D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST );
	}
}
