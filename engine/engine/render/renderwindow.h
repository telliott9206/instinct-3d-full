/*
 * RenderWindow.h
 *
 * Created 30/11/2018
 * Written by Todd Elliott
 *
 * This class represents the physical rendering window and aggreates a pointer to the 
 * actual window handle.
 */

#ifndef _RENDERWINDOW_H
#define _RENDERWINDOW_H

#include <Instinct.h>

namespace Instinct
{
	class DeferredRenderTexture;

	ALIGN16 class RenderWindow
	{
	public:
		OPERATORS_ALIGN16

		enum tWindowFlags
		{
			kTitleBar,	//gives the window a titlebar along the top
			kWindowed,	//makes it windowed not full screen
			kCentred,	//centres the window for you
		};

	public:
		RenderWindow() = default;
		DLLEXPORT ~RenderWindow();

		DLLEXPORT static std::shared_ptr<RenderWindow> Create( int width, int height, int flags, const std::string& title );
		DLLEXPORT static std::shared_ptr<RenderWindow> Embed( HWND hwd, int width, int height );

		void CreateOrthogonalTexture( ID3D11Device& device );
		void BindOrthoTexture( ID3D11DeviceContext& deviceContext );

		DLLEXPORT void ComputeMouseRayInfo( XMFLOAT3& position, XMFLOAT3& direction );

		DLLEXPORT bool IsMouseInside() const;
		DLLEXPORT int GetWidth() const								{ return mWidth; }
		DLLEXPORT int GetHeight() const								{ return mHeight; }
		DLLEXPORT int GetFlags() const								{ return mFlags; }
		DLLEXPORT bool IsWindowed() const							{ return mIsWindowed; }
		DLLEXPORT bool IsOpen() const								{ return mIsOpen; }

		const HWND& GetWindowHandle() const							{ return mWindow; }
		const DeferredRenderTexture& GetOrthogonalTexture() const	{ return *mOrthoTexture.get(); }
		const XMMATRIX& GetOrthogonalMatrix() const					{ return mOrthoMat; }

	private:
		DLLEXPORT void CreateRenderWindow();
		void CreateOrthogonalMatrix();

	private:
		int										mWidth;
		int										mHeight;
		int										mFlags;
		std::string								mTitle;	
		bool									mIsWindowed;
		bool									mIsOpen;
		HWND									mWindow;

		std::unique_ptr<DeferredRenderTexture>	mOrthoTexture;
		XMMATRIX								mOrthoMat;
	};

	LRESULT CALLBACK WindowProcess( HWND hwnd, UINT message, WPARAM wparam, LPARAM lparam );
};

#endif
