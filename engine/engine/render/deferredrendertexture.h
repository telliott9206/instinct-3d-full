/*
 * DefferedRenderTexture.h
 *
 * Created 05/03/2019
 * Written by Todd Elliott
 *
 * An orthogonal render texture used for the deferred renderer.
 */

#ifndef _DEFFEREDRENDERTEXTURE_H
#define _DEFFEREDRENDERTEXTURE_H

#include <Engine/Model/Mesh.h>
#include <Engine/Model/Vertex.h>

#define DEFFERED_RENDER_TEXTURE_VERTEX_COUNT	 6

namespace Instinct
{
	class DeferredRenderTexture : public Mesh<Vertex2D>
	{
	public:
		DeferredRenderTexture( const std::vector<Vertex2D>& vertices, const std::vector<uint32_t>& indices );
		~DeferredRenderTexture();

		static std::unique_ptr<DeferredRenderTexture> Create( ID3D11Device& device, int width, int height );

		void BindBuffers( ID3D11DeviceContext& deviceContext ) override;
	};
};

#endif
