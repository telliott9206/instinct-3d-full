/*
 * Context.h
 *
 * Created 30/11/2018
 * Written by Todd Elliott
 *
 * Encapsulates all functionality for changing anything related to the current rendering state.
 */

#ifndef _CONTEXT_H
#define _CONTEXT_H

#include <Instinct.h>
#include <Engine/Render/Renderwindow.h>
#include <mutex>

namespace Instinct
{
	class Texture2DArray;

	ALIGN16 class Context
	{
	public:
		OPERATORS_ALIGN16

		typedef enum
		{
			kDiffuse,
			kNormal,
			kTangent, //TODO: needs renaming - its not used for this anymore
		}
		tGBuffer;

		typedef enum
		{
			kLow,		//256x256
			kMedium,	//512x512
			kHigh,		//1024x1024
			kUltra		//2048x204
		}
		tShadowQuality;

		typedef enum
		{
			kCullBack,
			kCullFront,
			kCullNone,
			kWireframe,
			kWireframeNoCull
		}
		tRasterState;

		typedef enum
		{
			kAlpha,
			kAlphaToCoverage,
			kDisabled
		}
		tBlendState;

		struct MultisamplesQuery
		{
			int mMultisamples;
			bool mSupported = false;

			MultisamplesQuery( int multisamples, bool supported )
			{
				this->mMultisamples = multisamples;
				this->mSupported = supported;
			}

			MultisamplesQuery() = default;
		};

	public:
		Context();
		DLLEXPORT ~Context();

		DLLEXPORT static std::shared_ptr<Context> Create( std::shared_ptr<RenderWindow> window );

		void SceneBegin( float red, float green, float blue, float alpha );
		void SceneEnd();
		void SetGraphicBufferRenderTarget( ID3D11DeviceContext& deviceContext );
		void ResetViewAndBackBuffer();

		void EnableDepthBuffer();
		void DisableDepthBuffer();

		DLLEXPORT void SetShadowQuality( int quality );
		DLLEXPORT int GetShadowQuality() const			{ return mShadowQuality; }

		DLLEXPORT void SetRasterState( int state );
		void SetEngineRasterState( int state );
		DLLEXPORT int GetRasterState() const			{ return mRasterState; }
		void SetBlendState( int state );

		void LockDeviceContext()						{ mDeviceContextLock.lock(); }
		void UnlockDeviceContext()						{ mDeviceContextLock.unlock(); }
		std::mutex& GetDeviceContextLock()				{ return mDeviceContextLock; }

		DLLEXPORT void QueryMSAASupport();	//TODO: This function has test code in it .. fix it then make it return something, not void
		DLLEXPORT int GetHighestMSAALevel() const;

		void BindRenderWindowOrthoTexture( ID3D11DeviceContext& deviceContext );

		RenderWindow* GetRenderWindow()					{ return mRenderWindow.get(); }
		ID3D11Device& GetDevice() const					{ return *mDevice; }
		ID3D11DeviceContext& GetDeviceContext() const	{ return *mDeviceContext; }

		const std::string& GetDeviceDescription() const { return mDeviceDesc; }
		int GetDeviceMemory() const						{ return mDeviceMemory; }

		void SetScreenNear( float dist )				{ mScreenNear = dist; }
		float GetScreenNear() const						{ return mScreenNear; }

		void SetScreenFar( float dist )					{ mScreenFar = dist; }
		float GetScreenFar() const						{ return mScreenFar; }

		int GetSampleCount() const						{ return mMsaa; }
		int GetSampleQuality() const					{ return mMsaaQuality; }

		Texture2DArray* GetGraphicBuffer()				{ return mGraphicBuffer.get(); }

	private:
		bool GetDeviceData();
		bool CreateDevice();
		bool CreateSwapchain();
		bool CreateDepthBuffer();
		bool CreateRenderTarget();
		bool CreateViewport();
		bool CreateRasterStates();
		bool CreateDepthStencilState();
		bool CreateBlendStates();

	public:
		std::shared_ptr<RenderWindow>	mRenderWindow;

		bool							mIsInEditor;

		int								mMsaa;
		int								mMsaaQuality;

		std::string						mDeviceDesc;
		int								mDeviceMemory;

		int								mRasterState;

		DXGI_FORMAT						mDxgiFormat;
		D3D11_VIEWPORT					mViewPort;			
		IDXGIFactory*					mFactory;
		IDXGISwapChain*					mSwapChain;
		ID3D11Device*					mDevice;
		ID3D11DeviceContext*			mDeviceContext;
		ID3D11RenderTargetView*			mBackBuffer;	
		ID3D11DepthStencilView*			mDepthBuffer;	
		ID3D11Texture2D*				mDepthStencilBuffer;
		ID3D11DepthStencilState*		mDepthStencilState;			//TODO: put these states into an array
		ID3D11DepthStencilState*		mDepthStencilStateDisabled;	
		ID3D11SamplerState*				mSamplerState;
		ID3D11RasterizerState*			mRasterStates[ 5 ];
		ID3D11BlendState*				mAlphaBlendState[ 3 ];

		float							mScreenNear;
		float							mScreenFar;

		int								mGBufferTexture;
		int								mShadowQuality;

		std::unique_ptr<Texture2DArray>	mGraphicBuffer;

		MultisamplesQuery				mMultisamples[ 3 ];	// ??? TODO: Fix this fixed array

		std::mutex						mDeviceContextLock;
	};
};

#endif
