/*
 * Context.cpp
 *
 * Created 30/11/2018
 * Written by Todd Elliott
 */

#include <Engine/Render/Context.h>
#include <Engine/Texture/Texture2d.h>
#include <Engine/Texture/Texture2darray.h>
#include <Engine/Utility/DirectXHelper.h>
#include <Engine.h>

namespace Instinct
{
	Context::Context() :
		mIsInEditor( false ),
		mRasterState( tRasterState::kCullBack ),
		mScreenNear( 0.1f ),
		mScreenFar( 1000.f ),
		mGBufferTexture( -1 ),
		mShadowQuality( tShadowQuality::kHigh )
	{
	}

	Context::~Context()
	{
		ReleaseCOM( mFactory );
		ReleaseCOM( mSwapChain );
		ReleaseCOM( mDevice );
		ReleaseCOM( mDeviceContext );
		ReleaseCOM( mBackBuffer );
		ReleaseCOM( mDepthBuffer );
		ReleaseCOM( mDepthStencilBuffer );
		ReleaseCOM( mDepthStencilState );
		ReleaseCOM( mDepthStencilStateDisabled );

		ReleaseCOM( mRasterStates[ tRasterState::kCullBack ] );
		ReleaseCOM( mRasterStates[ tRasterState::kCullFront ] );
		ReleaseCOM( mRasterStates[ tRasterState::kCullNone ] );
		ReleaseCOM( mRasterStates[ tRasterState::kWireframe ] );
		ReleaseCOM( mRasterStates[ tRasterState::kWireframeNoCull ] );

		ReleaseCOM( mAlphaBlendState[ tBlendState::kAlpha ] );
		ReleaseCOM( mAlphaBlendState[ tBlendState::kAlphaToCoverage ] );
		ReleaseCOM( mAlphaBlendState[ tBlendState::kDisabled ] );
	}

	std::shared_ptr<Context> Context::Create( std::shared_ptr<RenderWindow> renderWindow )
	{
		auto context = std::shared_ptr<Context>( new Context() );	//Manual shared_ptr for aligned 16 class
		context->mRenderWindow = renderWindow;

		if( !context->CreateDevice() ||
			!context->CreateSwapchain() ||
			!context->CreateDepthBuffer() ||
			!context->CreateDepthStencilState() ||
			!context->CreateRenderTarget() ||
			!context->CreateViewport() ||
			!context->CreateRasterStates() ||
			!context->CreateBlendStates() )
		{
			return nullptr;
		}
	
		Engine::Instance().SetContext( context );
		context->mRenderWindow->CreateOrthogonalTexture( *context->mDevice );
	
		int width = context->GetRenderWindow()->GetWidth();
		int height = context->GetRenderWindow()->GetHeight();
		context->mGraphicBuffer = Texture2DArray::Create( width, height, Texture2DArray::BUFFER_COUNT_MAX );

		return context;
	}

	void Context::SceneBegin( float red, float green, float blue, float alpha )
	{
		//clear the backbuffer for drawing
		const float colour[ 4 ] = { red, green, blue, alpha };
		mDeviceContext->ClearRenderTargetView( mBackBuffer, colour );

		//clear the depth buffer
		mDeviceContext->ClearDepthStencilView( mDepthBuffer, D3D11_CLEAR_DEPTH, 1.f, 0 );
	}

	void Context::SceneEnd()
	{
		//present the redrawn frame
		mSwapChain->Present( 0, 0 );
		mDeviceContext->ClearState();
	}

	void Context::SetGraphicBufferRenderTarget( ID3D11DeviceContext& deviceContext )
	{
		//set and clear all targets in the gbuffer
		mGraphicBuffer->SetRenderTarget( deviceContext );
		mGraphicBuffer->ClearRenderTarget( deviceContext );
	}

	void Context::ResetViewAndBackBuffer()
	{
		//set the render target to the back buffer
		mDeviceContext->OMSetRenderTargets( 1, &mBackBuffer, mDepthBuffer );

		//set the main viewport active
		mDeviceContext->RSSetViewports( 1, &mViewPort );
	}

	void Context::EnableDepthBuffer()
	{
		mDeviceContext->OMSetDepthStencilState( mDepthStencilState, 1 );
	}

	void Context::DisableDepthBuffer()
	{
		mDeviceContext->OMSetDepthStencilState( mDepthStencilStateDisabled, 1 );
	}

	void Context::SetShadowQuality( int iValue )
	{
	}

	void Context::SetRasterState( int iState )
	{
		mRasterState = iState;
	}

	void Context::SetEngineRasterState( int iState )
	{
		mDeviceContext->RSSetState( mRasterStates[ iState ] );
	}

	void Context::SetBlendState( int iState )
	{
		mDeviceContext->OMSetBlendState( mAlphaBlendState[ iState ], 0, 0xffffffff );
	}

	bool Context::CreateDevice()
	{
		D3D_FEATURE_LEVEL featureLevel = D3D_FEATURE_LEVEL_11_0;
		uint32_t numFeatureLevels { 1 };

		D3D11CreateDevice(	nullptr,		//adapter
			D3D_DRIVER_TYPE_HARDWARE,		//driver type
			nullptr,						//software
			D3D11_CREATE_DEVICE_DEBUG,
			&featureLevel,					//feature level
			numFeatureLevels,				//number of feature levels
			D3D11_SDK_VERSION,				//sdk version
			&mDevice,						//virtual device
			nullptr,						
			&mDeviceContext );				//context

		//create a directx factory for an adapter
		if( FAILED( CreateDXGIFactory( __uuidof( IDXGIFactory ), (void**)&mFactory ) ) )
		{
			Engine::Error( d3derr, "Failed to create DirectX factory" );
			return false;
		}

		IDXGIAdapter* adapter;
		DXGI_ADAPTER_DESC desc;
		uint32_t len;

		mFactory->EnumAdapters( 0, &adapter );
		adapter->GetDesc( &desc );

		mDeviceMemory = static_cast<int>( desc.DedicatedVideoMemory / 1024 / 1024 );

		char description[ 128 ];
		wcstombs_s( &len, description, sizeof( description ) - 1, desc.Description, sizeof( description ) - 1 );
		mDeviceDesc = std::string( description );
		mDxgiFormat	= DXGI_FORMAT_R8G8B8A8_UNORM;

		QueryMSAASupport();
		mMsaaQuality = mMsaa >= 4 ? mMsaaQuality - 1 : 0;

		return true;
	}

	bool Context::CreateSwapchain()
	{
		DXGI_SWAP_CHAIN_DESC desc {};

		desc.BufferCount							= 1;
		desc.BufferDesc.RefreshRate.Numerator		= 60;
		desc.BufferDesc.RefreshRate.Denominator		= 1;
		desc.BufferDesc.Format						= mDxgiFormat;
		desc.BufferDesc.ScanlineOrdering			= DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
		desc.BufferDesc.Scaling						= DXGI_MODE_SCALING_UNSPECIFIED;
		desc.BufferUsage							= DXGI_USAGE_RENDER_TARGET_OUTPUT;
		desc.OutputWindow							= mRenderWindow->GetWindowHandle();
		desc.Windowed								= mRenderWindow->GetFlags() | RenderWindow::tWindowFlags::kWindowed;
		desc.Flags									= DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH;
		desc.SwapEffect								= DXGI_SWAP_EFFECT_DISCARD;

		desc.BufferDesc.Width						= mRenderWindow->GetWidth();
		desc.BufferDesc.Height						= mRenderWindow->GetHeight();
		desc.BufferCount							= 1;
		desc.OutputWindow							= mRenderWindow->GetWindowHandle();

		desc.SampleDesc.Count						= mMsaa;
		desc.SampleDesc.Quality						= mMsaaQuality;

		if( FAILED( mFactory->CreateSwapChain( mDevice, &desc, &mSwapChain ) ) )
		{
			Engine::Error( d3derr, "Failed to create swap chain" );
			return false;
		}

		return true;
	}

	bool Context::CreateDepthBuffer()
	{
		if( !DirectXHelper::CreateDepthStencilView( *mDevice, mRenderWindow->GetWidth(), mRenderWindow->GetHeight(), DXGI_FORMAT_D24_UNORM_S8_UINT, mMsaa, mMsaaQuality, 
			D3D11_DSV_DIMENSION_TEXTURE2DMS, mDepthStencilBuffer, mDepthBuffer ) )
		{
			Engine::Error( d3derr, "Failed to create depth stencil view" );
			return false;
		}
		
		mDeviceContext->OMSetRenderTargets( 1, &mBackBuffer, mDepthBuffer);
		return true;
	}

	bool Context::CreateRenderTarget()
	{
		HRESULT result;
		ID3D11Texture2D* buffer;
		mSwapChain->GetBuffer( 0, __uuidof( ID3D11Texture2D ), (LPVOID*)&buffer ); //Get the address of the buffer

		result = mDevice->CreateRenderTargetView( buffer, NULL, &mBackBuffer );	//Create render target from back buffer
		buffer->Release();

		if( FAILED( result ) )
		{
			Engine::Error( d3derr, "Failed to create render target" );
			return false;
		}

		//set the back buffer as the render target
		mDeviceContext->OMSetRenderTargets( 1, &mBackBuffer, mDepthBuffer );

		return true;
	}

	bool Context::CreateViewport()
	{
		DirectXHelper::CreateViewport( mRenderWindow->GetWidth(), mRenderWindow->GetHeight(), mViewPort );
		mDeviceContext->RSSetViewports( 1, &mViewPort );
	
		return true;
	}

	bool Context::CreateRasterStates()
	{
		if( !DirectXHelper::CreateRasterizerState( *mDevice, D3D11_CULL_BACK, D3D11_FILL_SOLID, mRasterStates[ tRasterState::kCullBack ] ) ||
			!DirectXHelper::CreateRasterizerState( *mDevice, D3D11_CULL_FRONT, D3D11_FILL_SOLID, mRasterStates[ tRasterState::kCullFront ] ) ||
			!DirectXHelper::CreateRasterizerState( *mDevice, D3D11_CULL_NONE, D3D11_FILL_SOLID, mRasterStates[ tRasterState::kCullNone ] ) ||
			!DirectXHelper::CreateRasterizerState( *mDevice, D3D11_CULL_NONE, D3D11_FILL_WIREFRAME, mRasterStates[ tRasterState::kWireframeNoCull ] ) )
		{
			Engine::Error( d3derr, "Failed to create rasterizer states" );
			return false;
		}
	
		mDeviceContext->RSSetState( mRasterStates[ tRasterState::kCullBack ] );	//Set the standard state active
		mRasterState = tRasterState::kCullBack;

		return true;
	}

	bool Context::CreateDepthStencilState()
	{
		if( !DirectXHelper::CreateDepthStencilState( *mDevice, true, mDepthStencilState ) ||
			!DirectXHelper::CreateDepthStencilState( *mDevice, false, mDepthStencilStateDisabled ) )
		{
			Engine::Error( d3derr, "Failed to create depth stencil states" );
			return false;
		}
				
		mDeviceContext->OMSetDepthStencilState( mDepthStencilState, 1 );
		return true;
	}

	bool Context::CreateBlendStates()
	{
		if( !DirectXHelper::CreateBlendState( *mDevice, false, true, D3D11_BLEND_ZERO, 0x0F, mAlphaBlendState[ tBlendState::kAlpha ] ) ||
			!DirectXHelper::CreateBlendState( *mDevice, false, false, D3D11_BLEND_ONE, D3D11_COLOR_WRITE_ENABLE_ALL, mAlphaBlendState[ tBlendState::kDisabled ] ) ||
			!DirectXHelper::CreateBlendState( *mDevice, true, true, D3D11_BLEND_ZERO, 0x0F, mAlphaBlendState[ tBlendState::kAlphaToCoverage ] ) )
		{
			return false;
		}

		const float blend[ 4 ] = { 0.f, 0.f, 0.f, 0.f };
		mDeviceContext->OMSetBlendState( mAlphaBlendState[ tBlendState::kDisabled ], 0, 0xFFFFFFFF );

		return true;
	}

	void Context::QueryMSAASupport()
	{
		uint32_t res;
		uint32_t lastSamples = 0u;
		uint32_t lastQuality = 0u;

		for( int i = 1 ; i <= 32 ; i *= 2 )
		{
			mDevice->CheckMultisampleQualityLevels( mDxgiFormat, i, &res );

			if( res == 0 )
			{
				mMsaa = lastSamples;
				mMsaaQuality = lastQuality;

				break;
			}

			lastSamples = i;
			lastQuality = res;
		}
	}

	int Context::GetHighestMSAALevel() const
	{
		int size = sizeof( mMultisamples ) / sizeof( MultisamplesQuery );

		for( int i = 0 ; i < size ; i ++ )
		{
			if( mMultisamples[ i ].mSupported )
			{
				return mMultisamples[ i ].mMultisamples;
			}
		}

		return 1;
	}

	void Context::BindRenderWindowOrthoTexture( ID3D11DeviceContext& deviceContext )
	{
		mRenderWindow->BindOrthoTexture( deviceContext );
	}
}
