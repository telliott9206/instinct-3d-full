/*
 * GeometryGenerator.cpp
 *
 * Created 18/03/2019
 * Written by Todd Elliott
 */

#include "GeometryGenerator.h"
#include <Engine/Terrain/Terrain.h>
#include <Engine/Model/Mesh.h>
#include <Engine/Model/Vertex.h>

namespace Instinct
{
	std::shared_ptr<Mesh<Vertex>> GeometryGenerator::CreateGrid( uint32_t width, uint32_t height )
	{
		//TODO: Allow this function to have choice of squares per unit
		const uint32_t vertexCount = width * height * 6;
		const uint32_t indexCount = vertexCount;
		const uint32_t verticesPerLength = ( width - 1 )  * 6;

		std::vector<Vertex> vertices( vertexCount );
		std::vector<uint32_t> indices( indexCount );

		//Generate the base vertices
		for( uint32_t j = 0u ; j < height - 1 ; ++ j )
		{
			for( uint32_t i = 0u ; i < width - 1 ; ++ i )
			{
				const uint32_t vertexBase = ( j * verticesPerLength ) + ( i * 6 );

				vertices[ vertexBase + 0 ].mPosition = XMFLOAT3( i * 1.f, 0.f, j * 1.f );
				vertices[ vertexBase + 1 ].mPosition = XMFLOAT3( i * 1.f, 0.f, j * 1.f + 1.f );
				vertices[ vertexBase + 2 ].mPosition = XMFLOAT3( i * 1.f + 1.f, 0.f, j * 1.f + 1.f );

				vertices[ vertexBase + 3 ].mPosition = XMFLOAT3( i * 1.f + 1.f, 0.f, j * 1.f );
				vertices[ vertexBase + 4 ].mPosition = XMFLOAT3( i * 1.f, 0.f, j * 1.f );
				vertices[ vertexBase + 5 ].mPosition = XMFLOAT3( i * 1.f + 1.f, 0.f, j * 1.f + 1.f );

				for( uint32_t k = 0U ; k < 6 ; ++ k )
				{
					//By default normal is up vector
					vertices[ vertexBase + k ].mNormal = XMFLOAT3( 0.f, 1.f, 0.f );

					const auto x = vertices[ vertexBase + k ].mPosition.x;
					const auto y = vertices[ vertexBase + k ].mPosition.z;
					vertices[ vertexBase + k ].mTexcoord = XMFLOAT2( x / width, y / height );
				}
			}
		}

		//Generate the base indices
		for( uint32_t i = 0u ; i < indexCount ; ++ i )
		{
			indices[ i ] = i;
		}

		auto mesh = Mesh<Vertex>::Create( vertices, indices );
		mesh->SetMaterialIndex( 0 );

		return mesh;
	}

	std::shared_ptr<Mesh<Vertex>> GeometryGenerator::CreateFace( float width )
	{
		const auto halfWidth = width / 2.f;
		std::vector<Vertex> vertices( FACE_VERTEX_COUNT );
		std::vector<uint32_t> indices( FACE_VERTEX_COUNT );

		vertices[ 0 ].mPosition = XMFLOAT3( -width, width, 0.f );
		vertices[ 0 ].mTexcoord = XMFLOAT2( 0.f, 0.f );

		vertices[ 1 ].mPosition = XMFLOAT3( width, width, 0.f );
		vertices[ 1 ].mTexcoord = XMFLOAT2( halfWidth, 0.f );

		vertices[ 2 ].mPosition = XMFLOAT3( -width, -width, 0.f );
		vertices[ 2 ].mTexcoord = XMFLOAT2( 0.f, halfWidth );

		vertices[ 3 ].mPosition = XMFLOAT3( width, width, 0.f );
		vertices[ 3 ].mTexcoord = XMFLOAT2( halfWidth, 0.f );

		vertices[ 4 ].mPosition = XMFLOAT3( width, -width, 0.f );
		vertices[ 4 ].mTexcoord = XMFLOAT2( halfWidth, halfWidth );

		vertices[ 5 ].mPosition = XMFLOAT3( -width, -width, 0.f );
		vertices[ 5 ].mTexcoord = XMFLOAT2( 0.f, halfWidth );

		for( uint32_t i = 0u ; i < FACE_VERTEX_COUNT ; ++ i )
		{
			vertices[ i ].mNormal = XMFLOAT3( 0.f, 1.f, 0.f );
			indices[ i ] = i;
		}

		auto mesh = Mesh<Vertex>::Create( vertices, indices );
		mesh->SetMaterialIndex( 0 );

		return mesh;
	}

	//Note: The original logic for generating the sphere in this function was not my own
	std::shared_ptr<Model> GeometryGenerator::CreateSphere( float radius, uint32_t sliceCount, uint32_t stackCount, float textureScale )
	{
		Vertex top( { 0.f, radius, 0.f }, { 0.f, 0.f }, { 0.f, 1.f, 0.f } );
		Vertex bottom( { 0.f, -radius, 0.f }, { 0.f, 0.f }, { 0.f, -1.f, 0.f } );

		std::vector<Vertex> vertices;
		vertices.push_back( top );

		const auto phiStep   = XM_PI / stackCount;
		const auto thetaStep = 2.f* XM_PI / sliceCount;

		//Compute vertices for each stack ring (do not count the poles as rings)
		for( uint32_t i = 1u ; i < stackCount ; ++ i )
		{
			const auto phi = i * phiStep;

			//Vertices of ring.
			for( uint32_t j = 0u ; j <= sliceCount ; ++ j )
			{
				const auto theta = j * thetaStep;

				//spherical to cartesian
				XMFLOAT3 pos, normal;
				pos.x = radius * sinf( phi ) * cosf( theta );
				pos.y = radius * cosf( phi );
				pos.z = radius * sinf( phi ) * sinf( theta );

				const auto p = XMLoadFloat3( &pos );
				XMStoreFloat3( &normal, XMVector3Normalize( p ) );

				vertices.emplace_back( pos, XMFLOAT2( theta / XM_2PI * textureScale, phi / XM_PI * textureScale ), normal );
			}
		}

		vertices.push_back( bottom );
		std::vector<uint32_t> indices;

		for( uint32_t i = 1u ; i <= sliceCount ; ++ i )
		{
			indices.push_back( 0 );
			indices.push_back( i + 1 );
			indices.push_back( i );
		}
	
		uint32_t baseIndex = 1u;
		const uint32_t ringVertexCount = sliceCount + 1;

		for( uint32_t i = 0u ; i < stackCount - 2 ; ++ i )
		{
			for( uint32_t j = 0u ; j < sliceCount ; ++ j )
			{
				indices.push_back( baseIndex + i * ringVertexCount + j );
				indices.push_back( baseIndex + i * ringVertexCount + j + 1 );
				indices.push_back( baseIndex + ( i + 1 ) * ringVertexCount + j );

				indices.push_back( baseIndex + ( i + 1 ) * ringVertexCount + j );
				indices.push_back( baseIndex + i * ringVertexCount + j + 1 );
				indices.push_back( baseIndex + ( i + 1 ) * ringVertexCount + j + 1 );
			}
		}

		//South pole vertex was added last.
		const auto southPoleIndex = static_cast<uint32_t>( vertices.size() - 1 );

		//Offset the indices to the index of the first vertex in the last ring.
		baseIndex = southPoleIndex - ringVertexCount;
	
		for( uint32_t i = 0u ; i < sliceCount ; ++ i )
		{
			indices.push_back( southPoleIndex );
			indices.push_back( baseIndex + i );
			indices.push_back( baseIndex + i + 1 );
		}

		//replicate the data in our format!
		uint32_t vertexCount, indexCount;
		vertexCount = vertices.size();
		indexCount = indices.size();

		auto mesh = Mesh<Vertex>::Create( vertices, indices );
		mesh->SetMaterialIndex( 0 );

		auto model = std::make_shared<Model>();
		model->AddModelEntry( mesh );

		return model;
	}
}
