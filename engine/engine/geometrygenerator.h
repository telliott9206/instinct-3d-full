/*
 * GeometryGenerator.h
 *
 * Created 18/03/2019
 * Written by Todd Elliott
 *
 * Provides a selection of functionality for generating geometrical objects.
 */

#ifndef _GEOMETRYGENERATOR_H
#define _GEOMETRYGENERATOR_H

#include <Instinct.h>
#include <Engine/Model/Vertex.h>
#include <Engine/Model/Mesh.h>

namespace Instinct
{
	class Terrain;
	class Model;

	class GeometryGenerator
	{
	public:
		GeometryGenerator() = delete;
		GeometryGenerator( const GeometryGenerator& ) = delete;
		GeometryGenerator& operator = ( const GeometryGenerator& ) = delete;
		~GeometryGenerator() = delete;

		DLLEXPORT static std::shared_ptr<Mesh<Vertex>> CreateGrid( uint32_t width, uint32_t height );
		DLLEXPORT static std::shared_ptr<Mesh<Vertex>> CreateFace( float width );
		DLLEXPORT static std::shared_ptr<Model> CreateSphere( float radius, uint32_t sliceCount, uint32_t stackCount, float textureScale );
	};
};

#endif
