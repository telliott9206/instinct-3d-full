/*
 * Resource.h
 *
 * Created 07/12/2018
 * Written by Todd Elliott
 *
 * A basic class to represent any kind of loadable resource such as a
 * texture, model, or a sound.
 */

#ifndef _RESOURCE_H
#define _RESOURCE_H

#include <Instinct.h>

namespace Instinct
{
	class Resource
	{
	public:
		Resource() = default;
		DLLEXPORT virtual ~Resource() = 0;

		const std::string& GetFileName() const				{ return mFileName; }
		void SetFileName( const std::string& fileName )		{ mFileName = fileName; }

	private:
		std::string mFileName;
	};
};

#endif
