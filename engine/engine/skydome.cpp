/*
 * Skydome.cpp
 *
 * Created 18/03/2019
 * Written by Todd Elliott
 */

#include <Engine/Skydome.h>
#include <Engine/GeometryGenerator.h>
#include <Engine/Model/Model.h>
#include <Engine/Texture/Texture.h>
#include <Engine/Camera.h>

namespace Instinct
{
	Skydome::Skydome( float height, const XMFLOAT4& centreColour, const XMFLOAT4& apexColour ) :
		mCentreColour( centreColour ),
		mApexColour( apexColour ),
		mHeight( height )
	{
	}

	Skydome::~Skydome()
	{
	}

	std::shared_ptr<Skydome> Skydome::Create( float scale, float height, const XMFLOAT4& centreColour, const XMFLOAT4& apexColour )
	{
		auto skydome = std::make_shared<Skydome>( height, centreColour, apexColour );
		auto model = std::shared_ptr<Model>( GeometryGenerator::CreateSphere( 4.f, 26, 26, 4.f ) );

		skydome->SetScale( XMFLOAT3( scale, scale, scale ) );

		model->Loaded();
		skydome->SetModel( model );

		return skydome;
	}

	void Skydome::Update( const std::shared_ptr<Camera>& camera )
	{
		mPosition = camera->GetPosition();
		mPosition.y += mHeight;	//TODO: test code that should be a value passed in
	}
}
