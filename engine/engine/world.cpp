/*
 * World.cpp
 *
 * Created 17/12/2018
 * Written by Todd Elliott
 */

#include "World.h"

#include <Engine/Camera.h>
#include <Engine/Ocean.h>
#include <Engine/Skydome.h>
#include <Engine/Light/DirectionalLight.h>
#include <Engine/Shaders/DetailShader.h>
#include <Engine/Shaders/WaterShader.h>
#include <Engine/Shaders/SkydomeShader.h>
#include <Engine/Shaders/DepthShader.h>
#include <Engine/Shaders/SimpleShader.h>
#include <Engine/Manager/ShaderManager.h>
#include <Engine/Manager/InstanceManager.h>
#include <Engine/Utility/MathHelper.h>
#include <Engine/Thread/ThreadPool.h>

namespace Instinct
{
	World::World() :
		mShadowMap( SHADOW_CASCADES_COUNT )
	{
	}

	World::~World()
	{
	}

	std::shared_ptr<World> World::Create()
	{
		auto world = std::make_shared<World>();
		world->GenerateCascadedShadowMap();

		return world;
	}

	void World::CreateAndGo()
	{
		auto world = World::Create();
		Engine::Instance().SetCurrentWorld( world );
	}

	void World::SetActiveCamera( std::shared_ptr<Camera> camera ) //TODO: Revise system to allow multiple cameras / viewports
	{
		mCamera = camera;

		//Freelook camera hides the cursor
		if( mCamera->GetType() == Camera::tCameraType::kFreeLook )
		{
			ShowCursor( false );
		}
	}

	void World::AddEntity( std::shared_ptr<Entity> entity )
	{
		mAllEntities.push_back( entity );
	}

	void World::Update( ID3D11DeviceContext& deviceContext, float delta )
	{
		if( mCamera != nullptr )
		{
			mCamera->Update( delta );

			if( mSkydome != nullptr )
			{
				mSkydome->Update( mCamera );
			}
		}
	}

	void World::Render( ID3D11DeviceContext& deviceContext, int renderPhase )
	{
		if( mCamera != nullptr )
		{
			auto& shaderManager = ShaderManager::Instance();

			//begin rendering //TODO: Revise implementation
			if( renderPhase == Engine::tRenderPhase::kDepth )
			{
				RenderDepthPass( deviceContext, shaderManager );
			}
			else if( renderPhase == Engine::tRenderPhase::kDiffuse )
			{
				mRenderPackage.Clear();

				const auto& view = mCamera->GetViewMatrix();
				const auto& proj = mCamera->GetProjectionMatrix();
				GenerateViewFrustum( view, proj, mCameraFrustum );	

				//start threads
				auto terrainResult = ThreadPool::Instance().Run( [ this ]
					{
						mGeometryCellCollectionBuilder.BuildCollection( mCamera, mTerrain, { &mCameraFrustum }, mRenderPackage.mTerrainGeometryCellList );
					} );

				auto oceanResult = ThreadPool::Instance().Run( [ this ] 
					{ 
						mGeometryCellCollectionBuilder.BuildCollection( mCamera, mOcean, { &mCameraFrustum }, mRenderPackage.mOceanGeometryCellList );
						mOcean->Update( 1.f, mRenderPackage.mOceanGeometryCellList );
					} );

				auto entityResult = ThreadPool::Instance().Run( [ this ]
					{
						mEntityCollectionBuilder.BuildCollection( mCameraFrustum, mAllEntities, mRenderPackage );
					} );

				JOIN( oceanResult );
				JOIN( terrainResult );
				JOIN( entityResult );

				//render diffuse
				RenderDiffusePass( deviceContext, shaderManager );
			}
		}
	}

	void World::RenderDepthPass( ID3D11DeviceContext& deviceContext, ShaderManager& shaderManager )
	{
		auto& shader = shaderManager.GetDepthShader();

		mLight->Update( Engine::Instance().GetContext() );
		deviceContext.IASetPrimitiveTopology( D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST );

		shader.Bind( deviceContext );
		for( auto i = 0 ; i < SHADOW_CASCADES_COUNT ; ++ i )
		{
			mRenderPackage.Clear();
			mShadowMap[ i ]->SetRenderTarget( deviceContext );
			mShadowMap[ i ]->ClearRenderTarget( deviceContext );

			const auto& lightView = mLight->GetViewMatrix( i );
			const auto& lightProj = mLight->GetProjectionMatrix( i );
			GenerateViewFrustum( lightView, lightProj, mLightFrustum );	

			//start threads
			auto terrainResult = ThreadPool::Instance().Run( [ this ] 
				{ 
					mGeometryCellCollectionBuilder.BuildCollection( mCamera, mTerrain, { &mCameraFrustum, &mLightFrustum }, mRenderPackage.mTerrainGeometryCellList );
				} );

			auto entityResult = ThreadPool::Instance().Run( [ this ] 
				{
					mEntityCollectionBuilder.BuildCollection( mCameraFrustum, mAllEntities, mRenderPackage );
				} );

			//join threads
			JOIN( terrainResult );
			JOIN( entityResult );

			for( const auto entity : mRenderPackage.mEntityList )
			{
				StageEntityForInstancing( entity );
			}

			InstanceManager::Instance().RenderInstancedModels( deviceContext, Engine::tRenderPhase::kDepth, i );

			if( mTerrain != nullptr )
			{
				shader.RenderTerrain( deviceContext, mTerrain, mCamera, mRenderPackage.mTerrainGeometryCellList, i );
			}
		}
	}

	void World::RenderDiffusePass( ID3D11DeviceContext& deviceContext, ShaderManager& shaderManager )
	{
		auto context = Engine::Instance().GetContext();

		//The topolgoy is never changed anywhere else
		deviceContext.IASetPrimitiveTopology( D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST );

		auto& basicShader = shaderManager.GetBasicShader();
		auto& simpleShader = shaderManager.GetSimpleShader();
		auto& detailShader = shaderManager.GetDetailShader();
		auto& waterShader = shaderManager.GetWaterShader();

		if( mAllEntities.size() > 0 )
		{
			//TODO: This should be done in the shader
			//Bind the shadow maps as a resource to the diffuse shader
			for( auto i = 0 ; i < SHADOW_CASCADES_COUNT ; ++ i )
			{
				auto* resource = mShadowMap[ i ]->GetShaderResourceView();
				deviceContext.PSSetShaderResources( TERRAINSHADER_PS_Offset_ShadowMaps + i, 1, &resource );	//TODO: Check the resource offset here? Is this right?
			}
		
			//----------------------------------------------------------------------------------------------------------------------------
			//TODO: Test code only! used for displaying bounding boxes for entities
			for( const auto& entity : mAllEntities )
			{
				if( !entity->GetModel()->IsModelLoaded() )
				{
					continue;	//dont render if the model has not finished loading
				}

				if( entity->GetModel()->IsBoundingBoxVisible() )
				{
					simpleShader.Bind( deviceContext );

					context->SetEngineRasterState( Context::tRasterState::kWireframeNoCull );
					simpleShader.Render( deviceContext, entity );
					context->SetEngineRasterState( Context::tRasterState::kCullBack );
				}
			}
			//----------------------------------------------------------------------------------------------------------------------------
		
			//Check all visible entities for instancing
			basicShader.Bind( deviceContext );
			for( const auto entity : mRenderPackage.mEntityList )
			{
				StageEntityForInstancing( entity );
			}

			//Render all instanced models
			context->SetBlendState( Context::tBlendState::kAlphaToCoverage );
			basicShader.Bind( deviceContext );
			InstanceManager::Instance().RenderInstancedModels( deviceContext, Engine::tRenderPhase::kDiffuse );

			context->SetBlendState( Context::tBlendState::kDisabled );
		}

		//Render the skydome if it exists
		if( mSkydome != nullptr )
		{
			context->SetEngineRasterState( Context::tRasterState::kCullFront );
			shaderManager.GetSkydomeShader().Render( deviceContext, mSkydome );
			context->SetEngineRasterState( Context::tRasterState::kCullBack );
		}

		context->SetEngineRasterState( context->GetRasterState() );

		//Bind the shadow cascades as textures for the pixel shader
		for( auto i = 0 ; i < SHADOW_CASCADES_COUNT ; ++ i )
		{
			auto* resource = mShadowMap[ i ]->GetShaderResourceView();
			deviceContext.PSSetShaderResources( 1 + i, 1, &resource );
		}

		/*
		//----------------------------------------------------------------------------------------------------------------------------
		//TODO: Test code only! used for displaying bounding boxes for all terrain cells
		context->SetEngineRasterState( Context::tRasterState::kWireframeNoCull );
		simpleShader->Bind( deviceContext );
		for( const auto& cell : mRenderPackage.mTerrainGeometryCellList )
		{
			simpleShader->Render( deviceContext, cell );
		}
		context->SetEngineRasterState( Context::tRasterState::kCullBack );
		//----------------------------------------------------------------------------------------------------------------------------
		*/

		//render the terrain if it exists
		if( mTerrain != nullptr )
		{
			detailShader.Bind( deviceContext );
			detailShader.RenderGeometry( deviceContext, mTerrain, mCamera, mRenderPackage.mTerrainGeometryCellList );
		}

		//render the ocean if it exists
		if( mOcean != nullptr )
		{
			context->SetBlendState( Context::tBlendState::kAlphaToCoverage );
			waterShader.RenderGeometry( deviceContext, mOcean, mCamera, mRenderPackage.mOceanGeometryCellList );
			context->SetBlendState( Context::tBlendState::kDisabled );
		}

		context->SetEngineRasterState( Context::tRasterState::kCullBack );
	}

	void World::StageEntityForInstancing( std::shared_ptr<Entity> entity )
	{
		auto model = entity->GetModel();
		InstanceManager::Instance().AddEntityToInstanceMap( model->GetFileName(), entity );
	}

	void World::GenerateViewFrustum( const XMMATRIX& view, const XMMATRIX& proj, DirectX::BoundingFrustum& frustumOut )
	{
		DirectX::BoundingFrustum::CreateFromMatrix( frustumOut, proj );
		DirectX::XMMATRIX matInverse = DirectX::XMMatrixInverse( nullptr, view );

		frustumOut.Transform( frustumOut, matInverse );
	}

	void World::GenerateCascadedShadowMap()
	{
		auto context = Engine::Instance().GetContext();

		//TODO: Move all of these values to Settings class, perhaps make the values configurable also
		const uint32_t qualityValues[ 4 ][ 3 ] = 
		{
			{ 512, 512, 512		},
			{ 1024, 1024, 1024	},
			{ 2048, 2048, 2048	},
			{ 4096, 4096, 4096	}
		};

		const auto index = context->GetShadowQuality();

		uint32_t quality[ SHADOW_CASCADES_COUNT ];

		//TODO: Tweak and fix quality settings
		quality[ 0 ] = qualityValues[ index ][ 0 ];
		quality[ 1 ] = qualityValues[ index ][ 0 ];
		quality[ 2 ] = qualityValues[ index ][ 1 ];

		for( auto i = 0 ; i < SHADOW_CASCADES_COUNT ; ++ i )
		{
			mShadowMap[ i ] = Texture2D::Create( quality[ i ], quality[ i ] );
		}
	}
}
