/*
 * Ocean.h
 *
 * Created 08/03/2019
 * Written by Todd Elliott
 *
 * A specialization of the Terrain class, with the added functionality of
 * vertex manipulation via waves.
 */

#ifndef _OCEAN_H
#define _OCEAN_H

#include <Instinct.h>
#include <Engine/terrain/terrain.h>

namespace Instinct
{
	class Ocean : public Terrain
	{
	public:
		explicit Ocean( int width, int height, float waterLevel, int unitsPerSquare );
		~Ocean();

		DLLEXPORT static std::shared_ptr<Ocean> Create( int width, int height, float waterLevel, int unitsPerSquare );

		void Update( float delta, std::vector<std::shared_ptr<GeometryCell>>& geometryCells );
		DLLEXPORT float GetWaterLevel() const { return mWaterLevel; }

	private:
		float mWaterLevel;
	};
};

#endif
