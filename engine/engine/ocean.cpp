/*
 * Ocean.cpp
 *
 * Created 01/03/2019
 * Written by Todd Elliott
 */

#include <Engine/Ocean.h>
#include <Engine/GeometryGenerator.h>

namespace Instinct
{
	Ocean::Ocean( int width, int height, float waterLevel, int unitsPerSquare ) :
		Terrain( width, height, unitsPerSquare ),
		mWaterLevel( waterLevel )
	{
	}

	Ocean::~Ocean()
	{
	}

	std::shared_ptr<Ocean> Ocean::Create( int width, int height, float waterLevel, int unitsPerSquare )
	{
		auto ocean = std::make_shared<Ocean>( width, height, waterLevel, unitsPerSquare );
		auto grid = GeometryGenerator::CreateGrid( width, height );

		//TODO: Fix this these values, these should not have to be calculated here, seems wrong. Also should depend on units per square
		ocean->mVerticesPerLength = width * VERTICES_PER_SQUARE;
		ocean->mVertexCount = width * height * VERTICES_PER_SQUARE;

		ocean->mModels.push_back( grid );

		ocean->GenerateTerrainCells( false );
		ocean->Loaded();

		return ocean;
	}

	void Ocean::Update( float deltaTime, std::vector<std::shared_ptr<GeometryCell>>& geometryCells )
	{
		for( const auto& cell : geometryCells )
		{
			cell->ApplyWaves();
		}
	}
}
