/*
 * Billboard.h
 *
 * Created 22/03/2019
 * Written by Todd Elliott
 *
 * A model that only requires 6 vertices. It has a texture rendered to it that is representative of a complex model.
 * This is used in place of the complex model at far distances from the camera when the level of detail is simply
 * not required. It always faces the camera so the texture is visible at all times when in view.
 */

#ifndef _BILLBOARD_H
#define _BILLBOARD_H

#include <Instinct.h>
#include <Engine/Entity/Entity.h>

namespace Instinct
{
	class Texture;
	class Camera;
	
	class Billboard: public Entity
	{
	public:
		Billboard();
		~Billboard();

		/* @brief	factory function for instantiating a billboard object
		 *
		 * @return	shared_ptr to newly instantiated billboard object
		 */
		DLLEXPORT static std::shared_ptr<Billboard> Create( std::shared_ptr<Texture> texture, bool verticalAxisOnly );

		XMMATRIX CalculateWorldMatrix() const override;

	private:
		bool mVerticalAxisOnly;
	};
};

#endif
