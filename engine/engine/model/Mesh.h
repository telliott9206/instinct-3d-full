/*
 * Mesh.h
 *
 * Created 18/11/2021
 * Written by Todd Elliott
 *
 * Templated class of any type of vertex (2D, 3D, and other provided types). Stores the vertices and buffers of
 * any given mesh loaded into the engine.
 */

#ifndef _MESH_H
#define _MESH_H

#include <Engine/Model/Renderable.h>

namespace Instinct
{
	template<class T> class Mesh : public Renderable<T>
	{
	public:
		Mesh( const std::vector<T>& vertices, const std::vector<uint32_t>& indices );
		~Mesh();

		static std::shared_ptr<Mesh> Create( const std::vector<T>& vertices, const std::vector<uint32_t>& indices );

		virtual void BindBuffers( ID3D11DeviceContext& deviceContext );

		void CalculateNormals();
		void UpdateVertices( ID3D11DeviceContext& deviceContext, T* vertices );

		void SetMaterialIndex( int index )	{ mMaterialIndex = index; }
		int GetMaterialIndex() const		{ return mMaterialIndex; }

	private:
		int mMaterialIndex;
	};

	template<class T>
	Mesh<T>::Mesh( const std::vector<T>& vertices, const std::vector<uint32_t>& indices ) :
		Renderable<T>( vertices, indices )
	{
	}

	template<class T>
	Mesh<T>::~Mesh()
	{
	}

	template<class T>
	std::shared_ptr<Mesh<T>> Mesh<T>::Create( const std::vector<T>& vertices, const std::vector<uint32_t>& indices )
	{
		auto mesh = std::make_unique<Mesh<T>>( vertices, indices );

		mesh->CreateVertexBuffer( D3D11_USAGE_DYNAMIC, D3D11_CPU_ACCESS_WRITE );
		mesh->CreateIndexBuffer();

		return mesh;
	}

	template<class T>
	void Mesh<T>::BindBuffers( ID3D11DeviceContext& deviceContext )
	{
		uint32_t stride = sizeof( T );
		uint32_t offset = 0u;

		deviceContext.IASetVertexBuffers( 0, 1, &this->mVertexBuffer, &stride, &offset );
		deviceContext.IASetIndexBuffer( this->mIndexBuffer, DXGI_FORMAT_R32_UINT, 0 );
	}

	template<class T>
	void Mesh<T>::CalculateNormals()
	{
		XMFLOAT3 vertex1, vertex2, vertex3, vector1, vector2, normal;
		std::vector<XMFLOAT3> normals( this->mVertexCount );

		for( uint32_t i = 0u ; i < this->mVertexCount ; i += 6 )
		{
			//find the 3 vertices
			vertex1 = this->mVertices[ i + 0 ].vPosition;
			vertex2 = this->mVertices[ i + 1 ].vPosition;
			vertex3 = this->mVertices[ i + 2 ].vPosition;

			//find the 2 vectors
			vector1.x = vertex1.x - vertex3.x;
			vector1.y = vertex1.y - vertex3.y;
			vector1.z = vertex1.z - vertex3.z;

			vector2.x = vertex3.x - vertex2.x;
			vector2.y = vertex3.y - vertex2.y;
			vector2.z = vertex3.z - vertex2.z;

			//calculate the normal
			normal.x = ( vector1.y * vector2.z ) - ( vector1.z * vector2.y );
			normal.y = ( vector1.z * vector2.x ) - ( vector1.x * vector2.z );
			normal.z = ( vector1.x * vector2.y ) - ( vector1.y * vector2.x );

			//assign the normal
			for( uint32_t j = 0u ; j < 6 ; j ++ )
			{
				this->mVertices[ i + j ].vNormal = normal;
			}
		}
	}

	template<class T>
	void Mesh<T>::UpdateVertices( ID3D11DeviceContext& deviceContext, T* vertices )
	{
		D3D11_MAPPED_SUBRESOURCE resource;

		deviceContext.Map( this->mVertexBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &resource );
		memcpy( resource.pData, vertices, sizeof( T ) * this->mVertexCount );
		deviceContext.Unmap( this->mVertexBuffer, 0 );
	}
}

#endif
