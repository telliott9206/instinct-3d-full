/*
 * Renderable.h
 *
 * Created 31/10/2021
 * Written by Todd Elliott
 *
 * Templated class of type T being any type of vertex struct. Any object class that can be rendered
 * should inheret this class.
 */

#ifndef _RENDERABLE_H
#define _RENDERABLE_H

#include <Instinct.h>
#include <Engine.h>

namespace Instinct
{
	template<class T> class Renderable
	{
	public:
		Renderable( const std::vector<T>& vertices, const std::vector<uint32_t>& indices );
		~Renderable();

		bool CreateVertexBuffer( D3D11_USAGE usage, int cpuAccessFlag );
		bool CreateIndexBuffer();

		std::vector<T>& GetVertices() 					{ return mVertices; }	//TODO: Should not be const so it can be modified ... is there a better way here?
		const std::vector<uint32_t>& GetIndices() const	{ return mIndices; }

		uint32_t GetVertexCount() const					{ return mVertexCount; }
		uint32_t GetIndexCount() const					{ return mIndexCount; }

		ID3D11Buffer* GetVertexBuffer()					{ return mVertexBuffer; }
		ID3D11Buffer* GetIndexBuffer() 					{ return mIndexBuffer; }

	protected:
		uint32_t				mVertexCount;
		uint32_t				mIndexCount;

		std::vector<T>			mVertices;
		std::vector<uint32_t>	mIndices;
		
		ID3D11Buffer*			mVertexBuffer;
		ID3D11Buffer*			mIndexBuffer;
	};

	template<class T>
	Renderable<T>::Renderable( const std::vector<T>& vertices, const std::vector<uint32_t>& indices ) :
		mVertexCount( vertices.size() ),
		mIndexCount( indices.size() ),
		mVertices( std::move( vertices ) ),
		mIndices( std::move( indices ) ),
		mVertexBuffer( nullptr ),
		mIndexBuffer( nullptr )
	{
	}

	template<class T>
	Renderable<T>::~Renderable()
	{
		ReleaseCOM( mVertexBuffer );
		ReleaseCOM( mIndexBuffer );
	}

	template<class T>
	bool Renderable<T>::CreateVertexBuffer( D3D11_USAGE usage, int cpuAccessFlag )
	{
		D3D11_BUFFER_DESC desc;
		ZeroMemory( &desc, sizeof( desc ) );

		desc.Usage = usage;
		desc.ByteWidth = sizeof( T ) * mVertexCount;
		desc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
		desc.CPUAccessFlags = cpuAccessFlag;
		desc.MiscFlags = 0;

		D3D11_SUBRESOURCE_DATA data;

		ZeroMemory( &data, sizeof( data ) );
		data.pSysMem = &mVertices[ 0 ];

		auto& device = Engine::Instance().GetContext()->GetDevice();
		if( FAILED( device.CreateBuffer( &desc, &data, &mVertexBuffer ) ) )	
		{
			Engine::Error( d3derr, "Failed to create model vertex buffer" );
			return false;
		}

		return true;
	}

	template<class T>
	bool Renderable<T>::CreateIndexBuffer()
	{
		D3D11_BUFFER_DESC desc;
		ZeroMemory( &desc, sizeof( desc ) );

		desc.Usage = D3D11_USAGE_DEFAULT;
		desc.ByteWidth = sizeof( uint32_t ) * mIndexCount;
		desc.BindFlags = D3D11_BIND_INDEX_BUFFER;
		desc.CPUAccessFlags = 0;
		desc.MiscFlags = 0;

		D3D11_SUBRESOURCE_DATA data;
		data.pSysMem = &mIndices[ 0 ];

		auto& device = Engine::Instance().GetContext()->GetDevice();
		if( FAILED( device.CreateBuffer( &desc, &data, &mIndexBuffer ) ) )
		{
			Engine::Error( d3derr, "Failed to create model index buffer" );
			return false;
		}

		return true;
	}
}

#endif
