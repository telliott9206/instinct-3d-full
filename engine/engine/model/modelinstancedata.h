/*
 * ModelInstanceData
 *
 * Created 26/03/2020
 * Written by Todd Elliott
 *
 * Contains the structs used for geometry instancing.
 */

#ifndef _MODELINSTANCEDATA_H
#define _MODELINSTANCEDATA_H

#include <Instinct.h>

namespace Instinct
{
	ALIGN16 struct BasicInstanceData
	{
		OPERATORS_ALIGN16;

		BasicInstanceData( const XMMATRIX& world ) :
			mWorldMatrix( world )
		{
		}

		XMMATRIX mWorldMatrix;
	};

	ALIGN16 struct ModelInstanceData
	{
		OPERATORS_ALIGN16;

		ModelInstanceData( const XMMATRIX& world, float alpha, float reflectivePower, float specularPower, bool lightAffected, bool sunSource ) :
			mBasicInstanceData( world ),
			mAlpha( alpha ),
			mReflectivePower( reflectivePower ),
			mSpecularPower( specularPower ),
			mIsLightAffected( lightAffected ),
			mSunSource( sunSource )
		{
		}

		BasicInstanceData	mBasicInstanceData;
		float				mAlpha;
		float				mReflectivePower;
		float				mSpecularPower;
		bool				mIsLightAffected;
		bool				mSunSource;
	};
};

#endif
