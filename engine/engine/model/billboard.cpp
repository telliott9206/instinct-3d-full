/*
 * Billboard.cpp
 *
 * Created 22/03/2019
 * Written by Todd Elliott
 */

#include "Billboard.h"
#include <Engine/World.h>
#include <Engine/Camera.h>
#include <Engine/Model/Model.h>
#include <Engine/Texture/Texture.h>
#include <Engine/Model/Visualboundingbox.h>
#include <Engine/GeometryGenerator.h>

namespace Instinct
{
	Billboard::Billboard() :
		mVerticalAxisOnly( false )
	{
	}

	Billboard::~Billboard()
	{
	}

	std::shared_ptr<Billboard> Billboard::Create( std::shared_ptr<Texture> texture, bool verticalAxisOnly )
	{
		auto billboard = std::make_shared<Billboard>();
		auto model = std::make_shared<Model>();
		model->AddDiffuseTexture( texture );

		auto mesh = GeometryGenerator::CreateFace( 2.f ); //Size shouldn't really matter here? Scale can always be changed if necessary??
		mesh->SetMaterialIndex( 0 );

		model->AddModelEntry( mesh );
		billboard->SetModel( model );
		billboard->SetLightAffected( false );

		billboard->mVerticalAxisOnly = verticalAxisOnly;
		model->Loaded();

		return billboard;
	}

	XMMATRIX Billboard::CalculateWorldMatrix() const
	{
		const auto camera		= Engine::Instance().GetCurrentWorld()->GetActiveCamera();
		const auto& camPos		= camera->GetPosition();

		//Calculate the rotation that needs to be applied to the billboard model to face the current camera position using the arc tangent function.
		const auto angle		= atan2( mPosition.x - camPos.x, mPosition.z - camPos.z ) * ( 180.f / XM_PI );
		const auto rotation		= angle * RADIAN;

		const auto world		= XMMatrixRotationY( rotation );
		const auto translate	= XMMatrixTranslation( mPosition.x, mPosition.y, mPosition.z );
		const auto scale		= XMMatrixScaling( mScale.x, mScale.y, mScale.z );

		if( !mVerticalAxisOnly )
		{
			const auto angle2 = atan2( mPosition.z - camPos.z, mPosition.y - camPos.y ) * ( 180.f / XM_PI );
			const auto angle3 = atan2( mPosition.z - camPos.z, mPosition.y - camPos.y ) * ( 180.f / XM_PI );

			const auto rotation2 = angle2 * RADIAN;
			const auto rotation3 = angle3 * RADIAN;

			const auto world2 = XMMatrixRotationX( rotation2 );
			const auto world3 = XMMatrixRotationZ( rotation3 );

			return ( XMMatrixIdentity() * scale * world * world2 * world3 * translate );
		}

		return ( XMMatrixIdentity() * scale * world * translate );
	}
}
