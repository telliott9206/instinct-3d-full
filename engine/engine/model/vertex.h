/*
 * Vertex.h
 *
 * Created 06/12/2018
 * Written by Todd Elliott
 *
 * Contains all vertex type structs used for rendering.
 */

#ifndef _VERTEX_H
#define _VERTEX_H

#include <Instinct.h>
#include <fstream>

namespace Instinct
{
	struct Vertex
	{
		XMFLOAT3 mPosition;
		XMFLOAT2 mTexcoord;
		XMFLOAT3 mNormal;

		Vertex() = default;
		Vertex( const XMFLOAT3& position, const XMFLOAT2& texcoord, const XMFLOAT3& normal ) :
			mPosition( position ),
			mTexcoord( texcoord ),
			mNormal( normal )
		{
		}

		friend std::ofstream& operator << ( std::ofstream& ofs, const Vertex& vertex )
		{
			ofs << vertex.mPosition.x << " " << vertex.mPosition.y << " " << vertex.mPosition.z << " " << 
				vertex.mNormal.x << " " << vertex.mNormal.y << " " << vertex.mNormal.z;

			return ofs;
		}

		friend std::ifstream& operator >> ( std::ifstream& ifs, Vertex& vertex )
		{
			ifs >> vertex.mPosition.x >> vertex.mPosition.y >> vertex.mPosition.z >> 
				vertex.mNormal.x >> vertex.mNormal.y >> vertex.mNormal.z;

			return ifs;
		}
	};

	struct SimpleVertex
	{
		XMFLOAT3 mPosition;
		XMFLOAT4 mColour;
	};

	struct WaterVertex
	{
		XMFLOAT3 mPosition;
		XMFLOAT2 mTexcoord;
		XMFLOAT3 mNormal;
		float mTerrainHeight;
	};

	struct Vertex2D
	{
		XMFLOAT3 mPosition;
		XMFLOAT2 mTexcoord;
	};
};

#endif