/*
* VisualBoundingBox.cpp
*
* Created 28/10/2021
* Written by Todd Elliott
*/

#include "VisualBoundingBox.h"

namespace Instinct
{
	VisualBoundingBox::VisualBoundingBox( const DirectX::BoundingBox& boundingBox, const std::vector<SimpleVertex>& vertices, const std::vector<uint32_t>& indices ) :
		Model(),
		Renderable<SimpleVertex>( vertices, indices )
	{
	}

	VisualBoundingBox::~VisualBoundingBox()
	{
	}

	std::unique_ptr<VisualBoundingBox> VisualBoundingBox::Create( const DirectX::BoundingBox& boundingBox )
	{
		//TODO: This is just a budget bounding box right now, create a real one that doesn't look like trash

		std::vector<SimpleVertex> vertices( VISUAL_BOUNDING_BOX_VERTEX_COUNT );
		std::vector<uint32_t> indices( VISUAL_BOUNDING_BOX_VERTEX_COUNT );

		const auto extents = boundingBox.Extents;
		const auto center = boundingBox.Center;

		//Top face
		vertices[ 0 ].mPosition = { center.x - extents.x, center.y + extents.y, center.z - extents.z };
		vertices[ 1 ].mPosition = { center.x - extents.x, center.y + extents.y, center.z + extents.z };
		vertices[ 2 ].mPosition = { center.x + extents.x, center.y + extents.y, center.z - extents.z };
		vertices[ 3 ].mPosition = vertices[ 2 ].mPosition;
		vertices[ 4 ].mPosition = vertices[ 1 ].mPosition;
		vertices[ 5 ].mPosition = { center.x + extents.x, center.y + extents.y, center.z + extents.z };

		//Bottom face
		vertices[ 6 ].mPosition = { center.x - extents.x, center.y - extents.y, center.z - extents.z };
		vertices[ 7 ].mPosition = { center.x - extents.x, center.y - extents.y, center.z + extents.z };
		vertices[ 8 ].mPosition = { center.x + extents.x, center.y - extents.y, center.z - extents.z };
		vertices[ 9 ].mPosition = vertices[ 8 ].mPosition;
		vertices[ 10 ].mPosition = vertices[ 7 ].mPosition;
		vertices[ 11 ].mPosition = { center.x + extents.x, center.y - extents.y, center.z + extents.z };

		//Front face (runs along X axis)
		vertices[ 12 ].mPosition = { center.x - extents.x, center.y + extents.y, center.z + extents.z };
		vertices[ 13 ].mPosition = { center.x - extents.x, center.y - extents.y, center.z + extents.z };
		vertices[ 14 ].mPosition = { center.x + extents.x, center.y + extents.y, center.z + extents.z };
		vertices[ 15 ].mPosition = vertices[ 14 ].mPosition;
		vertices[ 16 ].mPosition = vertices[ 13 ].mPosition;
		vertices[ 17 ].mPosition = { center.x + extents.x, center.y - extents.y, center.z + extents.z };

		//Back face (runs along X axis)
		vertices[ 23 ].mPosition = { center.x - extents.x, center.y + extents.y, center.z - extents.z };
		vertices[ 22 ].mPosition = { center.x - extents.x, center.y - extents.y, center.z - extents.z };
		vertices[ 21 ].mPosition = { center.x + extents.x, center.y + extents.y, center.z - extents.z };
		vertices[ 20 ].mPosition = vertices[ 21 ].mPosition;
		vertices[ 19 ].mPosition = vertices[ 22 ].mPosition;
		vertices[ 18 ].mPosition = { center.x + extents.x, center.y - extents.y, center.z - extents.z };

		//Left face (runs along Z axis)
		vertices[ 24 ].mPosition = { center.x - extents.x, center.y + extents.y, center.z - extents.z };
		vertices[ 25 ].mPosition = { center.x - extents.x, center.y - extents.y, center.z - extents.z };
		vertices[ 26 ].mPosition = { center.x - extents.x, center.y + extents.y, center.z + extents.z };
		vertices[ 27 ].mPosition = vertices[ 26 ].mPosition;
		vertices[ 28 ].mPosition = vertices[ 25 ].mPosition;
		vertices[ 29 ].mPosition = { center.x - extents.x, center.y - extents.y, center.z + extents.z };

		//Right face (runs along Z axis)
		vertices[ 35 ].mPosition = { center.x + extents.x, center.y + extents.y, center.z - extents.z };
		vertices[ 34 ].mPosition = { center.x + extents.x, center.y - extents.y, center.z - extents.z };
		vertices[ 33 ].mPosition = { center.x + extents.x, center.y + extents.y, center.z + extents.z };
		vertices[ 32 ].mPosition = vertices[ 33 ].mPosition;
		vertices[ 31 ].mPosition = vertices[ 34 ].mPosition;
		vertices[ 30 ].mPosition = { center.x + extents.x, center.y - extents.y, center.z + extents.z };

		for( uint32_t i = 0u ; i < VISUAL_BOUNDING_BOX_VERTEX_COUNT ; ++ i )
		{
			indices[ i ] = i;
			vertices[ i ].mColour = { 1.f, 0.f, 0.f, 1.f };
		}

		auto box = std::make_unique<VisualBoundingBox>( boundingBox, vertices, indices );
		box->CreateVertexBuffer( D3D11_USAGE_DYNAMIC, D3D11_CPU_ACCESS_WRITE );
		box->CreateIndexBuffer();
		box->mIsLoaded = true;	//DO NOT USE Loaded() here or it will cause a stack overflow!

		return box;
	}

	void VisualBoundingBox::Render( ID3D11DeviceContext& deviceContext )
	{
		if( !mIsLoaded )
		{
			return;
		}

		uint32_t stride = sizeof( SimpleVertex );
		uint32_t offset = 0;

		deviceContext.IASetVertexBuffers( 0, 1, &mVertexBuffer, &stride, &offset );
		deviceContext.IASetIndexBuffer( mIndexBuffer, DXGI_FORMAT_R32_UINT, 0 );

		deviceContext.DrawIndexed( mVertexCount, 0, 0 );
	}
}
