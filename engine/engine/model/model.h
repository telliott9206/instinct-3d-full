/*
 * Model.h
 *
 * Created 07/12/2018
 * Written by Todd Elliott
 *
 * Contains any number of meshes, when altogether, makes up the model.
 * Also contains all required texture information for each of the meshes/submodels it contains.
 */

#ifndef _MODEL_H
#define _MODEL_H

#include <Instinct.h>
#include <Engine.h>
#include <Engine/Resource.h>
#include <Engine/Model/Mesh.h>
#include <Engine/Model/Vertex.h>
#include <Engine/Model/Material.h>

namespace Instinct
{
	class Texture;
	class Billboard;
	class VisualBoundingBox;

	class Model : public Resource
	{
	public:
		typedef enum
		{
			kDiffuse,
			kNormal
		}
		tTextureIndex;

	public:
		Model();
		DLLEXPORT virtual ~Model();

		/* @brief	factory function for instantiating a Model object 
		 *
		 * @return	shared ptr to newly instantiated model object
		 */
		DLLEXPORT static std::shared_ptr<Model> Load( const std::string& strFile );
		
		DLLEXPORT virtual void Render( ID3D11DeviceContext& deviceContext ); //TODO: Why does this need to be exported!?!?!?
		void Render( ID3D11DeviceContext& deviceContext, uint32_t instanceCount );

		void RenderTerrainCell( ID3D11DeviceContext& deviceContext );

		DLLEXPORT virtual void Loaded();
		DLLEXPORT virtual void CalculateBoundingBox();

		DLLEXPORT void AddMaterial( const Material& material );
		DLLEXPORT void AddDiffuseTexture( std::shared_ptr<Texture> texture );
		DLLEXPORT void AddNormalTexture( std::shared_ptr<Texture> texture );
		DLLEXPORT void AddSpecularTexture( std::shared_ptr<Texture> texture );

		DLLEXPORT const std::vector<std::shared_ptr<Texture>>& GetDiffuseTextureList() const	{ return mDiffuseTextures; }
		DLLEXPORT const std::vector<std::shared_ptr<Texture>>& GetNormalTextureList() const		{ return mNormalTextures; }
		DLLEXPORT int GetModelEntriesCount() const												{ return mModels.size(); }

		DLLEXPORT const std::vector<std::shared_ptr<Mesh<Vertex>>>& GetEntryList() const		{ return mModels; }
		DLLEXPORT void AddModelEntry( std::shared_ptr<Mesh<Vertex>> entry )						{ mModels.push_back( entry ); }

		DLLEXPORT bool IsModelLoaded() const								{ return mIsLoaded; }
		int GetInstanceCount() const										{ return mInstanceCount; }

		void IncrementInstanceCount()										{ mInstanceCount ++; }
		void DecrementInstanceCount()										{ if( mInstanceCount > 0 ) mInstanceCount --; }

		DLLEXPORT void ShowBoundingBox()									{ mShowBoundingBox = true; }
		DLLEXPORT void HideBoundingBox()									{ mShowBoundingBox = false; }
		DLLEXPORT bool IsBoundingBoxVisible() const							{ return mShowBoundingBox; }
		VisualBoundingBox* GetVisualBoundingBox() const						{ return mBoundingBox.get(); }

		const DirectX::BoundingBox& GetAABB() const							{ return mAabb; }

	private:
		static void LoadAsync( const std::string& file, Model* model );

	protected:
		std::vector<std::shared_ptr<Mesh<Vertex>>>	mModels;
		bool										mIsLoaded;
		DirectX::BoundingBox						mAabb;

	private:
		std::vector<Material>						mMaterials;

		//TODO: Try and store these textures in a simpler way
		std::vector<std::shared_ptr<Texture>>		mDiffuseTextures;
		std::vector<std::shared_ptr<Texture>>		mNormalTextures;
		std::vector<std::shared_ptr<Texture>>		mSpecularTextures;

		std::shared_ptr<Billboard>					mBillboard;
		float										mBillboardDistance;

		int											mNumEntries;
		int											mInstanceCount;

		std::unique_ptr<VisualBoundingBox>			mBoundingBox;
		bool										mShowBoundingBox;
	};
}

#endif
