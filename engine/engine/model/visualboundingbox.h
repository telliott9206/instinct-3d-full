/*
 * VisualBoundingBox.h
 *
 * Created 28/10/2021
 * Written by Todd Elliott
 *
 * A specialized model type class conttains a bounding box model for another model class that
 * aggregrates this class as a member.
 */

#ifndef _VISUALBOUNDINGBOX_H
#define _VISUALBOUNDINGBOX_H

#include <Engine/Model/Model.h>
#include <Engine/Model/Vertex.h>
#include <Engine/Model/Renderable.h>

#define VISUAL_BOUNDING_BOX_VERTEX_COUNT	36

namespace Instinct
{
	class VisualBoundingBox : public Model, public Renderable<SimpleVertex>
	{
	public:
		explicit VisualBoundingBox( const DirectX::BoundingBox& boundingBox, const std::vector<SimpleVertex>& vertices, const std::vector<uint32_t>& indices );
		~VisualBoundingBox();

		static std::unique_ptr<VisualBoundingBox> Create( const DirectX::BoundingBox& boundingBox );

		void Render( ID3D11DeviceContext& deviceContext ) override;
	};
}

#endif
