/*
 * Model.cpp
 *
 * Created 07/12/2018
 * Written by Todd Elliott
 */

#include <Engine/Model/Model.h>
#include <Engine/Texture/Texture.h>
#include <Engine/Utility/Util.h>
#include <Engine/Utility/AssimpHelper.h>
#include <Engine/Model/Mesh.h>
#include <Engine/Manager/ResourceManager.h>
#include <Engine/Model/ModelInstanceData.h>
#include <Engine/Model/VisualBoundingBox.h>
#include <Engine/Thread/ThreadPool.h>
#include <Instinct.h>
#include <Engine.h>

namespace Instinct
{
	Model::Model() :
		mIsLoaded( false ),
		mBillboard( nullptr ),
		mBillboardDistance( -1.f ),
		mNumEntries( 0 ),
		mInstanceCount( 0 ),
		mShowBoundingBox( false )
	{
	}

	Model::~Model()
	{
	}

	std::shared_ptr<Model> Model::Load( const std::string& file )
	{
		auto model = std::make_shared<Model>();
		model->mIsLoaded = false;

		//Pass in a raw ptr; once the model has been loaded, the original raw ptr will point to the model and mLoaded will be set
		ThreadPool::Instance().Run( [ file, model ]
		{
			Model::LoadAsync( file, model.get() );
		} );

		return model;
	}

	void Model::Render( ID3D11DeviceContext& deviceContext )
	{
		if( !mIsLoaded )
		{ 
			return;
		}

		const auto numTextures = mDiffuseTextures.size();
		for( const auto& mesh : mModels )
		{
			mesh->BindBuffers( deviceContext );
		
			if( numTextures > 0 )
			{
				const auto index = mesh->GetMaterialIndex();
				auto texture = mDiffuseTextures[ index ];

				if( texture != nullptr )
				{
					auto** resource = texture->GetResource();
					deviceContext.PSSetShaderResources( 0, 1, resource );
				}
			}

			deviceContext.DrawIndexed( mesh->GetIndexCount(), 0, 0 );
		}
	}

	void Model::Render( ID3D11DeviceContext& deviceContext, uint32_t instanceCount )
	{
		if( !mIsLoaded )
		{
			return;
		}

		const auto numTextures = mDiffuseTextures.size();
		for( const auto& modelEntry : mModels )
		{
			modelEntry->BindBuffers( deviceContext );

			if( numTextures > 0 )
			{
				const auto index	= modelEntry->GetMaterialIndex();
				auto texture		= mDiffuseTextures[ index ];

				if( texture != nullptr )
				{
					auto** resource = texture->GetResource();
					deviceContext.PSSetShaderResources( 0, 1, resource );
				}
			}

			deviceContext.DrawIndexedInstanced( modelEntry->GetIndexCount(), instanceCount, 0, 0, 0 );
		}
	}

	void Model::RenderTerrainCell( ID3D11DeviceContext& deviceContext )
	{
		auto mesh = mModels.at( 0 );

		mesh->BindBuffers( deviceContext );
		deviceContext.DrawIndexed( mesh->GetIndexCount(), 0, 0 );
	}

	void Model::Loaded()
	{
		mIsLoaded = true;

		CalculateBoundingBox();
		mBoundingBox = std::move( VisualBoundingBox::Create( mAabb ) );
	}

	void Model::CalculateBoundingBox()
	{
		if( mModels.empty() )
		{
			return;
		}

		const auto startPos = mModels[ 0 ]->GetVertices()[ 0 ].mPosition;
		auto mins = XMFLOAT3( startPos.x, startPos.y, startPos.z );
		auto maxs = mins;

		for( const auto entry : mModels )
		{
			const auto& vertices = entry->GetVertices();
			const auto vertexCount = entry->GetVertexCount();

			for( const auto& vertex : vertices )
			{
				const auto& pos = vertex.mPosition;

				mins.x = min( mins.x, pos.x );
				maxs.x = max( maxs.x, pos.x );
				mins.y = min( mins.y, pos.y );
				maxs.y = max( maxs.y, pos.y );
				mins.z = min( mins.z, pos.z );
				maxs.z = max( maxs.z, pos.z );
			}
		}

		const auto size = XMFLOAT3( maxs.x - mins.x, maxs.y - mins.y, maxs.z - mins.z );
		const auto center = XMFLOAT3( mins.x + ( size.x / 2.f ), mins.y + ( size.y / 2.f ), mins.z + ( size.z / 2.f ) );
		const auto extents = XMFLOAT3( size.x / 2.f, size.y / 2.f, size.z / 2.f );

		XMStoreFloat3( &mAabb.Center, XMLoadFloat3( &center ) );
		XMStoreFloat3( &mAabb.Extents, XMLoadFloat3( &extents) );
	}

	void Model::AddMaterial( const Material& material )
	{
		mMaterials.push_back( material );
	}

	void Model::AddDiffuseTexture( std::shared_ptr<Texture> texture )
	{
		mDiffuseTextures.push_back( texture );
	}

	void Model::AddNormalTexture( std::shared_ptr<Texture> texture )
	{
		mNormalTextures.push_back( texture );
	}

	void Model::AddSpecularTexture( std::shared_ptr<Texture> texture )
	{
		mSpecularTextures.push_back( texture );
	}

	void Model::LoadAsync( const std::string& file, Model* modelOut )
	{
		//Can we open this file?
		std::ifstream fin( file );
		if( fin.fail() )
		{
			Engine::Error( generr, "Failed to load model %s", file.c_str() );
			return;
		}
		fin.close();

		//Need to know if this model has already been loaded
		auto& manager = ResourceManager::Instance();
		if( manager.LookupModel( file ) )
		{
			Engine::Print( "Already loaded model %s", file.c_str() );
			return;
		}

		//Create a shared_ptr for this to be fed to the ResourceManager
		auto model = std::shared_ptr<Model>( modelOut );
		AssimpHelper::LoadModelToInstinct( file, modelOut );

		model->SetFileName( file );
		model->Loaded();
		manager.AddModelLookup( model );

		Engine::Print( "Loaded model %s", file.c_str() );
	}
}
