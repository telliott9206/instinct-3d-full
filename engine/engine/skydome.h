/*
 * Skydome.h
 *
 * Created 18/03/2019
 * Written by Todd Elliott
 *
 * Used for managing a dome object around the camera to simulate the outer sky.
 */

#ifndef _SKYDOME_H
#define _SKYDOME_H

#include <Engine/Entity/Entity.h>

namespace Instinct
{
	class Camera;
	class Model;

	class Skydome: public Entity
	{
	public:
		explicit Skydome( float height, const XMFLOAT4& centreColour, const XMFLOAT4& apexColour );
		~Skydome();

		/* @brief	factory function for instantiating a directional light object in which
		*			the developer app will be responsible for managing ownership of the returned 
		*			pointer
		*
		* @return	unique_ptr to newly instantiated directional light object
		*/
		DLLEXPORT static std::shared_ptr<Skydome> Create( float scale, float height, const XMFLOAT4& centreColour, const XMFLOAT4& apexColour );

		void Update( const std::shared_ptr<Camera>& camera );

		DLLEXPORT void SetCentreColour( const XMFLOAT4& centre )	{ mCentreColour = centre; }
		DLLEXPORT const XMFLOAT4& GetCentreColour() const			{ return mCentreColour; }

		DLLEXPORT void SetApexColour( const XMFLOAT4& apex )		{ mApexColour = apex; }
		DLLEXPORT const XMFLOAT4&  GetApexColour() const			{ return mApexColour; }

	private:
		XMFLOAT4	mCentreColour;
		XMFLOAT4	mApexColour;

		float		mHeight;
	};
};

#endif
