/*
 * ThreadPool.cpp
 *
 * Created 19/05/2020
 * Written by Todd Elliott
 */

#include "ThreadPool.h"

namespace Instinct
{
	ThreadPool& ThreadPool::Instance()
	{
		static ThreadPool inst;
		return inst;
	}

	void ThreadPool::Test()
	{
		auto& pool = ThreadPool::Instance();

		std::vector< std::future<void>> futures;

		for( int i = 0 ; i < 1000 ; ++ i )
		{
			futures.emplace_back( pool.Run( [ i ] { std::cout << i << std::endl; } ) );
		}

		for( int i = 0 ; i < 1000 ; ++ i )
		{
			futures[ i ].get();
		}

		std::cout << "TEST COMPLETE!" << std::endl;
	}

	ThreadPool::ThreadPool()
	{
		const auto threadsMax = std::thread::hardware_concurrency();
		mNumThreads = static_cast<int>( threadsMax );

		for( int i = 0 ; i < mNumThreads ; ++ i )
		{
			mThreads.emplace_back( [ this ] 
			{
				while( true )
				{
					std::unique_lock<std::mutex> lock( mMutex );
					mCondVar.wait( lock, [ this ] { return !mTasks.empty(); } );

					auto task = std::move( mTasks.front() );
					if( task.valid() )
					{
						mTasks.pop();
						lock.unlock();

						task();
					}
					else
					{
						break;
					}
				}
			} );
		}
	}

	ThreadPool::~ThreadPool()
	{
		{
			std::lock_guard<std::mutex> lock( mMutex );
			mTasks.push( {} );
		}

		mCondVar.notify_all();

		for( auto&& t : mThreads )
		{
			t.join();
		}
	}
}
