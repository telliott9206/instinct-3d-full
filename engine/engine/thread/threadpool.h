/*
 * ThreadPool.h
 *
 * Created 15/05/2020
 * Written by Todd Elliott
 *
 * A thread pool that only creates and destroys its threads once.
 * Any lambda can be passed in as a task and will be executed when the next thread is ready.
 */

#ifndef _THREAD_POOL_H
#define _THREAD_POOL_H

#include <Instinct.h>

#include <queue>
#include <future>
#include <condition_variable>
#include <thread>

#define JOIN( x )	x.get()

namespace Instinct
{
	typedef std::future<void> tAsyncTask;

	class ThreadPool
	{
	public:
		static ThreadPool& Instance();
		DLLEXPORT static void Test();

	private:
		ThreadPool();
		~ThreadPool();

	public:
		template<typename F, typename R = std::result_of_t<F&&()>>
		std::future<R> Run( F&& f ) const
		{
			auto task = std::packaged_task<R()>( std::forward<F>( f ) );
			auto future = task.get_future();
			{
				std::lock_guard<std::mutex> lock( mMutex );
				mTasks.push( std::packaged_task<void()>( std::move( task ) ) );
			}

			mCondVar.notify_one();

			return future;
		}

	private:
		int mNumThreads;

		std::vector<std::thread> mThreads;
		mutable std::queue<std::packaged_task<void()>> mTasks;

		mutable std::mutex mMutex;
		mutable std::condition_variable mCondVar;
	};
};

#endif
