/*
 * World.h
 *
 * Created 17/12/2018
 * Written by Todd Elliott
 *
 * Keeps collections of everything that can be present in the world. Such as
 * all entities and terrain. It is also responsible for updating and calling
 * into the render functions for these objects.
 */

#ifndef _WORLD_H
#define _WORLD_H

#include <Instinct.h>
#include <Engine/RenderPackage/RenderPackage.h>
#include <Engine/RenderPackage/EntityCollectionBuilder.h>
#include <Engine/RenderPackage/GeometryCellCollectionBuilder.h>
#include <Engine/Texture/Texture2D.h>

namespace Instinct
{
	class Entity;
	class Camera;
	class Terrain;
	class Ocean;
	class DirectionalLight;
	class Skydome;
	class ShaderManager;
	class GeometryCell;
	class Billboard;
	class CascadedShadowMap;

	class World
	{
	public:
		World();
		DLLEXPORT ~World();

		/* @brief	factory function for instantiating a world object
		 *
		 * @return	shared_ptr to newly instantiated world object
		 */
		DLLEXPORT static std::shared_ptr<World> Create();

		/* @brief	factory function for instantiating a world which is automatically
		 *			passed into the engine instance as the current active world
		 *
		 * @return	none
		 */
		DLLEXPORT static void CreateAndGo();

		DLLEXPORT void SetActiveCamera( std::shared_ptr<Camera> camera );
		DLLEXPORT std::shared_ptr<Camera> GetActiveCamera() const { return mCamera; }

		DLLEXPORT void AddEntity( std::shared_ptr<Entity> entity );

		virtual void Update( ID3D11DeviceContext& deviceContext, float delta );
		virtual void Render( ID3D11DeviceContext& deviceContext, int renderPhase );

		void GenerateViewFrustum( const XMMATRIX& view, const XMMATRIX& proj, DirectX::BoundingFrustum& frustumOut );

		DLLEXPORT void SetTerrain( std::shared_ptr<Terrain> terrain )			{ mTerrain = terrain; }
		DLLEXPORT std::shared_ptr<Terrain> GetTerrain() const					{ return mTerrain; }

		DLLEXPORT void SetOcean( std::shared_ptr<Ocean> ocean )					{ mOcean = ocean; }
		DLLEXPORT std::shared_ptr<Ocean> GetOcean() const						{ return mOcean; }

		DLLEXPORT void SetLight( std::shared_ptr<DirectionalLight> light )		{ mLight = light; }
		DLLEXPORT std::shared_ptr<DirectionalLight> GetLight() const			{ return mLight; }

		DLLEXPORT void SetSkydome( std::shared_ptr<Skydome> skydome )			{ mSkydome = skydome; }
		DLLEXPORT std::shared_ptr<Skydome> GetSkydome() const					{ return mSkydome; }

		DLLEXPORT void SetActiveSun( std::shared_ptr<Billboard> billboard )		{ mActiveSun = billboard; }
		DLLEXPORT std::shared_ptr<Billboard> GetActiveSun() const				{ return mActiveSun; }

	private:
		void RenderDepthPass( ID3D11DeviceContext& deviceContext, ShaderManager& shaderManager );
		void RenderDiffusePass( ID3D11DeviceContext& deviceContext, ShaderManager& shaderManager );
		void StageEntityForInstancing( std::shared_ptr<Entity> entity );

		void GenerateCascadedShadowMap();

	private:
		std::shared_ptr<Camera>					mCamera;
		std::shared_ptr<Terrain>				mTerrain;
		std::shared_ptr<Ocean>					mOcean;
		std::shared_ptr<DirectionalLight>		mLight; //TODO: eventually this should be a vector so we can have multiple direct lights
		std::shared_ptr<Skydome>				mSkydome;
		std::shared_ptr<Billboard>				mActiveSun;	//TODO: This should go eventually, this can be controlled by the developer app
		std::vector<std::shared_ptr<Entity>>	mAllEntities;

		std::vector<std::unique_ptr<Texture2D>>	mShadowMap;

		DirectX::BoundingFrustum				mCameraFrustum;
		DirectX::BoundingFrustum				mLightFrustum;

		RenderPackage							mRenderPackage;
		EntityCollectionBuilder					mEntityCollectionBuilder;
		GeometryCellCollectionBuilder			mGeometryCellCollectionBuilder;
	};
}

#endif
