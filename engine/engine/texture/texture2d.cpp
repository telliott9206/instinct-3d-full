/*
* Texture2D.cpp
*
* Created 03/04/2019
* Written by Todd Elliott
*/

#include "Texture2D.h"
#include <Engine.h>
#include <Engine/Utility/DirectXHelper.h>

namespace Instinct
{
	Texture2D::~Texture2D()
	{
		ReleaseCOM( mRenderTargetTexture );
		ReleaseCOM( mRenderTargetView );
		ReleaseCOM( mShaderResourceView );
		ReleaseCOM( mDepthStencilBuffer );
		ReleaseCOM( mDepthStencilView );
	}

	std::unique_ptr<Texture2D> Texture2D::Create( int width, int height )
	{
		auto& device = Engine::Instance().GetContext()->GetDevice();
		auto texture = std::make_unique<Texture2D>();

		if( !DirectXHelper::CreateRenderTargetTexture( device, width, height, DXGI_FORMAT_R32G32B32A32_FLOAT, texture->mRenderTargetTexture ) ||
			!DirectXHelper::CreateRenderTargetView( device, texture->mRenderTargetTexture, DXGI_FORMAT_R32G32B32A32_FLOAT, D3D11_RTV_DIMENSION_TEXTURE2D, texture->mRenderTargetView ) ||
			!DirectXHelper::CreateShaderResourceView( device, texture->mRenderTargetTexture, DXGI_FORMAT_R32G32B32A32_FLOAT, D3D11_SRV_DIMENSION_TEXTURE2D, texture->mShaderResourceView ) ) 
		{
			Engine::Error( d3derr, "Failed to create Texture2D." );
			return nullptr;
		}

		if( !DirectXHelper::CreateDepthStencilView( device, width, height, DXGI_FORMAT_D24_UNORM_S8_UINT, 1, 0, D3D11_DSV_DIMENSION_TEXTURE2D, texture->mDepthStencilBuffer, texture->mDepthStencilView ) )
		{
			Engine::Error( d3derr, "Failed to create depth stencil buffer for Texture2D" );
			return nullptr;
		}

		DirectXHelper::CreateViewport( width, height, texture->mViewport );

		return std::move( texture );
	}

	void Texture2D::SetRenderTarget( ID3D11DeviceContext& deviceContext )
	{
		deviceContext.OMSetRenderTargets( 1, &mRenderTargetView, mDepthStencilView );
		deviceContext.RSSetViewports( 1, &mViewport );
	}

	void Texture2D::ClearRenderTarget( ID3D11DeviceContext& deviceContext )
	{
		const float colour[ 4 ] =
		{
			0.f,
			0.f,
			0.f,
			1.f
		};

		deviceContext.ClearRenderTargetView( mRenderTargetView, colour );
		deviceContext.ClearDepthStencilView( mDepthStencilView, D3D11_CLEAR_DEPTH, 1.f, 0 );
	}
}
