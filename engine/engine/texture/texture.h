/*
 * Texture2D.h
 *
 * Created 06/12/2018
 * Written by Todd Elliott
 *
 * The most basic texture for loading texture data from a model.
 */

#ifndef _TEXTURE_H
#define _TEXTURE_H

#include "Engine/Resource.h"

namespace Instinct
{
	class Texture: public Resource
	{
	public:
		Texture() = default;
		virtual ~Texture();

		DLLEXPORT static std::shared_ptr<Texture> Create();	//at this stage creates a blank texture ... maybe not necessary?
		DLLEXPORT static std::shared_ptr<Texture> Load( const std::string& file, bool generateMips = false );

		ID3D11ShaderResourceView** GetResource() { return &mResource; }

	public:
		ID3D11ShaderResourceView* mResource	{ nullptr };
	};
};

#endif
