/*
 * Texture2DArraySimple.cpp
 *
 * Created 23/10/2021
 * Written by Todd Elliott
 */

#include "Texture2DArraySimple.h"
#include <Engine/Texture/Loader/WIC/WICTextureLoader.h>
#include <Engine.h>

namespace Instinct
{
	Texture2DArraySimple::~Texture2DArraySimple()
	{
		ReleaseCOM( mTexture2D );
		ReleaseCOM( mShaderResourceView );
	}

	//TODO: Needs to be tidied up and use DirectXHelper for texture instantation.
	std::unique_ptr<Texture2DArraySimple> Texture2DArraySimple::Create( ID3D11Device& device, ID3D11DeviceContext& deviceContext, const std::vector<std::wstring>& fileNames,
		ID3D11Texture2D** textureArray, ID3D11ShaderResourceView** textureArrayView, bool generateMips )
	{
		//Check if the device and file name array is not empty
		if( fileNames.empty() )
		{
			return nullptr;
		}

		HRESULT hr;
		const auto arraySize = static_cast<uint32_t>( fileNames.size() );

		//Read the first texture
		ID3D11Texture2D* texture;
		D3D11_TEXTURE2D_DESC texDesc;

		hr =  DirectX::CreateWICTextureFromFileEx( &device, fileNames[ 0 ].c_str(), 0, D3D11_USAGE_STAGING, 0, D3D11_CPU_ACCESS_WRITE | D3D11_CPU_ACCESS_READ, 
			0, false, reinterpret_cast<ID3D11Resource**>( &texture ), nullptr );

		if( FAILED( hr ) )
		{
			return nullptr;
		}

		//Read the created texture information
		texture->GetDesc( &texDesc );

		//Create texture array
		D3D11_TEXTURE2D_DESC texArrayDesc;
		texArrayDesc.Width				= texDesc.Width;
		texArrayDesc.Height				= texDesc.Height;
		texArrayDesc.MipLevels			= generateMips ? 0 : texDesc.MipLevels;
		texArrayDesc.ArraySize			= arraySize;
		texArrayDesc.Format				= texDesc.Format;
		texArrayDesc.SampleDesc.Count	= 1; // Multi-sampling cannot be used
		texArrayDesc.SampleDesc.Quality = 0;
		texArrayDesc.Usage				= D3D11_USAGE_DEFAULT;
		texArrayDesc.BindFlags			= D3D11_BIND_SHADER_RESOURCE | (generateMips ? D3D11_BIND_RENDER_TARGET : 0);
		texArrayDesc.CPUAccessFlags		= 0;
		texArrayDesc.MiscFlags			= generateMips ? D3D11_RESOURCE_MISC_GENERATE_MIPS : 0;

		ID3D11Texture2D* texArray = nullptr;
		hr = device.CreateTexture2D( &texArrayDesc, nullptr, &texArray );
		if( FAILED( hr ) )
		{
			ReleaseCOM( texture );
			return nullptr;
		}

		//Get the texture array information actually created
		texArray->GetDesc( &texArrayDesc );
		uint32_t updateMipLevels = generateMips ? 1u : texArrayDesc.MipLevels;

		//Write to the first element of the texture array
		D3D11_MAPPED_SUBRESOURCE mappedTex2D;
		for( uint32_t i = 0u ; i < updateMipLevels ; ++ i )
		{
			deviceContext.Map( texture, i, D3D11_MAP_READ, 0, &mappedTex2D );
			deviceContext.UpdateSubresource( texArray, i, nullptr, mappedTex2D.pData, mappedTex2D.RowPitch, mappedTex2D.DepthPitch );
			deviceContext.Unmap( texture, i );
		}

		ReleaseCOM( texture );

		//Read the remaining texture and load it into the texture array
		D3D11_TEXTURE2D_DESC currTexDesc;
		for( uint32_t i = 1u ; i < texArrayDesc.ArraySize; ++ i )
		{
			hr = CreateWICTextureFromFileEx( &device, fileNames[i].c_str(), 0, D3D11_USAGE_STAGING, 0, D3D11_CPU_ACCESS_WRITE | D3D11_CPU_ACCESS_READ,
				0, WIC_LOADER_DEFAULT, reinterpret_cast<ID3D11Resource**>( &texture ), nullptr );	

			if( FAILED( hr ) )
			{
				ReleaseCOM( texArray );
				return nullptr;
			}

			texture->GetDesc( &currTexDesc );

			//Need to check whether the mipLevels, width and height, and data format of all textures are consistent,
			//If the data format is inconsistent, please use dxtex.exe (DirectX Texture Tool)
			//Convert all pictures into a consistent data format
			if( currTexDesc.MipLevels != texDesc.MipLevels || currTexDesc.Width != texDesc.Width ||
				currTexDesc.Height != texDesc.Height || currTexDesc.Format != texDesc.Format )
			{
				ReleaseCOM( texArray );
				ReleaseCOM( texture );

				return nullptr;
			}

			//Write to the corresponding element of the texture array
			for( uint32_t j = 0u ; j < updateMipLevels ; ++ j )
			{
				//Allow to map the 2D texture of the mipmap level of index j in the texture index i
				deviceContext.Map( texture, j, D3D11_MAP_READ, 0, &mappedTex2D );
				deviceContext.UpdateSubresource( texArray, D3D11CalcSubresource( j, i, texArrayDesc.MipLevels ),
					nullptr, mappedTex2D.pData, mappedTex2D.RowPitch, mappedTex2D.DepthPitch );

				deviceContext.Unmap( texture, j );
			}

			ReleaseCOM( texture );
		}

		//Create the SRV of the texture array if necessary
		ID3D11ShaderResourceView* texArraySrv = nullptr;
		if( generateMips || textureArrayView )
		{
			D3D11_SHADER_RESOURCE_VIEW_DESC viewDesc;
			viewDesc.Format							= texArrayDesc.Format;
			viewDesc.ViewDimension					= D3D11_SRV_DIMENSION_TEXTURE2DARRAY;
			viewDesc.Texture2DArray.MostDetailedMip = 0;
			viewDesc.Texture2DArray.MipLevels		= -1;
			viewDesc.Texture2DArray.FirstArraySlice = 0;
			viewDesc.Texture2DArray.ArraySize		= arraySize;

			hr = device.CreateShaderResourceView( texArray, &viewDesc, &texArraySrv );
			if( FAILED( hr ) )
			{
				ReleaseCOM( texArray );
				return nullptr;
			}

			// Generate mipmaps
			if( generateMips )
			{
				deviceContext.GenerateMips( texArraySrv );
			}
		}

		ReleaseCOM( texArray );

		auto texArraySimple = std::make_unique<Texture2DArraySimple>();
		texArraySimple->mShaderResourceView = texArraySrv;

		return std::move( texArraySimple );
	}
}
