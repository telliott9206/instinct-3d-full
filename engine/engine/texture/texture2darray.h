/*
 * Texture2DArray.h
 *
 * Created 05/03/2019
 * Written by Todd Elliott
 *
 * Used for keeping a collection of 2D textures together which altogether makes up the G Buffer.
 */

#ifndef _TEXTURE2DARRAY_H
#define _TEXTURE2DARRAY_H

#include <Instinct.h>

namespace Instinct
{
	class Texture2DArray
	{
	public:
		static const int BUFFER_COUNT_MAX = 4;

		/*
			Specific to the GBuffer
	
			buffer 0 : Diffuse Buffer	:	r, g, b, a = alpha
			buffer 1 : Normal Buffer	:	r, g, b, a = light calculations 1.f / 0.f
			buffer 2 : Viewdir Buffer	:	r, g, b, a = Spec map / power
			buffer 3 : Other/Extra		:	r = shadow buffer

			TODO: Re-add tangents, possibly rename class because this is only used for the gbuffer?
		*/

		Texture2DArray() = default;
		virtual ~Texture2DArray();

		static std::unique_ptr<Texture2DArray> Create( int width, int height, int bufferCount );

		void SetRenderTarget( ID3D11DeviceContext& deviceContext );
		void ClearRenderTarget( ID3D11DeviceContext& deviceContext );

		ID3D11ShaderResourceView* const GetShaderResourceView( int index ) const { return mShaderResourceView[ index ]; }

	protected:
		ID3D11Texture2D*			mRenderTargetTexture[ BUFFER_COUNT_MAX ];
		ID3D11RenderTargetView*		mRenderTargetView[ BUFFER_COUNT_MAX ];
		ID3D11ShaderResourceView*	mShaderResourceView[ BUFFER_COUNT_MAX ];
		ID3D11Texture2D*			mDepthStencilBuffer;
		ID3D11DepthStencilView*		mDepthStencilView;
		D3D11_VIEWPORT				mViewport;

		int							mBufferCount;
	};
};

#endif