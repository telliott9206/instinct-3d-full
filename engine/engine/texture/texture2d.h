/*
 * Texture2D.h
 *
 * Created 03/04/2019
 * Written by Todd Elliott
 *
 * A texture used for a render target.
 */

#ifndef _TEXTURE2D_H
#define _TEXTURE2D_H

#include <Instinct.h>

namespace Instinct
{
	class Texture2D
	{
	public:
		Texture2D() = default;
		~Texture2D();

		static std::unique_ptr<Texture2D> Create( int width, int height );

		void SetRenderTarget( ID3D11DeviceContext& deviceContext );
		void ClearRenderTarget( ID3D11DeviceContext& deviceContext );

		ID3D11RenderTargetView* const GetRenderTargetView() const		{ return mRenderTargetView; }
		ID3D11ShaderResourceView* const GetShaderResourceView() const	{ return mShaderResourceView; }

	private:
		ID3D11Texture2D*			mRenderTargetTexture;
		ID3D11RenderTargetView*		mRenderTargetView;
		ID3D11ShaderResourceView*	mShaderResourceView;
		ID3D11Texture2D*			mDepthStencilBuffer;
		ID3D11DepthStencilView*		mDepthStencilView;
		D3D11_VIEWPORT				mViewport;
	};
};

#endif
