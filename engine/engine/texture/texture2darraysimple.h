/*
 * Texture2DArraySimple.h
 *
 * Created 23/10/2021
 * Written by Todd Elliott
 *
 * Used for loading in a collection of textures for simply passing as a resource to a shader.
 */

#ifndef _TEXTURE2DARRAYSIMPLE_H
#define _TEXTURE2DARRAYSIMPLE_H

#include <Instinct.h>

namespace Instinct
{
	class Texture2DArraySimple
	{
	public:
		Texture2DArraySimple() = default;
		virtual ~Texture2DArraySimple();

		static std::unique_ptr<Texture2DArraySimple> Create( ID3D11Device& device, ID3D11DeviceContext& deviceContext, const std::vector<std::wstring>& filenames );

		static std::unique_ptr<Texture2DArraySimple> Create( ID3D11Device& device, ID3D11DeviceContext& deviceContext, const std::vector<std::wstring>& fileNames,
			ID3D11Texture2D** textureArray, ID3D11ShaderResourceView** textureArrayView, bool generateMips );

		ID3D11ShaderResourceView** GetShaderResourceView() { return &mShaderResourceView; }

	private:
		ID3D11Texture2D*			mTexture2D;
		ID3D11ShaderResourceView*	mShaderResourceView;
	};
}

#endif
