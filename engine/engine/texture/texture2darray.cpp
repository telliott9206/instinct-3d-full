/*
 * Texture2DArray.cpp
 *
 * Created 05/03/2019
 * Written by Todd Elliott
 */

#include "Texture2DArray.h"
#include <Engine.h>
#include <Engine/Utility/Util.h>
#include <Engine/Utility/DirectXHelper.h>

namespace Instinct
{
	Texture2DArray::~Texture2DArray()
	{
		for( auto i = 0 ; i < mBufferCount ; ++ i )
		{
			ReleaseCOM( mRenderTargetTexture[ i ] );
			ReleaseCOM( mRenderTargetView[ i ] );
			ReleaseCOM( mShaderResourceView[ i ] );
		}

		ReleaseCOM( mDepthStencilBuffer );
		ReleaseCOM( mDepthStencilView );
	}

	std::unique_ptr<Texture2DArray> Texture2DArray::Create( int width, int height, int bufferCount )
	{
		if( bufferCount > BUFFER_COUNT_MAX )
		{
			Engine::Error( generr, "Number of buffers cannot exceed BUFFER_COUNT_MAX in Texture2DArray" );
			return nullptr;
		}

		auto& device = Engine::Instance().GetContext()->GetDevice();
		auto texture = std::make_unique<Texture2DArray>();

		for( auto i = 0 ; i < bufferCount ; ++ i )
		{
			if( !DirectXHelper::CreateRenderTargetTexture( device, width, height, DXGI_FORMAT_R32G32B32A32_FLOAT, texture->mRenderTargetTexture[ i ] ) ||
				!DirectXHelper::CreateRenderTargetView( device, texture->mRenderTargetTexture[ i ], DXGI_FORMAT_R32G32B32A32_FLOAT, D3D11_RTV_DIMENSION_TEXTURE2D, texture->mRenderTargetView[ i ] ) ||
				!DirectXHelper::CreateShaderResourceView( device, texture->mRenderTargetTexture[ i ], DXGI_FORMAT_R32G32B32A32_FLOAT, D3D11_SRV_DIMENSION_TEXTURE2D, texture->mShaderResourceView[ i ] ) ) 
			{
				Engine::Error( d3derr, "Failed to create Texture2DArray." );
				return nullptr;
			}
		}

		if( !DirectXHelper::CreateDepthStencilView( device, width, height, DXGI_FORMAT_D24_UNORM_S8_UINT, 1, 0, D3D11_DSV_DIMENSION_TEXTURE2D, texture->mDepthStencilBuffer, texture->mDepthStencilView ) )
		{
			Engine::Error( d3derr, "Failed to create depth stencil buffer for Texture2DArray" );
			return nullptr;
		}

		DirectXHelper::CreateViewport( width, height, texture->mViewport );
		texture->mBufferCount = bufferCount;

		return std::move( texture );
	}

	void Texture2DArray::SetRenderTarget( ID3D11DeviceContext& deviceContext )
	{
		deviceContext.OMSetRenderTargets( mBufferCount, mRenderTargetView, mDepthStencilView );
		deviceContext.RSSetViewports( 1, &mViewport );
	}

	void Texture2DArray::ClearRenderTarget( ID3D11DeviceContext& deviceContext )
	{
		const float colour[ 4 ] =
		{
			0.f,
			0.f,
			0.f,
			1.f
		};

		for( auto i = 0 ; i < mBufferCount ; ++ i )
		{
			deviceContext.ClearRenderTargetView( mRenderTargetView[ i ], colour );
		}
    
		deviceContext.ClearDepthStencilView( mDepthStencilView, D3D11_CLEAR_DEPTH, 1.f, 0 );
	}
}	
