/*
 * Texture.cpp
 *
 * Created 06/12/2018
 * Written by Todd Elliott
 */

#include "Texture.h"
#include <Engine/Utility/Util.h>
#include <Engine/Manager/ResourceManager.h>
#include <Engine/Texture/Loader/WIC/WICTextureLoader.h>
#include <Engine/Texture/Loader/DDS/DDSTextureLoader11.h>
#include <Engine/Texture/Loader/DirectXTex/DirectXTex.h>
#include <Engine.h>

namespace Instinct
{
	Texture::~Texture()
	{
	}

	std::shared_ptr<Texture> Texture::Create()
	{
		return std::make_shared<Texture>();
	}

	std::shared_ptr<Texture> Texture::Load( const std::string& file, bool generateMips )
	{
		auto& manager = ResourceManager::Instance();
		auto texture = std::shared_ptr<Texture>( manager.LookupTexture( file ) );

		if( texture == nullptr )
		{
			texture				= std::make_shared<Texture>();
		
			auto context		= Engine::Instance().GetContext();
			auto& device		= context->GetDevice();
			auto& deviceContext	= context->GetDeviceContext();
			const auto str		= Util::MultibyteToWideChar( file.c_str() );
			const auto ext		= file.substr( file.find_last_of( "." ) + 1 );

			context->LockDeviceContext();
			HRESULT hr;

			if( ext == "dds" )
			{
				if( generateMips )
				{
					hr = DirectX::CreateDDSTextureFromFile( &device, &deviceContext, str, nullptr, &texture->mResource ); 
				}
				else
				{
					hr = DirectX::CreateDDSTextureFromFile( &device, str, nullptr, &texture->mResource ); 
				}
			}
			else if( ext == "tga" )
			{
				TexMetadata metadata;
				ScratchImage image;

				std::wstring wc( 128, L'#' );
				mbstowcs( &wc[ 0 ], file.c_str(), 128 );

				hr = DirectX::LoadFromTGAFile( wc.c_str(), &metadata, image );
				if( !FAILED( hr ) )
				{
					hr = CreateShaderResourceView( &device, image.GetImages(), image.GetImageCount(), image.GetMetadata(), &texture->mResource );
				}
			}
			else
			{
				if( generateMips )
				{
					hr = DirectX::CreateWICTextureFromFile( &device, &deviceContext, str, nullptr, &texture->mResource );
				}
				else
				{
					hr = DirectX::CreateWICTextureFromFile( &device, str, nullptr, &texture->mResource );
				}
			}

			if( FAILED( hr ) )
			{	
				Engine::Print( "Failed to load texture %s", file.c_str() );
				return nullptr;
			}

			context->UnlockDeviceContext();
			texture->SetFileName( file );
			manager.AddTextureLookup( texture );

		#ifdef _DEBUG
			Engine::Print( "Loaded texture %s", file.c_str() );
		#endif
		}
		else
		{
		#ifdef _DEBUG
			Engine::Print( "Using texture %s", file.c_str() );
		#endif
		}

		return texture;
	}
}
