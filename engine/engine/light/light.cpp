/*
 * Light.cpp
 *
 * Created 30/11/2018
 * Written by Todd Elliott
 */

#include "Light.h"

namespace Instinct
{
	Light::Light( const XMFLOAT4& diffuseColour, const XMFLOAT4& ambientColour ) :
		mIsEnabled( true ),
		mDiffuseColour( diffuseColour ),
		mAmbientColour( ambientColour )
	{
	}

	Light::~Light()
	{
	}
}
