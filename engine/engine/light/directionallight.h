/*
 * DirectionalLight.h
 *
 * Created 02/03/2019
 * Written by Todd Elliott
 *
 * A light that has no exact origin the world that has infinite power and points in a single direction.
 */

#ifndef _DIRECTIONAL_LIGHT_H
#define _DIRECTIONAL_LIGHT_H

#include "Light.h"

#define NUM_FRUSTUM_CORNERS 8

namespace Instinct
{
	class Context;

	ALIGN16 class DirectionalLight : public Light
	{
	public:
		OPERATORS_ALIGN16

	struct cbShadowCascades
	{
		float mCascades[ 3 ];
		float _padding;
	};

	public:
		explicit DirectionalLight( const XMFLOAT4& diffuseColour, const XMFLOAT4& ambientColour );
		virtual ~DirectionalLight();

		/* @brief	factory function for instantiating a directional light object
		 *
		 * @return	shared_ptr to newly instantiated directional light object
		 */
		DLLEXPORT static std::shared_ptr<DirectionalLight> Create( const XMFLOAT3& direction, const XMFLOAT4& diffuseColour, const XMFLOAT4& ambientColour );

		void Update( const std::shared_ptr<Context>& context );

		DLLEXPORT void SetPosition( const XMFLOAT3& position ) override;
		DLLEXPORT void SetPosition( float x, float y, float z ) override;

		void BindShadowCascadesBuffer( ID3D11DeviceContext& deviceContext );

		const XMMATRIX& GetViewMatrix( int index ) const 			{ return mView[ index ]; }
		const XMMATRIX& GetProjectionMatrix( int index ) const 		{ return mProj[ index ]; }
		const XMFLOAT3& GetCascadePositions( int index ) const 		{ return mViewPositions[ index ]; }

	private:
		XMMATRIX					mView[ SHADOW_CASCADES_COUNT ];
		XMMATRIX					mProj[ SHADOW_CASCADES_COUNT ];
		XMFLOAT3					mViewPositions[ SHADOW_CASCADES_COUNT ];

		float						mShadowCascadesAmounts[ SHADOW_CASCADES_COUNT ]		{ 4.f, 22.f, 100.f };	//TODO: This test code HAS TO GO! Needs to be configurable
		float						mShadowCascadesDists[ SHADOW_CASCADES_COUNT ];

		ID3D11Buffer*				mShadowCascadesBuffer;

		DirectX::BoundingFrustum	mViewFrustum;
	};
};

#endif
