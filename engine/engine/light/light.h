/*
 * Light.h
 *
 * Created 29/11/2018
 * Written by Todd Elliott
 *
 * THe base class for all types of lights that can be placed in the world.
 */

#ifndef _LIGHT_H
#define _LIGHT_H

#include <Engine/Entity/Entity.h>

namespace Instinct
{
	class Texture2D;

	class Light: public Entity
	{
	public:
		explicit Light( const XMFLOAT4& diffuseColour, const XMFLOAT4& ambientColour );
		virtual ~Light() = 0;

		DLLEXPORT void Disable()									{ mIsEnabled = false; }
		DLLEXPORT void Enable()										{ mIsEnabled = true; }

		DLLEXPORT bool IsEnabled()									{ return mIsEnabled; }

		DLLEXPORT void SetDiffuseColour( const XMFLOAT4& colour )	{ mDiffuseColour = colour; }
		DLLEXPORT const XMFLOAT4& GetDiffuseColour() const			{ return mDiffuseColour; }

		DLLEXPORT void SetAmbientColour( const XMFLOAT4& colour )	{ mAmbientColour = colour; }
		DLLEXPORT const XMFLOAT4& GetAmbientColour() const			{ return mAmbientColour; }

	protected:
		bool		mIsEnabled;

		XMFLOAT4	mDiffuseColour;
		XMFLOAT4	mAmbientColour;
	};
};

#endif
