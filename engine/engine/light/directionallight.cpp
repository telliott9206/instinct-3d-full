/*
 * DirectionalLight.cpp
 *
 * Created 02/03/2019
 * Written by Todd Elliott
 */

#include <Engine/Light/DirectionalLight.h>
#include <Engine/Camera.h>
#include <Engine/World.h>
#include <Engine/Utility/MathHelper.h>
#include <Engine/Render/Context.h>
#include <Engine/Model/Billboard.h>
#include <Engine/Utility/DirectXHelper.h>
#include <Engine/Thread/ThreadPool.h>
#include <Engine.h>

namespace Instinct
{
	DirectionalLight::DirectionalLight( const XMFLOAT4& diffuseColour, const XMFLOAT4& ambientColour ) :
		Light( diffuseColour, ambientColour )
	{
	}

	DirectionalLight::~DirectionalLight()
	{
	}

	std::shared_ptr<DirectionalLight> DirectionalLight::Create( const XMFLOAT3& direction, const XMFLOAT4& diffuseColour, const XMFLOAT4& ambientColour )
	{
		auto light = std::shared_ptr<DirectionalLight>( new DirectionalLight( diffuseColour, ambientColour ) ); //Manual shared_ptr for aligned 16 class

		light->mRotation = direction;
		light->mPosition = XMFLOAT3( 0.f, 0.f, 0.f );

		if( !DirectXHelper::CreateConstantBuffer<cbShadowCascades>( light->mShadowCascadesBuffer, D3D11_USAGE_DYNAMIC, D3D11_CPU_ACCESS_WRITE ) )
		{
			return nullptr;
		}

		return light;
	}

	void DirectionalLight::Update( const std::shared_ptr<Context>& context )
	{
		auto& system = Engine::Instance();
		const auto camera = system.GetCurrentWorld()->GetActiveCamera();
		auto window = system.GetContext()->GetRenderWindow();

		const auto screenRatio = static_cast<float>( window->GetWidth() ) / static_cast<float>( window->GetHeight() );
		const auto screenNear = context->GetScreenNear();
		const auto screenFar = context->GetScreenFar();

		XMMATRIX proj[ SHADOW_CASCADES_COUNT ];
		DirectX::BoundingFrustum frustum[ SHADOW_CASCADES_COUNT ];

		//Retrieve the inverse of the camera view matrix for transforming each frustum
		const auto cameraInverse = DirectX::XMMatrixInverse( nullptr, camera->GetViewMatrix() );

		//Calculate the shadow cascade distances
		for( auto i = 0 ; i < SHADOW_CASCADES_COUNT ; ++ i )
		{
			mShadowCascadesDists[ i ] = screenFar * ( mShadowCascadesAmounts[ i ] / 100.f );
		}
	
		//Generate the projection matrices (first and last to be auto generated)
		proj[ 0 ] = XMMatrixPerspectiveFovLH( 0.4f * XM_PI, screenRatio, screenNear, mShadowCascadesDists[ 0 ] );
		proj[ SHADOW_CASCADES_COUNT - 1 ] = XMMatrixPerspectiveFovLH( 0.4f * XM_PI, screenRatio, mShadowCascadesDists[ 1 ], screenFar );

		for( auto i = 1 ; i < SHADOW_CASCADES_COUNT - 1 ; ++ i )
		{
			proj[ i ] = XMMatrixPerspectiveFovLH( 0.4f * XM_PI, screenRatio, mShadowCascadesDists[ i - 1 ], mShadowCascadesDists[ i ] );
		}

		//For each cascade, calculate the light viewing position and the view matrix
		for( auto i = 0 ; i < SHADOW_CASCADES_COUNT ; ++ i )
		{
			DirectX::BoundingFrustum::CreateFromMatrix( frustum[ i ], proj[ i ] );
			frustum[ i ].Transform( frustum[ i ], cameraInverse );

			auto centroid = XMFLOAT3( 0.f, 0.f, 0.f );
			XMFLOAT3 corners[ NUM_FRUSTUM_CORNERS ];
			frustum[ i ].GetCorners( corners );

			//Calculate the center position for this cascade
			for( auto j = 0 ; j < NUM_FRUSTUM_CORNERS ; ++ j )
			{
				centroid = MathHelper::AddVector3D( centroid, corners[ j ] );
			}

			centroid = MathHelper::DivideVector3D( centroid, 8.f );

			//Calculate the viewing position for this cascade
			mViewPositions[ i ] = MathHelper::AddVector3D( centroid, { -mRotation.x, -mRotation.y, -mRotation.z } );
		
			//Calculate and generate the view matrix for this cascade
			mView[ i ] = DirectXHelper::GenerateViewMatrixLookAt( mViewPositions[ i ], centroid );
								
			//Transform the corner coordinates from world space to light space
			XMFLOAT3 out[ NUM_FRUSTUM_CORNERS ];
			for( auto j = 0 ; j < NUM_FRUSTUM_CORNERS ; ++ j )
			{
				XMVECTOR s( XMLoadFloat3( &corners[ j ] ) );
				XMVECTOR t( XMVector3Transform( s, mView[ i ] ) );
				XMStoreFloat3( &out[ j ], t );
			}

			//determine the mins and maxes for the ortho projection matrix
			XMFLOAT3 maxes( 0.f, 0.f, 0.f );
			XMFLOAT3 mins( 0.f, 0.f, 0.f );

			for( auto j = 0 ; j < NUM_FRUSTUM_CORNERS ; ++ j )
			{
				mins.x = min( mins.x, out[ j ].x );
				maxes.x = max( maxes.x, out[ j ].x );
				mins.y = min( mins.y, out[ j ].y );
				maxes.y = max( maxes.y, out[ j ].y );
				mins.z = min( mins.z, out[ j ].z );
				maxes.z = max( maxes.z, out[ j ].z );
			}

			mProj[ i ] = XMMatrixOrthographicOffCenterLH( mins.x, maxes.x, mins.y, maxes.y, mins.z - screenFar, maxes.z );
		}
	}

	void DirectionalLight::SetPosition( const XMFLOAT3& position )
	{
		mPosition = position;
	}

	void DirectionalLight::SetPosition( float x, float y, float z )
	{
		mPosition = XMFLOAT3( x, y, z );
	}

	void DirectionalLight::BindShadowCascadesBuffer( ID3D11DeviceContext& deviceContext )
	{
		cbShadowCascades buf;

		for( int i = 0 ; i < SHADOW_CASCADES_COUNT ; ++ i )
		{
			buf.mCascades[ i ] = mShadowCascadesDists[ i ];
		}

		DirectXHelper::CopyToBuffer<cbShadowCascades>( deviceContext, mShadowCascadesBuffer, &buf );
		deviceContext.PSSetConstantBuffers( 1, 1, &mShadowCascadesBuffer );
	}
}
