/*
 * ShaderManager.h
 *
 * Created 03/04/2019
 * Written by Todd Elliott
 *
 * A singleton accessible class that holds a pointer to each shader that is used in the engine.
 */

#ifndef _SHADERMANAGER_H
#define _SHADERMANAGER_H

#include <Instinct.h>

namespace Instinct
{
	class Shader;
	class DetailShader;
	class DepthShader;
	class MergeShader;
	class SkydomeShader;
	class WaterShader;
	class SimpleShader;
	class SunShader;
	class Texture2D;
	class PPEffect;
	class PPShader;

	class ShaderManager
	{
	public:
		DLLEXPORT static ShaderManager& Instance()
		{
			static ShaderManager inst;
			return inst;
		}

	private:
		ShaderManager();
		~ShaderManager();

	public:
		ShaderManager( const ShaderManager& ) = delete;
		ShaderManager& operator = ( const ShaderManager& ) = delete;

		//Preset effects that come with the engine!
		enum PPFXPresets
		{
			PPFX_GODGRAYS
		};

		Shader& GetBasicShader() 			{ return *mBasicShader.get(); }		//None of these can be nullptr if the engine loads correctly
		DetailShader& GetDetailShader() 	{ return *mDetailShader.get(); }
		DepthShader& GetDepthShader() 		{ return *mDepthShader.get(); }
		MergeShader& GetMergeShader() 		{ return *mMergeShader.get(); }
		SkydomeShader& GetSkydomeShader()	{ return *mSkydomeShader.get(); }
		WaterShader& GetWaterShader() 		{ return *mWaterShader.get(); }
		SimpleShader& GetSimpleShader() 	{ return *mSimpleShader.get(); }

		void RenderPPEffects( ID3D11DeviceContext& deviceContext, Texture2D& renderTexture );

		DLLEXPORT void AddPPEffect( std::shared_ptr<PPEffect> effect );

		const std::vector<std::shared_ptr<PPEffect>>& GetPPEffects() const	{ return mEffects; }
		const std::shared_ptr<Texture2D>& GetPPEffectsTexture() const		{ return mPPFXTexture; }

		void CreateSunShader();
		//Texture2D* RenderSunShader( ID3D11DeviceContext& deviceContext, Texture2D* original );

	private:
		std::unique_ptr<Shader>					mBasicShader;
		std::unique_ptr<DetailShader>			mDetailShader;
		std::unique_ptr<DepthShader>			mDepthShader;
		std::unique_ptr<MergeShader>			mMergeShader;
		std::unique_ptr<SkydomeShader>			mSkydomeShader;
		std::unique_ptr<WaterShader>			mWaterShader;
		std::unique_ptr<SimpleShader>			mSimpleShader;
		std::unique_ptr<SunShader>				mSunShader;

		std::vector<std::shared_ptr<PPEffect>>	mEffects;
		std::shared_ptr<Texture2D>				mPPFXTexture;
	};
};

#endif
