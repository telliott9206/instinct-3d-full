/*
 * ResourceManager.cpp
 *
 * Created 07/12/2018
 * Written by Todd Elliott
 */

#include <Engine/Manager/Resourcemanager.h>
#include <Engine/Texture/Texture.h>
#include <Engine/Terrain/Terrain.h>
#include <Engine/Entity/Entity.h>

namespace Instinct
{
	ResourceManager::~ResourceManager()
	{
	}

	void ResourceManager::AddTextureLookup( std::shared_ptr<Texture> texture )
	{
		std::lock_guard<std::mutex> lock( mTextureLock );
		mTextures.push_back( texture );
	}

	void ResourceManager::AddModelLookup( std::shared_ptr<Model> model )
	{
		std::lock_guard<std::mutex> lock( mModelLock );
		mModels.push_back( model );
	}

	std::shared_ptr<Texture> ResourceManager::LookupTexture( const std::string& file )
	{
		std::lock_guard<std::mutex> lock( mTextureLock );

		const auto iter = std::find_if( mTextures.cbegin(), mTextures.cend(), [ & ] ( const auto& texture )
			{
				return file.compare( texture->GetFileName() ) == 0;
			} );

		return iter != mTextures.cend() ? *iter : nullptr;
	}

	std::shared_ptr<Model> ResourceManager::LookupModel( const std::string& file )
	{
		std::lock_guard<std::mutex> lock( mModelLock );

		const auto iter = std::find_if( mModels.cbegin(), mModels.cend(), [ & ] ( const auto& model )
			{
				return file.compare( model.get()->GetFileName() ) == 0;
			} );

		return iter != mModels.cend() ? *iter : nullptr;
	}

	void ResourceManager::AddResourceLoading( std::shared_ptr<Resource> pResource )
	{
		//TODO: Check if this needs a lock guard?
		mLoadingResources.push_back( pResource );
	}

	void ResourceManager::RemoveResourceLoading( const std::string& search )
	{
		auto iter = std::find_if( mLoadingResources.begin(), mLoadingResources.end(), [ & ] ( const auto& resource )
			{
				return resource->GetFileName().compare( search ) == 0;
			} );

		if( iter != mLoadingResources.end() )
		{
			mLoadingResources.erase( iter );
		}
	}

	bool ResourceManager::AreResourcesLoading() const
	{
		//TODO: Check if this needs a lock guard?
		return mLoadingResources.size() > 0;
	}
}
