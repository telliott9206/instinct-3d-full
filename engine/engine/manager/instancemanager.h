/*
 * InstanceManager.h
 *
 * Created 20/03/2020
 * Written by Todd Elliott
 *
 * A single accessible class responsible for managing the process of maintaining a data collection of models that need to be instanced.
 * Renders instanced models during the first render phase of the render cycle.
 */

#ifndef _INSTANCEMANAGER_H
#define _INSTANCEMANAGER_H

#include <Instinct.h>

namespace Instinct
{
	class Model;
	class Basic;
	class Entity;
	class ModelEntry;
	struct ModelInstanceData;

	typedef std::map<std::string, std::vector<std::shared_ptr<Entity>>> tInstanceMap;

	class InstanceManager
	{
	public:
		static InstanceManager& Instance()
		{
			static InstanceManager inst;
			return inst;
		}

	private:
		InstanceManager();
		~InstanceManager();

		void CreateInstanceBuffer();

	public:
		InstanceManager( const InstanceManager& ) = delete;
		InstanceManager& operator = ( const InstanceManager& ) = delete;

		void AddEntityToInstanceMap( const std::string& model, std::shared_ptr<Entity> entity );
		void RenderInstancedModels( ID3D11DeviceContext& deviceContext, int renderPhase, int cascadeIndex = 0 );

	private:
		tInstanceMap mInstanceMap;
		ID3D11Buffer* mInstanceBuffer;
	};
};

#endif
