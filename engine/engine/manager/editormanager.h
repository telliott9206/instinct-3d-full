/*
 * EditorManager.h
 *
 * Created 29/11/2018
 * Written by Todd Elliott
 *
 * A singleton accessible class that is used to control information and functionality that is used in the world editor.
 */

#ifndef _EDITORMANAGER_H
#define _EDITORMANAGER_H

#include <Instinct.h>

namespace Instinct
{
	class EditorManager
	{
	public:
		DLLEXPORT static EditorManager& Instance()
		{
			static EditorManager inst;
			return inst;
		}

	private:
		EditorManager() = default;
		~EditorManager();

	public:
		EditorManager( const EditorManager& ) = delete;
		EditorManager& operator = ( const EditorManager& ) = delete;
		
		void SetPainBrushStrength( int value )	{ mPaintBrushStrength = value; }
		void SetPaintBrushSize( int value )		{ mPaintBrushSize = value; }
		void SetPaintBrushShape( int value )	{ mPaintBrushShape = value; }

	private:
		int mPaintBrushStrength;
		int mPaintBrushSize;
		int mPaintBrushShape;
	};
};

#endif
