/*
 * ResourceManager.h
 *
 * Created 07/12/2018
 * Written by Todd Elliott
 *
 * A singleton accessible class that is responsible for managing the collection of resources that the engine is using
 * and ensuring that no resources are loaded more than once, rather use what is already loaded.
 */

#ifndef _RESOURCEMANAGER_H
#define _RESOURCEMANAGER_H

#include <Instinct.h>
#include <Engine/Model/Model.h>

namespace Instinct
{
	class Texture;
	class Terrain;
	class Entity;
	class Resource;
	class PPEffect;

	class ResourceManager
	{
	public:
		static ResourceManager& Instance()
		{
			static ResourceManager inst;
			return inst;
		}

	private:
		ResourceManager() = default;
		~ResourceManager();

	public:
		ResourceManager( const ResourceManager& ) = delete;
		ResourceManager& operator = ( const ResourceManager& ) = delete;

		void AddTextureLookup( std::shared_ptr<Texture> texture );
		void AddModelLookup( std::shared_ptr<Model> model );

		std::shared_ptr<Texture> LookupTexture( const std::string& file );		//Can be nullptr!
		std::shared_ptr<Model> LookupModel( const std::string& file );			//Can be nullptr!

		const std::vector<std::shared_ptr<Model>>& GetModelList() const { return mModels; }

		void AddResourceLoading( std::shared_ptr<Resource> resource );
		void RemoveResourceLoading( const std::string& resource );
		bool AreResourcesLoading() const;

	private:
		std::vector<std::shared_ptr<Texture>>	mTextures;
		std::vector<std::shared_ptr<Model>>		mModels;
		std::vector<std::shared_ptr<Resource>>	mLoadingResources;

		std::mutex								mTextureLock;
		std::mutex								mModelLock;
	};
};

#endif
