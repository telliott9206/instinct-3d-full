/*
 * InstanceManager.cpp
 *
 * Created 25/03/2020
 * Written by Todd Elliott
 */

#include "InstanceManager.h"
#include <Engine/Engine.h>
#include <Engine/Model/Model.h>
#include <Engine/Model/Mesh.h>
#include <Engine/Entity/Entity.h>
#include <Engine/Shaders/Shader.h>
#include <Engine/Shaders/DepthShader.h>
#include <Engine/Model/ModelInstanceData.h>
#include <Engine/Manager/ShaderManager.h>
#include <Engine/Utility/DirectXHelper.h>

namespace Instinct
{
	InstanceManager::InstanceManager()
	{
		CreateInstanceBuffer();
	}

	InstanceManager::~InstanceManager()
	{
	}

	void InstanceManager::CreateInstanceBuffer()
	{
		D3D11_BUFFER_DESC desc;
		ZeroMemory( &desc, sizeof( desc ) );

		desc.Usage					= D3D11_USAGE_DYNAMIC;
		desc.BindFlags				= D3D11_BIND_VERTEX_BUFFER;
		desc.CPUAccessFlags			= D3D11_CPU_ACCESS_WRITE;
		desc.MiscFlags				= 0;
		desc.StructureByteStride	= 0;
		desc.ByteWidth				= sizeof( ModelInstanceData ) * 200; //TODO: Don't use a fixed number ... recreate buffer per frame?

		auto& device = Engine::Instance().GetContext()->GetDevice();
		if( FAILED( device.CreateBuffer( &desc, nullptr, &mInstanceBuffer ) ) )
		{
			Engine::Error( "Instance Manager", "Failed to create instance buffer for Instance Manager" );
		}
	}

	void InstanceManager::AddEntityToInstanceMap( const std::string& model, std::shared_ptr<Entity> entity )
	{
		tInstanceMap::iterator iter = mInstanceMap.find( model );
		if( iter == mInstanceMap.end() )
		{
			std::vector<std::shared_ptr<Entity>> vec;
			vec.push_back( entity );

			mInstanceMap.emplace( std::pair<std::string, std::vector<std::shared_ptr<Entity>>>( model, vec ) );
		}
		else
		{
			auto& vec = iter->second;
			vec.push_back( entity );
		}
	}

	void InstanceManager::RenderInstancedModels( ID3D11DeviceContext& deviceContext, int renderPhase, int cascadeIndex )
	{
		auto& device = Engine::Instance().GetContext()->GetDevice();
		auto* shader = &ShaderManager::Instance().GetBasicShader();

		//Ensure to render with depth shader if using depth pass
		if( renderPhase == Engine::tRenderPhase::kDepth )
		{
			shader = &ShaderManager::Instance().GetDepthShader();
		}

		for( const auto& mesh : mInstanceMap )	//iterate though each model entry (by model file name)
		{
			const auto& entityList = mesh.second;

			if( entityList.empty() )
			{
				continue;
			}

			const auto model = entityList[ 0 ]->GetModel();
			const auto instanceCount = model->GetInstanceCount();
			const auto modelEntriesCount = model->GetModelEntriesCount();

			uint32_t vertexCount, indexCount;

			/*
				Each entity has the same model and same model entry list, so iterate through each entity
				and start by populating the instance buffer.
			*/
			std::vector<ModelInstanceData> data;
			data.reserve( instanceCount );

			for( const auto& entity : entityList )
			{
				const auto alpha			= entity->GetAlpha();
				const auto reflective		= entity->GetReflectivePower();
				const auto specular			= entity->GetSpecularPower();
				const auto lightAffected	= entity->IsLightAffected();
				const auto sunSource		= false; //TODO: wrong way, fix this
				const auto& worldMatrix		= entity->CalculateWorldMatrix();

				data.emplace_back( worldMatrix, alpha, reflective, specular, lightAffected, sunSource );
			}

			//Copy all instance data
			DirectXHelper::CopyToBuffer<ModelInstanceData>( deviceContext, mInstanceBuffer, &data[ 0 ], instanceCount );

			//For each model entry, bind and render
			for( auto i = 0 ; i < modelEntriesCount ; ++ i )
			{
				//Now render each model entry using the given instance buffer from above, as the buffer will be applicable
				//to each set of entries
				auto& mesh = model->GetEntryList().at( i );
				vertexCount = mesh->GetVertexCount();
				indexCount = mesh->GetIndexCount();

				shader->BindInstanceBuffer( deviceContext, mesh, mInstanceBuffer );
				shader->RenderInstanced( deviceContext, instanceCount, vertexCount, indexCount, i, entityList[ 0 ], cascadeIndex );
			}
		}

		mInstanceMap.clear();
	}
}
