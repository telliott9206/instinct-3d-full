/*
 * shadermanager.cpp
 *
 * Created 03/04/2019
 * Written by Todd Elliott
 */

#include "Shadermanager.h"
#include <Engine/Utility/DirectXHelper.h>
#include <Engine/Texture/Texture2D.h>
#include <Engine/World.h>
#include <Engine/Model/Billboard.h>
#include <Engine/Camera.h>
#include <Engine/Shaders/PPFX/PresetInfo.h>
#include <Engine/Shaders/Depthshader.h>
#include <Engine/Shaders/Detailshader.h>
#include <Engine/Shaders/Mergeshader.h>
#include <Engine/Shaders/Simpleshader.h>
#include <Engine/Shaders/Shader.h>
#include <Engine/Shaders/Skydomeshader.h>
#include <Engine/Shaders/Watershader.h>
#include <Engine/Shaders/PPFX/PPEffect.h>
#include <Engine/Shaders/PPFX/PPShader.h>
#include <Engine/Shaders/PPFX/Presets/Sunshader.h>
#include <Engine.h>

namespace Instinct
{
	ShaderManager::ShaderManager()
	{
		//TODO: Why this is done here, should be moved into shader
		D3D11_INPUT_ELEMENT_DESC layout[] =
		{
			{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
			{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
			{ "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },

			{ "TEXCOORD", 1, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, 0, D3D11_INPUT_PER_INSTANCE_DATA, 1 },
			{ "TEXCOORD", 2, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_INSTANCE_DATA, 1 },
			{ "TEXCOORD", 3, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_INSTANCE_DATA, 1 },
			{ "TEXCOORD", 4, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_INSTANCE_DATA, 1 },

			{ "TEXCOORD", 5, DXGI_FORMAT_R32_FLOAT, 1, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_INSTANCE_DATA, 1 },
			{ "TEXCOORD", 6, DXGI_FORMAT_R32_FLOAT, 1, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_INSTANCE_DATA, 1 },
			{ "TEXCOORD", 7, DXGI_FORMAT_R32_FLOAT, 1, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_INSTANCE_DATA, 1 },
			{ "TEXCOORD", 8, DXGI_FORMAT_R32_UINT, 1, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_INSTANCE_DATA, 1 },
			{ "TEXCOORD", 9, DXGI_FORMAT_R32_UINT, 1, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_INSTANCE_DATA, 1 }
		};

		const int elementCount = sizeof( layout ) / sizeof( layout[ 0 ] );

		mBasicShader = Shader::Load( Engine::GetResourceDirectory() + "Shaders/Basic.fx", "VS", "PS", *layout, elementCount );	//TODO: possibly rename shader uses for models
		mDetailShader = DetailShader::Load();													//TODO: possibly rename shader used for terrain rendering
		mDepthShader = DepthShader::Load();														//Renders depth
		mMergeShader = MergeShader::Load();														//Merges the 2 render passes together
		mSkydomeShader = SkydomeShader::Load();													//Renders the skydome
		mWaterShader = WaterShader::Load();														//Renders water/ocean
		mSimpleShader = SimpleShader::Load();													//Renders colour only models eg. bounding boxes

		//NOTE: POSSIBLE BREAKAGE HERE CAUSED BY INSTANCING OF MODELS
		//mSunShader = SunShader::Load( "Shaders/Basic.fx", "VS", "PS", *layout, elementCount );	//TODO: this mechanism needs to be redone

		//CreateSunShader();

		//Create PPFX texture
		auto* renderWindow	= Engine::Instance().GetContext()->GetRenderWindow();
		const auto width	= renderWindow->GetWidth();
		const auto height	= renderWindow->GetHeight();
		mPPFXTexture = Texture2D::Create( width, height );
	}

	ShaderManager::~ShaderManager()
	{
	}

	void ShaderManager::RenderPPEffects( ID3D11DeviceContext& deviceContext, Texture2D& renderTexture )
	{
		//Render each PPEffect
		for( auto effect : mEffects )
		{
			effect->Render( deviceContext, renderTexture );
		}
	}

	void ShaderManager::AddPPEffect( std::shared_ptr<PPEffect> effect )
	{
		mEffects.push_back( effect );
	}

	void ShaderManager::CreateSunShader()
	{
		auto context = Engine::Instance().GetContext();
		auto* window = context->GetRenderWindow();

		const auto width = window->GetWidth();
		const auto height = window->GetHeight();

		std::vector<std::shared_ptr<PPShader>> shaders;
		const auto dir = Engine::GetResourceDirectory();

		shaders.push_back( PPShader::Load( dir + "Shaders/PPFX/Godrays/Godrays.fx", width, height ) );
		shaders.push_back( PPShader::Load( dir + "Shaders/PPFX/Godrays/Blend.fx", width, height ) );
		shaders.push_back( PPShader::Load( dir + "Shaders/Final.fx", width, height ) );

		auto effect = PPEffect::Create( shaders );
		mEffects.push_back( effect );
	}

	/*
	Texture2D* ShaderManager::RenderSunShader( ID3D11DeviceContext& deviceContext, Texture2D* original )
	{
		mSunShader->BindSunBuffer( deviceContext );
		auto previous = mSunShaders[ 0 ]->Render( deviceContext, original, nullptr, false );

		return mSunShaders[ 1 ]->Render( deviceContext, original, previous, false );
	}
	*/
}
