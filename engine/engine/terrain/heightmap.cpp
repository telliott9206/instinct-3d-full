/*
 * HeightMap.cpp
 *
 * Created 01/03/2019
 * Written by Todd Elliott
 */

#include "HeightMap.h"
#include <Engine/Model/Vertex.h>
#include <Engine.h>

namespace Instinct
{
	HeightMap::HeightMap( int width, int height, const std::vector<Vertex>& vertices ) :
		mWidth( width ),
		mHeight( height ),
		mVertices( vertices )
	{
	}

	HeightMap::~HeightMap()
	{
	}


	std::shared_ptr<HeightMap> HeightMap::Load( const std::string& file )
	{
		FILE* filePtr;
		int error;

		BITMAPFILEHEADER fileHeader;
		BITMAPINFOHEADER infoHeader;
		uint32_t count;

		error = fopen_s( &filePtr, file.c_str(), "rb" );
		if( error != 0)
		{
			Engine::Error( "File Error", "Error loading heightmap file %s", file.c_str() );
			return nullptr;
		}

		//read the header data
		count = fread( &fileHeader, sizeof( BITMAPFILEHEADER ), 1, filePtr );
		if( count != 1 )
		{
			Engine::Error( "File Error", "Error loading heightmap file header for file %s", file.c_str() );
			return nullptr;
		}

		count = fread( &infoHeader, sizeof( BITMAPINFOHEADER ), 1, filePtr );
		if( count != 1 )
		{
			Engine::Error( "File Error", "Error loading heightmap info header for file %s", file.c_str() );
			return nullptr;
		}

		const auto width = infoHeader.biWidth;
		const auto height = infoHeader.biHeight;

		const auto size = width * height * 3;
		auto image = std::unique_ptr<unsigned char[]>( new unsigned char[ size ] );

		fseek( filePtr, fileHeader.bfOffBits, SEEK_SET );
		count = fread( image.get(), 1, size, filePtr );
		if( count != size )
		{
			return nullptr;
		}

		fclose( filePtr );

		std::vector<Vertex> data( width * height );
		int k = 0, index;

		for( auto j = 0 ; j < height ; j ++ )
		{
			for( auto i = 0 ; i < width ; i ++ )
			{
				index = ( height * j ) + i;

				data[ index ].mPosition.x = static_cast<float>( i );
				data[ index ].mPosition.y = static_cast<float>( image[ k ] );
				data[ index ].mPosition.z = static_cast<float>( j );

				k += 3;
			}
		}

		return std::make_shared<HeightMap>( width, height, data );
	}

	void HeightMap::Normalize( float factor )
	{
		for( auto j = 0 ; j < mHeight ; j ++ )
		{
			for( auto i = 0 ; i < mWidth ; i ++ )
			{
				mVertices[ ( mHeight * j ) + i ].mPosition.y /= factor;
			}
		}
	}
}
