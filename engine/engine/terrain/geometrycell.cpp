/*
 * GeometryCell.cpp
 *
 * Created 26/03/2019
 * Written by Todd Elliott
 */

#include "GeometryCell.h"
#include <Engine/Model/Mesh.h>
#include <Engine/Render/Context.h>
#include <Engine/Utility/MathHelper.h>
#include <Engine/GameTime.h>
#include <Engine.h>

namespace Instinct
{
	GeometryCell::GeometryCell() :
		mOriginalPositions( { 0.f, 0.f, 0.f } )
	{
	}

	GeometryCell::~GeometryCell()
	{
	}

	void GeometryCell::CalculateBoundingBox()
	{
		auto mesh = mModels[ 0 ];	//Only a single mesh for geometry cell
		const auto& vertices = mesh->GetVertices();
		const auto size = mSize / 2.f;

		float minY, maxY;
		minY = 99999.f;
		maxY = -99999.f;
	
		for( const auto& vertex : vertices )
		{
			const auto pos = vertex.mPosition;

			minY = min( minY, pos.y );
			maxY = max( maxY, pos.y );
		}

		const auto sizeY = maxY - minY;
		const auto centerY =  minY + ( sizeY / 2.f );

		mAabb.Center = XMFLOAT3( size, centerY, size );
		mAabb.Extents = XMFLOAT3( size, sizeY / 2.f, size );

		//Reset the Y position in the world based on the center position
		mPosition.y = centerY - centerY;
	}

	void GeometryCell::SetGeometry( const std::vector<Vertex>& inVertices, int m, int n, float size, int width, int unitsPerSquare )
	{
		const auto cellsPerLength	= width / static_cast<uint32_t>( size );
		const auto verticesPerLength = ( width - 1 ) * 6;
	
		const auto squaresPerCell	= static_cast<uint32_t>( size ) / unitsPerSquare;
		const auto verticesPerCell	= squaresPerCell * 6;

		const auto vertexCount		= static_cast<uint32_t>( size ) * static_cast<uint32_t>( size ) * 6;
		const auto indexCount		= vertexCount;

		std::vector<Vertex> vertices( vertexCount );
		std::vector<uint32_t> indices( indexCount );

		uint32_t index = 0;

		const uint32_t startPos	= ( n * ( verticesPerLength * squaresPerCell ) ) + ( m * verticesPerCell );
		mSize				= size;
		mPosition			= inVertices[ startPos ].mPosition;

		for( uint32_t j = 0u ; j < squaresPerCell ; ++ j )
		{
			for( uint32_t i = 0u ; i < squaresPerCell ; ++ i )
			{
				for( uint32_t k = 0u ; k < 6 ; ++ k )
				{
					const auto pos = startPos + ( j * verticesPerLength ) + ( i * 6 ) + k;

					vertices[ index ].mPosition = inVertices.at( pos ).mPosition;
					vertices[ index ].mNormal = inVertices[ pos ].mNormal;
					vertices[ index ].mTexcoord = inVertices[ pos ].mTexcoord;

					indices[ index ] = index;
					index ++;
				}
			}
		}

		auto mesh = Mesh<Vertex>::Create( vertices, indices );
		mModels.push_back( mesh );
	}

	void GeometryCell::ModifyVertices( const XMFLOAT2& inPosition, int strength, int size, tTerrainVertexModificationMode function )
	{
		switch( function )
		{
			case tTerrainVertexModificationMode::kFlatten:
			{
				FlattenVertices( inPosition, strength, size );
				break;
			}

			case tTerrainVertexModificationMode::kRaise:
			case tTerrainVertexModificationMode::kLower:
			{
				ModifyVerticesHeight( inPosition, strength, size, function );
				break;
			}
		}
	}

	void GeometryCell::ModifyVertices( const XMFLOAT3& position )
	{
		auto model = mModels.at( 0 );
		const auto& vertices = model->GetVertices();
		const auto vertexCount = model->GetVertexCount();

		//TODO: Find out what this was supposed to do before bad code was removed
		for( uint32_t i = 0u ; i < vertexCount ; ++ i )
		{
		}
	}

	void GeometryCell::ApplyWaves()
	{
		//The worry about this is that it is likely going to be very slow. Perhaps this may not be used and just use tesselation 
		//to form the waves on the GPU.

		//Test Code only!
		const auto steepness = 0.9f;
		const auto amplitude = 0.75f;
		const auto waveLength = 10.f;
		const auto speed = 4.f;
		const auto direction = 45.f;
		//-----------------------------------------------|

		//TODO: Find the gerstner wave paper again, and re-attempt double gerstner wave for realism look
		const auto time = Time::Instance().GetGameTime();

		auto model = mModels.at( 0 );
		auto& vertices = model->GetVertices();
		const auto vertexCount = model->GetVertexCount();

		for( auto& vertex : vertices )
		{
			auto position = vertex.mPosition;

			const auto k = 2.f * XM_PI / waveLength;
			const auto c = sqrt( 9.8f / k ) * time;
			const auto a = steepness / k;
			const auto d = MathHelper::AngleToVector( direction );
			const auto e = XMFLOAT2( position.x, position.z );

			auto f = GerstnerWave( 0, k, c, a, d, position );
			f = GerstnerWave( 1, k, c, a, d, position );
			position.y = amplitude * sinf( f );

			vertex.mPosition = position;
	
			const auto tangent = XMFLOAT3( 1.f, k * steepness * cosf( f ), 0.f );
			const auto normal = XMFLOAT3( -tangent.y, tangent.x, 0.f );
			vertex.mNormal = normal;
		}

		auto& deviceContext = Engine::Instance().GetContext()->GetDeviceContext();
		model->UpdateVertices( deviceContext, &vertices[ 0 ] );
	}

	void GeometryCell::ModifyVerticesHeight( const XMFLOAT2& inPosition, int strength, int size, tTerrainVertexModificationMode function )
	{
		const uint32_t cellWidth = 64u;	//TODO: Should this be configurable somewhere?

		auto model = mModels.at( 0 );
		auto vertices = model->GetVertices();

		const uint32_t vertexCount = model->GetVertexCount();

		int reverseMutliplier = 1;
		if( function == tTerrainVertexModificationMode::kLower )
		{
			reverseMutliplier = -1;
		}

		for( uint32_t i = 0u ; i < vertexCount ; ++ i )
		{
			auto& vertex = vertices[ i ];

			const auto position = vertex.mPosition;
			auto newPosition( position );
			const auto dist = MathHelper::PointDistance2D( inPosition, XMFLOAT2( position.x, position.z ) );

			if( dist <= static_cast<float>( size ) )
			{
				newPosition.x = position.x;
				const auto amount = ( strength - ( strength  / ( static_cast<float>( size ) / dist  ) ) );

				newPosition.y += reverseMutliplier * (float)pow( amount, 1.2 );
				newPosition.z = position.z;

				vertices[ i ].mPosition = newPosition;

				if( ( i + 1 ) % 6 == 0 )
				{
					XMFLOAT3 normal;
					normal = MathHelper::CrossProduct( vertices[ i - 5 ].mPosition, vertices[ i - 3 ].mPosition, vertices[ i - 4 ].mPosition );
					MathHelper::VectorNormalize3D( normal );

					vertices[ i - 5 ].mPosition = normal;
					vertices[ i - 4 ].mPosition = normal;
					vertices[ i - 3 ].mPosition = normal;

					normal = MathHelper::CrossProduct( vertices[ i - 2 ].mPosition, vertices[ i - 0 ].mPosition, vertices[ i - 1 ].mPosition );
					MathHelper::VectorNormalize3D( normal );

					vertices[ i - 2 ].mPosition = normal;
					vertices[ i - 1 ].mPosition = normal;
					vertices[ i - 0 ].mPosition = normal;
				}
			}
		}

		auto& deviceContext = Engine::Instance().GetContext()->GetDeviceContext();
		model->UpdateVertices( deviceContext, &vertices[ 0 ] );
	}

	void GeometryCell::FlattenVertices( const XMFLOAT2& position,int iStregth, int iSize )
	{
	}

	float GeometryCell::GerstnerWave( int index, float k, float c, float a, const XMFLOAT2& dir, XMFLOAT3& output )
	{
		const auto f = k * ( MathHelper::DotProduct( dir, { output.x, output.y } ) - c );

		if( index > 0 )
		{
			output.y += a * sinf( f );
		}
		else
		{
			output.y = a * sinf( f );
		}

		return f;
	}
}
