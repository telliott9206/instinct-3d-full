/*
 * GeometryContainer.h
 *
 * Created 26/03/2019
 * Written by Todd Elliott
 *
 * A class to be inherited by any kind terrain geometry class such as the Geometry Cell and
 * any other larger version of Geometry Cell in the future.
 */

#ifndef _GEOMETRY_CONTAINER_H
#define _GEOMETRY_CONTAINER_H

#include <Instinct.h>

namespace Instinct
{
	struct GeometryContainer
	{
		GeometryContainer() = default;
		~GeometryContainer() = default;

		void SetPosition( const XMFLOAT3& qPosition )	{ mPosition = qPosition; }
		float GetSize() const							{ return mSize; }
		const XMFLOAT3& GetPosition() const				{ return mPosition; }

		float		mSize;
		XMFLOAT3	mPosition;
	};
};

#endif
