/*
 * FoliageLayer.h
 *
 * Created 14/11/2021
 * Written by Todd Elliott
 */

#ifndef _FOLIAGELAYER_H
#define _FOLIAGELAYER_H

#include <Instinct.h>

namespace Instinct
{
	class Model;

	struct FoliageLayer
	{
		FoliageLayer() :
			mUseGrassTriQuad( true ),
			mDensity( 0.5f ),
			mMinHeight( 0.f ),
			mMaxHeight( 0.f ),
			mMinSlope( 0.f ),
			mMaxSlope( 0.f ),
			mCastShadows( false )
		{
		}

		std::vector<Model*> mModelList;

		bool mUseGrassTriQuad;	//Should this the tri quad for grass or are we using a loaded model?

		float mDensity;
		float mMinHeight;
		float mMaxHeight;
		float mMinSlope;
		float mMaxSlope;
		bool mCastShadows;
	};
}

#endif
