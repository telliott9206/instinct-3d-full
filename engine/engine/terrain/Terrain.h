/*
 * Terrain.h
 *
 * Created 01/03/2019
 * Written by Todd Elliott
 *
 * Encapsulates all the functionality for constructing terrain.
 */

#ifndef _TERRAIN_H
#define _TERRAIN_H

#include <Engine/Terrain/TerrainTextureLayer.h>
#include <Engine/Terrain/GeometryCell.h>
#include <Engine/Texture/Texture.h>
#include <Engine/Model/Model.h>
#include <Engine/Texture/Texture2DArraySimple.h>

#define VERTICES_PER_SQUARE		6		//TODO: Not sure if this should be here
#define TERRAIN_CELL_SIZE		64

namespace Instinct
{
	//Positions of the 6 vertices that make up a square
	enum VertexSquarePos
	{
		kVertexTopLeft_T1 = 0,
		kVertexBottomLeft_T1,
		kVertexBottomRight_T1,
		kVertexTopRight_T2,
		kVertexTopleft_T2,
		kVertexBottomRight_T2
	};

	class HeightMap;
	class TerranLayer;

	class Terrain: public Model
	{
	public:
		explicit Terrain( int width, int height, int unitsPerSquare );
		DLLEXPORT virtual ~Terrain();

		/* @brief	factory function for instantiating a terrain object
		 *
		 * @return	shared_ptr to newly instantiated terrain object
		 */
		DLLEXPORT static std::shared_ptr<Terrain> Create( int width, int height, int unitsPerSquare );

		/* @brief	factory function for instantiating a terrain object from a heightmap
		 *
		 * @return	shared_ptr to newly instantiated terrain object
		 */
		DLLEXPORT static std::shared_ptr<Terrain> CreateFromHeightMap( std::shared_ptr<HeightMap> heightmap );

		DLLEXPORT void AddTextureLayer( TerrainTextureLayer* textureLayer );
		DLLEXPORT void AddTextureLayer( const TerrainTextureLayer& textureLayer );
		DLLEXPORT void DeleteTextureLayer( int index );
		DLLEXPORT void UpdateTextureLayer( int index, const TerrainTextureLayer& layer );
		DLLEXPORT void DeleteAllTextureLayers();
		DLLEXPORT const std::vector<TerrainTextureLayer>& GetTextureLayers()		{ return mTerrainLayers; }
		DLLEXPORT int GetTextureLayerCount() const									{ return mTextureLayerCount; }
		DLLEXPORT void ModifyTerrain( XMFLOAT2& position, int strength, int size, tTerrainVertexModificationMode function );

		DLLEXPORT void UpdateTextureArray();
		DLLEXPORT void Loaded() override;

		const std::vector<std::shared_ptr<GeometryCell>>& GetGeometryCells() const	{ return mGeometryCells; }
		int GetGeometryCellsCount() const											{ return mGeometryCellsCount; }

		int Width() const															{ return mWidth; }
		int Height() const															{ return mHeight; }
		int GetVerticesPerLength() const											{ return mVerticesPerLength; }

		Texture2DArraySimple* GetTextureArray()										{ return mTextureArray.get(); }

		float GetTerrainHeightAtPosition( const XMFLOAT2& position ) const;

	protected:
		void CalculateFaceNormals( std::vector<XMFLOAT3>& vertexNormals, std::vector<Vertex>& vertices );
		bool CalculateBumpInfo();
		void GenerateTerrainCells( bool bIsWater );

	private:
		void ModifyGeometryCellVertices( const XMFLOAT2& position, int strength, int size, tTerrainVertexModificationMode function );

	protected:
		int											mWidth;
		int											mHeight;
		int											mUnitsPerSquare;
		int											mVerticesPerLength;

		int											mVertexCount;

		int											mGeometryCellsCount;
		std::vector<std::shared_ptr<GeometryCell>>	mGeometryCells;

	private:
		bool										mTextureLayerBufferBuilt;
		int											mTextureLayerCount;
		
		std::vector<TerrainTextureLayer>			mTerrainLayers;
		std::unique_ptr<Texture2DArraySimple>		mTextureArray;

		float*										mTerrainHeights;
	};
};

#endif
