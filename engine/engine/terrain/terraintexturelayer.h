/*
 * TerrainTextureLayers.h
 *
 * Created 01/03/2019
 * Written by Todd Elliott
 *
 * Encapsulates the information for texture layer that is passed to a terrain object.
 */

#ifndef _TERRAINTEXTURELAYER_H
#define _TERRAINTEXTURELAYER_H

#include <Engine/Texture/Texture.h>

namespace Instinct
{
	struct cbTerrainTextureLayerOptions
	{
		cbTerrainTextureLayerOptions() = default;

		cbTerrainTextureLayerOptions( float minHeight, float maxHeight, float minSlope, float maxSlope, int slopeConnectorBottom, int slopeConnectorTop ) :
			mMinHeight( minHeight ),
			mMaxHeight( maxHeight ),
			mMinSlope( minSlope ),
			mMaxSlope( maxSlope ),
			mSlopeConnectorBottom( slopeConnectorBottom ),
			mSlopeConnectorTop( slopeConnectorTop )
		{
		}

		float mMinHeight			{ 0.f };
		float mMaxHeight			{ 0.f };
		float mMinSlope				{ 0.f };
		float mMaxSlope				{ 0.f };

		int mSlopeConnectorBottom	{ 0 };
		int mSlopeConnectorTop		{ 0 };

		XMFLOAT2 _padding;
	};

	struct TerrainTextureLayer
	{
		std::shared_ptr<Texture> mDiffuseTexture;
		std::shared_ptr<Texture> mNormalTexture;

		cbTerrainTextureLayerOptions mOptions;
	};
};

#endif
