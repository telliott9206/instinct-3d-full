/*
 * Heightmap.h
 *
 * Created 01/03/2019
 * Written by Todd Elliott
 *
 * Used for loading in a height map; a greyscale texture where white represents the highest
 * point and black represents the lowest point. This is converted in vertices and hence used
 * for constructing terrain.
 */

#ifndef _HEIGHTMAP_H
#define _HEIGHTMAP_H

#include <Instinct.h>

namespace Instinct
{
	struct Vertex;

	class HeightMap
	{
	public:
		HeightMap( int x, int y, const std::vector<Vertex>& vertices );
		~HeightMap();

		DLLEXPORT static std::shared_ptr<HeightMap> Load( const std::string& file );

		DLLEXPORT void Normalize( float flFactor );
		int GetWidth() const	{ return mWidth; }
		int GetHeight() const	{ return mHeight; }
		
		const std::vector<Vertex>& GetData() const { return mVertices; }

	private:
		int mWidth;
		int mHeight;

		std::vector<Vertex> mVertices;
	};
};

#endif
