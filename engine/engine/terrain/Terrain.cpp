/*
 * Terrain.cpp
 *
 * Created 01/03/2019
 * Written by Todd Elliott
 */

#include <Engine/Terrain/Terrain.h>
#include <Engine/Terrain/Heightmap.h>
#include <Engine/Utility/MathHelper.h>
#include <Engine/Terrain/GeometryCell.h>
#include <Engine/Shaders/DetailShader.h>
#include <Engine/Model/Vertex.h>
#include <Engine/Manager/ShaderManager.h>
#include <Engine/Texture/Loader/WIC/WICTextureLoader.h>
#include <Engine/Texture/Texture2DArraySimple.h>
#include <Engine/GeometryGenerator.h>
#include <Engine.h>

namespace Instinct
{
	Terrain::Terrain( int width, int height, int unitsPerSquare ) :
		mWidth( width ),
		mHeight( height ),
		mUnitsPerSquare( unitsPerSquare ),
		mVerticesPerLength( 0 ),
		mVertexCount( 0 ),
		mGeometryCellsCount( 0 ),
		mTextureLayerBufferBuilt( false ),
		mTextureLayerCount( 0 )
	{
	}

	Terrain::~Terrain()
	{
	}

	std::shared_ptr<Terrain> Terrain::Create( int width, int height, int unitsPerSquare )
	{
		if( unitsPerSquare <= 0 )
		{
			Engine::Print( "Attempt to create Terrain failed: Units Per Square must be greater than 0" );
			return nullptr;
		}

		if( width % unitsPerSquare != 0 || height % unitsPerSquare != 0 )
		{
			Engine::Print( "Attempt to create Terrain failed: Both terrain width and height must be divisible by Units Per Square" );
			return nullptr;
		}

		auto terrain = std::make_shared<Terrain>( width, height, unitsPerSquare );
		terrain->AddModelEntry( GeometryGenerator::CreateGrid( width, height ) );
		terrain->GenerateTerrainCells( false );
		terrain->Loaded();

		return terrain;
	}

	std::shared_ptr<Terrain> Terrain::CreateFromHeightMap( std::shared_ptr<HeightMap> heightmap )
	{
		if( heightmap == nullptr )
		{
			Engine::Error( "Null Pointer", "A null pointer HeightMap was passed into Terrain::CreateFromHeightMap()" );
			return nullptr;
		}

		//Test code only
		const auto texRepeatX = 256.f;
		const auto texRepeatY = 256.f;
		//----------------------------|

		const auto width = heightmap->GetWidth();
		const auto height = heightmap->GetHeight();
		const auto verticesPerLength = ( width - 1 )  * VERTICES_PER_SQUARE;
		auto terrain = std::make_shared<Terrain>( width, height, 1 );

		terrain->mVerticesPerLength = verticesPerLength;

		//Calculate the vertex index counts
		const auto vertexCount = width * height * VERTICES_PER_SQUARE;
		const auto indexCount = vertexCount;
		terrain->mVertexCount = vertexCount;
		terrain->mTerrainHeights = new float[ vertexCount ];
		
		//Assign vector memory for mesh
		std::vector<Vertex> vertices( vertexCount );
		std::vector<uint32_t> indices( indexCount );

		//Setup mapping from heightmap to vertex
		int index1, index2, index3, index4, index = 0;
		const auto& heightData = heightmap->GetData();

		//Assign vector memory for vertex normals
		std::vector<XMFLOAT3> normals( indexCount );

		//Iteratively calculate the position, normal, and texcoords for each vertex
		for( auto j = 0 ; j < height - 1 ; ++ j )
		{
			for( auto i = 0 ; i < width - 1 ; ++ i )
			{
				index1 = ( height * j ) + i;					//top left
				index2 = ( height * j ) + ( i + 1 );			//top right
				index3 = ( height * ( j + 1 ) ) + i;			//bottom left
				index4 = ( height * ( j + 1 ) ) + ( i + 1 );	//bottom right

				const uint32_t vertexBase = ( j * verticesPerLength ) + ( i * VERTICES_PER_SQUARE );

				vertices[ vertexBase + 0 ].mPosition = XMFLOAT3( i * 1.f, heightData[ index1 ].mPosition.y, j * 1.f );
				vertices[ vertexBase + 1 ].mPosition = XMFLOAT3( i * 1.f, heightData[ index3 ].mPosition.y, j * 1.f + 1.f );
				vertices[ vertexBase + 2 ].mPosition = XMFLOAT3( i * 1.f + 1.f, heightData[ index4 ].mPosition.y, j * 1.f + 1.f );

				vertices[ vertexBase + 3 ].mPosition = XMFLOAT3( i * 1.f + 1.f, heightData[ index2 ].mPosition.y, j * 1.f );
				vertices[ vertexBase + 4 ].mPosition = XMFLOAT3( i * 1.f, heightData[ index1 ].mPosition.y, j * 1.f );
				vertices[ vertexBase + 5 ].mPosition = XMFLOAT3( i * 1.f + 1.f, heightData[ index4 ].mPosition.y, j * 1.f + 1.f );

				const XMFLOAT3 normal = MathHelper::CrossProduct( 
					vertices[ vertexBase + 1 ].mPosition,
					vertices[ vertexBase + 0 ].mPosition,
					vertices[ vertexBase + 2 ].mPosition );

				for( uint32_t k = 0u ; k < VERTICES_PER_SQUARE ; ++ k )
				{
					indices[ vertexBase + k ] = index;
					index ++;

					const uint32_t vertex = vertexBase + k;
					const auto x = vertices[ vertex ].mPosition.x;
					const auto y = vertices[ vertex ].mPosition.z;

					vertices[ vertex ].mTexcoord = XMFLOAT2( x / width * texRepeatX, y / height * texRepeatY );

					normals[ vertex ] = normal;
					terrain->mTerrainHeights[ vertex ] = vertices[ vertex ].mPosition.y;
				}
			}
		}

		//Pass vertex normals in, get face normals out!
		terrain->CalculateFaceNormals( normals, vertices );

		auto mesh = Mesh<Vertex>::Create( vertices, indices );
		mesh->SetMaterialIndex( 0 );
		terrain->mModels.push_back( mesh );

		terrain->GenerateTerrainCells( false );
		terrain->Loaded();

		return terrain;
	}

	void Terrain::AddTextureLayer( TerrainTextureLayer* textureLayer )
	{
		mTerrainLayers.push_back( *textureLayer );
		mTextureLayerCount ++;
	}

	void Terrain::AddTextureLayer( const TerrainTextureLayer& textureLayer )
	{
		mTerrainLayers.push_back( textureLayer );
		mTextureLayerCount ++;
	}

	void Terrain::DeleteTextureLayer( int index )
	{
		mTerrainLayers.erase( mTerrainLayers.begin() + index );
		mTextureLayerCount --;
	}

	void Terrain::UpdateTextureLayer( int index, const TerrainTextureLayer& layer )
	{
		mTerrainLayers[ index ] = layer;
	}

	void Terrain::DeleteAllTextureLayers()
	{
		mTerrainLayers.clear();
		mTextureLayerCount = 0;
	}

	void Terrain::ModifyTerrain( XMFLOAT2& position, int strength, int size, tTerrainVertexModificationMode function )
	{
		ModifyGeometryCellVertices( position, strength, size, function );
	}

	void Terrain::UpdateTextureArray()
	{
		auto context = Engine::Instance().GetContext();
		auto& device = context->GetDevice();
		auto& deviceContext = context->GetDeviceContext();

		std::vector<std::wstring> fileNames;
		for( const auto resource : mTerrainLayers )
		{
			std::string name = resource.mDiffuseTexture->GetFileName();
			std::wstring wname( name.begin(), name.end() );


			fileNames.push_back( wname );
		}

		//TODO: The function for this needs to be fixed/updated - it doesn't allow MSAA in its current state - looks bad!
		mTextureArray = Texture2DArraySimple::Create( device, deviceContext, fileNames, nullptr, nullptr, true );

		ShaderManager::Instance().GetDetailShader().UpdateTerrainLayerBuffer( deviceContext, mTerrainLayers );
	}

	void Terrain::Loaded()
	{
		mIsLoaded = true;
	}

	float Terrain::GetTerrainHeightAtPosition( const XMFLOAT2& position ) const
	{
		auto height = 0.f;

		const auto vertex = ( static_cast<int>( round( position.y ) ) * mVerticesPerLength ) + ( static_cast<int>( round( position.x ) ) * VERTICES_PER_SQUARE );
		if( vertex >= 0 && vertex <= mVertexCount )
		{
			height = mTerrainHeights[ vertex ];
		}

		return height;
	}

	void Terrain::CalculateFaceNormals( std::vector<XMFLOAT3>& vertexNormals, std::vector<Vertex>& vertices )
	{
		//Calculate the average normal for the surrounding faces per vertex
		const auto verticesPerLength = ( mWidth - 1 ) * VERTICES_PER_SQUARE;
		int index = 0;

		//TODO: Find a cleaner way of doing this whole process, its very dirty!
		for( auto j = 0 ; j < mHeight - 1 ; ++ j )
		{
			for( auto i = 0 ; i < mWidth - 1 ; ++ i )
			{
				const auto topLeft = ( ( j - 1 ) * verticesPerLength ) + ( i * VERTICES_PER_SQUARE ) - VERTICES_PER_SQUARE;
				const auto topRight = ( ( j - 1 ) * verticesPerLength ) + ( i * VERTICES_PER_SQUARE ) + VERTICES_PER_SQUARE;
				const auto bottomLeft = ( ( j + 1 ) * verticesPerLength ) + ( i * VERTICES_PER_SQUARE ) - VERTICES_PER_SQUARE;
				const auto bottomRight = ( ( j + 1 ) * verticesPerLength ) + ( i * VERTICES_PER_SQUARE ) + VERTICES_PER_SQUARE;

				for( auto k = 0 ; k < VERTICES_PER_SQUARE ; ++ k )
				{
					XMFLOAT3 sum( 0.f, 0.f, 0.f );

					//top left
					if( ( k == kVertexTopLeft_T1 || k == kVertexTopleft_T2 ) && topLeft >= 0 )
					{
						sum = MathHelper::AddVector3D( sum, vertexNormals[ topLeft + 2 ] );
						sum = MathHelper::AddVector3D( sum, vertexNormals[ ( j  * verticesPerLength ) + ( i * VERTICES_PER_SQUARE ) - VERTICES_PER_SQUARE + 3 ] ); //left

						const auto val = ( ( j - 1 ) * verticesPerLength ) + ( i * VERTICES_PER_SQUARE ) + 1;
						sum = MathHelper::AddVector3D( sum, vertexNormals[ val >= 0 ? val : 0 ] ); //top 
					}
					//top right
					else if( k == kVertexTopRight_T2 && topRight >= 0 && topRight <= mVertexCount )
					{
						sum = MathHelper::AddVector3D( sum, vertexNormals[ topRight + 1 ] );
						sum = MathHelper::AddVector3D( sum, vertexNormals[ ( j  * verticesPerLength ) + ( i * VERTICES_PER_SQUARE ) + VERTICES_PER_SQUARE + 0 ] ); //right

						const auto val =(  ( j - 1 ) * verticesPerLength ) + ( i * VERTICES_PER_SQUARE ) + 2;
						sum = MathHelper::AddVector3D( sum, vertexNormals[ val >= 0 ? val : 0  ] ); //top 
					}
					//bottom left
					else if( k == kVertexBottomLeft_T1 && bottomLeft >= 0 && bottomLeft <= mVertexCount )
					{
						sum = MathHelper::AddVector3D( sum, vertexNormals[ bottomLeft + 3 ] );
						sum = MathHelper::AddVector3D( sum, vertexNormals[ ( ( j + 1 ) * verticesPerLength ) + ( i * VERTICES_PER_SQUARE ) + 0 ] ); //bottom 

						const auto val = ( j  * verticesPerLength ) + ( i * VERTICES_PER_SQUARE ) - VERTICES_PER_SQUARE + 2;
						sum = MathHelper::AddVector3D( sum, vertexNormals[ val >= 0 ? val : 0 ] ); //left
					}
					//bottom right
					else if( ( k == kVertexBottomRight_T1 || k == kVertexBottomRight_T2 ) && bottomRight <= mVertexCount )
					{
						sum = MathHelper::AddVector3D( sum, vertexNormals[ bottomRight + 0 ] );
						sum = MathHelper::AddVector3D( sum, vertexNormals[ ( j  * verticesPerLength ) + ( i * VERTICES_PER_SQUARE ) + VERTICES_PER_SQUARE + 1 ] ); //right
						sum = MathHelper::AddVector3D( sum, vertexNormals[ ( ( j + 1 ) * verticesPerLength ) + ( i * VERTICES_PER_SQUARE ) + 3 ] ); //bottom 
					}

					//Get the average of these faces
					sum = MathHelper::DivideVector3D( sum, 3.f );

					//Calculate the length of this normal
					const auto length = MathHelper::VectorLength3D( sum );

					//Normalize the final averaged normal
					index = ( j * verticesPerLength ) + ( i * VERTICES_PER_SQUARE ) + k;
					vertexNormals[ index ].x = ( sum.x / length );
					vertexNormals[ index ].y = ( sum.y / length );
					vertexNormals[ index ].z = ( sum.z / length );
					vertices[ index ].mNormal = vertexNormals[ index ];
				}
			}
		}
	}

	void Terrain::GenerateTerrainCells( bool isWater )
	{
		const auto size = TERRAIN_CELL_SIZE;
		const auto rows	= mWidth / static_cast<int>( size );
		const auto count = rows * rows;
		int index;

		mGeometryCellsCount = count;

		auto vertices = mModels[ 0 ]->GetVertices();

		for( int j = 0 ; j < rows ; ++ j )
		{
			for( int i = 0 ; i < rows ; ++ i )
			{
				index = ( rows * j ) + i;

				auto cell = std::make_shared<GeometryCell>();
				cell->SetGeometry( vertices, i, j, size, mWidth, mUnitsPerSquare );
				cell->Loaded();

				mGeometryCells.push_back( cell );
			}
		}
	}

	bool Terrain::CalculateBumpInfo()
	{
		//TODO: Implement this function
		return true;
	}

	void Terrain::ModifyGeometryCellVertices( const XMFLOAT2& inPosition, int strength, int size, tTerrainVertexModificationMode function )
	{
		//Mainly used for the world editor, but could be used for modifying terrain for explosions etc.
		const auto halfWidth = static_cast<int>( mGeometryCells[ 0 ]->GetSize() ) / 2;
		const auto h = static_cast<float>( sqrt( pow( static_cast<float>( halfWidth ), 2 ) + pow( static_cast<float>( halfWidth ), 2 ) ) );

		for( auto& cell : mGeometryCells )
		{
			//Makes sure that we modify all surrounding cells that are touched
			const auto& vertexPos = cell->GetPosition();
			const XMFLOAT2 position( vertexPos.x, vertexPos.z );
			const XMFLOAT2 centrePos( position.x + halfWidth, position.y + halfWidth );
			const auto dist = MathHelper::PointDistance2D( inPosition, centrePos );

			if( dist <= static_cast<float>( size ) + h )
			{
				cell->ModifyVertices( inPosition, strength, size, function );
			}
		}
	}
}
