/*
 * GeometryCell.h
 *
 * Created 26/03/2019
 * Written by Todd Elliott
 *
 * This class is simply an NNxNN section of the terrain that is checked at run time using
 * view frustums to determine if it is within the given view and hence either chosen to be
 * processes and rendered or not.
 */

#ifndef _GEOMETRY_CELL_H
#define _GEOMETRY_CELL_H

#include <Engine/Terrain/GeometryContainer.h>
#include <Engine/Model/Model.h>
#include <Engine/Model/VisualBoundingBox.h>
#include <Engine/Model/Vertex.h>
#include <Engine/Entity/Basic.h>

namespace Instinct
{
	enum class tTerrainVertexModificationMode
	{
		kRaise = 0,
		kLower,
		kFlatten,
		kSet
	};

	class GeometryCell : public Model, public GeometryContainer
	{
	public:
		GeometryCell();
		~GeometryCell();

		void SetGeometry( const std::vector<Vertex>& inVertices, int i, int j, float size, int width, int initsPerSquare );
		void ModifyVertices( const XMFLOAT2& inPosition, int strength, int size, tTerrainVertexModificationMode function );
		void ModifyVertices( const XMFLOAT3& inPosition );
		void ApplyWaves();

	private:		
		void ModifyVerticesHeight( const XMFLOAT2& position, int strength, int size, tTerrainVertexModificationMode function );
		void FlattenVertices( const XMFLOAT2& position, int strength, int size );
		float GerstnerWave( int index, float k, float c, float a, const XMFLOAT2& dir, XMFLOAT3& output );
		void CalculateBoundingBox();

	private:
		XMFLOAT3							mOriginalPositions;
		std::vector<std::unique_ptr<Basic>> mFoliages;
	};
};

#endif
