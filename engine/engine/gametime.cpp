/*
 * GameTime.cpp
 *
 * Created 04/03/2019
 * Written by Todd Elliott
 */

#include "GameTime.h"
#include <Engine/Utility/Util.h>
#include <Engine.h>

namespace Instinct
{
	Time::Time() :
		mSecondsPerCount( 0.0 ),
		mDeltaTime( -1.0 ),
		mBaseTime( static_cast<__int64>( 0.0 ) ),
		mPausedTime( static_cast<__int64>( 0.0 ) ),
		mPrevTime( static_cast<__int64>( 0.0 ) ),
		mCurrentTime( static_cast<__int64>( 0.0 ) ),
		mStopped( false )
	{
		__int64 countsPerSecond;
		QueryPerformanceFrequency( (LARGE_INTEGER*)&countsPerSecond );

		mSecondsPerCount = 1.0 / static_cast<double>( countsPerSecond );
	}

	float Time::GetDeltaTime() const
	{
		return static_cast<float>( mDeltaTime );
	}

	float Time::GetGameTime() const
	{
		return static_cast<float>( mStopped ?
			( ( mStopTime - mPausedTime ) - mBaseTime ) * mSecondsPerCount :
			( ( mCurrentTime - mPausedTime ) - mBaseTime ) * mSecondsPerCount );
	}

	void Time::Reset()
	{
		QueryPerformanceCounter( (LARGE_INTEGER*)&mCurrentTime );

		mBaseTime = mCurrentTime;
		mPrevTime = mCurrentTime;
		mStopTime = 0;
		mStopped = false;
	}

	void Time::Tick()
	{
		if( mStopped )
		{
			mDeltaTime = 0.0;
			return;
		}

		//get the time this frame
		QueryPerformanceCounter( (LARGE_INTEGER*)&mCurrentTime );

		//determine the time difference between this frame and previous
		mDeltaTime = ( mCurrentTime - mPrevTime ) * mSecondsPerCount;

		//prepare for next frame
		mPrevTime = mCurrentTime;

		//force non-negative. if the processor goes into save mode or we get shuffled to another processor
		//then deltaTime can be negative.
		if( mDeltaTime < 0.0 )
		{
			mDeltaTime = 0.0;
		}

		CalculateFrameStats();
	}

	void Time::CalculateFrameStats()
	{
		static auto frameCount = 0;
		static auto timeElapsed = 0.f;

		++ frameCount;

		if( ( GetGameTime() - timeElapsed ) >= 1.f )
		{
			const auto fps = static_cast<float>( frameCount ); // fps = frameCount / 1
			const auto mspf = 1000.0f / fps;

	#ifdef _DEBUG
			Engine::Print( "FPS: %.0f || Frame Time: %.6fms", fps, mspf );
	#endif

			//reset for next average
			frameCount = 0;
			timeElapsed += 1.0f;
		}
	}
}
