/*
 * Camera.cpp
 *
 * Created 17/12/2018
 * Written by Todd Elliott
 */

#include "Camera.h"
#include <Engine/Render/Renderwindow.h>
#include <Engine/Render/Context.h>
#include <Engine/Input/Keyboard.h>
#include <Engine/Input/Mouse.h>
#include <Engine/Utility/DirectXHelper.h>
#include <Engine/Utility/MathHelper.h>
#include <Engine/World.h>
#include <Engine/Terrain/Terrain.h>
#include <Engine.h>
#include <Engine/Ray/RayCast.h>
#include <Engine/Ray/RayResult.h>

namespace Instinct
{
	Camera::Camera( tCameraType cameraType ) :
		mType( cameraType ),
		mMoveSpeed( 100.f ),
		mTarget( XMFLOAT3( 0.f, 0.f, 0.f ) ),
		mUp( XMFLOAT3( 0.f, 1.f, 0.f ) )
	{
		mPosition = XMFLOAT3( 0.f, 0.f, -1.f );
		mRotation = XMFLOAT3( 0.f, 0.f, 0.f );
	}

	Camera::~Camera()
	{
	}

	std::shared_ptr<Camera> Camera::Create()
	{
		//TODO: Ideally this should be added to the entity list of the world automatically
		auto camera = std::shared_ptr<Camera>( new Camera() ); //Manual shared_ptr for aligned 16 class

		if( !camera->Init() )
		{
			return nullptr;
		}

		return camera;
	}

	void Camera::Update( float delta )
	{
		switch( mType )
		{
			case tCameraType::kStatic:
			{
				//TODO: work out if anything is needed here?
				break;
			}

			case tCameraType::kFreeLook:
			{
				HandleFreeLookUpdate( delta );
				break;
			}

			case tCameraType::kFirstPerson:
			{
				//TODO: implement type of camera
				break;
			}

			case tCameraType::kThirdPerson:
			{
				//TODO: implement type of camera
				break;
			}

			case tCameraType::kEditor:
			{
				HandleEditorUpdate( delta );
				break;
			};
		};

		mView = DirectXHelper::GenerateViewMatrixLookDir( mPosition, mRotation );
	}

	void Camera::GenerateProjectionMatrix()
	{
		auto context = Engine::Instance().GetContext();
		auto* window = context->GetRenderWindow();

		const auto width = static_cast<float>( window->GetWidth() );
		const auto height = static_cast<float>( window->GetHeight() );

		mProjection = XMMatrixPerspectiveFovLH( 0.4f * XM_PI, width / height, context->GetScreenNear(), context->GetScreenFar() );
	}

	void Camera::GenerateProjectionMatrix( XMMATRIX& proj, float distance )
	{
		auto context = Engine::Instance().GetContext();
		auto* window = context->GetRenderWindow();

		const auto width = static_cast<float>( window->GetWidth() );
		const auto height = static_cast<float>( window->GetHeight() );

		proj = XMMatrixPerspectiveFovLH( 0.4f * XM_PI, width / height, context->GetScreenNear(), distance );
	}

	void Camera::LookAtPosition( const XMFLOAT3& targetPos )
	{
		mView = DirectXHelper::GenerateViewMatrixLookAt( mPosition, targetPos );
	}

	bool Camera::Init()
	{
		auto context = Engine::Instance().GetContext();
		if( context == nullptr )
		{
			Engine::Error( "Failed to create Camera Object", "A Context object must be created before attempting to create a Camera." );
			return false;
		}

		mView = DirectXHelper::GenerateViewMatrixLookDir( mPosition, mRotation );
		mBaseView = mView;
		GenerateProjectionMatrix();
	
		return true;
	}

	void Camera::AcceptKeyboardInput( float delta )
	{
		auto accelerator = 1.f;
		if( Keyboard::KeyDown( DIK_LSHIFT ) || Keyboard::KeyDown( DIK_RSHIFT ) )
		{
			accelerator = 4.f;
		}

		auto pos = mPosition;
		const auto dir = mRotation.y;
		const auto pitch = mRotation.x;
		const auto speed = mMoveSpeed * delta * accelerator;

		if( Keyboard::KeyDown( DIK_A ) )	//Left
		{
			pos.x -= sinf( XMConvertToRadians( dir + 90 ) ) * speed;
			pos.z -= cosf( XMConvertToRadians( dir + 90 ) ) * speed;
		}

		if( Keyboard::KeyDown( DIK_D ) )	//Right
		{
			pos.x += sinf( XMConvertToRadians( dir + 90 ) ) * speed;
			pos.z += cosf( XMConvertToRadians( dir + 90 ) ) * speed;
		}

		if( Keyboard::KeyDown( DIK_W ) )	//Forward
		{
			pos.x += sinf( XMConvertToRadians( dir ) ) * speed;
			pos.z += cosf( XMConvertToRadians( dir ) ) * speed;
			pos.y += sinf( XMConvertToRadians( pitch ) ) * speed;
		}

		if( Keyboard::KeyDown( DIK_S ) )	//Backward
		{
			pos.x += sinf( XMConvertToRadians( dir + 180 ) ) * speed;
			pos.z += cosf( XMConvertToRadians( dir + 180 ) ) * speed;
			pos.y += sinf( XMConvertToRadians( pitch + 180 ) ) * speed;
		}

		if( Keyboard::KeyDown( DIK_Q ) )	//Up
		{
			pos.y += speed;
		}

		if( Keyboard::KeyDown( DIK_Z ) )	//Down
		{
			pos.y -= speed;
		}

		mPosition = pos;
	}

	void Camera::AcceptMouseInput()
	{
		auto& mouse = Mouse::Instance();
		mRotation.x += -mouse.GetChangeY();
		mRotation.y += mouse.GetChangeX();
	}

	void Camera::HandleFreeLookUpdate( float delta )
	{
		AcceptKeyboardInput( delta );
		AcceptMouseInput();
		
		//Get the window centre and lock the mouse there
		auto window = Engine::Instance().GetContext()->GetRenderWindow();
		RECT rect;
		GetWindowRect( window->GetWindowHandle(), &rect );

		const auto x = rect.left;
		const auto y = rect.top;

		if( window->GetWindowHandle() == GetFocus() )
		{
			SetCursorPos( window->GetWidth() / 2 + x, window->GetHeight() / 2 + y );	//set the cursor position
		}
	}

	void Camera::HandleEditorUpdate( float delta )
	{
		const auto window = Engine::Instance().GetContext()->GetRenderWindow();

		//Only accept input if the mouse is inside the render window
		if( window->IsMouseInside() )
		{
			auto& mouse = Mouse::Instance();
			if( mouse.ButtonDown( MouseButtons::kMouseRight ) )
			{
				AcceptKeyboardInput( delta );
				AcceptMouseInput();
			}
			else if( mouse.ButtonPressed( MouseButtons::kMouseLeft ) )
			{
				const auto window = Engine::Instance().GetContext()->GetRenderWindow();

				XMFLOAT3 origin, direction;
				window->ComputeMouseRayInfo( origin, direction );
				const auto result = RayCast::Create( origin, direction, RayCast::RayType::kTerrainOnly );

				if( result->mHitSomething )
				{
					//mPosition = result->mHitPosition;
					auto terrain = Engine::Instance().GetCurrentWorld()->GetTerrain();
					auto pos = XMFLOAT2( result->mHitPosition.x, result->mHitPosition.z );
					terrain->ModifyTerrain( pos, 50, 50, tTerrainVertexModificationMode::kRaise );
				}
			}
		}
	}
}
