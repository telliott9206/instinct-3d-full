/*
 * Settings.h
 *
 * Created 18/12/2018
 * Written by Todd Elliott
 *
 * Will eventually be used to control all graphical settings and user inputs
 * from the developer app.
 */

#ifndef _SETTINGS_H
#define _SETTINGS_H

#include <Instinct.h>

namespace Instinct
{
	class Settings
	{
	public:
		typedef enum
		{
			kNone,
			kReflect
		}
		tWaterReflectionMode;

		DLLEXPORT static Settings* Instance()
		{
			static Settings instance;
			return &instance;
		}

	private:
		Settings();
		~Settings();

	public:
		Settings( const Settings& ) = delete;
		Settings& operator = ( const Settings& ) = delete;
		

		DLLEXPORT tWaterReflectionMode GetWaterReflectionMode()				{ return mWaterReflectionMode; }
		DLLEXPORT void SetWaterReflectionMode( tWaterReflectionMode mode )	{ mWaterReflectionMode = mode; }

		DLLEXPORT int GetMouseSensitivity()									{ return mMouseSensitivity; }
		DLLEXPORT void SetMouseSensitivity( int value )						{ mMouseSensitivity = value; }

	private:
		tWaterReflectionMode mWaterReflectionMode { tWaterReflectionMode::kReflect };
		int mMouseSensitivity;
	};
}

#endif
