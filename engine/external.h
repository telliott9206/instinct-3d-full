/*
 * External.h
 *
 * Created 30/11/2018
 * Written by Todd Elliott
 *
 * This header file is simply included for embedding the engine into a windows application.
 * Used only by the World Editor.
 */

#ifndef _EXTERNAL_H
#define _EXTERNAL_H

#include <Windows.h>
#include "Instinct.h"

extern "C" DLLEXPORT void EmbedEngine( HWND* hwnd, int width, int height );

#endif
