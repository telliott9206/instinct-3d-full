/*
 * Engine.cpp
 *
 * Created 30/11/2018
 * Written by Todd Elliott
 *
 * The main top level class containing just the barebones to run the engine.
 */

#ifndef _ENGINE_H
#define _ENGINE_H

#include <Instinct.h>
#include <Engine/Render/Context.h>
#include <Engine/Texture/Texture2D.h>

#include <functional>
#include <mutex>

namespace Instinct
{
	class World;
	class Texture2D;
	class Camera;
	class GUI;

	static std::mutex mConsoleOutMutex;

	class Engine
	{
	public:
		enum tRenderPhase
		{
			kDiffuse,
			kDepth,
			kShadow
		};

		DLLEXPORT static Engine& Instance()
		{
			static Engine inst;
			return inst;
		}

		Engine();
		Engine( const Engine& ) = delete;
		Engine& operator = ( const Engine& ) = delete;
		~Engine() = default;

	public:
		DLLEXPORT void LoadEngine( const std::function<void()>& onStarted, const std::function<bool()>& onFrame, const std::function<void()>& onEnd );
		DLLEXPORT void PrintHardwareInfo();
		DLLEXPORT static std::string GetResourceDirectory();

		DLLEXPORT static void Error( const std::string& caption, const char format[], ... );
		DLLEXPORT static void Print( const char format[], ... );

		std::shared_ptr<Context> GetContext()									{ return mContext; }	//Can be nullptr!
		void SetContext( const std::shared_ptr<Context>& context )				{ mContext = context; }

		DLLEXPORT std::shared_ptr<World> GetCurrentWorld()						{ return mWorld; }		//Can be nullptr!
		DLLEXPORT void SetCurrentWorld( const std::shared_ptr<World>& world )	{ mWorld = world; };

		IDirectInput8* GetDirectInput()		{ return mDirectInput; }

		void Shutdown()						{ mShutdown = true; }
		void Run( std::shared_ptr<Context> context, std::shared_ptr<World> world ); //used for embedded version through windows forms
		void Run();	

	private:
		
		void RenderFrame();

		void BindDeferredRenderPass( ID3D11DeviceContext& deviceContext );
		void BindBackBufferRenderPass( ID3D11DeviceContext& deviceContext );

	private:
		bool						mShutdown;

		std::function<void()>		mOnStartedCallback;
		std::function<bool()>		mOnFrameCallback;
		std::function<void()>		mOnEndCallback;

		std::shared_ptr<Context>	mContext;
		std::shared_ptr<World>		mWorld;

		IDirectInput8*				mDirectInput;
	};
};

#endif
