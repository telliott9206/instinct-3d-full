/*
 * Main.cpp
 *
 * Created 29/11/2018
 * Written by Todd Elliott
 */

#include "external.h"
#define EXPORTING_DLL

#include <Engine.h>
#include <Engine/Render/RenderWindow.h>
#include <Engine/Render/Context.h>
#include <Engine/Camera.h>
#include <Engine/World.h>
#include <Engine/Light/DirectionalLight.h>
#include <Engine/Terrain/Terrain.h>
using namespace Instinct;

void EmbedEngine( HWND* hwnd, int width, int height )
{
	Engine::Print( "Embedding engine into window %p %d / %d ...", hwnd, width, height );

	auto window = RenderWindow::Embed( *hwnd, width, height );
	auto context = Context::Create( window );

	auto camera = Camera::Create();
	camera->SetType( Camera::tCameraType::kEditor );
	camera->SetPosition( 0.f, 0.f, 0.f );
	camera->LookAtPosition( XMFLOAT3( 1.f, 1.f, 1.f ) );

	auto world = World::Create();
	world->SetActiveCamera( camera );

	const auto sunVector = XMFLOAT3( XMConvertToRadians( 55.f ), XMConvertToRadians( -45.f ), XMConvertToRadians( 55.f ) );
	auto light = DirectionalLight::Create( sunVector, XMFLOAT4( 1.f, 1.f, 1.f, 1.f ), XMFLOAT4( 0.75f, 0.75f, 0.75f, 1.f ) );
	light->SetRotation( sunVector );
	world->SetLight( light );

	Engine::Instance().Run( context, world );
	Engine::Print( "Engine embedded!" );
}

BOOL APIENTRY DllMain(
	HANDLE hModule,	   // Handle to DLL module 
	DWORD ul_reason_for_call, 
	LPVOID lpReserved )     // Reserved
{
	switch ( ul_reason_for_call )
	{
	case DLL_PROCESS_ATTACH:
	{
		break;
	}

	case DLL_THREAD_ATTACH:
		// A process is creating a new thread.
		break;

	case DLL_THREAD_DETACH:
		// A thread exits normally.
		break;

	case DLL_PROCESS_DETACH:
		// A process unloads the DLL.
		break;
	}

	return TRUE;
}