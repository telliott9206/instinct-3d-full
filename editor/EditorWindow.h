/*
 * EditorWindow.h
 *
 * Created 08/07/2022
 * Written by Todd Elliott
 *
 * Main Qt window class for the World Editor
 */

#ifndef _EDITORWINDOW_H
#define _EDITORWINDOW_H

#include <QtWidgets/QMainWindow>
#include <QColor>
#include "ui_EditorWindow.h"

#include <Instinct.h>

#define WINDOW_TITLE	"World Editor"

namespace Instinct
{
	class EngineThread;

	class EditorWindow : public QMainWindow
	{
		Q_OBJECT

	public:
		EditorWindow( QWidget* parent = nullptr );
		~EditorWindow();

		void StartEngine();
		void closeEvent( QCloseEvent* event );

	private slots:
		void OnQuitActionClicked();
		void OnAboutActionClicked();
		void OnViewSolidClicked();
		void OnViewWireframeClicked();
		
		void OnCreateDeleteTerrain();
		void OnCreateTextureLayerClicked();
		void OnDeleteTextureLayerClicked();
		void OnTextureLayerSelected();
		void OnTerrainDiffuseTextureClicked();
		void OnTerrainNormalTextureClicked();
		void OnRaiseTerrainClicked();
		void OnLowerTerrainClicked();
		void OnFlattenTerrainClicked();
		void OnTerrainSculptStrengthSliderChanged( int value );
		void OnTerrainSculptSizeSliderChanged( int value );

		void OnDirectionalLightDiffuseColourClicked();
		void OnDirectionalLightAmbientColourClicked();
		void OnOpenColourPicker();

	signals:
		void InitEngine( HWND* hwnd, int width, int height );
		void ShutdownEngine();

	private:
		void InitControls();

		void UpdateCreateDeleteTerrainButtonText( bool enabled );
		void UpdateTerrainModificationControls( bool enabled );
		void UpdateTerrainTextureLayerControls( bool enabled );
		void EnableTerrainSculptControls( bool enable );
		void EnableTerrainSculptSliders( bool enable );

		static QColor XMFLOAT4ToQColor( const XMFLOAT4& colour );
		static XMFLOAT4 QColorToXMFLOAT4( const QColor& colour );

	private:
		Ui::EditorWindowClass mUi;
		EngineThread* mThread;

		QString mProjectTitle;
	};
}

#endif
