/*
 * SaveableContentsInterface.h
 *
 * Created 15/07/2022
 * Written by Todd Elliott
 *
 * Provides commons for controllers that have content that can be saved
 */

#ifndef _SAVEABLECONTENTSINTERFACE_H
#define _SAVEABLECONTENTSINTERFACE_H

#include <string>

#define FILEEXT_OPENMAP			"id3map"
#define FILEEXT_EXPORTEDMAP		"id3w"
#define FILEEXT_TERRAIN			"id3tn"
#define FILEEXT_TEXTURELAYERS	"i3dtl"
#define FILEEXT_ENTITIES		"i3des"
#define FILEEXT_SKY				"i3dsy"

namespace Instinct
{
	class SaveableContentsInterface
	{
	public:
		virtual bool SaveContents( const std::string& path ) = 0;
		virtual bool LoadContents( const std::string& path ) = 0;

		bool ChangedForSave() const		{ return mChangedForSave; }
		void SetChanged( bool changed )	{ mChangedForSave = changed; }

	protected:
		bool mChangedForSave;
	};
}

#endif
