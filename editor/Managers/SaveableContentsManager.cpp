/*
 * SaveableContentsManager.cpp
 *
 * Created 22/07/2022
 * Written by Todd Elliott
 */

#include "SaveableContentsManager.h"
#include <editor/SaveableContentsInterface.h>
#include <Editor/Controllers/TerrainController.h>
#include <Editor/Controllers/DirectionalLightController.h>

namespace Instinct
{
	SaveableContentsManager::SaveableContentsManager()
	{
		mSaveables.push_back( &TerrainController::Instance() );
		mSaveables.push_back( &DirectionalLightController::Instance() );
	}

	SaveableContentsManager::~SaveableContentsManager()
	{
	}

	bool SaveableContentsManager::isUnsaved() const
	{
		return std::any_of( mSaveables.cbegin(), mSaveables.cend(), [] ( const auto* saveable )
			{
				return saveable->ChangedForSave();
			} );
	}
}
