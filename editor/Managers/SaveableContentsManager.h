/*
 * SaveableContentsManager.h
 *
 * Created 22/07/2022
 * Written by Todd Elliott
 *
 * Manages all the possible data that can be saved
 */

#ifndef _SAVEABLECONTENTSMANAGER_H
#define _SAVEABLECONTENTSMANAGER_H

#include <Instinct.h>

namespace Instinct
{
	class SaveableContentsInterface;

	class SaveableContentsManager
	{
	public:
		static SaveableContentsManager& Instance()
		{
			static SaveableContentsManager inst;
			return inst;
		}

	private:
		SaveableContentsManager();
		~SaveableContentsManager();

	public:
		SaveableContentsManager( const SaveableContentsManager& ) = delete;
		SaveableContentsManager& operator = ( const SaveableContentsManager& ) = delete;

		bool isUnsaved() const;

	private:
		std::vector<SaveableContentsInterface*> mSaveables;
	};
}

#endif
