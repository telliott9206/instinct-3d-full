/*
 * EditorWindow.cpp
 *
 * Created 08/07/2022
 * Written by Todd Elliott
 */

#include "EditorWindow.h"
#include <Editor/Controllers/TerrainController.h>
#include <Editor/Controllers/DirectionalLightController.h>
#include <Editor/EngineThread.h>
#include <QMessageBox>
#include <QInputDialog>
#include <QFileDialog>
#include <QColorDialog>
#include <Engine.h>
#include <Engine/Render/Context.h>

namespace Instinct
{
	EditorWindow::EditorWindow( QWidget* parent ) :
		QMainWindow( parent )
	{
		mUi.setupUi( this );
	}

	EditorWindow::~EditorWindow()
	{
	}

	void EditorWindow::StartEngine()
	{
		mThread = new EngineThread( "Engine Thread" );
		mThread->start();

		//connect engine main functions
		connect( this, &EditorWindow::InitEngine, mThread, &EngineThread::InitEngine );
		connect( this, &EditorWindow::ShutdownEngine, mThread, &EngineThread::ShutdownEngine );

		//Connect the toolbar actions
		connect( mUi.actionQuit, &QAction::triggered, this, &EditorWindow::OnQuitActionClicked );
		connect( mUi.actionAbout, &QAction::triggered, this, &EditorWindow::OnAboutActionClicked );
		connect( mUi.actionSolid, &QAction::triggered, this, &EditorWindow::OnViewSolidClicked );
		connect( mUi.actionWireframe, &QAction::triggered, this, &EditorWindow::OnViewWireframeClicked );

		//Connect needed terrain interaction functions
		connect( mUi.buttonCreateTerrain, &QPushButton::released, this, &EditorWindow::OnCreateDeleteTerrain );
		connect( mUi.buttonCreateTerrainLayer, &QPushButton::released, this, &EditorWindow::OnCreateTextureLayerClicked );
		connect( mUi.buttonDeleteTerrainLayer, &QPushButton::released, this, &EditorWindow::OnDeleteTextureLayerClicked );
		connect( mUi.listTerrainLayers, &QListWidget::itemSelectionChanged, this, &EditorWindow::OnTextureLayerSelected );
		connect( mUi.buttonSelectDiffuseTexture, &QPushButton::released, this, &EditorWindow::OnTerrainDiffuseTextureClicked );
		connect( mUi.buttonSelectNormalTexture, &QPushButton::released, this, &EditorWindow::OnTerrainNormalTextureClicked );
		connect( mUi.buttonTerrainRaise, &QPushButton::released, this, &EditorWindow::OnRaiseTerrainClicked );
		connect( mUi.buttonTerrainLower, &QPushButton::released, this, &EditorWindow::OnLowerTerrainClicked );
		connect( mUi.buttonTerrainFlatten, &QPushButton::released, this, &EditorWindow::OnFlattenTerrainClicked );
		connect( mUi.sliderTerrainSculptStrength, &QSlider::valueChanged, this, &EditorWindow::OnTerrainSculptStrengthSliderChanged );
		connect( mUi.sliderTerrainSculptSize, &QSlider::valueChanged, this, &EditorWindow::OnTerrainSculptSizeSliderChanged );

		//Connect direct light
		connect( mUi.buttonSelectDirectLightDiffuseColour, &QPushButton::released, this, &EditorWindow::OnDirectionalLightDiffuseColourClicked );
		connect( mUi.buttonSelectDirectLightAmbientColour, &QPushButton::released, this, &EditorWindow::OnDirectionalLightAmbientColourClicked );

		auto hwnd = (HWND)mUi.d3dFrame->winId();
		const auto width = mUi.d3dFrame->width();
		const auto height = mUi.d3dFrame->height();

		//Misc
		InitControls();

		//Start the engine!
		emit InitEngine( &hwnd, width, height );	

	}

	void EditorWindow::closeEvent( QCloseEvent* event )
	{
		emit ShutdownEngine();
	}

	void EditorWindow::OnAboutActionClicked()
	{
		QMessageBox::information( this, WINDOW_TITLE, "Welcome to the Instinct 3D World Editor Pre-alpha version!" );
	}

	void EditorWindow::OnViewSolidClicked()
	{
		mUi.actionSolid->setChecked( true );
		mUi.actionWireframe->setChecked( false );

		Engine::Instance().GetContext()->SetRasterState( Context::tRasterState::kCullBack );
	}

	void EditorWindow::OnViewWireframeClicked()
	{
		mUi.actionSolid->setChecked( false );
		mUi.actionWireframe->setChecked( true );

		Engine::Instance().GetContext()->SetRasterState( Context::tRasterState::kWireframeNoCull );
	}

	void EditorWindow::OnQuitActionClicked()
	{
		emit ShutdownEngine();
	}

	void EditorWindow::OnCreateDeleteTerrain()
	{
		if( !TerrainController::Instance().TerrainExists() )
		{
			const auto width = mUi.fieldCreateTerrainWidth->text().toInt();
			const auto height = mUi.fieldCreateTerrainHeight->text().toInt();

			if( !TerrainController::Instance().CreateNewTerrain( width, height ) )
			{
				QMessageBox::warning( this, WINDOW_TITLE, "Width and height must be a multiple of " + QString::number( MIN_TERRAIN_MULTIPLE ) );
			}
		}
		else
		{
			QMessageBox::StandardButton reply;

			reply = QMessageBox::question( this, WINDOW_TITLE, "Are you sure you want to delete the terrain? This action cannot be undone!", QMessageBox::Yes | QMessageBox::No );
			if( reply == QMessageBox::Yes ) 
			{
				TerrainController::Instance().DeleteTerrain();
				mUi.listTerrainLayers->clear();
			} 
		}

		const auto enabled = TerrainController::Instance().TerrainExists();
		UpdateCreateDeleteTerrainButtonText( enabled );
		UpdateTerrainModificationControls( enabled );
	}

	void EditorWindow::OnCreateTextureLayerClicked()
	{
		bool ok;
		const auto text = QInputDialog::getText( this, "New Texture Layer", "Enter the name of the new texture layer:", QLineEdit::Normal, "", &ok );

		if( ok )
		{
			if( !text.isEmpty() ) //TODO: check for unique texture layer title
			{
				mUi.listTerrainLayers->addItem( text );
				TerrainController::Instance().CreateNewTextureLayer();
				mUi.listTerrainLayers->setCurrentRow( mUi.listTerrainLayers->count() - 1 );
			}
			else
			{
				QMessageBox::warning( this, "New Texture Layer", "Please enter a valid name" );
				OnCreateTextureLayerClicked();
			}
		}
	}

	void EditorWindow::OnDeleteTextureLayerClicked()
	{
		const auto layerIndex = mUi.listTerrainLayers->currentIndex().row();

		if( !TerrainController::Instance().DeleteTextureLayer( layerIndex ) )
		{
			QMessageBox::warning( this, "Delete Texture Layer", "No texture layer was selected for deletion!" );
		}
		else
		{
			const auto item = mUi.listTerrainLayers->currentRow();
			const auto it = mUi.listTerrainLayers->takeItem( item );
			delete it;
		}
	}
	
	void EditorWindow::OnTextureLayerSelected()
	{
		const auto index = mUi.listTerrainLayers->currentIndex().row();
		if( index == -1 || !TerrainController::Instance().TerrainExists() || TerrainController::Instance().GetTextureLayerCount() == 0 )
		{
			mUi.fieldDiffuseLayer->setText( "" );
			mUi.fieldNormalLayer->setText( "" );
			mUi.fieldTextureXRepeat->setText( "" );
			mUi.fieldTextureYRepeat->setText( "" );
			mUi.fieldTextureMinSlope->setText( "" );
			mUi.fieldTextureMaxSlope->setText( "" );
			mUi.fieldTextureMinHeight->setText( "" );
			mUi.fieldTextureMaxHeight->setText( "" );

			UpdateTerrainTextureLayerControls( false );

			return;
		}

		const auto layer = TerrainController::Instance().GetTextureLayerByIndex( index );

		//Textures
		mUi.fieldDiffuseLayer->setText( QString::fromStdString( layer.mDiffuseTexture.get()->GetFileName() ) );
		mUi.fieldNormalLayer->setText( QString::fromStdString( layer.mNormalTexture.get()->GetFileName() ) );

		//TODO: texture repeat needs to be fixed ; repeats likely must be sent to graphic pipeline
		mUi.fieldTextureXRepeat->setText( "32.0" );
		mUi.fieldTextureYRepeat->setText( "32.0" );

		//Heights and slopes
		mUi.fieldTextureMinSlope->setText( QString::number( layer.mOptions.mMinSlope ) );
		mUi.fieldTextureMaxSlope->setText( QString::number( layer.mOptions.mMaxSlope ) );
		mUi.fieldTextureMinHeight->setText( QString::number( layer.mOptions.mMinHeight ) );
		mUi.fieldTextureMaxHeight->setText( QString::number( layer.mOptions.mMaxHeight ) );
	}

	void EditorWindow::OnTerrainDiffuseTextureClicked()
	{
		const auto index = mUi.listTerrainLayers->currentIndex().row();
		if( index == -1 )
		{
			return;
		}

		const auto file = QFileDialog::getOpenFileName( this, "Select texture", "", "Image Files (*.png *.jpg *.bmp)" );
		mUi.fieldDiffuseLayer->setText( file );

		auto texture = Texture::Load( file.toStdString() );
		if( texture != nullptr )
		{
			auto layer = TerrainController::Instance().GetTextureLayerByIndex( index );
			layer.mDiffuseTexture = texture;
			TerrainController::Instance().UpdateTextureLayer( index, layer );
		}
		else
		{
			QMessageBox::warning( this, "Load Texture", "Failed to load texture" );
		}
	}

	void EditorWindow::OnTerrainNormalTextureClicked()
	{
		const auto index = mUi.listTerrainLayers->currentIndex().row();
		if( index == -1 )
		{
			return;
		}

		const auto file = QFileDialog::getOpenFileName( this, "Select texture", "", "Image Files (*.png *.jpg *.bmp)" );
		mUi.fieldNormalLayer->setText( file );

		auto texture = Texture::Load( file.toStdString() );
		if( texture != nullptr )
		{
			auto layer = TerrainController::Instance().GetTextureLayerByIndex( index );
			layer.mNormalTexture = texture;
			TerrainController::Instance().UpdateTextureLayer( index, layer );
		}
		else
		{
			QMessageBox::warning( this, "Load Texture", "Failed to load texture" );
		}
	}

	void EditorWindow::OnRaiseTerrainClicked()
	{
		mUi.buttonTerrainLower->setChecked( false );
		mUi.buttonTerrainFlatten->setChecked( false );
		EnableTerrainSculptSliders( mUi.buttonTerrainRaise->isChecked() );
	}

	void EditorWindow::OnLowerTerrainClicked()
	{
		mUi.buttonTerrainRaise->setChecked( false );
		mUi.buttonTerrainFlatten->setChecked( false );
		EnableTerrainSculptSliders( mUi.buttonTerrainLower->isChecked() );
	}

	void EditorWindow::OnFlattenTerrainClicked()
	{
		mUi.buttonTerrainRaise->setChecked( false );
		mUi.buttonTerrainLower->setChecked( false );
		EnableTerrainSculptSliders( mUi.buttonTerrainFlatten->isChecked() );
	}

	void EditorWindow::OnTerrainSculptStrengthSliderChanged( int value )
	{
		mUi.fieldTerrainSculptStrength->setText( QString::number( value ) );
	}

	void EditorWindow::OnTerrainSculptSizeSliderChanged( int value )
	{
		mUi.fieldTerrainSculptSize->setText( QString::number( value ) );
	}

	void EditorWindow::OnDirectionalLightDiffuseColourClicked()
	{
		const auto& currentColour = DirectionalLightController::Instance().GetDiffuseColour();
		const auto newColour = QColorDialog::getColor( XMFLOAT4ToQColor( currentColour ), parentWidget());

		DirectionalLightController::Instance().SetDiffuseColour( QColorToXMFLOAT4( newColour ) );

		if( newColour.isValid() )
		{
			const auto qss = QString( "background-color: %1" ).arg( newColour.name() );
			mUi.buttonSelectDirectLightDiffuseColour->setStyleSheet( qss );
		}
	}

	void EditorWindow::OnDirectionalLightAmbientColourClicked()
	{
		const auto& currentColour = DirectionalLightController::Instance().GetAmbientColour();
		const auto newColour = QColorDialog::getColor( XMFLOAT4ToQColor( currentColour ), parentWidget());

		DirectionalLightController::Instance().SetAmbientColour( QColorToXMFLOAT4( newColour ) );

		if( newColour.isValid() )
		{
			const auto qss = QString( "background-color: %1" ).arg( newColour.name() );
			mUi.buttonSelectDirectLightAmbientColour->setStyleSheet( qss );
		}
	}

	void EditorWindow::OnOpenColourPicker()
	{
		QColor c;
		
	}

	void EditorWindow::InitControls()
	{
		//Set the window title
		mProjectTitle = "untitled";
		setWindowTitle( "Instinct 3D: World Editor - " + mProjectTitle + "*" );

		//Update texture controls
		UpdateCreateDeleteTerrainButtonText( false );
		UpdateTerrainModificationControls( false );
		EnableTerrainSculptControls( false );
		EnableTerrainSculptSliders( false );
	}

	void EditorWindow::UpdateCreateDeleteTerrainButtonText( bool enabled )
	{
		mUi.buttonCreateTerrain->setText( enabled ? "Delete Terrain" : "Create Terrain" );
	}

	void EditorWindow::UpdateTerrainModificationControls( bool enabled )
	{
		//Sculpting
		mUi.buttonTerrainRaise->setEnabled( enabled );
		mUi.buttonTerrainLower->setEnabled( enabled );
		mUi.buttonTerrainFlatten->setEnabled( enabled );

		//Texture layers
		UpdateTerrainTextureLayerControls( enabled );
	}

	void EditorWindow::UpdateTerrainTextureLayerControls( bool enabled )
	{
		mUi.buttonCreateTerrainLayer->setEnabled( enabled );
		mUi.buttonDeleteTerrainLayer->setEnabled( enabled );
		mUi.listTerrainLayers->setEnabled( enabled );
		mUi.fieldDiffuseLayer->setEnabled( enabled );
		mUi.fieldNormalLayer->setEnabled( enabled );
		mUi.buttonSelectDiffuseTexture->setEnabled( enabled );
		mUi.buttonSelectNormalTexture->setEnabled( enabled );
		mUi.cbTextureLayerEnabled->setEnabled( enabled );

		mUi.fieldTextureXRepeat->setEnabled( enabled );
		mUi.fieldTextureMinSlope->setEnabled( enabled );
		mUi.fieldTextureMinHeight->setEnabled( enabled );
		mUi.fieldTextureYRepeat->setEnabled( enabled );
		mUi.fieldTextureMaxSlope->setEnabled( enabled );
		mUi.fieldTextureMaxHeight->setEnabled( enabled );
	}

	void EditorWindow::EnableTerrainSculptControls( bool enable )
	{
		mUi.buttonTerrainLower->setEnabled( enable );
		mUi.buttonTerrainRaise->setEnabled( enable );
		mUi.buttonTerrainFlatten->setEnabled( enable );
	}

	void EditorWindow::EnableTerrainSculptSliders( bool enable )
	{
		mUi.sliderTerrainSculptStrength->setEnabled( enable );
		mUi.sliderTerrainSculptSize->setEnabled( enable );
		mUi.fieldTerrainSculptStrength->setEnabled( enable );
		mUi.fieldTerrainSculptSize->setEnabled( enable );
	}

	QColor EditorWindow::XMFLOAT4ToQColor( const XMFLOAT4& colour )
	{
		QColor ret;
		ret.setAlphaF( colour.w );
		ret.setRedF( colour.x );
		ret.setBlueF( colour.y );
		ret.setGreenF( colour.z );

		return ret;
	}

	XMFLOAT4 EditorWindow::QColorToXMFLOAT4( const QColor& colour )
	{
		XMFLOAT4 ret;
		ret.x = colour.redF();
		ret.y = colour.blueF();
		ret.z = colour.greenF();
		ret.w = colour.alphaF();

		return ret;
	}
}
