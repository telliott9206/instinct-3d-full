/*
 * main.cpp
 *
 * Created 08/07/2022
 * Written by Todd Elliott
 */

#include "EditorWindow.h"
#include <QtWidgets/QApplication>

int main( int argc, char* argv[] )
{
    AllocConsole();
    freopen( "conin$", "r", stdin );
    freopen( "conout$", "w", stdout );
    freopen( "conout$", "w", stderr );

    std::cout << "Starting Instinct 3D World Editor ..." << std::endl;

    QApplication a( argc, argv );
    Instinct::EditorWindow window;
    window.show();

    window.showMaximized();
    window.centralWidget()->setContentsMargins( 0, 0, 0, 0 );

    window.StartEngine();

    return a.exec();
}
