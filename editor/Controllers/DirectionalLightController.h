/*
 * DirectionalLightController.h
 *
 * Created 08/07/2022
 * Written by Todd Elliott
 *
 * Interfaces between the user and the directional light
 */

#ifndef _DIRECTIONALLIGHTCONTROLLER_H
#define _DIRECTIONALLIGHTCONTROLLER_H

#include <Instinct.h>
#include <Editor/SaveableContentsInterface.h>

namespace Instinct
{
	class DirectionalLightController : public SaveableContentsInterface
	{
	public:
		static DirectionalLightController& Instance()
		{
			static DirectionalLightController inst;
			return inst;
		}

	private:
		DirectionalLightController();
		~DirectionalLightController();

	public:
		DirectionalLightController( const DirectionalLightController& ) = delete;
		DirectionalLightController& operator = ( const DirectionalLightController& ) = delete;

		bool SaveContents( const std::string& path ) override;
		bool LoadContents( const std::string& path ) override;

		void SetDiffuseColour( const XMFLOAT4& colour );
		void SetAmbientColour( const XMFLOAT4& colour );
		void SetXAxis( float x );
		void SetYAxis( float y );
		void SetZAxis( float z );

		const XMFLOAT4& GetDiffuseColour() const;
		const XMFLOAT4& GetAmbientColour() const;
	};
}

#endif
