/*
 * TerrainController.h
 *
 * Created 15/07/2022
 * Written by Todd Elliott
 *
 * Interfaces between the user and the editor terrain
 */

#ifndef _TERRAINCONTROLLER_H
#define _TERRAINCONTROLLER_H

#include <Editor/SaveableContentsInterface.h>
#include <Engine/Terrain/TerrainTextureLayer.h>

#define MIN_TERRAIN_MULTIPLE	64

namespace Instinct
{
	class TerrainController : public SaveableContentsInterface
	{
	public:
		static TerrainController& Instance()
		{
			static TerrainController inst;
			return inst;
		}

	private:
		TerrainController();
		~TerrainController();

	public:
		TerrainController( const TerrainController& ) = delete;
		TerrainController& operator = ( const TerrainController& ) = delete;

		bool SaveContents( const std::string& path ) override;
		bool LoadContents( const std::string& path ) override;

		bool TerrainExists() const;
		bool CreateNewTerrain( int width, int height, int unitsPerSquare = 1 );
		void DeleteTerrain();

		bool CreateNewTextureLayer();
		bool DeleteTextureLayer( int index );
		bool UpdateTextureLayer( int index, const TerrainTextureLayer& layer );
		const TerrainTextureLayer& GetTextureLayerByIndex( int index ) const;
		int GetTextureLayerCount() const;
	};
}

#endif
