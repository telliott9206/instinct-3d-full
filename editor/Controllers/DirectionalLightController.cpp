/*
 * DirectionalLightController.cpp
 *
 * Created 08/07/2022
 * Written by Todd Elliott
 */

#include "DirectionalLightController.h"
#include <Engine/Light/DirectionalLight.h>
#include <Engine/Terrain/Terrain.h>
#include <Engine/World.h>

namespace Instinct
{
	DirectionalLightController::DirectionalLightController()
	{
	}

	DirectionalLightController::~DirectionalLightController()
	{
	}

	bool DirectionalLightController::SaveContents( const std::string& path )
	{
		//TODO: implement
		return true;
	}

	bool DirectionalLightController::LoadContents( const std::string& path )
	{
		//TODO: implement
		return true;
	}

	void DirectionalLightController::SetDiffuseColour( const XMFLOAT4& colour )
	{
		Engine::Instance().GetCurrentWorld()->GetLight()->SetDiffuseColour( colour );
	}

	void DirectionalLightController::SetAmbientColour( const XMFLOAT4& colour )
	{
		Engine::Instance().GetCurrentWorld()->GetLight()->SetAmbientColour( colour );
	}

	void DirectionalLightController::SetXAxis( float x )
	{
		auto direction = Engine::Instance().GetCurrentWorld()->GetLight()->GetRotation();
		Engine::Instance().GetCurrentWorld()->GetLight()->SetRotation( XMFLOAT3( x, direction.y, direction.z ) );
	}

	void DirectionalLightController::SetYAxis( float y )
	{
		auto direction = Engine::Instance().GetCurrentWorld()->GetLight()->GetRotation();
		Engine::Instance().GetCurrentWorld()->GetLight()->SetRotation( XMFLOAT3( direction.x, y, direction.z ) );
	}

	void DirectionalLightController::SetZAxis( float z )
	{
		auto direction = Engine::Instance().GetCurrentWorld()->GetLight()->GetRotation();
		Engine::Instance().GetCurrentWorld()->GetLight()->SetRotation( XMFLOAT3( direction.x, direction.y, z ) );
	}

	const XMFLOAT4& DirectionalLightController::GetDiffuseColour() const
	{
		return Engine::Instance().GetCurrentWorld()->GetLight()->GetDiffuseColour();
	}

	const XMFLOAT4& DirectionalLightController::GetAmbientColour() const
	{
		return Engine::Instance().GetCurrentWorld()->GetLight()->GetAmbientColour();
	}
}
