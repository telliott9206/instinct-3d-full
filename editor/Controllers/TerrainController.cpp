/*
 * TerrainController.cpp
 *
 * Created 15/07/2022
 * Written by Todd Elliott
 */

#include "TerrainController.h"
#include <Engine.h>
#include <Engine/World.h>
#include <Engine/Terrain/Terrain.h>
#include <fstream>

namespace Instinct
{
	TerrainController::TerrainController()
	{
	}

	TerrainController::~TerrainController()
	{
	}

	bool TerrainController::SaveContents( const std::string& path )
	{
		std::ofstream file;
		file.open( path );

		if( file.fail() )
		{
			//Failed to write to file
			return false;
		}

		std::string line;
		const auto terrain = Engine::Instance().GetCurrentWorld()->GetTerrain();
		const auto& submodel = terrain->GetEntryList().at( 0 );
		const auto& vertices = submodel->GetVertices();
		const auto& indices = submodel->GetIndices();
		const auto vertexCount = vertices.size();
		const auto indexCount = indices.size();

		//Write vertex count and vertices
		file << vertexCount << "\n";
		for( const auto& vertex : vertices )
		{
			file << vertex << "\n";
		}

		//Write index count and indices
		file << indexCount << "\n";
		uint32_t count = 0;
		for( const auto& index : indices )
		{
			file << index << " ";
			++ count;

			if( count % 3 == 0 )
			{
				file << "\n";
			}
		}

		file.close();

		return true;
	}

	bool TerrainController::LoadContents( const std::string& path )
	{
		std::ifstream file;
		file.open( path );

		if( file.fail() )
		{
			//Failed to read from file
			return false;
		}

		//Read vertex count
		uint32_t vertexCount, indexCount;
		file >> vertexCount;

		//Read vertices
		std::vector<Vertex> vertices( vertexCount );
		for( auto i = 0u ; i < vertexCount ; ++ i )
		{
			file >> vertices[ i ];
		}

		//Read index count
		file >> indexCount;

		//Read indices
		std::vector<uint32_t> indices( indexCount );
		for( auto i = 0u ; i < indexCount ; ++ i )
		{
			file >> indices[ i ];
		}

		file.close();

		return true;
	}

	bool TerrainController::TerrainExists() const
	{
		return Engine::Instance().GetCurrentWorld()->GetTerrain() != nullptr;
	}

	bool TerrainController::CreateNewTerrain( int width, int height, int unitsPerSquare )
	{
		if( width % MIN_TERRAIN_MULTIPLE != 0 || height % MIN_TERRAIN_MULTIPLE != 0 || width < MIN_TERRAIN_MULTIPLE || height < MIN_TERRAIN_MULTIPLE )
		{
			return false;
		}

		auto terrain = Terrain::Create( width, height, unitsPerSquare );
		if( terrain == nullptr )
		{
			return false;
		}

		Engine::Instance().GetCurrentWorld()->SetTerrain( terrain );
		Engine::Print( "Created new terrain of dimensions %d x %d", width, height );

		return true;
	}

	void TerrainController::DeleteTerrain()
	{
		//Delete each of teh texture layers
		Engine::Instance().GetCurrentWorld()->GetTerrain()->DeleteAllTextureLayers();

		//Now remove the terrain
		Engine::Instance().GetCurrentWorld()->SetTerrain( nullptr );
		Engine::Print( "Terrain deleted!" );
	}

	bool TerrainController::CreateNewTextureLayer()
	{
		TerrainTextureLayer layer;
		layer.mDiffuseTexture = Texture::Load( Engine::GetResourceDirectory() + "textures/notexture.png" );
		layer.mNormalTexture = Texture::Load( Engine::GetResourceDirectory() + "textures/notexture.png" );
		layer.mOptions = { -100.f, 100.f, 0.f, 0.2f, -1, 1 };

		auto terrain = Engine::Instance().GetCurrentWorld()->GetTerrain();
		terrain->AddTextureLayer( layer );
		terrain->UpdateTextureArray();

		return true;
	}

	bool TerrainController::DeleteTextureLayer( int index )
	{
		if( index < 0 )
		{
			return false;
		}

		auto terrain = Engine::Instance().GetCurrentWorld()->GetTerrain();
		terrain->DeleteTextureLayer( index );
		terrain->UpdateTextureArray();

		return true;
	}

	bool TerrainController::UpdateTextureLayer( int index, const TerrainTextureLayer& layer )
	{
		if( index < 0 )
		{
			return false;
		}

		auto terrain = Engine::Instance().GetCurrentWorld()->GetTerrain();
		terrain->UpdateTextureLayer( index, layer );
		terrain->UpdateTextureArray();

		return true;
	}

	const TerrainTextureLayer& TerrainController::GetTextureLayerByIndex( int index ) const
	{
		return Engine::Instance().GetCurrentWorld()->GetTerrain()->GetTextureLayers().at( index );
	}

	int TerrainController::GetTextureLayerCount() const
	{
		return Engine::Instance().GetCurrentWorld()->GetTerrain()->GetTextureLayerCount();
	}
}
