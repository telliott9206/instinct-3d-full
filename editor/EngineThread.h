/*
 * EngineThread.h
 *
 * Created 08/07/2022
 * Written by Todd Elliott
 *
 * Engine thread for editor window
 */

#ifndef _ENGINETHREAD_H
#define _ENGINETHREAD_H

#include <QtCore/QThread.h>
#include <QtCore/QString.h>
#include <Instinct.h>

namespace Instinct
{
	class Engine;

	class EngineThread: public QThread
	{
		Q_OBJECT

	public:
		explicit EngineThread( const QString& strThreadName );
		~EngineThread();

		void run();

	public slots:
		void InitEngine( HWND* pHwnd, int iWidth, int iHeight );
		void ShutdownEngine();

		void SetDirectLightXAxis( float flValue );
		void SetDirectLightYAxis( float flValue );
		void SetDirectLightZAxis( float flValue );
		void SetDirectLightDiffuseColour( XMFLOAT3& vColour );
		void SetDirectLightAmbientColour( XMFLOAT3& vColour );

	private:
		QString mName;
	};
}

#endif
