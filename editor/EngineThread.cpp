/*
 * EngineThread.cpp
 *
 * Created 08/07/2022
 * Written by Todd Elliott
 */

#include <Editor/EngineThread.h>

#include <External.h>
#include <Engine/Engine.h>
#include <Engine/Light/DirectionalLight.h>
#include <Engine/World.h>

namespace Instinct
{
	EngineThread::EngineThread( const QString& threadname ) :
		mName( threadname )
	{
	}

	EngineThread::~EngineThread()
	{
	}

	void EngineThread::run()
	{
	}

	void EngineThread::InitEngine( HWND* pHwnd, int width, int height )
	{
		EmbedEngine( pHwnd, width, height );
	}

	void EngineThread::ShutdownEngine()
	{
		printf( "Shutting down engine ...\n" );

		Engine::Instance().Shutdown();

		quit();
		wait();
	}

	//DIRECT LIGHT FUNCTIONS
	void EngineThread::SetDirectLightXAxis( float flValue )
	{
		auto pLight = Engine::Instance().GetCurrentWorld()->GetLight();

		XMFLOAT3 vRotation = pLight->GetRotation();
		vRotation.x = flValue;

		pLight->SetRotation( vRotation );
	}

	void EngineThread::SetDirectLightYAxis( float flValue )
	{
		auto pLight = Engine::Instance().GetCurrentWorld()->GetLight();

		XMFLOAT3 vRotation = pLight->GetRotation();
		vRotation.y = flValue;

		pLight->SetRotation( vRotation );
	}

	void EngineThread::SetDirectLightZAxis( float flValue )
	{
		auto pLight = Engine::Instance().GetCurrentWorld()->GetLight();

		XMFLOAT3 vRotation = pLight->GetRotation();
		vRotation.z = flValue;

		pLight->SetRotation( vRotation );
	}

	void EngineThread::SetDirectLightDiffuseColour( XMFLOAT3& vColour )
	{
		auto pLight = Engine::Instance().GetCurrentWorld()->GetLight();

		auto vCurrent = pLight->GetDiffuseColour();

		XMFLOAT4 vNew( vCurrent );
		vNew.x = vColour.x;
		vNew.y = vColour.y;
		vNew.z = vColour.z;

		pLight->SetDiffuseColour( vNew );
	}

	void EngineThread::SetDirectLightAmbientColour( XMFLOAT3& vColour )
	{
		auto pLight = Engine::Instance().GetCurrentWorld()->GetLight();

		auto vCurrent = pLight->GetDiffuseColour();

		XMFLOAT4 vNew( vCurrent );
		vNew.x = vColour.x;
		vNew.y = vColour.y;
		vNew.z = vColour.z;

		pLight->SetAmbientColour( vNew );
	}
}
