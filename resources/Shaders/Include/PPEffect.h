#ifndef _PPEFFECT_H
#define _PPEFFECT_H

//TODO: remove unneeded cbuffer information
cbuffer cbPerObject
{
	matrix gWorld;
	float gAlpha;
	float gSpecularPower;
	float gReflectivePower;
	bool gLightAffected;
};

cbuffer cbPerFrame
{
	matrix gView;
	matrix gProj;
	float3 gEyePosition;
	float _padding2;
};

struct VertexInput
{
    float4 mPosition : POSITION;
    float2 mTexCoord : TEXCOORD0;
};

struct PixelInput
{
    float4 mPosition : SV_POSITION;
    float2 mTexCoord : TEXCOORD0;
};

#endif
