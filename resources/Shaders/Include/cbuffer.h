/*
 * cbuffer.h
 *
 * Created 19/07/2019
 * Written by Todd Elliott
 */
dsfdsfdsfds
cbuffer cbPerObject
{
	matrix gWorld;
	
	float gAlpha;
	float gSpecularPower;
	float gReflectivePower;
	bool gLightAffected;
};

cbuffer cbPerFrame
{
	matrix gView;
	matrix gProj;
	
	float3 gEyePosition;
	float _padding2;
	
	matrix gLightView[ SHADOW_CASCADE_COUNT ];
	matrix gLightProj[ SHADOW_CASCADE_COUNT ];
	
	float3 gLightPosition[ SHADOW_CASCADE_COUNT ]; //3*12 = 36, 48-36 = 12 .... = float3 remainder
	float3 _padding3;
};