/*
 * Merge.fx
 *
 * Created 30/11/2018
 * Written by Todd Elliott
 *
 * Merges the bound GBuffer shader resurces ands outputs to the final
 * texture to the backbuffer (unless there are post processing effects).
 */

/*----------------------------------------------------------|
|					VS SHADER START							|
|----------------------------------------------------------*/

cbuffer cbPerObject
{
	matrix gWorld;
	float gAlpha;
	float gSpecularPower;
	float gReflectivePower;
	bool gLightAffected;
};

cbuffer cbPerFrame
{
	matrix gView;
	matrix gProj;
	
	float3 gEyePosition;
	float _padding2;
};

struct VertexInput
{
    float4 mPosition : POSITION;
    float2 mTexCoord : TEXCOORD0;
};

struct PixelInput
{
    float4 mPosition : SV_POSITION;
    float2 mTexCoord : TEXCOORD0;
};

PixelInput VS( VertexInput input )
{
    PixelInput output;
    
    input.mPosition.w = 1.f;

    output.mPosition = mul( input.mPosition, gWorld );
    output.mPosition = mul( output.mPosition, gView );
    output.mPosition = mul( output.mPosition, gProj );
    
    output.mTexCoord = input.mTexCoord;
	
	return output;
}

/*----------------------------------------------------------|
|					PS SHADER START							|
|----------------------------------------------------------*/

SamplerState gSamSampler 	: register( s0 );

//TODO: ideally these should be an array
Texture2D gTexDiffuse 		: register( t0 );
Texture2D gTexNormal 		: register( t1 );
Texture2D gTexViewdir 		: register( t2 );
Texture2D gTexShadow 		: register( t3 );

cbuffer cbPerLightingChange
{
    float3 gLightDirection;
	float _padding3;
	float4 gDiffuseColour;
	float4 gAmbientColour;
};

float4 PS( PixelInput input ) : SV_TARGET
{
	float4 extra = gTexShadow.Sample( gSamSampler, input.mTexCoord );
	float4 diffuse = gTexDiffuse.Sample( gSamSampler, input.mTexCoord );
	float4 normals = gTexNormal.Sample( gSamSampler, input.mTexCoord );
	
	if( normals.a <= 0.f )
	{
		return diffuse;
	}
	
	float4 viewDir = gTexViewdir.Sample( gSamSampler, input.mTexCoord ) * 2.f - 1.f;
	
	float4 colour = gAmbientColour;
	float3 lightDir;
	float intensity;
	
	normals = normalize( normals );
	
	intensity = saturate( dot( normals.xyz, -gLightDirection ) );
	float intensity2 = saturate( dot( normals.xyz, gLightDirection ) );
	
	float4 specular = float4( 0.f, 0.f, 0.f, 0.f );
	if( intensity > 0.f )
    {
		colour += gDiffuseColour * intensity;

		float3 R = reflect( normalize( -gLightDirection ), normalize( normals.xyz ) );
		specular += intensity * pow( saturate( dot( -R, viewDir.xyz ) ), 32.f ); //TODO: Pass in the specular multipler (and base off spec map when its implemented)
    }

    colour = saturate( colour * diffuse );
	if( extra.r == 0.f )
	{
		return colour * float4( gAmbientColour.r, gAmbientColour.g, gAmbientColour.b, 1.f );
	}
	
	colour = saturate( colour + specular );
	
    return colour;
}
