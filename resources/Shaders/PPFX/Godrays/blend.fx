/*
 * Blend.fx
 *
 * Created 08/05/2022
 * Written by Todd Elliott
 */

/*----------------------------------------------------------|
|					VS SHADER START							|
|----------------------------------------------------------*/

struct VertexInput
{
    float4 mPosition : POSITION;
    float2 mTexcoord : TEXCOORD;
};

struct PixelInput
{ 
    float4 mPosition : POSITION;
    float2 mTexcoord : TEXCOORD;
};

PixelInput VS( VertexInput input )
{
    PixelInput output;
    
    input.mPosition.w = 1.f;

    output.mPosition = mul( input.mPosition, gWorld );
    output.mPosition = mul( output.mPosition, gView );
    output.mPosition = mul( output.mPosition, gProj );
    
    output.mTexCoord = input.mTexCoord;
	
	return output;
}

/*----------------------------------------------------------|
|					PS SHADER START							|
|----------------------------------------------------------*/

SamplerState gSamSampler 	: register( s0 );

//TODO: Turn this into a texture array
Texture2D gTexFinal			: register( t0 );
Texture2D gTexPrev			: register( t1 );
Texture2D gTexDiffuse 		: register( t2 );
Texture2D gTexNormal		: register( t3 );
Texture2D gTexView			: register( t4 );
Texture2D gTexExtra			: register( t5 );

cbuffer cbPerLightingChange
{
    float3 gLightDirection;
	float _padding3;
	float4 gDiffuseColour;
	float4 gAmbientColour;
};

float4 PS( PixelInput input ) : SV_TARGET
{
	float4 diffuse = gTexFinal.Sample( gSamSampler, input.mTexCoord );
	float4 blend = gTexPrev.Sample( gSamSampler, input.mTexCoord );

    return saturate( diffuse + blend );
}