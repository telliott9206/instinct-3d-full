/*
 * Godrays.fx
 *
 * Created 08/05/2022
 * Written by Todd Elliott
 */

/*----------------------------------------------------------|
|					VS SHADER START							|
|----------------------------------------------------------*/

cbuffer cbPerObject
{
	matrix gWorld;
	float g_flAlpha;
	float g_flSpecularPower;
	float g_glReflectivePower;
	bool g_bLightAffected;
};

cbuffer cbPerFrame
{
	matrix gView;
	matrix gProj;
	float3 g_flEyePosition;
	float _padding2;
};

struct VertexInput
{
    float4 mPosition : POSITION;
    float2 mTexcoord : TEXCOORD;
};

struct PixelInput
{ 
    float4 mPosition : POSITION;
    float2 mTexcoord : TEXCOORD;
};

PixelInput VS( VertexInput input )
{
    PixelInput output;
    
    input.mPosition.w = 1.f;

    output.mPosition = mul( input.mPosition, gWorld );
    output.mPosition = mul( output.mPosition, gView );
    output.mPosition = mul( output.mPosition, gProj );
	
    output.mTexCoord = input.mTexCoord;
	
	return output;
}

/*----------------------------------------------------------|
|					PS SHADER START							|
|----------------------------------------------------------*/

SamplerState gSamSampler 	: register( s0 );

Texture2D gTexFinal			: register( t0 );
Texture2D gTexPrev			: register( t1 );
Texture2D gTexDiffuse 		: register( t2 );
Texture2D gTexNormal		: register( t3 );
Texture2D gTexView			: register( t4 );
Texture2D gTexExtra			: register( t5 );

cbuffer cbSunPosition
{
	float2 gSunPosition;
	float2 _padding;
};

#define DITHER
#define DECAY		0.98f
#define EXPOSURE	0.24f
#define SAMPLES		150
#define DENSITY		0.99f
#define WEIGHT		0.25f

//Note: Original logic here was not written by me
float4 PS( PixelInput input ) : SV_TARGET
{
	float2 uv = input.mTexCoord;
	
    float2 coord = uv;
    float2 lightpos = gSunPosition;
	
    float occ = gTexExtra.Sample( gSamSampler, input.mTexCoord ).z; //light
    float obj = gTexExtra.Sample( gSamSampler, input.mTexCoord ).y; //objects
    float dither = gTexExtra.Sample( gSamSampler, uv ).z;  
        
    float2 dtc = ( coord - lightpos ) * ( 1.f / float( SAMPLES ) * DENSITY );
    float illumdecay = 1.0;
    
    for( int i = 0 ; i < SAMPLES ; ++ i )
    {
        coord -= dtc;
		
        #ifdef DITHER
        	float s = gTexExtra.Sample( gSamSampler, coord + ( dtc * dither ) ).y;
        #else
        	float s = gTexExtra.Sample( gSamSampler, input.vTexCoord ).g;
        #endif
		
        s *= illumdecay * WEIGHT;
        occ += s;
        illumdecay *= DECAY;
    }
        
	return float4( float3( 0.f, 0.f, obj * 0.333f ) + occ * EXPOSURE, 1.f );
}