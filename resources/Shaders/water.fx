/*
 * Water.fx
 *
 * Created 30/11/2018
 * Written by Todd Elliott
 */

/*----------------------------------------------------------|
|					VS SHADER START							|
|----------------------------------------------------------*/

#include "include/Globals.h"

cbuffer cbPerObject : register( b0 )
{
	matrix gWorld;
};

//TODO: Remove most of this because it shouldnt really be needed for water
cbuffer cbPerFrame : register( b1 )
{
	matrix gView;
	matrix gProj;
	
	float3 gEyePosition;
	float _padding2;
	
	matrix gLightView[ SHADOW_CASCADE_COUNT ];
	matrix gLightProj[ SHADOW_CASCADE_COUNT ];
	
	float3 gLightPosition[ SHADOW_CASCADE_COUNT ]; //3*12 = 36, 48-36 = 12 .... = float3 remainder
	float3 _padding3;
};

struct VertexInput
{
    float4 mPosition 	: POSITION;
    float2 mTexcoord 	: TEXCOORD0;
	float3 mNormal 		: NORMAL;
};

struct PixelInput
{ 
    float4 mPosition 	: SV_POSITION;
    float2 mTexcoord 	: TEXCOORD0;
	float3 mNormal 		: NORMAL;
	float3 mViewVec 	: TEXCOORD1;
	//float mAlpha 		: ALPHA;
};

PixelInput VS( VertexInput input )
{
    PixelInput output;
    
    input.mPosition.w = 1.f;
	
	output.mViewVec = mul( gEyePosition, (float3x3)gWorld ) - mul( input.mPosition.xyz, (float3x3)gWorld );
    output.mViewVec = normalize( output.mViewVec );
    
	output.mPosition = mul( input.mPosition, gWorld );
    output.mPosition = mul( output.mPosition, gView );
    output.mPosition = mul( output.mPosition, gProj );
    
    output.mTexcoord = input.mTexcoord;
    output.mNormal = normalize(  mul( input.mNormal, (float3x3)gWorld ) );
	
	//output.mAlpha = 1.f;
	
	return output;
}

/*----------------------------------------------------------|
|					PS SHADER START							|
|----------------------------------------------------------*/

Texture2D gTexDiffuse 		: register( t0 );
Texture2D gTexNormal 		: register( t1 );

SamplerState gSamSampler	: register( s0 );

struct PixelOutput
{
    float4 mDiffuse : SV_Target0;
    float4 mNormal 	: SV_Target1;
	float4 mViewVec : SV_Target2;
};

PixelOutput PS( PixelInput input )
{
	PixelOutput output;

	output.mDiffuse = gTexDiffuse.Sample( gSamSampler, input.mTexcoord );
	//output.mDiffuse = float4( 0.f, 0.2f, 0.6f, 1.f );
	//output.mDiffuse.a = input.mAlpha;
	
	output.mNormal = float4( input.mNormal, 1.f );
	output.mViewVec = float4( input.mViewVec * 0.5f + 0.5f, 0.f );
	
    return output;
}