/*
 * Depth.fx
 *
 * Created 30/11/2018
 * Written by Todd Elliott
 */

/*----------------------------------------------------------|
|					VS SHADER START							|
|----------------------------------------------------------*/

cbuffer cbPerObject : register( b0 )
{
	matrix gWorld;
};

cbuffer cbDepthBuffer : register( b1 )
{
	matrix gView;
	matrix gProj;
	
	bool gIsTerrain;
	float3 _padding;
};

struct VertexInputInstanced
{
	//Input per vertex data
	float4 mPosition 		: POSITION;
    float2 mTexcoord 		: TEXCOORD0;
	float3 mNormal 			: NORMAL;

	//Input per instance data
	float4 mInstanceMat0	: TEXCOORD1;
	float4 mInstanceMat1	: TEXCOORD2;
	float4 mInstanceMat2	: TEXCOORD3;
	float4 mInstanceMat3	: TEXCOORD4;
	//
	float mAlpha			: TEXCOORD5;
	float mSpecularPower	: TEXCOORD6;
	float mReflectivePower	: TEXCOORD7;
	bool mLightAffected		: TEXCOORD8;
	bool mSunSource			: TEXCOORD9;
};

struct PixelInput
{
    float4 mPosition : SV_POSITION;
    float4 mDepthPos : TEXTURE0;
};

PixelInput VS( VertexInputInstanced input )
{
    PixelInput output;
    
    input.mPosition.w = 1.f;
	
	//Terrain will not be rendered instance, wheras normal objects will
	if( !gIsTerrain )
	{
		matrix world = matrix( input.mInstanceMat0, input.mInstanceMat1, input.mInstanceMat2, input.mInstanceMat3 ); 
		output.mPosition = mul( input.mPosition, world );
	}
	else
	{
		output.mPosition = mul( input.mPosition, gWorld );
	}

    output.mPosition = mul( output.mPosition, gView );
    output.mPosition = mul( output.mPosition, gProj );

	output.mDepthPos = output.mPosition;
	
	return output;
}

/*----------------------------------------------------------|
|					PS SHADER START							|
|----------------------------------------------------------*/

float4 PS( PixelInput input ) : SV_TARGET
{
	const float depth = input.mDepthPos.z / input.mDepthPos.w;
	return float4( depth, depth, depth, 1.f );
}