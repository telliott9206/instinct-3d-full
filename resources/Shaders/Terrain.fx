/*
 * Terrain.fx
 *
 * Created 30/11/2018
 * Written by Todd Elliott
 */

/*----------------------------------------------------------|
|					VS SHADER START							|
|----------------------------------------------------------*/

#include "include/Globals.h"

struct VertexInput
{
    float4 mPosition 	: POSITION;
    float2 mTexcoord 	: TEXCOORD0;
	float3 mNormal 		: NORMAL;
};

struct PixelInput
{ 
    float4 mPosition 				: SV_POSITION;
    float2 mTexcoord 				: TEXCOORD0;
	float3 mNormal 					: NORMAL;
	float3 mViewVec 				: TEXCOORD1;
	float4 mLightViewPos[ 3 ]		: TEXCOORD2;
	float mWorldHeight	 			: TEXCOORD5;
	float mClipSpaceZ				: TEXCOORD6;
};

cbuffer cbPerObject : register( b0 )
{
	matrix gWorld;
};

cbuffer cbPerFrame : register( b1 )
{
	matrix gView;
	matrix gProj;
	
	float3 gEyePosition;
	float _padding2;
	
	matrix gLightView[ SHADOW_CASCADE_COUNT ];
	matrix gLightProj[ SHADOW_CASCADE_COUNT ];
	
	float3 gLightPosition[ SHADOW_CASCADE_COUNT ]; //3*12 = 36, 48-36 = 12 .... = float3 remainder
	float3 _padding3;
};

PixelInput VS( VertexInput input )
{
    PixelInput output;
    
    input.mPosition.w = 1.f;
	
	output.mViewVec = mul( gEyePosition, (float3x3)gWorld ) - mul( input.mPosition.xyz, (float3x3)gWorld );
    output.mViewVec = normalize( output.mViewVec );
    
	output.mPosition = mul( input.mPosition, gWorld );
    output.mPosition = mul( output.mPosition, gView );
    output.mPosition = mul( output.mPosition, gProj );
	
	[unroll]
	for( int i = 0 ; i < SHADOW_CASCADE_COUNT ; ++ i )
	{
		output.mLightViewPos[ i ] = mul( input.mPosition, gWorld );
		output.mLightViewPos[ i ] = mul( output.mLightViewPos[ i ], gLightView[ i ] );
		output.mLightViewPos[ i ] = mul( output.mLightViewPos[ i ], gLightProj[ i ] );
	}
    
    output.mTexcoord = input.mTexcoord;
    output.mNormal = normalize(  mul( input.mNormal, (float3x3)gWorld ) );
	
	output.mClipSpaceZ = output.mPosition.z;
	output.mWorldHeight = input.mPosition.y;
	
	return output;
}

/*----------------------------------------------------------|
|					PS SHADER START							|
|----------------------------------------------------------*/

#define MAX_TERRAIN_LAYERS 		3
struct TerrainLayerOptions
{
	float mMinHeight;						// +
	float mMaxHeight;						// +
	float mMinSlope;						// +
	float mMaxSlope;						// = 16 bytes
	
	int mSlopeConnectorBottom;				// +
	int mSlopeConnectorTop;					// +
	
	float2 _TerrainLayerOptions_padding;	// = 16 bytes
};

cbuffer cbTerrainLayers : register( b0 )
{
	int gNumTerrainLayers;										// +
	float3 _cbTerrainLayers_padding;							// = 16 bytes
	
	TerrainLayerOptions gTerrainLayers[ MAX_TERRAIN_LAYERS ];	//32 bytes * N
};

cbuffer cbShadowCascades : register( b1 )
{
	vector<float, 3> gShadowCascades;
	float _padding4;
};

Texture2D gTexDepth[ SHADOW_CASCADE_COUNT ]		: register( t1 );
Texture2DArray gTexLayer						: register( t4 );

SamplerState gSamClamp							: register( s0 );
SamplerState gSamWrap  							: register( s1 );

struct PixelOutput
{
    float4 mDiffuse : SV_Target0;
    float4 mNormal 	: SV_Target1;
	float4 mViewVec : SV_Target2;
	float4 mExtra	: SV_Target3;
};

bool ShadowedPixel( PixelInput input, int cascadeIndex )
{
	//TODO: Fix the following - it should be in a loop
	if( cascadeIndex == 0 && input.mClipSpaceZ > gShadowCascades[ 0 ] )
	{
		return false;
	}
	else if( cascadeIndex == SHADOW_CASCADE_COUNT - 1 && input.mClipSpaceZ < gShadowCascades[ cascadeIndex - 1 ] )
	{
		return false;
	}
	else if( cascadeIndex != 0 && ( input.mClipSpaceZ < gShadowCascades[ cascadeIndex - 1 ] || input.mClipSpaceZ > gShadowCascades[ cascadeIndex ] ) )
	{
		return false;
	}
	
	bool ret = false;
	
	const float bias = 0.001f;
    float2 projectedTexcoord;
    float depth;
    float lightDepth;
	
	projectedTexcoord.x = input.mLightViewPos[ cascadeIndex ].x / input.mLightViewPos[ cascadeIndex ].w / 2.f + 0.5f;
	projectedTexcoord.y = -input.mLightViewPos[ cascadeIndex ].y / input.mLightViewPos[ cascadeIndex ].w / 2.f + 0.5f;
		
	if( ( saturate( projectedTexcoord.x ) == projectedTexcoord.x ) && ( saturate( projectedTexcoord.y ) == projectedTexcoord.y ) )
	{		
		depth = gTexDepth[ cascadeIndex ].Sample( gSamClamp, projectedTexcoord ).r;
		lightDepth = ( input.mLightViewPos[ cascadeIndex ].z / input.mLightViewPos[ cascadeIndex ].w ) - bias;
			
		if( lightDepth >= depth )
		{
			ret = true;	//TODO: figure out why returning here causes a warning
		}
	}
	
	return ret;
}

PixelOutput PS( PixelInput input )
{
	PixelOutput output;
	
	//Calculate direct light shadows falling onto terrain
	bool shadowed = false;
	
	[unroll]
	for( int cascadeIndex = 0 ; cascadeIndex < SHADOW_CASCADE_COUNT ; ++ cascadeIndex )
	{
		shadowed = ShadowedPixel( input, cascadeIndex );
		if( shadowed )
		{
			break;
		}
	}
	
	//Procedural rendering for terrain texturing
	float4 final = float4( 1.f, 1.f, 1.f, 1.f );
	const float slope = 1.f - input.mNormal.y;
	const float height = input.mWorldHeight;
	
	//TODO: All of this is to be removed eventually; procedural height rendering will be replaced with texture mapping
	[unroll]
	for( int i = 0 ; i < gNumTerrainLayers ; ++ i )
	{
		TerrainLayerOptions options = gTerrainLayers[ i ];
		
		const bool noHeight = ( options.mMaxHeight == options.mMinHeight );
		const bool noSlope = ( options.mMaxSlope == options.mMinSlope );
		
		const bool renderHeight = ( height >= options.mMinHeight && height <= options.mMaxHeight ) || noHeight;
		const bool renderSlope = ( slope >= options.mMinSlope && slope <= options.mMaxSlope ) || noSlope;
		
		const float4 textureSample = gTexLayer.Sample( gSamWrap, float3( input.mTexcoord.x, input.mTexcoord.y, i ) );
		
		if( renderSlope && renderHeight )
		{
			if( options.mSlopeConnectorBottom == -1 && options.mSlopeConnectorTop != -1 )
			{
				const float amount = noSlope ? 1.f : slope / options.mMaxSlope;
				const float4 texture2 = gTexLayer.Sample( gSamWrap, float3( input.mTexcoord.x, input.mTexcoord.y, options.mSlopeConnectorTop ) );
				
				final = lerp( textureSample, texture2, amount );
			}
			else if( options.mSlopeConnectorBottom != -1 && options.mSlopeConnectorTop != -1 )
			{
				const float amount = noSlope ? 1.f : ( slope - options.mMinSlope ) * ( 1.f / ( options.mMaxSlope - options.mMinSlope ) );
				const float4 texture2 = gTexLayer.Sample( gSamWrap, float3( input.mTexcoord.x, input.mTexcoord.y, options.mSlopeConnectorTop ) );
				
				final = lerp( textureSample, texture2, amount );
			}
			else
			{
				if( height >= options.mMinHeight && height <= options.mMaxHeight )
				{
					const float4 texture2 = gTexLayer.Sample( gSamWrap, float3( input.mTexcoord.x, input.mTexcoord.y, 1 ) );
					final = lerp( textureSample, texture2, 1.f );	//TODO: Fix this, it looks bad
				}
				else
				{
					final = textureSample;
				}
			}
		}
	}

	output.mDiffuse = final;

	output.mNormal = float4( input.mNormal, 1.f );
	output.mNormal = normalize( output.mNormal );
	
	output.mViewVec = float4( input.mViewVec * 0.5f + 0.5f, 0.f );
	output.mExtra = float4( shadowed ? 0.f : 1.f, 0.f, 0.f, 1.f );

    return output;
}