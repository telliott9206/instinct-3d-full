/*
 * Basic.fx
 *
 * Created 30/11/2018
 * Written by Todd Elliott
 */

/*----------------------------------------------------------|
|					VS SHADER START							|
|----------------------------------------------------------*/

#include "include/Globals.h"

//TODO: Remove this, it is no longer needed now that all object specific info is now input per instance data
cbuffer cbPerObject : register( b0 )
{
	matrix gWorld;
};

cbuffer cbPerFrame : register( b1 )
{
	matrix gView;
	matrix gProj;
	
	float3 gEyePosition;
	float _padding2;
	
	matrix gLightView[ SHADOW_CASCADE_COUNT ];
	matrix gLightProj[ SHADOW_CASCADE_COUNT ];
	
	float3 gLightPosition[ SHADOW_CASCADE_COUNT ]; //3*12 = 36, 48-36 = 12 .... = float3 remainder
	float3 _padding3;
};

struct VertexInputInstanced
{
	//Input per vertex data
	float4 mPosition 		: POSITION;
    float2 mTexcoord 		: TEXCOORD0;
	float3 mNormal 			: NORMAL;

	//Input per instance data
	float4 mInstanceMat0	: TEXCOORD1;
	float4 mInstanceMat1	: TEXCOORD2;
	float4 mInstanceMat2	: TEXCOORD3;
	float4 mInstanceMat3	: TEXCOORD4;
	//
	float mAlpha			: TEXCOORD5;
	float mSpecularPower	: TEXCOORD6;
	float mReflectivePower	: TEXCOORD7;
	bool mLightAffected		: TEXCOORD8;
	bool mSunSource			: TEXCOORD9;
};

struct PixelInput
{
	float4 mPosition 				: SV_POSITION;
    float2 mTexcoord 				: TEXCOORD0;
	float3 mNormal 					: NORMAL;
	float3 mViewVec 				: TEXCOORD1;
	float4 mLightViewPos[ 3 ]		: TEXCOORD2;
	//float3 mLightPos				: TEXCOORD3;
	bool mLightAffected 			: TEXCOORD5;
	bool mSunSource	 				: TEXCOORD6;
	float mClipSpaceZ				: TEXCOORD7;
};

PixelInput VS( VertexInputInstanced input )
{
    PixelInput output;

	input.mPosition.w = 1.f;
	matrix world = matrix( input.mInstanceMat0, input.mInstanceMat1, input.mInstanceMat2, input.mInstanceMat3 ); 
	
	//Calculate final position
    output.mPosition = mul( input.mPosition, world );
    output.mPosition = mul( output.mPosition, gView );
    output.mPosition = mul( output.mPosition, gProj );
	
	//Calculate the direct/sun light position
	[unroll]
	for( int i = 0 ; i < SHADOW_CASCADE_COUNT ; ++ i )
	{
		output.mLightViewPos[ i ] = mul( input.mPosition, world );
		output.mLightViewPos[ i ] = mul( output.mLightViewPos[ i ], gLightView[ i ] );
		output.mLightViewPos[ i ] = mul( output.mLightViewPos[ i ], gLightProj[ i ] );
	}
	
	//Texcoords and normals
	output.mTexcoord = input.mTexcoord;
	output.mNormal = mul( input.mNormal, (float3x3)world );
    output.mNormal = normalize( output.mNormal );
	
	//Calculate the camera view vector position - used for calculating specular lighting
	output.mViewVec = gEyePosition - mul( input.mPosition.xyz, (float3x3)world );
	output.mViewVec = gEyePosition - input.mPosition.xyz;
    output.mViewVec = normalize( output.mViewVec );
	
	//Calculate all lighting info
	output.mLightAffected = input.mLightAffected;
	output.mSunSource = false;	//TODO: This should be renamed its misleading - any light source that can produce light rays
	
	output.mClipSpaceZ = output.mPosition.z;
	
    return output;
}

/*----------------------------------------------------------|
|					PS SHADER START							|
|----------------------------------------------------------*/

Texture2D gTexDiffuse 						: register( t0 );
Texture2D gTexNormal						: register( t1 );
Texture2D gTexDepth[ SHADOW_CASCADE_COUNT ]	: register( t2 );

SamplerState gSamClamp						: register( s0 );
SamplerState gSamWrap  						: register( s1 );
SamplerState gSamNormal  					: register( s2 );

//TODO: Rename the members of this struct so it makes more sense
struct PixelOutput
{
    float4 mDiffuse 	: SV_Target0;
    float4 mNormal 		: SV_Target1;
    float4 mViewVec 	: SV_Target2;
	float4 mExtra		: SV_Target3;
};

bool ShadowedPixel( PixelInput input, int cascadeIndex )
{
	bool ret = false;
	
	const float bias = 0.001f;
    float2 projectedTexcoord;
    float depth;
    float lightDepth;
	
	projectedTexcoord.x = input.mLightViewPos[ cascadeIndex ].x / input.mLightViewPos[ cascadeIndex ].w / 2.f + 0.5f;
	projectedTexcoord.y = -input.mLightViewPos[ cascadeIndex ].y / input.mLightViewPos[ cascadeIndex ].w / 2.f + 0.5f;
		
	if( ( saturate( projectedTexcoord.x ) == projectedTexcoord.x ) && ( saturate( projectedTexcoord.y ) == projectedTexcoord.y ) )
	{
		depth = gTexDepth[ cascadeIndex ].Sample( gSamClamp, projectedTexcoord ).r;
		lightDepth = ( input.mLightViewPos[ cascadeIndex ].z / input.mLightViewPos[ cascadeIndex ].w ) - bias;
			
		if( lightDepth >= depth )
		{
			ret = true;
		}
	}
	
	return ret;
}

PixelOutput PS( PixelInput input )
{
	PixelOutput output;
	
	//Calculate direct light shadows falling onto the model
	bool shadowed = false;
	
	[unroll]
	for( int i = 0 ; i < SHADOW_CASCADE_COUNT ; ++ i )
	{
		shadowed = ShadowedPixel( input, i );
		if( shadowed )
		{
			break;
		}
	}
	
	//TODO: this needs to be cleaned up and documented
	output.mDiffuse = gTexDiffuse.Sample( gSamNormal, input.mTexcoord );
	output.mNormal = float4( input.mNormal, input.mLightAffected ? 1.f : 0.f );
	output.mNormal = normalize( output.mNormal );
	output.mViewVec = float4( input.mViewVec * 0.5f + 0.5f, 0.f );
	output.mExtra = float4( shadowed ? 0.f : 1.f, input.mSunSource, 0.f, 0.f );
	
    return output;
}