/*
 * Skydome.fx
 *
 * Created 30/11/2018
 * Written by Todd Elliott
 */

/*----------------------------------------------------------|
|					VS SHADER START							|
|----------------------------------------------------------*/

#include "include/Globals.h"

cbuffer cbPerObject : register( b0 )
{
	matrix gWorld;
};

cbuffer cbPerFrame : register( b1 )
{
	matrix gView;
	matrix gProj;
	
	float3 gEyePosition;
	float _padding2;
};

struct VertexInput
{
    float4 mPosition 		: POSITION;
};

struct PixelInput
{ 
    float4 mPosition 		: SV_POSITION;
    float4 mSkyPos 			: TEXCOORD0;
};

PixelInput VS( VertexInput input )
{
    PixelInput output;
    
    input.mPosition.w = 1.f;

    output.mPosition = mul( input.mPosition, gWorld );
    output.mPosition = mul( output.mPosition, gView );
    output.mPosition = mul( output.mPosition, gProj );
	
	output.mSkyPos = input.mPosition;
	
	return output;
}

/*----------------------------------------------------------|
|					PS SHADER START							|
|----------------------------------------------------------*/

cbuffer cbPerColourChange : register( b0 )
{
	float4 gCenterColour;
	float4 gApexColour;
};

struct PixelOutput
{
    float4 gTexDiffuse 	: SV_Target0;
    float4 gTexNormal	: SV_Target1;
};

PixelOutput PS( PixelInput input )
{
	PixelOutput output;
	
	float height = input.mSkyPos.y;
	if( height < 0.f )
	{
		height = 0.f;
	}

	output.gTexDiffuse = lerp( gCenterColour, gApexColour, height );
	output.gTexNormal = float4( 0.f, 1.f, 0.f, 0.f );
	
    return output;
}
