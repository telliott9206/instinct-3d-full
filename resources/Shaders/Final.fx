cbuffer cbPerObject
{
	matrix g_matWorld;
	float g_flAlpha;
	float g_flSpecularPower;
	float g_glReflectivePower;
	bool g_bLightAffected;
};

cbuffer cbPerFrame
{
	matrix g_matView;
	matrix g_matProj;
	float3 g_flEyePosition;
	float _padding2;
};

struct VertexInput
{
    float4 vPosition : POSITION;
    float2 vTexCoord : TEXCOORD0;
};

struct PixelInput
{
    float4 vPosition : SV_POSITION;
    float2 vTexCoord : TEXCOORD0;
};

PixelInput VS( VertexInput input )
{
    PixelInput output;
    
    input.vPosition.w = 1.f;

    output.vPosition = mul( input.vPosition, g_matWorld );
    output.vPosition = mul( output.vPosition, g_matView );
    output.vPosition = mul( output.vPosition, g_matProj );
    
    output.vTexCoord = input.vTexCoord;
	
	return output;
}

SamplerState g_samSampler 	: register( s0 );

Texture2D g_texDiffuse 		: register( t0 );
Texture2D g_texPrefFX 		: register( t0 );

cbuffer cbPerLightingChange
{
    float3 g_vLightDirection;
	float _padding3;
	float4 g_vDiffuseColour;
	float4 g_vAmbientColour;
};

float4 PS( PixelInput input ) : SV_TARGET
{
	float4 vDiffuse = g_texDiffuse.Sample( g_samSampler, input.vTexCoord );
	
    return vDiffuse;
}