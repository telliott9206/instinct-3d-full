/*
 * Simple.fx
 *
 * Created 30/11/2018
 * Written by Todd Elliott
 */

/*----------------------------------------------------------|
|					VS SHADER START							|
|----------------------------------------------------------*/

cbuffer cbPerObject
{
	matrix gWorld;
};

cbuffer cbPerFrame
{
	matrix gView;
	matrix gProj;
	
	float3 gEyePosition;
	float _padding2;
	matrix gMatLightView;
	matrix gMatLightProj;
	float3 gMLightPosition;
	float _padding3;
};

struct VertexInput
{
	float4 mPosition 	: POSITION;
    float4 mColour	 	: TEXCOORD0;
};

struct PixelInput
{
	float4 mPosition 	: SV_POSITION;
    float4 mColour 		: TEXCOORD0;
};

PixelInput VS( VertexInput input )
{
    PixelInput output;

	input.mPosition.w = 1.f;
	output.mColour = input.mColour;

	output.mPosition = mul( input.mPosition, gWorld );
    output.mPosition = mul( output.mPosition, gView );
    output.mPosition = mul( output.mPosition, gProj );

    return output;
}

/*----------------------------------------------------------|
|					PS SHADER START							|
|----------------------------------------------------------*/

struct PixelOutput
{
    float4 mDiffuse : SV_Target0;
};

PixelOutput PS( PixelInput input )
{
	PixelOutput output;
	output.mDiffuse = input.mColour;
	
    return output;
}