/*
 * Main.cpp
 *
 * Created 29/11/2018
 * Written by Todd Elliott
 */

#include "Game.h"
#include "Engine.h"
#include <direct.h>

tCmdlineMap GetCmdlineArgs( int argc, char* argv[] );

int main( int argc, char* argv[] )
{ 	
	auto cmds = GetCmdlineArgs( argc, argv );
	Game::OnStart( cmds );

	Instinct::Engine::Instance().LoadEngine( Game::OnStarted, Game::OnFrame, Game::OnEnd );

	return 0;
}

tCmdlineMap GetCmdlineArgs( int argc, char* argv[] )
{
	tCmdlineMap ret;
	for( auto i = 0 ; i < argc ; ++ i )
	{
		std::string arg( argv[ i ] );
		const auto delimPos = arg.find_first_of( "=" );
		
		if( delimPos != std::string::npos )
		{
			ret[ arg.substr( 0, delimPos ) ] = arg.substr( delimPos + 1 );
		}
		else
		{
			ret[ arg ] = "";
		}
	}

	return ret;
}