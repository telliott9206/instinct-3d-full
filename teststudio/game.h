/*
 * game.cpp
 *
 * Created 30/11/2018
 * Written by Todd Elliott
 */

#ifndef _GAME_H
#define _GAME_H

#include <Windows.h>
#include <Instinct.h>

#include <Engine/Terrain/Terrain.h>

typedef std::map<std::string, std::string> tCmdlineMap;

class Game
{
public:
	static void OnStart( tCmdlineMap& cmds );
	static void OnStarted();
	static bool OnFrame();
	static void OnEnd();
};

class TestUtil
{
public:
	static void CreateBasicWorld();
	static std::shared_ptr<Instinct::Terrain> CreateHeightMapTerrain();
	static void CreateRandomTrees( int count );
	static void CreateOcean();
	static void LoadModelBrowser();
};

#endif