/*
 * game.cpp
 *
 * Created 30/11/2018
 * Written by Todd Elliott
 */

#include "game.h"

#include <engine/render/renderwindow.h>
#include <engine/render/context.h>
#include <engine/input/keyboard.h>
#include <engine/world.h>
#include <engine/camera.h>
#include <engine/model/model.h>
#include <engine/terrain/terrain.h>
#include <engine/terrain/heightmap.h>
#include <engine/light/directionallight.h>
#include <engine/ocean.h>
#include <engine/skydome.h>
#include <engine/entity/entity.h>
#include <engine/geometrygenerator.h>
#include <engine/model/billboard.h>
#include <engine/texture/texture.h>
#include <engine/utility/util.h>

#include <external.h>

//Namespace for testing purpose only
using namespace Instinct;

//Resource directory for testing purpose only
const std::string resourceDir = Engine::GetResourceDirectory();

//Test environment global vars
XMFLOAT3 gSunVector;

void Game::OnStart( tCmdlineMap& cmds )
{
	srand( static_cast<unsigned int>( time( NULL ) ) );

	//What resolution are we using?
	int res[ 2 ] = { 1280, 720 };
	const auto resolution = cmds[ "--resolution" ];
	if( !resolution.empty() )
	{
		const auto pos = resolution.find( "x" );
		if( pos != std::string::npos )
		{
			res[ 0 ] = std::stoi( resolution.substr( 0, pos ) );
			res[ 1 ] = std::stoi( resolution.substr( pos  + 1 ) );
		}
	}

	auto window = RenderWindow::Create( res[ 0 ], res[ 1 ], 0, "Instinct 3D Engine" );
	Context::Create( window );

	TestUtil::CreateBasicWorld();

	//What test are we actually running?
	if( cmds[ "--test" ] == "heightmap" )
	{
		auto terrain = TestUtil::CreateHeightMapTerrain();
		if( terrain )
		{
			Engine::Instance().GetCurrentWorld()->SetTerrain( terrain );
		}

		TestUtil::CreateRandomTrees( 200 );
	}
	else if( cmds[ "--test" ] == "ocean" )
	{
		TestUtil::CreateOcean();
	}
	else if( cmds[ "--test" ] == "modelviewer" )
	{
		TestUtil::LoadModelBrowser();
	}

	//What lighting will we use?
	if( cmds[ "--lighting" ] == "day" )
	{
		gSunVector = XMFLOAT3( XMConvertToRadians( 55.f ), XMConvertToRadians( -45.f ), XMConvertToRadians( 55.f ) );
		auto skydome = Skydome::Create( 150.f, 20.f, XMFLOAT4( 0.4f, 0.73f, 0.8f, 1.f ), XMFLOAT4( 0.066f, 0.6f, 0.95f, 1.f ) );
		Engine::Instance().GetCurrentWorld()->SetSkydome( skydome );
	}
	else if( cmds[ "--lighting" ] == "sunset" )
	{
		gSunVector = XMFLOAT3( XMConvertToRadians( 55.f ), XMConvertToRadians( -10.f ), XMConvertToRadians( 55.f ) );
		auto skydome = Skydome::Create( 5.f, 20.f, XMFLOAT4( 0.9f, 0.7f, 0.f, 1.f ), XMFLOAT4( 0.7f, 0.05f, 0.6f, 1.f ) );
		Engine::Instance().GetCurrentWorld()->SetSkydome( skydome );
	}
	else if( cmds[ "--lighting" ] == "night" )
	{
	}
}

void Game::OnStarted()
{
	auto world = Engine::Instance().GetCurrentWorld();

	auto light = DirectionalLight::Create( gSunVector, XMFLOAT4( 1.f, 1.f, 1.f, 1.f ), XMFLOAT4( 0.75f, 0.75f, 0.75f, 1.f ) );
	light->SetRotation( gSunVector );
	world->SetLight( light );

	auto sun = Billboard::Create( Texture::Load( resourceDir + "textures/sun.png" ), false );
	sun->SetScale( 10.f, 10.f, 10.f );
	world->SetActiveSun( sun );
	Engine::Instance().GetCurrentWorld()->AddEntity( sun );
}

bool Game::OnFrame()
{
	if( Keyboard::KeyDown( DIK_ESCAPE ) )
	{
		return false;
	}

	const auto dist = 250.f;
	const auto& position = Engine::Instance().GetCurrentWorld()->GetActiveCamera()->GetPosition();

	//Update the position of the sun in respect to the camera
	auto sun = Engine::Instance().GetCurrentWorld()->GetActiveSun();
	if( sun != nullptr )
	{
		sun->SetPosition( position.x - dist * cosf( gSunVector.x ), position.y - dist * tanf( gSunVector.y ) - 25.f, position.z - dist * cosf( gSunVector.z ) ) ;
	}

	return true;
}

void Game::OnEnd()
{
}

void TestUtil::CreateBasicWorld()
{
	auto camera = Camera::Create();
	camera->SetType( Camera::tCameraType::kFreeLook );
	camera->SetPosition( 0.f, 10.f, 0.f );

	auto world = World::Create();
	world->SetActiveCamera( camera );

	Engine::Instance().SetCurrentWorld( world );
}

std::shared_ptr<Terrain> TestUtil::CreateHeightMapTerrain()
{
	TerrainTextureLayer layers[ 3 ];
	layers[ 0 ].mDiffuseTexture = Texture::Load( resourceDir + "textures/grass/grass_thin.jpg" );
	layers[ 0 ].mNormalTexture = Texture::Load( resourceDir + "textures/grass/grassnm.jpg" );
	layers[ 0 ].mOptions = { -100.f, 100.f, 0.f, 0.2f, -1, 1 };

	layers[ 1 ].mDiffuseTexture = Texture::Load( resourceDir + "textures/cliff/rock2.jpg" );
	layers[ 1 ].mNormalTexture = Texture::Load( resourceDir + "textures/cliff/rock_mossnm.png" );
	layers[ 1 ].mOptions = { -100.f, 100.f, 0.2f, 0.7f, 0, 2 };

	layers[ 2 ].mDiffuseTexture = Texture::Load( resourceDir + "textures/cliff/rock.jpg" );
	layers[ 2 ].mNormalTexture = Texture::Load( resourceDir + "textures/cliff/rocknm.png" );
	layers[ 2 ].mOptions = { -100.f, 100.f, 0.7f, 1.f, 1, -1 };

	auto hm = HeightMap::Load( resourceDir + "textures/hm3.bmp" );
	hm->Normalize( 5.f );

	auto terrain = Terrain::CreateFromHeightMap( hm );
	terrain->AddTextureLayer( &layers[ 0 ] );
	terrain->AddTextureLayer( &layers[ 1 ] );
	terrain->AddTextureLayer( &layers[ 2 ] );
	terrain->UpdateTextureArray();	//This is stupid, TODO: rework this in a way that all textures are added at once and then auto update the texture array

	return terrain;
}

void TestUtil::CreateRandomTrees( int count )
{
	//auto* model = Model::Load( resourceDir + "models/foliage/grass1.fbx" );
	auto model = Model::Load( resourceDir + "models/tree_01/tree.obj" );
	//auto* model = Model::Load( resourceDir + "models/trees/bananatree version2 b(scaled).obj" );

	const auto terrain = Engine::Instance().GetCurrentWorld()->GetTerrain();

	for( auto i = 0 ; i < count ; ++ i )
	{
		auto entity = Entity::Create();

		const auto x = static_cast<float>( rand() % terrain->Width() );
		const auto z = static_cast<float>( rand() % terrain->Height() );
		const auto rotation = static_cast<float>( rand() % 360 );

		entity->SetPosition( x, 0.f, z );
		entity->SetModel( model );
		entity->SetRotation( 0.f, rotation, 0.f );

		const auto scale = static_cast<float>( rand() ) / RAND_MAX;

		entity->SetScale( 3.f, 3.f, 3.f );

		entity->SetAlpha( 1.f );
		entity->SetVerticalAlignmentToTerrain();

		Engine::Instance().GetCurrentWorld()->AddEntity( entity );
	}
}

void TestUtil::CreateOcean()
{
	auto ocean = Ocean::Create( 128, 128, 0.f, 1 );

	ocean->AddDiffuseTexture( Texture::Load( resourceDir + "textures/water/water.jpg" ) );
	ocean->AddNormalTexture( Texture::Load( resourceDir + "textures/water/waternm.jpg" ) );

	Engine::Instance().GetCurrentWorld()->SetOcean( ocean );
}

void TestUtil::LoadModelBrowser()
{
}